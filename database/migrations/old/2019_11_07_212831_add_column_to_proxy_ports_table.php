<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToProxyPortsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('proxy_ports', function (Blueprint $table) {
            $table->integer('network_type_id')->after('type')->nullable();
            $table->integer('ip_type_id')->after('ip_type')->nullable();
            $table->integer('use_unlimited_traffic')->default(0)->after('ip_type_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('proxy_ports', function (Blueprint $table) {
            $table->dropColumn('network_type_id');
            $table->dropColumn('ip_type_id');
            $table->dropColumn('use_unlimited_traffic');
        });
    }
}
