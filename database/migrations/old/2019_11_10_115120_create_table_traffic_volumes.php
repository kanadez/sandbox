<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Illuminate\Support\Facades\DB;

class CreateTableTrafficVolumes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('traffic_volumes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->double('price', 20, 5)->default(0);
            $table->double('ratio', 20, 5)->default(0);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('proxy_ports', function (Blueprint $table) {
            $table->integer('traffic_volume_id')->after('trafic');
            $table->integer('rotation')->after('traffic_volume_id');
            $table->integer('rotation_time_type')->after('rotation');
        });

        DB::table('traffic_volumes')->insert([
            [
                'name' => '10 GB',
                'slug' => '10gb',
            ], [
                'name' => '20 GB',
                'slug' => '10gb',
            ], [
                'name' => '30 GB',
                'slug' => '10gb',
            ], [
                'name' => '40 GB',
                'slug' => '10gb',
            ], [
                'name' => '50 GB',
                'slug' => '10gb',
            ], [
                'name' => '60 GB',
                'slug' => '10gb',
            ], [
                'name' => '70 GB',
                'slug' => '10gb',
            ], [
                'name' => '80 GB',
                'slug' => '10gb',
            ], [
                'name' => '90 GB',
                'slug' => '10gb',
            ], [
                'name' => '100 GB',
                'slug' => '10gb',
            ], [
                'name' => 'Безлимитный',
                'slug' => 'unlimited',
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('traffic_volumes');

        Schema::table('proxy_ports', function (Blueprint $table) {
            $table->dropColumn('traffic_volume_id');
            $table->dropColumn('rotation');
            $table->dropColumn('rotation_time_type');
        });
    }
}
