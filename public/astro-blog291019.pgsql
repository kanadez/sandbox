--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.17
-- Dumped by pg_dump version 9.5.17

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: backend_access_log; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.backend_access_log (
    id integer NOT NULL,
    user_id integer NOT NULL,
    ip_address character varying(191),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.backend_access_log OWNER TO "user";

--
-- Name: backend_access_log_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.backend_access_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.backend_access_log_id_seq OWNER TO "user";

--
-- Name: backend_access_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.backend_access_log_id_seq OWNED BY public.backend_access_log.id;


--
-- Name: backend_user_groups; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.backend_user_groups (
    id integer NOT NULL,
    name character varying(191) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    code character varying(191),
    description text,
    is_new_user_default boolean DEFAULT false NOT NULL
);


ALTER TABLE public.backend_user_groups OWNER TO "user";

--
-- Name: backend_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.backend_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.backend_user_groups_id_seq OWNER TO "user";

--
-- Name: backend_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.backend_user_groups_id_seq OWNED BY public.backend_user_groups.id;


--
-- Name: backend_user_preferences; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.backend_user_preferences (
    id integer NOT NULL,
    user_id integer NOT NULL,
    namespace character varying(100) NOT NULL,
    "group" character varying(50) NOT NULL,
    item character varying(150) NOT NULL,
    value text
);


ALTER TABLE public.backend_user_preferences OWNER TO "user";

--
-- Name: backend_user_preferences_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.backend_user_preferences_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.backend_user_preferences_id_seq OWNER TO "user";

--
-- Name: backend_user_preferences_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.backend_user_preferences_id_seq OWNED BY public.backend_user_preferences.id;


--
-- Name: backend_user_roles; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.backend_user_roles (
    id integer NOT NULL,
    name character varying(191) NOT NULL,
    code character varying(191),
    description text,
    permissions text,
    is_system boolean DEFAULT false NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.backend_user_roles OWNER TO "user";

--
-- Name: backend_user_roles_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.backend_user_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.backend_user_roles_id_seq OWNER TO "user";

--
-- Name: backend_user_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.backend_user_roles_id_seq OWNED BY public.backend_user_roles.id;


--
-- Name: backend_user_throttle; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.backend_user_throttle (
    id integer NOT NULL,
    user_id integer,
    ip_address character varying(191),
    attempts integer DEFAULT 0 NOT NULL,
    last_attempt_at timestamp(0) without time zone,
    is_suspended boolean DEFAULT false NOT NULL,
    suspended_at timestamp(0) without time zone,
    is_banned boolean DEFAULT false NOT NULL,
    banned_at timestamp(0) without time zone
);


ALTER TABLE public.backend_user_throttle OWNER TO "user";

--
-- Name: backend_user_throttle_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.backend_user_throttle_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.backend_user_throttle_id_seq OWNER TO "user";

--
-- Name: backend_user_throttle_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.backend_user_throttle_id_seq OWNED BY public.backend_user_throttle.id;


--
-- Name: backend_users; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.backend_users (
    id integer NOT NULL,
    first_name character varying(191),
    last_name character varying(191),
    login character varying(191) NOT NULL,
    email character varying(191) NOT NULL,
    password character varying(191) NOT NULL,
    activation_code character varying(191),
    persist_code character varying(191),
    reset_password_code character varying(191),
    permissions text,
    is_activated boolean DEFAULT false NOT NULL,
    role_id integer,
    activated_at timestamp(0) without time zone,
    last_login timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone,
    is_superuser boolean DEFAULT false NOT NULL
);


ALTER TABLE public.backend_users OWNER TO "user";

--
-- Name: backend_users_groups; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.backend_users_groups (
    user_id integer NOT NULL,
    user_group_id integer NOT NULL
);


ALTER TABLE public.backend_users_groups OWNER TO "user";

--
-- Name: backend_users_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.backend_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.backend_users_id_seq OWNER TO "user";

--
-- Name: backend_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.backend_users_id_seq OWNED BY public.backend_users.id;


--
-- Name: cache; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.cache (
    key character varying(191) NOT NULL,
    value text NOT NULL,
    expiration integer NOT NULL
);


ALTER TABLE public.cache OWNER TO "user";

--
-- Name: cms_theme_data; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.cms_theme_data (
    id integer NOT NULL,
    theme character varying(191),
    data text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.cms_theme_data OWNER TO "user";

--
-- Name: cms_theme_data_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.cms_theme_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cms_theme_data_id_seq OWNER TO "user";

--
-- Name: cms_theme_data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.cms_theme_data_id_seq OWNED BY public.cms_theme_data.id;


--
-- Name: cms_theme_logs; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.cms_theme_logs (
    id integer NOT NULL,
    type character varying(20) NOT NULL,
    theme character varying(191),
    template character varying(191),
    old_template character varying(191),
    content text,
    old_content text,
    user_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.cms_theme_logs OWNER TO "user";

--
-- Name: cms_theme_logs_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.cms_theme_logs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cms_theme_logs_id_seq OWNER TO "user";

--
-- Name: cms_theme_logs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.cms_theme_logs_id_seq OWNED BY public.cms_theme_logs.id;


--
-- Name: deferred_bindings; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.deferred_bindings (
    id integer NOT NULL,
    master_type character varying(191) NOT NULL,
    master_field character varying(191) NOT NULL,
    slave_type character varying(191) NOT NULL,
    slave_id character varying(191) NOT NULL,
    session_key character varying(191) NOT NULL,
    is_bind boolean DEFAULT true NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.deferred_bindings OWNER TO "user";

--
-- Name: deferred_bindings_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.deferred_bindings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.deferred_bindings_id_seq OWNER TO "user";

--
-- Name: deferred_bindings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.deferred_bindings_id_seq OWNED BY public.deferred_bindings.id;


--
-- Name: failed_jobs; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.failed_jobs (
    id integer NOT NULL,
    connection text NOT NULL,
    queue text NOT NULL,
    payload text NOT NULL,
    failed_at timestamp(0) without time zone,
    exception text
);


ALTER TABLE public.failed_jobs OWNER TO "user";

--
-- Name: failed_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.failed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.failed_jobs_id_seq OWNER TO "user";

--
-- Name: failed_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.failed_jobs_id_seq OWNED BY public.failed_jobs.id;


--
-- Name: jobs; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.jobs (
    id bigint NOT NULL,
    queue character varying(191) NOT NULL,
    payload text NOT NULL,
    attempts smallint NOT NULL,
    reserved_at integer,
    available_at integer NOT NULL,
    created_at integer NOT NULL
);


ALTER TABLE public.jobs OWNER TO "user";

--
-- Name: jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.jobs_id_seq OWNER TO "user";

--
-- Name: jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.jobs_id_seq OWNED BY public.jobs.id;


--
-- Name: migrations; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(191) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO "user";

--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO "user";

--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- Name: rainlab_blog_categories; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.rainlab_blog_categories (
    id integer NOT NULL,
    name character varying(191),
    slug character varying(191),
    code character varying(191),
    description text,
    parent_id integer,
    nest_left integer,
    nest_right integer,
    nest_depth integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.rainlab_blog_categories OWNER TO "user";

--
-- Name: rainlab_blog_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.rainlab_blog_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rainlab_blog_categories_id_seq OWNER TO "user";

--
-- Name: rainlab_blog_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.rainlab_blog_categories_id_seq OWNED BY public.rainlab_blog_categories.id;


--
-- Name: rainlab_blog_posts; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.rainlab_blog_posts (
    id integer NOT NULL,
    user_id integer,
    title character varying(191),
    slug character varying(191) NOT NULL,
    excerpt text,
    content text,
    content_html text,
    published_at timestamp(0) without time zone,
    published boolean DEFAULT false NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    metadata text
);


ALTER TABLE public.rainlab_blog_posts OWNER TO "user";

--
-- Name: rainlab_blog_posts_categories; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.rainlab_blog_posts_categories (
    post_id integer NOT NULL,
    category_id integer NOT NULL
);


ALTER TABLE public.rainlab_blog_posts_categories OWNER TO "user";

--
-- Name: rainlab_blog_posts_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.rainlab_blog_posts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rainlab_blog_posts_id_seq OWNER TO "user";

--
-- Name: rainlab_blog_posts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.rainlab_blog_posts_id_seq OWNED BY public.rainlab_blog_posts.id;


--
-- Name: rainlab_translate_attributes; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.rainlab_translate_attributes (
    id integer NOT NULL,
    locale character varying(191) NOT NULL,
    model_id character varying(191),
    model_type character varying(191),
    attribute_data text
);


ALTER TABLE public.rainlab_translate_attributes OWNER TO "user";

--
-- Name: rainlab_translate_attributes_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.rainlab_translate_attributes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rainlab_translate_attributes_id_seq OWNER TO "user";

--
-- Name: rainlab_translate_attributes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.rainlab_translate_attributes_id_seq OWNED BY public.rainlab_translate_attributes.id;


--
-- Name: rainlab_translate_indexes; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.rainlab_translate_indexes (
    id integer NOT NULL,
    locale character varying(191) NOT NULL,
    model_id character varying(191),
    model_type character varying(191),
    item character varying(191),
    value text
);


ALTER TABLE public.rainlab_translate_indexes OWNER TO "user";

--
-- Name: rainlab_translate_indexes_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.rainlab_translate_indexes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rainlab_translate_indexes_id_seq OWNER TO "user";

--
-- Name: rainlab_translate_indexes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.rainlab_translate_indexes_id_seq OWNED BY public.rainlab_translate_indexes.id;


--
-- Name: rainlab_translate_locales; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.rainlab_translate_locales (
    id integer NOT NULL,
    code character varying(191) NOT NULL,
    name character varying(191),
    is_default boolean DEFAULT false NOT NULL,
    is_enabled boolean DEFAULT false NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.rainlab_translate_locales OWNER TO "user";

--
-- Name: rainlab_translate_locales_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.rainlab_translate_locales_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rainlab_translate_locales_id_seq OWNER TO "user";

--
-- Name: rainlab_translate_locales_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.rainlab_translate_locales_id_seq OWNED BY public.rainlab_translate_locales.id;


--
-- Name: rainlab_translate_messages; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.rainlab_translate_messages (
    id integer NOT NULL,
    code character varying(191),
    message_data text
);


ALTER TABLE public.rainlab_translate_messages OWNER TO "user";

--
-- Name: rainlab_translate_messages_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.rainlab_translate_messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rainlab_translate_messages_id_seq OWNER TO "user";

--
-- Name: rainlab_translate_messages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.rainlab_translate_messages_id_seq OWNED BY public.rainlab_translate_messages.id;


--
-- Name: sessions; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.sessions (
    id character varying(191) NOT NULL,
    payload text,
    last_activity integer,
    user_id integer,
    ip_address character varying(45),
    user_agent text
);


ALTER TABLE public.sessions OWNER TO "user";

--
-- Name: system_event_logs; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.system_event_logs (
    id integer NOT NULL,
    level character varying(191),
    message text,
    details text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.system_event_logs OWNER TO "user";

--
-- Name: system_event_logs_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.system_event_logs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.system_event_logs_id_seq OWNER TO "user";

--
-- Name: system_event_logs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.system_event_logs_id_seq OWNED BY public.system_event_logs.id;


--
-- Name: system_files; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.system_files (
    id integer NOT NULL,
    disk_name character varying(191) NOT NULL,
    file_name character varying(191) NOT NULL,
    file_size integer NOT NULL,
    content_type character varying(191) NOT NULL,
    title character varying(191),
    description text,
    field character varying(191),
    attachment_id character varying(191),
    attachment_type character varying(191),
    is_public boolean DEFAULT true NOT NULL,
    sort_order integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.system_files OWNER TO "user";

--
-- Name: system_files_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.system_files_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.system_files_id_seq OWNER TO "user";

--
-- Name: system_files_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.system_files_id_seq OWNED BY public.system_files.id;


--
-- Name: system_mail_layouts; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.system_mail_layouts (
    id integer NOT NULL,
    name character varying(191),
    code character varying(191),
    content_html text,
    content_text text,
    content_css text,
    is_locked boolean DEFAULT false NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    options text
);


ALTER TABLE public.system_mail_layouts OWNER TO "user";

--
-- Name: system_mail_layouts_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.system_mail_layouts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.system_mail_layouts_id_seq OWNER TO "user";

--
-- Name: system_mail_layouts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.system_mail_layouts_id_seq OWNED BY public.system_mail_layouts.id;


--
-- Name: system_mail_partials; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.system_mail_partials (
    id integer NOT NULL,
    name character varying(191),
    code character varying(191),
    content_html text,
    content_text text,
    is_custom boolean DEFAULT false NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.system_mail_partials OWNER TO "user";

--
-- Name: system_mail_partials_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.system_mail_partials_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.system_mail_partials_id_seq OWNER TO "user";

--
-- Name: system_mail_partials_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.system_mail_partials_id_seq OWNED BY public.system_mail_partials.id;


--
-- Name: system_mail_templates; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.system_mail_templates (
    id integer NOT NULL,
    code character varying(191),
    subject character varying(191),
    description text,
    content_html text,
    content_text text,
    layout_id integer,
    is_custom boolean DEFAULT false NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.system_mail_templates OWNER TO "user";

--
-- Name: system_mail_templates_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.system_mail_templates_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.system_mail_templates_id_seq OWNER TO "user";

--
-- Name: system_mail_templates_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.system_mail_templates_id_seq OWNED BY public.system_mail_templates.id;


--
-- Name: system_parameters; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.system_parameters (
    id integer NOT NULL,
    namespace character varying(100) NOT NULL,
    "group" character varying(50) NOT NULL,
    item character varying(150) NOT NULL,
    value text
);


ALTER TABLE public.system_parameters OWNER TO "user";

--
-- Name: system_parameters_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.system_parameters_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.system_parameters_id_seq OWNER TO "user";

--
-- Name: system_parameters_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.system_parameters_id_seq OWNED BY public.system_parameters.id;


--
-- Name: system_plugin_history; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.system_plugin_history (
    id integer NOT NULL,
    code character varying(191) NOT NULL,
    type character varying(20) NOT NULL,
    version character varying(50) NOT NULL,
    detail text,
    created_at timestamp(0) without time zone
);


ALTER TABLE public.system_plugin_history OWNER TO "user";

--
-- Name: system_plugin_history_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.system_plugin_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.system_plugin_history_id_seq OWNER TO "user";

--
-- Name: system_plugin_history_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.system_plugin_history_id_seq OWNED BY public.system_plugin_history.id;


--
-- Name: system_plugin_versions; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.system_plugin_versions (
    id integer NOT NULL,
    code character varying(191) NOT NULL,
    version character varying(50) NOT NULL,
    created_at timestamp(0) without time zone,
    is_disabled boolean DEFAULT false NOT NULL,
    is_frozen boolean DEFAULT false NOT NULL
);


ALTER TABLE public.system_plugin_versions OWNER TO "user";

--
-- Name: system_plugin_versions_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.system_plugin_versions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.system_plugin_versions_id_seq OWNER TO "user";

--
-- Name: system_plugin_versions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.system_plugin_versions_id_seq OWNED BY public.system_plugin_versions.id;


--
-- Name: system_request_logs; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.system_request_logs (
    id integer NOT NULL,
    status_code integer,
    url character varying(191),
    referer text,
    count integer DEFAULT 0 NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.system_request_logs OWNER TO "user";

--
-- Name: system_request_logs_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.system_request_logs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.system_request_logs_id_seq OWNER TO "user";

--
-- Name: system_request_logs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.system_request_logs_id_seq OWNED BY public.system_request_logs.id;


--
-- Name: system_revisions; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.system_revisions (
    id integer NOT NULL,
    user_id integer,
    field character varying(191),
    "cast" character varying(191),
    old_value text,
    new_value text,
    revisionable_type character varying(191) NOT NULL,
    revisionable_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.system_revisions OWNER TO "user";

--
-- Name: system_revisions_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.system_revisions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.system_revisions_id_seq OWNER TO "user";

--
-- Name: system_revisions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.system_revisions_id_seq OWNED BY public.system_revisions.id;


--
-- Name: system_settings; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.system_settings (
    id integer NOT NULL,
    item character varying(191),
    value text
);


ALTER TABLE public.system_settings OWNER TO "user";

--
-- Name: system_settings_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.system_settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.system_settings_id_seq OWNER TO "user";

--
-- Name: system_settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.system_settings_id_seq OWNED BY public.system_settings.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.backend_access_log ALTER COLUMN id SET DEFAULT nextval('public.backend_access_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.backend_user_groups ALTER COLUMN id SET DEFAULT nextval('public.backend_user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.backend_user_preferences ALTER COLUMN id SET DEFAULT nextval('public.backend_user_preferences_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.backend_user_roles ALTER COLUMN id SET DEFAULT nextval('public.backend_user_roles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.backend_user_throttle ALTER COLUMN id SET DEFAULT nextval('public.backend_user_throttle_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.backend_users ALTER COLUMN id SET DEFAULT nextval('public.backend_users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.cms_theme_data ALTER COLUMN id SET DEFAULT nextval('public.cms_theme_data_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.cms_theme_logs ALTER COLUMN id SET DEFAULT nextval('public.cms_theme_logs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.deferred_bindings ALTER COLUMN id SET DEFAULT nextval('public.deferred_bindings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.failed_jobs ALTER COLUMN id SET DEFAULT nextval('public.failed_jobs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.jobs ALTER COLUMN id SET DEFAULT nextval('public.jobs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.rainlab_blog_categories ALTER COLUMN id SET DEFAULT nextval('public.rainlab_blog_categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.rainlab_blog_posts ALTER COLUMN id SET DEFAULT nextval('public.rainlab_blog_posts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.rainlab_translate_attributes ALTER COLUMN id SET DEFAULT nextval('public.rainlab_translate_attributes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.rainlab_translate_indexes ALTER COLUMN id SET DEFAULT nextval('public.rainlab_translate_indexes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.rainlab_translate_locales ALTER COLUMN id SET DEFAULT nextval('public.rainlab_translate_locales_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.rainlab_translate_messages ALTER COLUMN id SET DEFAULT nextval('public.rainlab_translate_messages_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.system_event_logs ALTER COLUMN id SET DEFAULT nextval('public.system_event_logs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.system_files ALTER COLUMN id SET DEFAULT nextval('public.system_files_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.system_mail_layouts ALTER COLUMN id SET DEFAULT nextval('public.system_mail_layouts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.system_mail_partials ALTER COLUMN id SET DEFAULT nextval('public.system_mail_partials_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.system_mail_templates ALTER COLUMN id SET DEFAULT nextval('public.system_mail_templates_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.system_parameters ALTER COLUMN id SET DEFAULT nextval('public.system_parameters_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.system_plugin_history ALTER COLUMN id SET DEFAULT nextval('public.system_plugin_history_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.system_plugin_versions ALTER COLUMN id SET DEFAULT nextval('public.system_plugin_versions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.system_request_logs ALTER COLUMN id SET DEFAULT nextval('public.system_request_logs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.system_revisions ALTER COLUMN id SET DEFAULT nextval('public.system_revisions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.system_settings ALTER COLUMN id SET DEFAULT nextval('public.system_settings_id_seq'::regclass);


--
-- Data for Name: backend_access_log; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.backend_access_log (id, user_id, ip_address, created_at, updated_at) FROM stdin;
1	1	162.158.89.235	2019-05-29 10:43:30	2019-05-29 10:43:30
2	1	162.158.91.137	2019-05-30 10:57:48	2019-05-30 10:57:48
\.


--
-- Name: backend_access_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.backend_access_log_id_seq', 2, true);


--
-- Data for Name: backend_user_groups; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.backend_user_groups (id, name, created_at, updated_at, code, description, is_new_user_default) FROM stdin;
1	Owners	2019-05-29 10:40:43	2019-05-29 10:40:43	owners	Default group for website owners.	f
\.


--
-- Name: backend_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.backend_user_groups_id_seq', 1, true);


--
-- Data for Name: backend_user_preferences; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.backend_user_preferences (id, user_id, namespace, "group", item, value) FROM stdin;
\.


--
-- Name: backend_user_preferences_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.backend_user_preferences_id_seq', 1, false);


--
-- Data for Name: backend_user_roles; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.backend_user_roles (id, name, code, description, permissions, is_system, created_at, updated_at) FROM stdin;
1	Publisher	publisher	Site editor with access to publishing tools.		t	2019-05-29 10:40:43	2019-05-29 10:40:43
2	Developer	developer	Site administrator with access to developer tools.		t	2019-05-29 10:40:43	2019-05-29 10:40:43
\.


--
-- Name: backend_user_roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.backend_user_roles_id_seq', 2, true);


--
-- Data for Name: backend_user_throttle; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.backend_user_throttle (id, user_id, ip_address, attempts, last_attempt_at, is_suspended, suspended_at, is_banned, banned_at) FROM stdin;
1	1	162.158.89.235	0	\N	f	\N	f	\N
2	1	162.158.89.205	0	\N	f	\N	f	\N
3	1	162.158.90.102	0	\N	f	\N	f	\N
4	1	162.158.88.158	0	\N	f	\N	f	\N
5	1	162.158.91.121	0	\N	f	\N	f	\N
6	1	162.158.92.238	0	\N	f	\N	f	\N
7	1	162.158.93.135	0	\N	f	\N	f	\N
8	1	162.158.91.215	0	\N	f	\N	f	\N
9	1	162.158.92.22	0	\N	f	\N	f	\N
10	1	162.158.88.74	0	\N	f	\N	f	\N
11	1	162.158.93.57	0	\N	f	\N	f	\N
12	1	162.158.91.137	0	\N	f	\N	f	\N
13	1	162.158.92.250	0	\N	f	\N	f	\N
14	1	162.158.90.6	0	\N	f	\N	f	\N
\.


--
-- Name: backend_user_throttle_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.backend_user_throttle_id_seq', 14, true);


--
-- Data for Name: backend_users; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.backend_users (id, first_name, last_name, login, email, password, activation_code, persist_code, reset_password_code, permissions, is_activated, role_id, activated_at, last_login, created_at, updated_at, deleted_at, is_superuser) FROM stdin;
1	Admin	Person	admin	admin@domain.tld	$2y$10$jiIH4gK2HAx4zgpJWTP0AeVi90rH2qX895HnWrMaYGH.4.Z7k2Z3C	\N	$2y$10$jzkIWtBaKgX3GvinxV7R7uFNSiI3/wyiuKZ7BznPLaNt.pOcn0lN.	\N		t	2	\N	2019-05-30 10:57:48	2019-05-29 10:40:43	2019-05-30 10:57:48	\N	t
\.


--
-- Data for Name: backend_users_groups; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.backend_users_groups (user_id, user_group_id) FROM stdin;
1	1
\.


--
-- Name: backend_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.backend_users_id_seq', 1, true);


--
-- Data for Name: cache; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.cache (key, value, expiration) FROM stdin;
\.


--
-- Data for Name: cms_theme_data; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.cms_theme_data (id, theme, data, created_at, updated_at) FROM stdin;
\.


--
-- Name: cms_theme_data_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.cms_theme_data_id_seq', 1, false);


--
-- Data for Name: cms_theme_logs; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.cms_theme_logs (id, type, theme, template, old_template, content, old_content, user_id, created_at, updated_at) FROM stdin;
\.


--
-- Name: cms_theme_logs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.cms_theme_logs_id_seq', 1, false);


--
-- Data for Name: deferred_bindings; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.deferred_bindings (id, master_type, master_field, slave_type, slave_id, session_key, is_bind, created_at, updated_at) FROM stdin;
\.


--
-- Name: deferred_bindings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.deferred_bindings_id_seq', 3, true);


--
-- Data for Name: failed_jobs; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.failed_jobs (id, connection, queue, payload, failed_at, exception) FROM stdin;
\.


--
-- Name: failed_jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.failed_jobs_id_seq', 1, false);


--
-- Data for Name: jobs; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.jobs (id, queue, payload, attempts, reserved_at, available_at, created_at) FROM stdin;
\.


--
-- Name: jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.jobs_id_seq', 1, false);


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.migrations (id, migration, batch) FROM stdin;
1	2013_10_01_000001_Db_Deferred_Bindings	1
2	2013_10_01_000002_Db_System_Files	1
3	2013_10_01_000003_Db_System_Plugin_Versions	1
4	2013_10_01_000004_Db_System_Plugin_History	1
5	2013_10_01_000005_Db_System_Settings	1
6	2013_10_01_000006_Db_System_Parameters	1
7	2013_10_01_000007_Db_System_Add_Disabled_Flag	1
8	2013_10_01_000008_Db_System_Mail_Templates	1
9	2013_10_01_000009_Db_System_Mail_Layouts	1
10	2014_10_01_000010_Db_Jobs	1
11	2014_10_01_000011_Db_System_Event_Logs	1
12	2014_10_01_000012_Db_System_Request_Logs	1
13	2014_10_01_000013_Db_System_Sessions	1
14	2015_10_01_000014_Db_System_Mail_Layout_Rename	1
15	2015_10_01_000015_Db_System_Add_Frozen_Flag	1
16	2015_10_01_000016_Db_Cache	1
17	2015_10_01_000017_Db_System_Revisions	1
18	2015_10_01_000018_Db_FailedJobs	1
19	2016_10_01_000019_Db_System_Plugin_History_Detail_Text	1
20	2016_10_01_000020_Db_System_Timestamp_Fix	1
21	2017_08_04_121309_Db_Deferred_Bindings_Add_Index_Session	1
22	2017_10_01_000021_Db_System_Sessions_Update	1
23	2017_10_01_000022_Db_Jobs_FailedJobs_Update	1
24	2017_10_01_000023_Db_System_Mail_Partials	1
25	2017_10_23_000024_Db_System_Mail_Layouts_Add_Options_Field	1
26	2013_10_01_000001_Db_Backend_Users	2
27	2013_10_01_000002_Db_Backend_User_Groups	2
28	2013_10_01_000003_Db_Backend_Users_Groups	2
29	2013_10_01_000004_Db_Backend_User_Throttle	2
30	2014_01_04_000005_Db_Backend_User_Preferences	2
31	2014_10_01_000006_Db_Backend_Access_Log	2
32	2014_10_01_000007_Db_Backend_Add_Description_Field	2
33	2015_10_01_000008_Db_Backend_Add_Superuser_Flag	2
34	2016_10_01_000009_Db_Backend_Timestamp_Fix	2
35	2017_10_01_000010_Db_Backend_User_Roles	2
36	2018_12_16_000011_Db_Backend_Add_Deleted_At	2
37	2014_10_01_000001_Db_Cms_Theme_Data	3
38	2016_10_01_000002_Db_Cms_Timestamp_Fix	3
39	2017_10_01_000003_Db_Cms_Theme_Logs	3
\.


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.migrations_id_seq', 39, true);


--
-- Data for Name: rainlab_blog_categories; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.rainlab_blog_categories (id, name, slug, code, description, parent_id, nest_left, nest_right, nest_depth, created_at, updated_at) FROM stdin;
1	Uncategorized	uncategorized	\N	\N	\N	1	2	0	2019-05-29 10:52:49	2019-05-29 10:52:49
\.


--
-- Name: rainlab_blog_categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.rainlab_blog_categories_id_seq', 1, true);


--
-- Data for Name: rainlab_blog_posts; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.rainlab_blog_posts (id, user_id, title, slug, excerpt, content, content_html, published_at, published, created_at, updated_at, metadata) FROM stdin;
1	\N	First blog post	first-blog-post	The first ever blog post is here. It might be a good idea to update this post with some more relevant content.	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p><strong><em>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</em></strong></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n<p><strong><em>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</em></strong></p>\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>	2019-05-29 10:52:49	t	2019-05-29 10:52:49	2019-05-29 15:16:12	\N
2	1	Some title here	some-title-here	Leem S25RG - 25-ваттный комбоусилитель для электрогитары с широким функционалом. Его мощьности хватит чтобы раскачать небольшое помещение. Идеальное решение для репетиций, выступлений, обучения, записи и просто занятий дома. Большой функционал позволит Вам более гибко настроить звук и применить его в своем творчестве.	<p>Leem S25RG - 25-ваттный комбоусилитель для электрогитары с широким функционалом. Его мощьности хватит чтобы раскачать небольшое помещение. Идеальное решение для репетиций, выступлений, обучения, записи и просто занятий дома. Большой функционал позволит Вам более гибко настроить звук и применить его в своем творчестве.&nbsp;</p>\r\n\r\n<p>Leem S25RG - 25-ваттный комбоусилитель для электрогитары с широким функционалом. Его мощьности хватит чтобы раскачать небольшое помещение. Идеальное решение для репетиций, выступлений, обучения, записи и просто занятий дома. Большой функционал позволит Вам более гибко настроить звук и применить его в своем творчестве.&nbsp;</p>\r\n\r\n<p>Leem S25RG - 25-ваттный комбоусилитель для электрогитары с широким функционалом. Его мощьности хватит чтобы раскачать небольшое помещение. Идеальное решение для репетиций, выступлений, обучения, записи и просто занятий дома. Большой функционал позволит Вам более гибко настроить звук и применить его в своем творчестве.&nbsp;</p>\r\n\r\n<p>Leem S25RG - 25-ваттный комбоусилитель для электрогитары с широким функционалом. Его мощьности хватит чтобы раскачать небольшое помещение. Идеальное решение для репетиций, выступлений, обучения, записи и просто занятий дома. Большой функционал позволит Вам более гибко настроить звук и применить его в своем творчестве. Leem S25RG - 25-ваттный комбоусилитель для электрогитары с широким функционалом. Его мощьности хватит чтобы раскачать небольшое помещение. Идеальное решение для репетиций, выступлений, обучения, записи и просто занятий дома. Большой функционал позволит Вам более гибко настроить звук и применить его в своем творчестве. Leem S25RG - 25-ваттный комбоусилитель для электрогитары с широким функционалом. Его мощьности хватит чтобы раскачать небольшое помещение. Идеальное решение для репетиций, выступлений, обучения, записи и просто занятий дома. Большой функционал позволит Вам более гибко настроить звук и применить его в своем творчестве. Leem S25RG - 25-ваттный комбоусилитель для электрогитары с широким функционалом. Его мощьности хватит чтобы раскачать небольшое помещение. Идеальное решение для репетиций, выступлений, обучения, записи и просто занятий дома. Большой функционал позволит Вам более гибко настроить звук и применить его в своем творчестве. </p>	<p>Leem S25RG - 25-ваттный комбоусилитель для электрогитары с широким функционалом. Его мощьности хватит чтобы раскачать небольшое помещение. Идеальное решение для репетиций, выступлений, обучения, записи и просто занятий дома. Большой функционал позволит Вам более гибко настроить звук и применить его в своем творчестве. </p>\n<p>Leem S25RG - 25-ваттный комбоусилитель для электрогитары с широким функционалом. Его мощьности хватит чтобы раскачать небольшое помещение. Идеальное решение для репетиций, выступлений, обучения, записи и просто занятий дома. Большой функционал позволит Вам более гибко настроить звук и применить его в своем творчестве. </p>\n<p>Leem S25RG - 25-ваттный комбоусилитель для электрогитары с широким функционалом. Его мощьности хватит чтобы раскачать небольшое помещение. Идеальное решение для репетиций, выступлений, обучения, записи и просто занятий дома. Большой функционал позволит Вам более гибко настроить звук и применить его в своем творчестве. </p>\n<p>Leem S25RG - 25-ваттный комбоусилитель для электрогитары с широким функционалом. Его мощьности хватит чтобы раскачать небольшое помещение. Идеальное решение для репетиций, выступлений, обучения, записи и просто занятий дома. Большой функционал позволит Вам более гибко настроить звук и применить его в своем творчестве. Leem S25RG - 25-ваттный комбоусилитель для электрогитары с широким функционалом. Его мощьности хватит чтобы раскачать небольшое помещение. Идеальное решение для репетиций, выступлений, обучения, записи и просто занятий дома. Большой функционал позволит Вам более гибко настроить звук и применить его в своем творчестве. Leem S25RG - 25-ваттный комбоусилитель для электрогитары с широким функционалом. Его мощьности хватит чтобы раскачать небольшое помещение. Идеальное решение для репетиций, выступлений, обучения, записи и просто занятий дома. Большой функционал позволит Вам более гибко настроить звук и применить его в своем творчестве. Leem S25RG - 25-ваттный комбоусилитель для электрогитары с широким функционалом. Его мощьности хватит чтобы раскачать небольшое помещение. Идеальное решение для репетиций, выступлений, обучения, записи и просто занятий дома. Большой функционал позволит Вам более гибко настроить звук и применить его в своем творчестве. </p>	2019-05-29 16:19:11	t	2019-05-29 16:19:12	2019-05-29 16:49:49	\N
3	1	Третий пост	tretij-post	Педаль гитарная Distortion. Регуляторы: Gain, Level и Tone. Индикатор Check. Разъемы: вход/выход (гнезда Jack), гнездо для адаптера 9V. Металлический корпус. Питание: батарея 9V `Крона`. В комплекте: руководство по эксплуатации, солевая батарея 9V. Адаптер PSA серии, приобретается отдельно.	<p>Педаль гитарная Distortion. Регуляторы: Gain, Level и Tone. Индикатор Check. Разъемы: вход/выход (гнезда Jack), гнездо для адаптера 9V. Металлический корпус. Питание: батарея 9V `Крона`. В ко<strong>мплекте: руководство по эксплуатации, солевая батарея 9V. Адаптер PSA серии, приобретается отдельно. Педаль гитарная Distortion. Регуляторы: Gain, Level и Tone. Индикатор Check. Разъемы: вход/выход (гнезда Jack), гнездо для адаптера 9V. Металлический корпус. Питание: батарея 9V `Крона`. В комплекте: руководство по эксплуатации, солевая батарея 9V. Адаптер PS</strong><em><strong>A серии, приобретается отдельно. Педаль гитарная Distortion. Регуляторы: Gain, Level и Tone. Индикатор Check. Разъемы: вход/выход (гнезда Jack), гнездо для адаптера 9V. Металлический корп</strong>ус. Питание: батарея 9V `Крона`. В комплекте: руководство по эксплуатации, солевая батарея 9V. Адаптер PSA серии, приобретается отдельно. Педаль гитарная Distortion. Регуляторы: Gain, Level и Tone. Индикатор Check. Разъемы: вход/выход (гнезда Jack), гнездо для адаптера 9V. Металлический корпус. Питание: батарея 9V `Крона`. В комплекте: руководство по эксплуатации, солевая батарея 9V. Адаптер PSA серии, приобретается отдельно. Педаль гитарная Distortion. Регуляторы: Gain, Level и Tone. Индикатор Check. Разъемы: вход/выход (гнезда Jack), гнездо для адаптера 9V. Металлическ</em>ий корпус. Питание: батарея 9V `Крона`. В комплекте: руководство по эксплуатации, солевая батарея 9V. Адаптер PSA серии, приобретается отдельно.&nbsp;</p>\r\n\r\n<p>\r\n\t<br>\r\n</p>\r\n\r\n<p>Педаль гитарная Distortion. Регуляторы: Gain, Level и Tone. Индикатор Check. Разъемы: вход/выход (гнезда Jack), гнездо для адаптера 9V. Металлический корпус. Питание: батарея 9V `Крона`. В комплекте: руководство по эксплуатации, солевая батарея 9V. Адаптер PSA серии, приобретается отдельно.&nbsp;</p>\r\n\r\n<p>Педаль гитарная Distortion. Регуляторы: Gain, Level и Tone. Индикатор Check. Разъемы: вход/выход (гнезда Jack), гнездо для адаптера 9V. Металлический корпус. Питание: батарея 9V `Крона`. В комплекте: руководство по эксплуатации, солевая батарея 9V. Адаптер PSA серии, приобретается отдельно. </p>	<p>Педаль гитарная Distortion. Регуляторы: Gain, Level и Tone. Индикатор Check. Разъемы: вход/выход (гнезда Jack), гнездо для адаптера 9V. Металлический корпус. Питание: батарея 9V `Крона`. В ко<strong>мплекте: руководство по эксплуатации, солевая батарея 9V. Адаптер PSA серии, приобретается отдельно. Педаль гитарная Distortion. Регуляторы: Gain, Level и Tone. Индикатор Check. Разъемы: вход/выход (гнезда Jack), гнездо для адаптера 9V. Металлический корпус. Питание: батарея 9V `Крона`. В комплекте: руководство по эксплуатации, солевая батарея 9V. Адаптер PS</strong><em><strong>A серии, приобретается отдельно. Педаль гитарная Distortion. Регуляторы: Gain, Level и Tone. Индикатор Check. Разъемы: вход/выход (гнезда Jack), гнездо для адаптера 9V. Металлический корп</strong>ус. Питание: батарея 9V `Крона`. В комплекте: руководство по эксплуатации, солевая батарея 9V. Адаптер PSA серии, приобретается отдельно. Педаль гитарная Distortion. Регуляторы: Gain, Level и Tone. Индикатор Check. Разъемы: вход/выход (гнезда Jack), гнездо для адаптера 9V. Металлический корпус. Питание: батарея 9V `Крона`. В комплекте: руководство по эксплуатации, солевая батарея 9V. Адаптер PSA серии, приобретается отдельно. Педаль гитарная Distortion. Регуляторы: Gain, Level и Tone. Индикатор Check. Разъемы: вход/выход (гнезда Jack), гнездо для адаптера 9V. Металлическ</em>ий корпус. Питание: батарея 9V `Крона`. В комплекте: руководство по эксплуатации, солевая батарея 9V. Адаптер PSA серии, приобретается отдельно. </p>\n<p>\n    <br></p>\n<p>Педаль гитарная Distortion. Регуляторы: Gain, Level и Tone. Индикатор Check. Разъемы: вход/выход (гнезда Jack), гнездо для адаптера 9V. Металлический корпус. Питание: батарея 9V `Крона`. В комплекте: руководство по эксплуатации, солевая батарея 9V. Адаптер PSA серии, приобретается отдельно. </p>\n<p>Педаль гитарная Distortion. Регуляторы: Gain, Level и Tone. Индикатор Check. Разъемы: вход/выход (гнезда Jack), гнездо для адаптера 9V. Металлический корпус. Питание: батарея 9V `Крона`. В комплекте: руководство по эксплуатации, солевая батарея 9V. Адаптер PSA серии, приобретается отдельно. </p>	2019-05-29 16:54:55	t	2019-05-29 16:55:21	2019-05-29 16:55:21	\N
\.


--
-- Data for Name: rainlab_blog_posts_categories; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.rainlab_blog_posts_categories (post_id, category_id) FROM stdin;
\.


--
-- Name: rainlab_blog_posts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.rainlab_blog_posts_id_seq', 3, true);


--
-- Data for Name: rainlab_translate_attributes; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.rainlab_translate_attributes (id, locale, model_id, model_type, attribute_data) FROM stdin;
\.


--
-- Name: rainlab_translate_attributes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.rainlab_translate_attributes_id_seq', 1, false);


--
-- Data for Name: rainlab_translate_indexes; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.rainlab_translate_indexes (id, locale, model_id, model_type, item, value) FROM stdin;
\.


--
-- Name: rainlab_translate_indexes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.rainlab_translate_indexes_id_seq', 1, false);


--
-- Data for Name: rainlab_translate_locales; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.rainlab_translate_locales (id, code, name, is_default, is_enabled, sort_order) FROM stdin;
2	ru	Русский	t	t	1
1	en	English	f	t	2
\.


--
-- Name: rainlab_translate_locales_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.rainlab_translate_locales_id_seq', 2, true);


--
-- Data for Name: rainlab_translate_messages; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.rainlab_translate_messages (id, code, message_data) FROM stdin;
1	блог	{"x":"\\u0411\\u043b\\u043e\\u0433"}
2	главная	{"x":"\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f"}
3	подробнее	{"x":"\\u041f\\u043e\\u0434\\u0440\\u043e\\u0431\\u043d\\u0435\\u0435"}
4	последние.статьи	{"x":"\\u041f\\u043e\\u0441\\u043b\\u0435\\u0434\\u043d\\u0438\\u0435 \\u0441\\u0442\\u0430\\u0442\\u044c\\u0438"}
\.


--
-- Name: rainlab_translate_messages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.rainlab_translate_messages_id_seq', 4, true);


--
-- Data for Name: sessions; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.sessions (id, payload, last_activity, user_id, ip_address, user_agent) FROM stdin;
\.


--
-- Data for Name: system_event_logs; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.system_event_logs (id, level, message, details, created_at, updated_at) FROM stdin;
1	error	Twig\\Error\\SyntaxError: Unclosed "(" in "/var/www/astro-web-blog/www/themes/demo/partials/site/header.htm" at line 34. in /var/www/astro-web-blog/www/vendor/twig/twig/src/Lexer.php:345\nStack trace:\n#0 /var/www/astro-web-blog/www/vendor/twig/twig/src/Lexer.php(292): Twig\\Lexer->lexExpression()\n#1 /var/www/astro-web-blog/www/vendor/twig/twig/src/Lexer.php(186): Twig\\Lexer->lexVar()\n#2 /var/www/astro-web-blog/www/vendor/twig/twig/src/Environment.php(542): Twig\\Lexer->tokenize(Object(Twig\\Source))\n#3 /var/www/astro-web-blog/www/vendor/twig/twig/src/Environment.php(595): Twig\\Environment->tokenize(Object(Twig\\Source))\n#4 /var/www/astro-web-blog/www/vendor/twig/twig/src/Environment.php(408): Twig\\Environment->compileSource(Object(Twig\\Source))\n#5 /var/www/astro-web-blog/www/vendor/twig/twig/src/Environment.php(381): Twig\\Environment->loadClass('__TwigTemplate_...', '/var/www/astro-...', NULL)\n#6 /var/www/astro-web-blog/www/modules/cms/classes/Controller.php(1089): Twig\\Environment->loadTemplate('/var/www/astro-...')\n#7 /var/www/astro-web-blog/www/modules/cms/twig/Extension.php(102): Cms\\Classes\\Controller->renderPartial('site/header', Array, true)\n#8 /var/www/astro-web-blog/www/storage/cms/twig/d4/d47a376b07d18142ebeb1d1223e6854a758761dac0453f07d6055b90b0901169.php(68): Cms\\Twig\\Extension->partialFunction('site/header', Array, true)\n#9 /var/www/astro-web-blog/www/vendor/twig/twig/src/Template.php(395): __TwigTemplate_e8d268281efb349fd6e5ed4e009be2c330444e868c237fd72c31445de4a2ded1->doDisplay(Array, Array)\n#10 /var/www/astro-web-blog/www/vendor/twig/twig/src/Template.php(372): Twig\\Template->displayWithErrorHandling(Array, Array)\n#11 /var/www/astro-web-blog/www/vendor/twig/twig/src/Template.php(380): Twig\\Template->display(Array)\n#12 /var/www/astro-web-blog/www/modules/cms/classes/Controller.php(434): Twig\\Template->render(Array)\n#13 /var/www/astro-web-blog/www/modules/cms/classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#14 /var/www/astro-web-blog/www/modules/cms/classes/CmsController.php(50): Cms\\Classes\\Controller->run('/')\n#15 [internal function]: Cms\\Classes\\CmsController->run('/')\n#16 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#17 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction('run', Array)\n#18 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), 'run')\n#19 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#20 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#21 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#22 /var/www/astro-web-blog/www/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#23 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#24 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#25 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#26 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#27 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#29 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#30 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#32 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#33 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#35 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#36 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#38 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#39 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#41 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#42 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#43 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#44 /var/www/astro-web-blog/www/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#45 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#46 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#47 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#48 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#49 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#50 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#51 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#52 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#53 /var/www/astro-web-blog/www/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#54 {main}	\N	2019-05-29 12:50:14	2019-05-29 12:50:14
2	error	ErrorException: Array to string conversion in /var/www/astro-web-blog/www/storage/cms/twig/d4/d47a376b07d18142ebeb1d1223e6854a758761dac0453f07d6055b90b0901169.php:41\nStack trace:\n#0 /var/www/astro-web-blog/www/storage/cms/twig/d4/d47a376b07d18142ebeb1d1223e6854a758761dac0453f07d6055b90b0901169.php(41): Illuminate\\Foundation\\Bootstrap\\HandleExceptions->handleError(8, 'Array to string...', '/var/www/astro-...', 41, Array)\n#1 /var/www/astro-web-blog/www/vendor/twig/twig/src/Template.php(395): __TwigTemplate_e8d268281efb349fd6e5ed4e009be2c330444e868c237fd72c31445de4a2ded1->doDisplay(Array, Array)\n#2 /var/www/astro-web-blog/www/vendor/twig/twig/src/Template.php(372): Twig\\Template->displayWithErrorHandling(Array, Array)\n#3 /var/www/astro-web-blog/www/vendor/twig/twig/src/Template.php(380): Twig\\Template->display(Array)\n#4 /var/www/astro-web-blog/www/modules/cms/classes/Controller.php(434): Twig\\Template->render(Array)\n#5 /var/www/astro-web-blog/www/modules/cms/classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#6 /var/www/astro-web-blog/www/modules/cms/classes/CmsController.php(50): Cms\\Classes\\Controller->run('/')\n#7 [internal function]: Cms\\Classes\\CmsController->run('/')\n#8 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#9 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction('run', Array)\n#10 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), 'run')\n#11 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#12 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#13 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#14 /var/www/astro-web-blog/www/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#15 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#16 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#17 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#18 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#19 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#20 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#21 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#22 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#23 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#24 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#25 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#26 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#28 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#29 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#31 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#32 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#34 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#35 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#36 /var/www/astro-web-blog/www/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#37 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#38 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#44 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#45 /var/www/astro-web-blog/www/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#46 {main}\n\nNext Twig\\Error\\RuntimeError: An exception has been thrown during the rendering of a template ("Array to string conversion") in "/var/www/astro-web-blog/www/themes/demo/layouts/default.htm" at line 5. in /var/www/astro-web-blog/www/vendor/twig/twig/src/Template.php:409\nStack trace:\n#0 /var/www/astro-web-blog/www/vendor/twig/twig/src/Template.php(372): Twig\\Template->displayWithErrorHandling(Array, Array)\n#1 /var/www/astro-web-blog/www/vendor/twig/twig/src/Template.php(380): Twig\\Template->display(Array)\n#2 /var/www/astro-web-blog/www/modules/cms/classes/Controller.php(434): Twig\\Template->render(Array)\n#3 /var/www/astro-web-blog/www/modules/cms/classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#4 /var/www/astro-web-blog/www/modules/cms/classes/CmsController.php(50): Cms\\Classes\\Controller->run('/')\n#5 [internal function]: Cms\\Classes\\CmsController->run('/')\n#6 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#7 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction('run', Array)\n#8 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), 'run')\n#9 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#10 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#11 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#12 /var/www/astro-web-blog/www/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#13 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#14 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#15 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#16 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#17 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#18 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#19 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#20 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#21 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#22 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#23 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#24 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#25 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#26 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#29 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#32 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#33 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#34 /var/www/astro-web-blog/www/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#35 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#36 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#38 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#39 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#41 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#42 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#43 /var/www/astro-web-blog/www/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#44 {main}	\N	2019-05-29 13:14:07	2019-05-29 13:14:07
3	error	ErrorException: Array to string conversion in /var/www/astro-web-blog/www/storage/cms/twig/d4/d47a376b07d18142ebeb1d1223e6854a758761dac0453f07d6055b90b0901169.php:41\nStack trace:\n#0 /var/www/astro-web-blog/www/storage/cms/twig/d4/d47a376b07d18142ebeb1d1223e6854a758761dac0453f07d6055b90b0901169.php(41): Illuminate\\Foundation\\Bootstrap\\HandleExceptions->handleError(8, 'Array to string...', '/var/www/astro-...', 41, Array)\n#1 /var/www/astro-web-blog/www/vendor/twig/twig/src/Template.php(395): __TwigTemplate_e8d268281efb349fd6e5ed4e009be2c330444e868c237fd72c31445de4a2ded1->doDisplay(Array, Array)\n#2 /var/www/astro-web-blog/www/vendor/twig/twig/src/Template.php(372): Twig\\Template->displayWithErrorHandling(Array, Array)\n#3 /var/www/astro-web-blog/www/vendor/twig/twig/src/Template.php(380): Twig\\Template->display(Array)\n#4 /var/www/astro-web-blog/www/modules/cms/classes/Controller.php(434): Twig\\Template->render(Array)\n#5 /var/www/astro-web-blog/www/modules/cms/classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#6 /var/www/astro-web-blog/www/modules/cms/classes/CmsController.php(50): Cms\\Classes\\Controller->run('/')\n#7 [internal function]: Cms\\Classes\\CmsController->run('/')\n#8 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#9 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction('run', Array)\n#10 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), 'run')\n#11 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#12 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#13 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#14 /var/www/astro-web-blog/www/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#15 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#16 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#17 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#18 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#19 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#20 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#21 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#22 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#23 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#24 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#25 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#26 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#28 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#29 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#31 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#32 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#34 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#35 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#36 /var/www/astro-web-blog/www/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#37 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#38 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#44 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#45 /var/www/astro-web-blog/www/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#46 {main}\n\nNext Twig\\Error\\RuntimeError: An exception has been thrown during the rendering of a template ("Array to string conversion") in "/var/www/astro-web-blog/www/themes/demo/layouts/default.htm" at line 5. in /var/www/astro-web-blog/www/vendor/twig/twig/src/Template.php:409\nStack trace:\n#0 /var/www/astro-web-blog/www/vendor/twig/twig/src/Template.php(372): Twig\\Template->displayWithErrorHandling(Array, Array)\n#1 /var/www/astro-web-blog/www/vendor/twig/twig/src/Template.php(380): Twig\\Template->display(Array)\n#2 /var/www/astro-web-blog/www/modules/cms/classes/Controller.php(434): Twig\\Template->render(Array)\n#3 /var/www/astro-web-blog/www/modules/cms/classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#4 /var/www/astro-web-blog/www/modules/cms/classes/CmsController.php(50): Cms\\Classes\\Controller->run('/')\n#5 [internal function]: Cms\\Classes\\CmsController->run('/')\n#6 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#7 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction('run', Array)\n#8 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), 'run')\n#9 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#10 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#11 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#12 /var/www/astro-web-blog/www/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#13 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#14 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#15 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#16 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#17 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#18 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#19 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#20 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#21 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#22 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#23 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#24 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#25 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#26 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#29 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#32 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#33 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#34 /var/www/astro-web-blog/www/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#35 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#36 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#38 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#39 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#41 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#42 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#43 /var/www/astro-web-blog/www/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#44 {main}	\N	2019-05-29 13:14:12	2019-05-29 13:14:12
4	error	Twig\\Error\\SyntaxError: Unknown "dd" function in "/var/www/astro-web-blog/www/themes/demo/layouts/default.htm" at line 5. in /var/www/astro-web-blog/www/vendor/twig/twig/src/ExpressionParser.php:735\nStack trace:\n#0 /var/www/astro-web-blog/www/vendor/twig/twig/src/ExpressionParser.php(451): Twig\\ExpressionParser->getFunctionNodeClass('dd', 5)\n#1 /var/www/astro-web-blog/www/vendor/twig/twig/src/ExpressionParser.php(235): Twig\\ExpressionParser->getFunctionNode('dd', 5)\n#2 /var/www/astro-web-blog/www/vendor/twig/twig/src/ExpressionParser.php(175): Twig\\ExpressionParser->parsePrimaryExpression()\n#3 /var/www/astro-web-blog/www/vendor/twig/twig/src/ExpressionParser.php(70): Twig\\ExpressionParser->getPrimary()\n#4 /var/www/astro-web-blog/www/vendor/twig/twig/src/Parser.php(142): Twig\\ExpressionParser->parseExpression()\n#5 /var/www/astro-web-blog/www/vendor/twig/twig/src/Parser.php(98): Twig\\Parser->subparse(NULL, false)\n#6 /var/www/astro-web-blog/www/vendor/twig/twig/src/Environment.php(563): Twig\\Parser->parse(Object(Twig\\TokenStream))\n#7 /var/www/astro-web-blog/www/vendor/twig/twig/src/Environment.php(595): Twig\\Environment->parse(Object(Twig\\TokenStream))\n#8 /var/www/astro-web-blog/www/vendor/twig/twig/src/Environment.php(408): Twig\\Environment->compileSource(Object(Twig\\Source))\n#9 /var/www/astro-web-blog/www/vendor/twig/twig/src/Environment.php(381): Twig\\Environment->loadClass('__TwigTemplate_...', '/var/www/astro-...', NULL)\n#10 /var/www/astro-web-blog/www/modules/cms/classes/Controller.php(433): Twig\\Environment->loadTemplate('/var/www/astro-...')\n#11 /var/www/astro-web-blog/www/modules/cms/classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#12 /var/www/astro-web-blog/www/modules/cms/classes/CmsController.php(50): Cms\\Classes\\Controller->run('/')\n#13 [internal function]: Cms\\Classes\\CmsController->run('/')\n#14 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#15 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction('run', Array)\n#16 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), 'run')\n#17 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#18 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#19 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#20 /var/www/astro-web-blog/www/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#21 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#22 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#23 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#24 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#25 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#26 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#28 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#29 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#31 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#32 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#34 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#35 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#37 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#38 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#40 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#41 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#42 /var/www/astro-web-blog/www/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#43 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#44 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#47 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#48 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#49 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#50 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#51 /var/www/astro-web-blog/www/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#52 {main}	\N	2019-05-29 13:14:21	2019-05-29 13:14:21
5	error	Twig\\Error\\SyntaxError: Unknown "var_dump" function in "/var/www/astro-web-blog/www/themes/demo/layouts/default.htm" at line 5. in /var/www/astro-web-blog/www/vendor/twig/twig/src/ExpressionParser.php:735\nStack trace:\n#0 /var/www/astro-web-blog/www/vendor/twig/twig/src/ExpressionParser.php(451): Twig\\ExpressionParser->getFunctionNodeClass('var_dump', 5)\n#1 /var/www/astro-web-blog/www/vendor/twig/twig/src/ExpressionParser.php(235): Twig\\ExpressionParser->getFunctionNode('var_dump', 5)\n#2 /var/www/astro-web-blog/www/vendor/twig/twig/src/ExpressionParser.php(175): Twig\\ExpressionParser->parsePrimaryExpression()\n#3 /var/www/astro-web-blog/www/vendor/twig/twig/src/ExpressionParser.php(70): Twig\\ExpressionParser->getPrimary()\n#4 /var/www/astro-web-blog/www/vendor/twig/twig/src/Parser.php(142): Twig\\ExpressionParser->parseExpression()\n#5 /var/www/astro-web-blog/www/vendor/twig/twig/src/Parser.php(98): Twig\\Parser->subparse(NULL, false)\n#6 /var/www/astro-web-blog/www/vendor/twig/twig/src/Environment.php(563): Twig\\Parser->parse(Object(Twig\\TokenStream))\n#7 /var/www/astro-web-blog/www/vendor/twig/twig/src/Environment.php(595): Twig\\Environment->parse(Object(Twig\\TokenStream))\n#8 /var/www/astro-web-blog/www/vendor/twig/twig/src/Environment.php(408): Twig\\Environment->compileSource(Object(Twig\\Source))\n#9 /var/www/astro-web-blog/www/vendor/twig/twig/src/Environment.php(381): Twig\\Environment->loadClass('__TwigTemplate_...', '/var/www/astro-...', NULL)\n#10 /var/www/astro-web-blog/www/modules/cms/classes/Controller.php(433): Twig\\Environment->loadTemplate('/var/www/astro-...')\n#11 /var/www/astro-web-blog/www/modules/cms/classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#12 /var/www/astro-web-blog/www/modules/cms/classes/CmsController.php(50): Cms\\Classes\\Controller->run('/')\n#13 [internal function]: Cms\\Classes\\CmsController->run('/')\n#14 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#15 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction('run', Array)\n#16 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), 'run')\n#17 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#18 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#19 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#20 /var/www/astro-web-blog/www/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#21 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#22 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#23 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#24 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#25 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#26 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#28 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#29 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#31 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#32 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#34 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#35 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#37 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#38 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#40 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#41 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#42 /var/www/astro-web-blog/www/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#43 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#44 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#47 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#48 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#49 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#50 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#51 /var/www/astro-web-blog/www/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#52 {main}	\N	2019-05-29 13:14:27	2019-05-29 13:14:27
6	error	Twig\\Error\\SyntaxError: Unexpected token "string" of value "blog.htm" ("end of statement block" expected) in "/var/www/astro-web-blog/www/themes/demo/pages/home.htm" at line 1. in /var/www/astro-web-blog/www/vendor/twig/twig/src/TokenStream.php:76\nStack trace:\n#0 /var/www/astro-web-blog/www/modules/cms/twig/PageTokenParser.php(25): Twig\\TokenStream->expect(3)\n#1 /var/www/astro-web-blog/www/vendor/twig/twig/src/Parser.php(185): Cms\\Twig\\PageTokenParser->parse(Object(Twig\\Token))\n#2 /var/www/astro-web-blog/www/vendor/twig/twig/src/Parser.php(98): Twig\\Parser->subparse(NULL, false)\n#3 /var/www/astro-web-blog/www/vendor/twig/twig/src/Environment.php(563): Twig\\Parser->parse(Object(Twig\\TokenStream))\n#4 /var/www/astro-web-blog/www/vendor/twig/twig/src/Environment.php(595): Twig\\Environment->parse(Object(Twig\\TokenStream))\n#5 /var/www/astro-web-blog/www/vendor/twig/twig/src/Environment.php(408): Twig\\Environment->compileSource(Object(Twig\\Source))\n#6 /var/www/astro-web-blog/www/vendor/twig/twig/src/Environment.php(381): Twig\\Environment->loadClass('__TwigTemplate_...', '/var/www/astro-...', NULL)\n#7 /var/www/astro-web-blog/www/modules/cms/classes/Controller.php(423): Twig\\Environment->loadTemplate('/var/www/astro-...')\n#8 /var/www/astro-web-blog/www/modules/cms/classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#9 /var/www/astro-web-blog/www/modules/cms/classes/CmsController.php(50): Cms\\Classes\\Controller->run('/')\n#10 [internal function]: Cms\\Classes\\CmsController->run('/')\n#11 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#12 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction('run', Array)\n#13 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), 'run')\n#14 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#15 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#16 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#17 /var/www/astro-web-blog/www/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#18 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#19 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#20 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#21 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#22 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#23 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#24 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#25 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#26 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#28 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#29 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#31 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#32 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#34 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#35 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#37 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#38 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#39 /var/www/astro-web-blog/www/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#40 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#41 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#47 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#48 /var/www/astro-web-blog/www/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#49 {main}	\N	2019-05-29 14:17:18	2019-05-29 14:17:18
7	error	Twig\\Error\\SyntaxError: Unexpected token "name" of value "blog" ("end of statement block" expected) in "/var/www/astro-web-blog/www/themes/demo/pages/home.htm" at line 1. in /var/www/astro-web-blog/www/vendor/twig/twig/src/TokenStream.php:76\nStack trace:\n#0 /var/www/astro-web-blog/www/modules/cms/twig/PageTokenParser.php(25): Twig\\TokenStream->expect(3)\n#1 /var/www/astro-web-blog/www/vendor/twig/twig/src/Parser.php(185): Cms\\Twig\\PageTokenParser->parse(Object(Twig\\Token))\n#2 /var/www/astro-web-blog/www/vendor/twig/twig/src/Parser.php(98): Twig\\Parser->subparse(NULL, false)\n#3 /var/www/astro-web-blog/www/vendor/twig/twig/src/Environment.php(563): Twig\\Parser->parse(Object(Twig\\TokenStream))\n#4 /var/www/astro-web-blog/www/vendor/twig/twig/src/Environment.php(595): Twig\\Environment->parse(Object(Twig\\TokenStream))\n#5 /var/www/astro-web-blog/www/vendor/twig/twig/src/Environment.php(408): Twig\\Environment->compileSource(Object(Twig\\Source))\n#6 /var/www/astro-web-blog/www/vendor/twig/twig/src/Environment.php(381): Twig\\Environment->loadClass('__TwigTemplate_...', '/var/www/astro-...', NULL)\n#7 /var/www/astro-web-blog/www/modules/cms/classes/Controller.php(423): Twig\\Environment->loadTemplate('/var/www/astro-...')\n#8 /var/www/astro-web-blog/www/modules/cms/classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#9 /var/www/astro-web-blog/www/modules/cms/classes/CmsController.php(50): Cms\\Classes\\Controller->run('/')\n#10 [internal function]: Cms\\Classes\\CmsController->run('/')\n#11 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#12 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction('run', Array)\n#13 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), 'run')\n#14 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#15 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#16 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#17 /var/www/astro-web-blog/www/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#18 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#19 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#20 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#21 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#22 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#23 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#24 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#25 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#26 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#28 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#29 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#31 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#32 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#34 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#35 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#37 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#38 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#39 /var/www/astro-web-blog/www/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#40 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#41 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#47 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#48 /var/www/astro-web-blog/www/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#49 {main}	\N	2019-05-29 14:18:32	2019-05-29 14:18:32
8	error	Twig\\Error\\SyntaxError: Unexpected token "string" of value "blog.htm" ("end of statement block" expected) in "/var/www/astro-web-blog/www/themes/demo/pages/home.htm" at line 1. in /var/www/astro-web-blog/www/vendor/twig/twig/src/TokenStream.php:76\nStack trace:\n#0 /var/www/astro-web-blog/www/modules/cms/twig/PageTokenParser.php(25): Twig\\TokenStream->expect(3)\n#1 /var/www/astro-web-blog/www/vendor/twig/twig/src/Parser.php(185): Cms\\Twig\\PageTokenParser->parse(Object(Twig\\Token))\n#2 /var/www/astro-web-blog/www/vendor/twig/twig/src/Parser.php(98): Twig\\Parser->subparse(NULL, false)\n#3 /var/www/astro-web-blog/www/vendor/twig/twig/src/Environment.php(563): Twig\\Parser->parse(Object(Twig\\TokenStream))\n#4 /var/www/astro-web-blog/www/vendor/twig/twig/src/Environment.php(595): Twig\\Environment->parse(Object(Twig\\TokenStream))\n#5 /var/www/astro-web-blog/www/vendor/twig/twig/src/Environment.php(408): Twig\\Environment->compileSource(Object(Twig\\Source))\n#6 /var/www/astro-web-blog/www/vendor/twig/twig/src/Environment.php(381): Twig\\Environment->loadClass('__TwigTemplate_...', '/var/www/astro-...', NULL)\n#7 /var/www/astro-web-blog/www/modules/cms/classes/Controller.php(423): Twig\\Environment->loadTemplate('/var/www/astro-...')\n#8 /var/www/astro-web-blog/www/modules/cms/classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#9 /var/www/astro-web-blog/www/modules/cms/classes/CmsController.php(50): Cms\\Classes\\Controller->run('/')\n#10 [internal function]: Cms\\Classes\\CmsController->run('/')\n#11 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#12 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction('run', Array)\n#13 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), 'run')\n#14 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#15 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#16 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#17 /var/www/astro-web-blog/www/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#18 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#19 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#20 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#21 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#22 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#23 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#24 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#25 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#26 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#28 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#29 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#31 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#32 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#34 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#35 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#37 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#38 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#39 /var/www/astro-web-blog/www/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#40 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#41 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#47 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#48 /var/www/astro-web-blog/www/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#49 {main}	\N	2019-05-29 14:18:45	2019-05-29 14:18:45
9	error	Twig\\Error\\SyntaxError: Unexpected token "string" of value "blog.htm" ("end of statement block" expected) in "/var/www/astro-web-blog/www/themes/demo/pages/home.htm" at line 1. in /var/www/astro-web-blog/www/vendor/twig/twig/src/TokenStream.php:76\nStack trace:\n#0 /var/www/astro-web-blog/www/modules/cms/twig/PageTokenParser.php(25): Twig\\TokenStream->expect(3)\n#1 /var/www/astro-web-blog/www/vendor/twig/twig/src/Parser.php(185): Cms\\Twig\\PageTokenParser->parse(Object(Twig\\Token))\n#2 /var/www/astro-web-blog/www/vendor/twig/twig/src/Parser.php(98): Twig\\Parser->subparse(NULL, false)\n#3 /var/www/astro-web-blog/www/vendor/twig/twig/src/Environment.php(563): Twig\\Parser->parse(Object(Twig\\TokenStream))\n#4 /var/www/astro-web-blog/www/vendor/twig/twig/src/Environment.php(595): Twig\\Environment->parse(Object(Twig\\TokenStream))\n#5 /var/www/astro-web-blog/www/vendor/twig/twig/src/Environment.php(408): Twig\\Environment->compileSource(Object(Twig\\Source))\n#6 /var/www/astro-web-blog/www/vendor/twig/twig/src/Environment.php(381): Twig\\Environment->loadClass('__TwigTemplate_...', '/var/www/astro-...', NULL)\n#7 /var/www/astro-web-blog/www/modules/cms/classes/Controller.php(423): Twig\\Environment->loadTemplate('/var/www/astro-...')\n#8 /var/www/astro-web-blog/www/modules/cms/classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#9 /var/www/astro-web-blog/www/modules/cms/classes/CmsController.php(50): Cms\\Classes\\Controller->run('/')\n#10 [internal function]: Cms\\Classes\\CmsController->run('/')\n#11 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#12 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction('run', Array)\n#13 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), 'run')\n#14 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#15 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#16 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#17 /var/www/astro-web-blog/www/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#18 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#19 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#20 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#21 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#22 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#23 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#24 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#25 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#26 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#28 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#29 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#31 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#32 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#34 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#35 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#37 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#38 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#39 /var/www/astro-web-blog/www/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#40 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#41 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#47 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#48 /var/www/astro-web-blog/www/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#49 {main}	\N	2019-05-29 14:44:50	2019-05-29 14:44:50
10	error	Cms\\Classes\\CmsException: The partial 'sections/blog-list' is not found. in /var/www/astro-web-blog/www/modules/cms/classes/Controller.php:1032\nStack trace:\n#0 /var/www/astro-web-blog/www/modules/cms/twig/Extension.php(102): Cms\\Classes\\Controller->renderPartial('sections/blog-l...', Array, true)\n#1 /var/www/astro-web-blog/www/storage/cms/twig/f8/f897352984b40cc5b00766adaa71df2d1c2cf5107a9bb405722cdeea29d4d686.php(36): Cms\\Twig\\Extension->partialFunction('sections/blog-l...', Array, true)\n#2 /var/www/astro-web-blog/www/vendor/twig/twig/src/Template.php(395): __TwigTemplate_f448a4cc46edb3fa4c5de7a5e8e615d8210ab9023b473a45e247eb526a801a30->doDisplay(Array, Array)\n#3 /var/www/astro-web-blog/www/vendor/twig/twig/src/Template.php(372): Twig\\Template->displayWithErrorHandling(Array, Array)\n#4 /var/www/astro-web-blog/www/vendor/twig/twig/src/Template.php(380): Twig\\Template->display(Array)\n#5 /var/www/astro-web-blog/www/modules/cms/classes/Controller.php(424): Twig\\Template->render(Array)\n#6 /var/www/astro-web-blog/www/modules/cms/classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#7 /var/www/astro-web-blog/www/modules/cms/classes/CmsController.php(50): Cms\\Classes\\Controller->run('/')\n#8 [internal function]: Cms\\Classes\\CmsController->run('/')\n#9 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#10 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction('run', Array)\n#11 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), 'run')\n#12 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#13 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#14 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#15 /var/www/astro-web-blog/www/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#16 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#17 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#18 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#19 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#20 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#21 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#22 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#23 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#24 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#25 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#26 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#29 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#32 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#35 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#36 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#37 /var/www/astro-web-blog/www/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#38 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#39 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#41 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#42 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#44 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#45 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#46 /var/www/astro-web-blog/www/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#47 {main}\n\nNext Twig\\Error\\RuntimeError: An exception has been thrown during the rendering of a template ("The partial 'sections/blog-list' is not found.") in "/var/www/astro-web-blog/www/themes/demo/pages/blog.htm" at line 1. in /var/www/astro-web-blog/www/vendor/twig/twig/src/Template.php:409\nStack trace:\n#0 /var/www/astro-web-blog/www/vendor/twig/twig/src/Template.php(372): Twig\\Template->displayWithErrorHandling(Array, Array)\n#1 /var/www/astro-web-blog/www/vendor/twig/twig/src/Template.php(380): Twig\\Template->display(Array)\n#2 /var/www/astro-web-blog/www/modules/cms/classes/Controller.php(424): Twig\\Template->render(Array)\n#3 /var/www/astro-web-blog/www/modules/cms/classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#4 /var/www/astro-web-blog/www/modules/cms/classes/CmsController.php(50): Cms\\Classes\\Controller->run('/')\n#5 [internal function]: Cms\\Classes\\CmsController->run('/')\n#6 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#7 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction('run', Array)\n#8 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), 'run')\n#9 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#10 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#11 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#12 /var/www/astro-web-blog/www/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#13 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#14 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#15 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#16 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#17 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#18 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#19 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#20 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#21 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#22 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#23 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#24 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#25 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#26 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#29 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#32 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#33 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#34 /var/www/astro-web-blog/www/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#35 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#36 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#38 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#39 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#41 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#42 /var/www/astro-web-blog/www/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#43 /var/www/astro-web-blog/www/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#44 {main}	\N	2019-05-29 14:57:01	2019-05-29 14:57:01
\.


--
-- Name: system_event_logs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.system_event_logs_id_seq', 10, true);


--
-- Data for Name: system_files; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.system_files (id, disk_name, file_name, file_size, content_type, title, description, field, attachment_id, attachment_type, is_public, sort_order, created_at, updated_at) FROM stdin;
1	5ceea214038d9421781486.png	2452210_935xp.png	55309	image/jpeg	\N	\N	featured_images	1	RainLab\\Blog\\Models\\Post	t	1	2019-05-29 15:15:32	2019-05-29 15:15:42
2	5ceeb0ec5e11d518132808.jpg	3429173_935xp.jpg	62797	image/jpeg	\N	\N	featured_images	2	RainLab\\Blog\\Models\\Post	t	2	2019-05-29 16:18:52	2019-05-29 16:19:12
3	5ceeb97661b93957412654.jpg	2452259_935xp.jpg	55669	image/jpeg	\N	\N	featured_images	3	RainLab\\Blog\\Models\\Post	t	3	2019-05-29 16:55:18	2019-05-29 16:55:21
\.


--
-- Name: system_files_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.system_files_id_seq', 3, true);


--
-- Data for Name: system_mail_layouts; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.system_mail_layouts (id, name, code, content_html, content_text, content_css, is_locked, created_at, updated_at, options) FROM stdin;
1	Default layout	default	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml">\n<head>\n    <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\n</head>\n<body>\n    <style type="text/css" media="screen">\n        {{ brandCss|raw }}\n        {{ css|raw }}\n    </style>\n\n    <table class="wrapper layout-default" width="100%" cellpadding="0" cellspacing="0">\n\n        <!-- Header -->\n        {% partial 'header' body %}\n            {{ subject|raw }}\n        {% endpartial %}\n\n        <tr>\n            <td align="center">\n                <table class="content" width="100%" cellpadding="0" cellspacing="0">\n                    <!-- Email Body -->\n                    <tr>\n                        <td class="body" width="100%" cellpadding="0" cellspacing="0">\n                            <table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0">\n                                <!-- Body content -->\n                                <tr>\n                                    <td class="content-cell">\n                                        {{ content|raw }}\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n                </table>\n            </td>\n        </tr>\n\n        <!-- Footer -->\n        {% partial 'footer' body %}\n            &copy; {{ "now"|date("Y") }} {{ appName }}. All rights reserved.\n        {% endpartial %}\n\n    </table>\n\n</body>\n</html>	{{ content|raw }}	@media only screen and (max-width: 600px) {\n    .inner-body {\n        width: 100% !important;\n    }\n\n    .footer {\n        width: 100% !important;\n    }\n}\n\n@media only screen and (max-width: 500px) {\n    .button {\n        width: 100% !important;\n    }\n}	t	2019-05-29 10:40:43	2019-05-29 10:40:43	\N
2	System layout	system	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml">\n<head>\n    <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\n</head>\n<body>\n    <style type="text/css" media="screen">\n        {{ brandCss|raw }}\n        {{ css|raw }}\n    </style>\n\n    <table class="wrapper layout-system" width="100%" cellpadding="0" cellspacing="0">\n        <tr>\n            <td align="center">\n                <table class="content" width="100%" cellpadding="0" cellspacing="0">\n                    <!-- Email Body -->\n                    <tr>\n                        <td class="body" width="100%" cellpadding="0" cellspacing="0">\n                            <table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0">\n                                <!-- Body content -->\n                                <tr>\n                                    <td class="content-cell">\n                                        {{ content|raw }}\n\n                                        <!-- Subcopy -->\n                                        {% partial 'subcopy' body %}\n                                            **This is an automatic message. Please do not reply to it.**\n                                        {% endpartial %}\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n                </table>\n            </td>\n        </tr>\n    </table>\n\n</body>\n</html>	{{ content|raw }}\n\n\n---\nThis is an automatic message. Please do not reply to it.	@media only screen and (max-width: 600px) {\n    .inner-body {\n        width: 100% !important;\n    }\n\n    .footer {\n        width: 100% !important;\n    }\n}\n\n@media only screen and (max-width: 500px) {\n    .button {\n        width: 100% !important;\n    }\n}	t	2019-05-29 10:40:43	2019-05-29 10:40:43	\N
\.


--
-- Name: system_mail_layouts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.system_mail_layouts_id_seq', 2, true);


--
-- Data for Name: system_mail_partials; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.system_mail_partials (id, name, code, content_html, content_text, is_custom, created_at, updated_at) FROM stdin;
\.


--
-- Name: system_mail_partials_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.system_mail_partials_id_seq', 1, false);


--
-- Data for Name: system_mail_templates; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.system_mail_templates (id, code, subject, description, content_html, content_text, layout_id, is_custom, created_at, updated_at) FROM stdin;
\.


--
-- Name: system_mail_templates_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.system_mail_templates_id_seq', 1, false);


--
-- Data for Name: system_parameters; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.system_parameters (id, namespace, "group", item, value) FROM stdin;
1	system	update	count	0
2	system	core	hash	"530fb2559d6b264485c60ac3797fe8ac"
3	system	core	build	"455"
4	system	update	retry	1559300271
\.


--
-- Name: system_parameters_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.system_parameters_id_seq', 4, true);


--
-- Data for Name: system_plugin_history; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.system_plugin_history (id, code, type, version, detail, created_at) FROM stdin;
1	October.Demo	comment	1.0.1	First version of Demo	2019-05-29 10:40:43
2	RainLab.Blog	script	1.0.1	create_posts_table.php	2019-05-29 10:52:49
3	RainLab.Blog	script	1.0.1	create_categories_table.php	2019-05-29 10:52:49
4	RainLab.Blog	script	1.0.1	seed_all_tables.php	2019-05-29 10:52:49
5	RainLab.Blog	comment	1.0.1	Initialize plugin.	2019-05-29 10:52:49
6	RainLab.Blog	comment	1.0.2	Added the processed HTML content column to the posts table.	2019-05-29 10:52:49
7	RainLab.Blog	comment	1.0.3	Category component has been merged with Posts component.	2019-05-29 10:52:49
8	RainLab.Blog	comment	1.0.4	Improvements to the Posts list management UI.	2019-05-29 10:52:49
9	RainLab.Blog	comment	1.0.5	Removes the Author column from blog post list.	2019-05-29 10:52:49
10	RainLab.Blog	comment	1.0.6	Featured images now appear in the Post component.	2019-05-29 10:52:49
11	RainLab.Blog	comment	1.0.7	Added support for the Static Pages menus.	2019-05-29 10:52:49
12	RainLab.Blog	comment	1.0.8	Added total posts to category list.	2019-05-29 10:52:49
13	RainLab.Blog	comment	1.0.9	Added support for the Sitemap plugin.	2019-05-29 10:52:49
14	RainLab.Blog	comment	1.0.10	Added permission to prevent users from seeing posts they did not create.	2019-05-29 10:52:49
15	RainLab.Blog	comment	1.0.11	Deprecate "idParam" component property in favour of "slug" property.	2019-05-29 10:52:49
16	RainLab.Blog	comment	1.0.12	Fixes issue where images cannot be uploaded caused by latest Markdown library.	2019-05-29 10:52:49
17	RainLab.Blog	comment	1.0.13	Fixes problem with providing pages to Sitemap and Pages plugins.	2019-05-29 10:52:49
18	RainLab.Blog	comment	1.0.14	Add support for CSRF protection feature added to core.	2019-05-29 10:52:49
19	RainLab.Blog	comment	1.1.0	Replaced the Post editor with the new core Markdown editor.	2019-05-29 10:52:49
20	RainLab.Blog	comment	1.1.1	Posts can now be imported and exported.	2019-05-29 10:52:49
21	RainLab.Blog	comment	1.1.2	Posts are no longer visible if the published date has not passed.	2019-05-29 10:52:49
22	RainLab.Blog	comment	1.1.3	Added a New Post shortcut button to the blog menu.	2019-05-29 10:52:49
23	RainLab.Blog	script	1.2.0	categories_add_nested_fields.php	2019-05-29 10:52:49
24	RainLab.Blog	comment	1.2.0	Categories now support nesting.	2019-05-29 10:52:49
25	RainLab.Blog	comment	1.2.1	Post slugs now must be unique.	2019-05-29 10:52:49
26	RainLab.Blog	comment	1.2.2	Fixes issue on new installs.	2019-05-29 10:52:49
27	RainLab.Blog	comment	1.2.3	Minor user interface update.	2019-05-29 10:52:49
28	RainLab.Blog	script	1.2.4	update_timestamp_nullable.php	2019-05-29 10:52:49
29	RainLab.Blog	comment	1.2.4	Database maintenance. Updated all timestamp columns to be nullable.	2019-05-29 10:52:49
30	RainLab.Blog	comment	1.2.5	Added translation support for blog posts.	2019-05-29 10:52:49
31	RainLab.Blog	comment	1.2.6	The published field can now supply a time with the date.	2019-05-29 10:52:49
32	RainLab.Blog	comment	1.2.7	Introduced a new RSS feed component.	2019-05-29 10:52:49
33	RainLab.Blog	comment	1.2.8	Fixes issue with translated `content_html` attribute on blog posts.	2019-05-29 10:52:49
34	RainLab.Blog	comment	1.2.9	Added translation support for blog categories.	2019-05-29 10:52:49
35	RainLab.Blog	comment	1.2.10	Added translation support for post slugs.	2019-05-29 10:52:49
36	RainLab.Blog	comment	1.2.11	Fixes bug where excerpt is not translated.	2019-05-29 10:52:49
37	RainLab.Blog	comment	1.2.12	Description field added to category form.	2019-05-29 10:52:49
38	RainLab.Blog	comment	1.2.13	Improved support for Static Pages menus, added a blog post and all blog posts.	2019-05-29 10:52:49
39	RainLab.Blog	comment	1.2.14	Added post exception property to the post list component, useful for showing related posts.	2019-05-29 10:52:49
40	RainLab.Blog	comment	1.2.15	Back-end navigation sort order updated.	2019-05-29 10:52:49
41	RainLab.Blog	comment	1.2.16	Added `nextPost` and `previousPost` to the blog post component.	2019-05-29 10:52:49
42	RainLab.Blog	comment	1.2.17	Improved the next and previous logic to sort by the published date.	2019-05-29 10:52:49
43	RainLab.Blog	comment	1.2.18	Minor change to internals.	2019-05-29 10:52:49
44	RainLab.Blog	comment	1.2.19	Improved support for Build 420+	2019-05-29 10:52:49
45	RainLab.Blog	script	1.3.0	posts_add_metadata.php	2019-05-29 10:52:49
46	RainLab.Blog	comment	1.3.0	Added metadata column for plugins to store data in	2019-05-29 10:52:49
47	RainLab.Blog	comment	1.3.1	Fixed metadata column not being jsonable	2019-05-29 10:52:49
48	RainLab.Translate	script	1.0.1	create_messages_table.php	2019-05-29 10:54:12
49	RainLab.Translate	script	1.0.1	create_attributes_table.php	2019-05-29 10:54:12
50	RainLab.Translate	script	1.0.1	create_locales_table.php	2019-05-29 10:54:12
51	RainLab.Translate	comment	1.0.1	First version of Translate	2019-05-29 10:54:12
52	RainLab.Translate	comment	1.0.2	Languages and Messages can now be deleted.	2019-05-29 10:54:12
53	RainLab.Translate	comment	1.0.3	Minor updates for latest October release.	2019-05-29 10:54:12
54	RainLab.Translate	comment	1.0.4	Locale cache will clear when updating a language.	2019-05-29 10:54:12
55	RainLab.Translate	comment	1.0.5	Add Spanish language and fix plugin config.	2019-05-29 10:54:12
56	RainLab.Translate	comment	1.0.6	Minor improvements to the code.	2019-05-29 10:54:12
57	RainLab.Translate	comment	1.0.7	Fixes major bug where translations are skipped entirely!	2019-05-29 10:54:12
58	RainLab.Translate	comment	1.0.8	Minor bug fixes.	2019-05-29 10:54:12
59	RainLab.Translate	comment	1.0.9	Fixes an issue where newly created models lose their translated values.	2019-05-29 10:54:12
60	RainLab.Translate	comment	1.0.10	Minor fix for latest build.	2019-05-29 10:54:12
61	RainLab.Translate	comment	1.0.11	Fix multilingual rich editor when used in stretch mode.	2019-05-29 10:54:12
62	RainLab.Translate	comment	1.1.0	Introduce compatibility with RainLab.Pages plugin.	2019-05-29 10:54:12
63	RainLab.Translate	comment	1.1.1	Minor UI fix to the language picker.	2019-05-29 10:54:12
64	RainLab.Translate	comment	1.1.2	Add support for translating Static Content files.	2019-05-29 10:54:12
65	RainLab.Translate	comment	1.1.3	Improved support for the multilingual rich editor.	2019-05-29 10:54:12
66	RainLab.Translate	comment	1.1.4	Adds new multilingual markdown editor.	2019-05-29 10:54:12
67	RainLab.Translate	comment	1.1.5	Minor update to the multilingual control API.	2019-05-29 10:54:12
68	RainLab.Translate	comment	1.1.6	Minor improvements in the message editor.	2019-05-29 10:54:12
69	RainLab.Translate	comment	1.1.7	Fixes bug not showing content when first loading multilingual textarea controls.	2019-05-29 10:54:12
70	RainLab.Translate	comment	1.2.0	CMS pages now support translating the URL.	2019-05-29 10:54:12
71	RainLab.Translate	comment	1.2.1	Minor update in the rich editor and code editor language control position.	2019-05-29 10:54:12
72	RainLab.Translate	comment	1.2.2	Static Pages now support translating the URL.	2019-05-29 10:54:12
73	RainLab.Translate	comment	1.2.3	Fixes Rich Editor when inserting a page link.	2019-05-29 10:54:12
74	RainLab.Translate	script	1.2.4	create_indexes_table.php	2019-05-29 10:54:12
75	RainLab.Translate	comment	1.2.4	Translatable attributes can now be declared as indexes.	2019-05-29 10:54:12
76	RainLab.Translate	comment	1.2.5	Adds new multilingual repeater form widget.	2019-05-29 10:54:12
77	RainLab.Translate	comment	1.2.6	Fixes repeater usage with static pages plugin.	2019-05-29 10:54:12
78	RainLab.Translate	comment	1.2.7	Fixes placeholder usage with static pages plugin.	2019-05-29 10:54:12
79	RainLab.Translate	comment	1.2.8	Improvements to code for latest October build compatibility.	2019-05-29 10:54:12
80	RainLab.Translate	comment	1.2.9	Fixes context for translated strings when used with Static Pages.	2019-05-29 10:54:12
81	RainLab.Translate	comment	1.2.10	Minor UI fix to the multilingual repeater.	2019-05-29 10:54:12
82	RainLab.Translate	comment	1.2.11	Fixes translation not working with partials loaded via AJAX.	2019-05-29 10:54:12
83	RainLab.Translate	comment	1.2.12	Add support for translating the new grouped repeater feature.	2019-05-29 10:54:12
84	RainLab.Translate	comment	1.3.0	Added search to the translate messages page.	2019-05-29 10:54:12
85	RainLab.Translate	script	1.3.1	builder_table_update_rainlab_translate_locales.php	2019-05-29 10:54:12
86	RainLab.Translate	script	1.3.1	seed_all_tables.php	2019-05-29 10:54:12
87	RainLab.Translate	comment	1.3.1	Added reordering to languages	2019-05-29 10:54:12
88	RainLab.Translate	comment	1.3.2	Improved compatibility with RainLab.Pages, added ability to scan Mail Messages for translatable variables.	2019-05-29 10:54:12
89	RainLab.Translate	comment	1.3.3	Fix to the locale picker session handling in Build 420 onwards.	2019-05-29 10:54:12
90	RainLab.Translate	comment	1.3.4	Add alternate hreflang elements and adds prefixDefaultLocale setting.	2019-05-29 10:54:12
91	RainLab.Translate	comment	1.3.5	Fix MLRepeater bug when switching locales.	2019-05-29 10:54:12
92	RainLab.Translate	comment	1.3.6	Fix Middleware to use the prefixDefaultLocale setting introduced in 1.3.4	2019-05-29 10:54:12
93	RainLab.Translate	comment	1.3.7	Fix config reference in LocaleMiddleware	2019-05-29 10:54:12
94	RainLab.Translate	comment	1.3.8	Keep query string when switching locales	2019-05-29 10:54:12
95	RainLab.Translate	comment	1.4.0	Add importer and exporter for messages	2019-05-29 10:54:12
96	RainLab.Translate	comment	1.4.1	Updated Hungarian translation. Added Arabic translation. Fixed issue where default texts are overwritten by import. Fixed issue where the language switcher for repeater fields would overlap with the first repeater row.	2019-05-29 10:54:12
97	RainLab.Translate	comment	1.4.2	Add multilingual MediaFinder	2019-05-29 10:54:12
98	RainLab.Translate	comment	1.4.3	!!! Please update OctoberCMS to Build 444 before updating this plugin. Added ability to translate CMS Pages fields (e.g. title, description, meta-title, meta-description)	2019-05-29 10:54:12
99	RainLab.Translate	comment	1.4.4	Minor improvements to compatibility with Laravel framework.	2019-05-29 10:54:12
100	RainLab.Translate	comment	1.4.5	Fixed issue when using the language switcher	2019-05-29 10:54:12
101	RainLab.Translate	comment	1.5.0	Compatibility fix with Build 451	2019-05-29 10:54:12
102	RainLab.Translate	comment	1.6.0	Make File Upload widget properties translatable. Merge Repeater core changes into MLRepeater widget. Add getter method to retrieve original translate data.	2019-05-29 10:54:12
103	Excodus.TranslateExtended	comment	1.0.1	First version of Translate Extended	2019-05-29 10:54:31
104	Excodus.TranslateExtended	comment	1.0.2	Added backend settings page with option to opt-out from the browser lang detection and route prefixing	2019-05-29 10:54:31
105	Excodus.TranslateExtended	comment	1.0.2	Bug fixes	2019-05-29 10:54:31
106	Excodus.TranslateExtended	comment	1.0.2	Improved browser language matching	2019-05-29 10:54:31
107	Excodus.TranslateExtended	comment	1.0.3	Fixed default plugin settings	2019-05-29 10:54:31
108	Excodus.TranslateExtended	comment	1.0.4	Added extended locale picker	2019-05-29 10:54:31
109	Excodus.TranslateExtended	comment	1.0.5	Bug fixes	2019-05-29 10:54:31
110	Excodus.TranslateExtended	comment	1.0.5	Added setting for homepage redirect	2019-05-29 10:54:31
111	Excodus.TranslateExtended	comment	1.0.6	Support for translated page URLs	2019-05-29 10:54:31
112	Excodus.TranslateExtended	comment	1.0.6	Fix for switcher for pages in subdirectory of domain	2019-05-29 10:54:31
113	Excodus.TranslateExtended	comment	1.0.7	Specified 'web' middleware for Laravel 5.5 support	2019-05-29 10:54:31
114	AnandPatel.WysiwygEditors	comment	1.0.1	First version of Wysiwyg Editors.	2019-05-29 12:11:29
115	AnandPatel.WysiwygEditors	comment	1.0.2	Automatic Injection to CMS & other code editors and October CMS`s Rich Editor added.	2019-05-29 12:11:29
116	AnandPatel.WysiwygEditors	comment	1.0.3	Automatic Injection to RainLab Static Pages & other plugin`s option is appear only if installed.	2019-05-29 12:11:29
117	AnandPatel.WysiwygEditors	comment	1.0.4	New Froala editor added (on request from emzero439), Height & width property added for editor, setting moved to My Setting tab and minor changes in settings.	2019-05-29 12:11:29
118	AnandPatel.WysiwygEditors	comment	1.0.5	Automatic Injection to Radiantweb`s Problog and ProEvents (option available in settings-content).	2019-05-29 12:11:29
119	AnandPatel.WysiwygEditors	comment	1.0.6	CKEditor updated and bug fixes.	2019-05-29 12:11:29
120	AnandPatel.WysiwygEditors	comment	1.0.7	Integrated elFinder (file browser) with TinyMCE & CKEditor, Image upload/delete for Froala Editor.	2019-05-29 12:11:29
121	AnandPatel.WysiwygEditors	comment	1.0.8	Added security to File Browser`s route (Authenticate users can only access File Browser).	2019-05-29 12:11:29
122	AnandPatel.WysiwygEditors	comment	1.0.9	Updated CKEditor, Froala and TinyMCE.	2019-05-29 12:11:29
123	AnandPatel.WysiwygEditors	comment	1.1.0	Support multilanguage, update elFinder and cleanup code.	2019-05-29 12:11:29
124	AnandPatel.WysiwygEditors	comment	1.1.1	Added Turkish language.	2019-05-29 12:11:29
125	AnandPatel.WysiwygEditors	comment	1.1.2	Added Hungarian language.	2019-05-29 12:11:29
126	AnandPatel.WysiwygEditors	comment	1.1.3	Fixed issue related to RC (Elfinder fix remaining).	2019-05-29 12:11:29
127	AnandPatel.WysiwygEditors	comment	1.1.4	Fixed Elfinder issue.	2019-05-29 12:11:29
128	AnandPatel.WysiwygEditors	comment	1.1.5	Updated CKEditor, Froala and TinyMCE.	2019-05-29 12:11:29
129	AnandPatel.WysiwygEditors	comment	1.1.6	Changed destination folder.	2019-05-29 12:11:29
130	AnandPatel.WysiwygEditors	comment	1.1.7	Added Czech language.	2019-05-29 12:11:29
131	AnandPatel.WysiwygEditors	comment	1.1.8	Added Russian language.	2019-05-29 12:11:29
132	AnandPatel.WysiwygEditors	comment	1.1.9	Fix hook and other issues (thanks to Vojta Svoboda).	2019-05-29 12:11:29
133	AnandPatel.WysiwygEditors	comment	1.2.0	Put settings inside CMS sidemenu (thanks to Amanda Tresbach).	2019-05-29 12:11:29
134	AnandPatel.WysiwygEditors	comment	1.2.1	Remove el-finder (Which fix issue of composer), added OC media manager support for image in TinyMCE & CkEditor, TinyMCE version updated, Fix Image upload for froala editor	2019-05-29 12:11:29
135	AnandPatel.WysiwygEditors	comment	1.2.2	Add multilingual support, Add new languages, Update the Hungarian language, Add the missing English language files (Special thanks to Szabó Gergő)	2019-05-29 12:11:29
136	AnandPatel.WysiwygEditors	comment	1.2.3	Added toolbar customization option (Special thanks to Szabó Gergő).	2019-05-29 12:11:29
137	AnandPatel.WysiwygEditors	comment	1.2.4	Added support for Content Plus Plugin & News and Newsletter plugin (thanks to Szabó Gergő)	2019-05-29 12:11:29
138	AnandPatel.WysiwygEditors	comment	1.2.5	Minor improvements and bugfixes.	2019-05-29 12:11:29
139	AnandPatel.WysiwygEditors	comment	1.2.6	Updated the CKEditor and TinyMCE editors.	2019-05-29 12:11:29
140	AnandPatel.WysiwygEditors	comment	1.2.7	Show locale switcher when using multilocale editor.	2019-05-29 12:11:29
141	AnandPatel.WysiwygEditors	comment	1.2.8	Added French language	2019-05-29 12:11:29
142	AnandPatel.WysiwygEditors	comment	1.2.9	Added permission for preferences	2019-05-29 12:11:29
\.


--
-- Name: system_plugin_history_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.system_plugin_history_id_seq', 142, true);


--
-- Data for Name: system_plugin_versions; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.system_plugin_versions (id, code, version, created_at, is_disabled, is_frozen) FROM stdin;
1	October.Demo	1.0.1	2019-05-29 10:40:43	f	f
5	AnandPatel.WysiwygEditors	1.2.9	2019-05-29 12:11:29	f	f
2	RainLab.Blog	1.3.1	2019-05-29 10:52:49	f	f
3	RainLab.Translate	1.6.0	2019-05-29 10:54:12	f	f
4	Excodus.TranslateExtended	1.0.7	2019-05-29 10:54:31	f	f
\.


--
-- Name: system_plugin_versions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.system_plugin_versions_id_seq', 5, true);


--
-- Data for Name: system_request_logs; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.system_request_logs (id, status_code, url, referer, count, created_at, updated_at) FROM stdin;
\.


--
-- Name: system_request_logs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.system_request_logs_id_seq', 1, false);


--
-- Data for Name: system_revisions; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.system_revisions (id, user_id, field, "cast", old_value, new_value, revisionable_type, revisionable_id, created_at, updated_at) FROM stdin;
\.


--
-- Name: system_revisions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.system_revisions_id_seq', 1, false);


--
-- Data for Name: system_settings; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.system_settings (id, item, value) FROM stdin;
1	anandpatel_wysiwygeditors_settings	{"editor":"richeditor","editor_width":"","editor_height":"","toolbar_tinymce":"undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media ocmediamanager","toolbar_ckeditor":"['Undo', 'Redo'], ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord'], ['Format', 'FontSize'], ['ShowBlocks', 'SelectAll', 'RemoveFormat'], ['Source'], ['Maximize'], '\\/', ['Bold', 'Italic', 'Underline', 'Strike'], ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'], ['BulletedList', 'NumberedList', 'Outdent', 'Indent'], ['TextColor', 'BGColor'], ['Link', 'Unlink', 'Anchor'], ['Image', 'Table', 'oembed', 'SpecialChar', 'OcMediaManager']","cms_page_as_wysiwyg":"1","cms_content_as_wysiwyg":"1","cms_partial_as_wysiwyg":"1","cms_layout_as_wysiwyg":"1","others_as_wysiwyg":"1","blog_as_wysiwyg":"1"}
2	excodus_translateextended_settings	{"browser_language_detection":"1","prefer_user_session":"1","route_prefixing":"0","homepage_redirect":"0"}
\.


--
-- Name: system_settings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.system_settings_id_seq', 2, true);


--
-- Name: backend_access_log_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.backend_access_log
    ADD CONSTRAINT backend_access_log_pkey PRIMARY KEY (id);


--
-- Name: backend_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.backend_user_groups
    ADD CONSTRAINT backend_user_groups_pkey PRIMARY KEY (id);


--
-- Name: backend_user_preferences_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.backend_user_preferences
    ADD CONSTRAINT backend_user_preferences_pkey PRIMARY KEY (id);


--
-- Name: backend_user_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.backend_user_roles
    ADD CONSTRAINT backend_user_roles_pkey PRIMARY KEY (id);


--
-- Name: backend_user_throttle_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.backend_user_throttle
    ADD CONSTRAINT backend_user_throttle_pkey PRIMARY KEY (id);


--
-- Name: backend_users_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.backend_users_groups
    ADD CONSTRAINT backend_users_groups_pkey PRIMARY KEY (user_id, user_group_id);


--
-- Name: backend_users_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.backend_users
    ADD CONSTRAINT backend_users_pkey PRIMARY KEY (id);


--
-- Name: cache_key_unique; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.cache
    ADD CONSTRAINT cache_key_unique UNIQUE (key);


--
-- Name: cms_theme_data_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.cms_theme_data
    ADD CONSTRAINT cms_theme_data_pkey PRIMARY KEY (id);


--
-- Name: cms_theme_logs_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.cms_theme_logs
    ADD CONSTRAINT cms_theme_logs_pkey PRIMARY KEY (id);


--
-- Name: deferred_bindings_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.deferred_bindings
    ADD CONSTRAINT deferred_bindings_pkey PRIMARY KEY (id);


--
-- Name: email_unique; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.backend_users
    ADD CONSTRAINT email_unique UNIQUE (email);


--
-- Name: failed_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.failed_jobs
    ADD CONSTRAINT failed_jobs_pkey PRIMARY KEY (id);


--
-- Name: jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.jobs
    ADD CONSTRAINT jobs_pkey PRIMARY KEY (id);


--
-- Name: login_unique; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.backend_users
    ADD CONSTRAINT login_unique UNIQUE (login);


--
-- Name: migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: name_unique; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.backend_user_groups
    ADD CONSTRAINT name_unique UNIQUE (name);


--
-- Name: rainlab_blog_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.rainlab_blog_categories
    ADD CONSTRAINT rainlab_blog_categories_pkey PRIMARY KEY (id);


--
-- Name: rainlab_blog_posts_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.rainlab_blog_posts_categories
    ADD CONSTRAINT rainlab_blog_posts_categories_pkey PRIMARY KEY (post_id, category_id);


--
-- Name: rainlab_blog_posts_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.rainlab_blog_posts
    ADD CONSTRAINT rainlab_blog_posts_pkey PRIMARY KEY (id);


--
-- Name: rainlab_translate_attributes_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.rainlab_translate_attributes
    ADD CONSTRAINT rainlab_translate_attributes_pkey PRIMARY KEY (id);


--
-- Name: rainlab_translate_indexes_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.rainlab_translate_indexes
    ADD CONSTRAINT rainlab_translate_indexes_pkey PRIMARY KEY (id);


--
-- Name: rainlab_translate_locales_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.rainlab_translate_locales
    ADD CONSTRAINT rainlab_translate_locales_pkey PRIMARY KEY (id);


--
-- Name: rainlab_translate_messages_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.rainlab_translate_messages
    ADD CONSTRAINT rainlab_translate_messages_pkey PRIMARY KEY (id);


--
-- Name: role_unique; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.backend_user_roles
    ADD CONSTRAINT role_unique UNIQUE (name);


--
-- Name: sessions_id_unique; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.sessions
    ADD CONSTRAINT sessions_id_unique UNIQUE (id);


--
-- Name: system_event_logs_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.system_event_logs
    ADD CONSTRAINT system_event_logs_pkey PRIMARY KEY (id);


--
-- Name: system_files_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.system_files
    ADD CONSTRAINT system_files_pkey PRIMARY KEY (id);


--
-- Name: system_mail_layouts_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.system_mail_layouts
    ADD CONSTRAINT system_mail_layouts_pkey PRIMARY KEY (id);


--
-- Name: system_mail_partials_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.system_mail_partials
    ADD CONSTRAINT system_mail_partials_pkey PRIMARY KEY (id);


--
-- Name: system_mail_templates_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.system_mail_templates
    ADD CONSTRAINT system_mail_templates_pkey PRIMARY KEY (id);


--
-- Name: system_parameters_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.system_parameters
    ADD CONSTRAINT system_parameters_pkey PRIMARY KEY (id);


--
-- Name: system_plugin_history_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.system_plugin_history
    ADD CONSTRAINT system_plugin_history_pkey PRIMARY KEY (id);


--
-- Name: system_plugin_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.system_plugin_versions
    ADD CONSTRAINT system_plugin_versions_pkey PRIMARY KEY (id);


--
-- Name: system_request_logs_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.system_request_logs
    ADD CONSTRAINT system_request_logs_pkey PRIMARY KEY (id);


--
-- Name: system_revisions_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.system_revisions
    ADD CONSTRAINT system_revisions_pkey PRIMARY KEY (id);


--
-- Name: system_settings_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.system_settings
    ADD CONSTRAINT system_settings_pkey PRIMARY KEY (id);


--
-- Name: act_code_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX act_code_index ON public.backend_users USING btree (activation_code);


--
-- Name: admin_role_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX admin_role_index ON public.backend_users USING btree (role_id);


--
-- Name: backend_user_throttle_ip_address_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX backend_user_throttle_ip_address_index ON public.backend_user_throttle USING btree (ip_address);


--
-- Name: backend_user_throttle_user_id_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX backend_user_throttle_user_id_index ON public.backend_user_throttle USING btree (user_id);


--
-- Name: cms_theme_data_theme_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX cms_theme_data_theme_index ON public.cms_theme_data USING btree (theme);


--
-- Name: cms_theme_logs_theme_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX cms_theme_logs_theme_index ON public.cms_theme_logs USING btree (theme);


--
-- Name: cms_theme_logs_type_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX cms_theme_logs_type_index ON public.cms_theme_logs USING btree (type);


--
-- Name: cms_theme_logs_user_id_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX cms_theme_logs_user_id_index ON public.cms_theme_logs USING btree (user_id);


--
-- Name: code_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX code_index ON public.backend_user_groups USING btree (code);


--
-- Name: deferred_bindings_master_field_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX deferred_bindings_master_field_index ON public.deferred_bindings USING btree (master_field);


--
-- Name: deferred_bindings_master_type_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX deferred_bindings_master_type_index ON public.deferred_bindings USING btree (master_type);


--
-- Name: deferred_bindings_session_key_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX deferred_bindings_session_key_index ON public.deferred_bindings USING btree (session_key);


--
-- Name: deferred_bindings_slave_id_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX deferred_bindings_slave_id_index ON public.deferred_bindings USING btree (slave_id);


--
-- Name: deferred_bindings_slave_type_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX deferred_bindings_slave_type_index ON public.deferred_bindings USING btree (slave_type);


--
-- Name: item_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX item_index ON public.system_parameters USING btree (namespace, "group", item);


--
-- Name: jobs_queue_reserved_at_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX jobs_queue_reserved_at_index ON public.jobs USING btree (queue, reserved_at);


--
-- Name: rainlab_blog_categories_parent_id_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX rainlab_blog_categories_parent_id_index ON public.rainlab_blog_categories USING btree (parent_id);


--
-- Name: rainlab_blog_categories_slug_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX rainlab_blog_categories_slug_index ON public.rainlab_blog_categories USING btree (slug);


--
-- Name: rainlab_blog_posts_slug_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX rainlab_blog_posts_slug_index ON public.rainlab_blog_posts USING btree (slug);


--
-- Name: rainlab_blog_posts_user_id_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX rainlab_blog_posts_user_id_index ON public.rainlab_blog_posts USING btree (user_id);


--
-- Name: rainlab_translate_attributes_locale_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX rainlab_translate_attributes_locale_index ON public.rainlab_translate_attributes USING btree (locale);


--
-- Name: rainlab_translate_attributes_model_id_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX rainlab_translate_attributes_model_id_index ON public.rainlab_translate_attributes USING btree (model_id);


--
-- Name: rainlab_translate_attributes_model_type_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX rainlab_translate_attributes_model_type_index ON public.rainlab_translate_attributes USING btree (model_type);


--
-- Name: rainlab_translate_indexes_item_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX rainlab_translate_indexes_item_index ON public.rainlab_translate_indexes USING btree (item);


--
-- Name: rainlab_translate_indexes_locale_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX rainlab_translate_indexes_locale_index ON public.rainlab_translate_indexes USING btree (locale);


--
-- Name: rainlab_translate_indexes_model_id_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX rainlab_translate_indexes_model_id_index ON public.rainlab_translate_indexes USING btree (model_id);


--
-- Name: rainlab_translate_indexes_model_type_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX rainlab_translate_indexes_model_type_index ON public.rainlab_translate_indexes USING btree (model_type);


--
-- Name: rainlab_translate_locales_code_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX rainlab_translate_locales_code_index ON public.rainlab_translate_locales USING btree (code);


--
-- Name: rainlab_translate_locales_name_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX rainlab_translate_locales_name_index ON public.rainlab_translate_locales USING btree (name);


--
-- Name: rainlab_translate_messages_code_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX rainlab_translate_messages_code_index ON public.rainlab_translate_messages USING btree (code);


--
-- Name: reset_code_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX reset_code_index ON public.backend_users USING btree (reset_password_code);


--
-- Name: role_code_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX role_code_index ON public.backend_user_roles USING btree (code);


--
-- Name: system_event_logs_level_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX system_event_logs_level_index ON public.system_event_logs USING btree (level);


--
-- Name: system_files_attachment_id_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX system_files_attachment_id_index ON public.system_files USING btree (attachment_id);


--
-- Name: system_files_attachment_type_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX system_files_attachment_type_index ON public.system_files USING btree (attachment_type);


--
-- Name: system_files_field_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX system_files_field_index ON public.system_files USING btree (field);


--
-- Name: system_mail_templates_layout_id_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX system_mail_templates_layout_id_index ON public.system_mail_templates USING btree (layout_id);


--
-- Name: system_plugin_history_code_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX system_plugin_history_code_index ON public.system_plugin_history USING btree (code);


--
-- Name: system_plugin_history_type_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX system_plugin_history_type_index ON public.system_plugin_history USING btree (type);


--
-- Name: system_plugin_versions_code_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX system_plugin_versions_code_index ON public.system_plugin_versions USING btree (code);


--
-- Name: system_revisions_field_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX system_revisions_field_index ON public.system_revisions USING btree (field);


--
-- Name: system_revisions_revisionable_id_revisionable_type_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX system_revisions_revisionable_id_revisionable_type_index ON public.system_revisions USING btree (revisionable_id, revisionable_type);


--
-- Name: system_revisions_user_id_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX system_revisions_user_id_index ON public.system_revisions USING btree (user_id);


--
-- Name: system_settings_item_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX system_settings_item_index ON public.system_settings USING btree (item);


--
-- Name: user_item_index; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX user_item_index ON public.backend_user_preferences USING btree (user_id, namespace, "group", item);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

