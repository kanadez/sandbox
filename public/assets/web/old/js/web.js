(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["/js/web"],{

/***/ "./modules/Landing/Resources/Assets/js/web.js":
/*!****************************************************!*\
  !*** ./modules/Landing/Resources/Assets/js/web.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ./web/bootstrap */ "./modules/Landing/Resources/Assets/js/web/bootstrap.js");

__webpack_require__(/*! ./../pRESIDENTPROXY/js/main */ "./modules/Landing/Resources/Assets/pRESIDENTPROXY/js/main.js");

__webpack_require__(/*! ./web/custom */ "./modules/Landing/Resources/Assets/js/web/custom.js");

/***/ }),

/***/ "./modules/Landing/Resources/Assets/js/web/bootstrap.js":
/*!**************************************************************!*\
  !*** ./modules/Landing/Resources/Assets/js/web/bootstrap.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

window.Popper = __webpack_require__(/*! popper.js */ "./node_modules/popper.js/dist/esm/popper.js")["default"];
/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
  window.$ = window.jQuery = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  __webpack_require__(/*! bootstrap */ "./node_modules/bootstrap/dist/js/bootstrap.js");
} catch (e) {}

__webpack_require__(/*! slick-carousel */ "./node_modules/slick-carousel/slick/slick.js");

/***/ }),

/***/ "./modules/Landing/Resources/Assets/js/web/custom.js":
/*!***********************************************************!*\
  !*** ./modules/Landing/Resources/Assets/js/web/custom.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

$(document).ready(function () {
  // Ajax form submit
  $("#create-proxy-request").submit(function (e) {
    var self = $(this);
    $.ajax({
      type: "POST",
      url: self.attr('action'),
      data: self.serialize(),
      beforeSend: function beforeSend() {
        $('.modal-body .alert', self).remove();
      },
      success: function success(data) {
        $('#exampleModalLong .modal-header h5').text("Thank you for request!");
        $('#exampleModalLong .modal-body').empty();
        $('#exampleModalLong .modal-footer').remove();
      },
      error: function error(xhr) {
        grecaptcha.reset();
        var data = xhr.responseJSON;

        for (var err in data.errors) {
          if (err === "email" && data.errors[err].indexOf("taken") > -1) {
            $('#exampleModalLong').modal('toggle');
            $('#takenError').modal('toggle');
          } // // find input
          // let input = $('#' + err).after('<div class="alert alert-danger">\n' +
          //     data.errors[err] + '\n' +
          //     '<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>\n' +
          //     '</div>');
          // propertyName is what you want
          // you can get the value like this: myObject[propertyName]

        }
      }
    });
    e.preventDefault(); // avoid to execute the actual submit of the form.
  });
});

/***/ }),

/***/ "./modules/Landing/Resources/Assets/pRESIDENTPROXY/js/main.js":
/*!********************************************************************!*\
  !*** ./modules/Landing/Resources/Assets/pRESIDENTPROXY/js/main.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

$(document).ready(function () {
  $(".slider-photo").slick({
    centerMode: true,
    slidesToShow: 3,
    asNavFor: ".slider-text",
    nextArrow: '<button type="button" class="slick-next"></button>',
    prevArrow: '<button type="button" class="slick-prev"></button>',
    responsive: [{
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        arrows: false
      }
    }]
  });
  $(".slider-text").slick({
    centerMode: true,
    slidesToShow: 1,
    asNavFor: ".slider-photo",
    arrows: false
  });
  $(".partners__slider").slick({
    slidesToShow: 6,
    variableWidth: true,
    nextArrow: '<button type="button" class="slick-next"></button>',
    prevArrow: '<button type="button" class="slick-prev"></button>',
    responsive: [{
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        variableWidth: false
      }
    }]
  }); // $(".faq-ul__li").each(function(a) {
  //     $(this).click(function(b) {
  //         b.preventDefault();
  //         $(".faq-ul__desk").fadeOut(0);
  //         $(this).find(".faq-ul__desk").fadeIn()
  //     }
  //     )
  // }
  // )
});
$(document).ready(function () {
  $(".header__nav").on("click", "a.scroll-link", function (event) {
    //отменяем стандартную обработку нажатия по ссылке
    event.preventDefault(); //забираем идентификатор бока с атрибута href

    var id = $(this).attr('href'),
        //узнаем высоту от начала страницы до блока на который ссылается якорь
    top = $(id).offset().top; //анимируем переход на расстояние - top за 1500 мс

    $('body,html').animate({
      scrollTop: top - 100
    }, 1500);
  });
});
$(".cat-go").mouseover(function () {
  if (!$(".cat-go__img").hasClass("active")) {
    $(".cat-go__img").addClass("active");
    setTimeout(function () {
      $(".cat-go__img").removeClass("active");
    }, 4000);
  }
});

/***/ }),

/***/ "./modules/Landing/Resources/Assets/sass/web.scss":
/*!********************************************************!*\
  !*** ./modules/Landing/Resources/Assets/sass/web.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "./node_modules/webpack/buildin/global.js":
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || new Function("return this")();
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),

/***/ 0:
/*!***********************************************************************************************************!*\
  !*** multi ./modules/Landing/Resources/Assets/js/web.js ./modules/Landing/Resources/Assets/sass/web.scss ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /home/alveri/workflow/astroproxy/dashboard/www/modules/Landing/Resources/Assets/js/web.js */"./modules/Landing/Resources/Assets/js/web.js");
module.exports = __webpack_require__(/*! /home/alveri/workflow/astroproxy/dashboard/www/modules/Landing/Resources/Assets/sass/web.scss */"./modules/Landing/Resources/Assets/sass/web.scss");


/***/ })

},[[0,"/js/manifest","/js/vendor"]]]);