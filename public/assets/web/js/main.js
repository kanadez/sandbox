
$(document).ready(function(){
    $(".header__nav").on("click","a.scroll-link", function (event) {
        //отменяем стандартную обработку нажатия по ссылке
        event.preventDefault();

        //забираем идентификатор бока с атрибута href
        var id  = $(this).attr('href'),

        //узнаем высоту от начала страницы до блока на который ссылается якорь
                top = $(id).offset().top;

        //анимируем переход на расстояние - top за 1500 мс
        $('body,html').animate({scrollTop: top-100}, 1500);
    });

    $(".cat-go").mouseover(function() {
    
        if (!$(".cat-go__img").hasClass("active")) {

            $(".cat-go__img").addClass("active");
            
            setTimeout(function() {
                $(".cat-go__img").removeClass("active");
            }, 4000);    
        }

    });

    $('.forums-slider').slick({
        arrows: true,
        dots: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        fade: true,
    });

    $(".faq-ul__li").each(function(a) {
        $(this).click(function(b) {
            b.preventDefault();
            $(".faq-ul__desc").fadeOut(0);
            $(this).find(".faq-ul__desc").fadeIn()
        })
    })

    $('.go-to__signup').click(function(){
        $('.modal').modal("hide");
        $('#exampleModalLong').modal("show");
    });
    
    $('.go-to__signin').click(function(){
        $('.modal').modal("hide");
        $('#login_modal').modal("show");
    });
    
    $('.go-to__forgot').click(function(){
        $('.modal').modal("hide");
        $('#forgot_modal').modal("show");
    });
    
    openPasswordResetIfNeeded();
    openSigninIfNeeded();
    openSignupIfNeeded();
    
    $("#create-proxy-request").submit(function (e) {
        var self = $(this);
        
        utils.setCatLoading('#exampleModalLong .modal-footer button'); 
        
        $.ajax({
            type: "POST",
            url: self.attr('action'),
            data: self.serialize(),
            beforeSend: function beforeSend() {
                $('.modal-body .alert', self).hide();
            },
            success: function success(data) {
                //$('#exampleModalLong .modal-header h5').text("Thank you for request!");
                $('#exampleModalLong .modal-body *').hide();
                $("#exampleModalLong .alert-success").show();
                $('#exampleModalLong .modal-footer').remove();
                utils.unsetCatLoading('#exampleModalLong .modal-footer button'); 
            },
            error: function error(xhr) {
                //grecaptcha.reset();
                var data = xhr.responseJSON;
                
                for (var err in data.errors) {
                    $("#exampleModalLong .alert-danger").text(data.errors[err].pop());
                    $("#exampleModalLong .alert-danger").show();
                }
                
                utils.unsetCatLoading('#exampleModalLong .modal-footer button'); 
            }
        });

        e.preventDefault(); // avoid to execute the actual submit of the form.
    });
    
    $("#login_modal form").submit(function (e) {
        var self = $(this);
        
        utils.setCatLoading('#login_modal .modal-footer button'); 
       
        $.ajax({
            type: "POST",
            url: self.attr('action'),
            data: self.serialize(),
            beforeSend: function beforeSend() {
                $('.modal-body .alert', self).hide();
            },
            success: function success(data) {
                if (data.status == "success"){
                    location.href = data.redirect;
                }
            },
            error: function error(xhr) {
                var data = xhr.responseJSON;
                
                for (var err in data.errors) {
                    $("#login_modal .alert-danger").text(data.errors[err]);
                    $("#login_modal .alert-danger").show();
                }
                
                utils.unsetCatLoading('#login_modal .modal-footer button');
            }
        });

        e.preventDefault(); // avoid to execute the actual submit of the form.
    });
    
    $("#forgot_modal form").submit(function (e) {
        var self = $(this);
        
        utils.setCatLoading('#forgot_modal .modal-footer button'); 
       
        $.ajax({
            type: "POST",
            url: self.attr('action'),
            data: self.serialize(),
            beforeSend: function beforeSend() {
                $('.modal-body .alert', self).hide();
            },
            success: function success(data) {
                $("#forgot_modal .alert-success").show();
                utils.unsetCatLoading('#forgot_modal .modal-footer button');
            },
            error: function error(xhr) {
                var data = xhr.responseJSON;
                
                for (var err in data.errors) {
                    $("#forgot_modal .alert-danger").text(data.errors[err]);
                    $("#forgot_modal .alert-danger").show();
                }
                
                utils.unsetCatLoading('#forgot_modal .modal-footer button');
            }
        });

        e.preventDefault(); // avoid to execute the actual submit of the form.
    });
    
    $("#reset_modal form").submit(function (e) {
        var self = $(this);
        
        utils.setCatLoading('#reset_modal .modal-footer button'); 
       
        $.ajax({
            type: "POST",
            url: self.attr('action'),
            data: self.serialize(),
            beforeSend: function beforeSend() {
                $('.modal-body .alert', self).hide();
            },
            success: function success(data) {
                location.href = "/dashboard";
            },
            error: function error(xhr) {
                var data = xhr.responseJSON;
                
                for (var err in data.errors) {
                    $("#reset_modal .alert-danger").text(data.errors[err]);
                    $("#reset_modal .alert-danger").show();
                }
                
                utils.unsetCatLoading('#reset_modal .modal-footer button');
            }
        });

        e.preventDefault(); // avoid to execute the actual submit of the form.
    });

});

function openPasswordResetIfNeeded(){
    if (utils.isStringContains(location.pathname, "password/reset")){
        var reset_token = location.pathname.split("/").pop();
        $('#reset_modal input[name="token"]').val(reset_token);
        $('#reset_modal').modal("show");
    }
}

function openSigninIfNeeded(){
    if (utils.isStringContains(location.pathname, "signin")){
        $('#login_modal').modal("show");
    }
}

function openSignupIfNeeded(){
    if (utils.isStringContains(location.pathname, "signup")){
        $('#exampleModalLong').modal("show");
    }
}