(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[0],{

/***/ "./modules/App/Resources/Assets/js/app/components/ArticleActions.vue":
/*!***************************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/components/ArticleActions.vue ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ArticleActions_vue_vue_type_template_id_72a8344c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ArticleActions.vue?vue&type=template&id=72a8344c& */ "./modules/App/Resources/Assets/js/app/components/ArticleActions.vue?vue&type=template&id=72a8344c&");
/* harmony import */ var _ArticleActions_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ArticleActions.vue?vue&type=script&lang=js& */ "./modules/App/Resources/Assets/js/app/components/ArticleActions.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ArticleActions_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ArticleActions_vue_vue_type_template_id_72a8344c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ArticleActions_vue_vue_type_template_id_72a8344c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "modules/App/Resources/Assets/js/app/components/ArticleActions.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/components/ArticleActions.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/components/ArticleActions.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleActions_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./ArticleActions.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/ArticleActions.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleActions_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/components/ArticleActions.vue?vue&type=template&id=72a8344c&":
/*!**********************************************************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/components/ArticleActions.vue?vue&type=template&id=72a8344c& ***!
  \**********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleActions_vue_vue_type_template_id_72a8344c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./ArticleActions.vue?vue&type=template&id=72a8344c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/ArticleActions.vue?vue&type=template&id=72a8344c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleActions_vue_vue_type_template_id_72a8344c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleActions_vue_vue_type_template_id_72a8344c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/components/ArticleMeta.vue":
/*!************************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/components/ArticleMeta.vue ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ArticleMeta_vue_vue_type_template_id_2a4bcb78___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ArticleMeta.vue?vue&type=template&id=2a4bcb78& */ "./modules/App/Resources/Assets/js/app/components/ArticleMeta.vue?vue&type=template&id=2a4bcb78&");
/* harmony import */ var _ArticleMeta_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ArticleMeta.vue?vue&type=script&lang=js& */ "./modules/App/Resources/Assets/js/app/components/ArticleMeta.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ArticleMeta_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ArticleMeta_vue_vue_type_template_id_2a4bcb78___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ArticleMeta_vue_vue_type_template_id_2a4bcb78___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "modules/App/Resources/Assets/js/app/components/ArticleMeta.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/components/ArticleMeta.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/components/ArticleMeta.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleMeta_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./ArticleMeta.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/ArticleMeta.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleMeta_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/components/ArticleMeta.vue?vue&type=template&id=2a4bcb78&":
/*!*******************************************************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/components/ArticleMeta.vue?vue&type=template&id=2a4bcb78& ***!
  \*******************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleMeta_vue_vue_type_template_id_2a4bcb78___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./ArticleMeta.vue?vue&type=template&id=2a4bcb78& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/ArticleMeta.vue?vue&type=template&id=2a4bcb78&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleMeta_vue_vue_type_template_id_2a4bcb78___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleMeta_vue_vue_type_template_id_2a4bcb78___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/ArticleActions.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./modules/App/Resources/Assets/js/app/components/ArticleActions.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var _store_actions_type__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../store/actions.type */ "./modules/App/Resources/Assets/js/app/store/actions.type.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "RwvArticleActions",
  props: {
    article: {
      type: Object,
      required: true
    },
    canModify: {
      type: Boolean,
      required: true
    }
  },
  computed: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapGetters"])(["profile", "isAuthenticated"])), {}, {
    editArticleLink: function editArticleLink() {
      return {
        name: "article-edit",
        params: {
          slug: this.article.slug
        }
      };
    },
    toggleFavoriteButtonClasses: function toggleFavoriteButtonClasses() {
      return {
        "btn-primary": this.article.favorited,
        "btn-outline-primary": !this.article.favorited
      };
    },
    followUserLabel: function followUserLabel() {
      return "".concat(this.profile.following ? "Unfollow" : "Follow", " ").concat(this.article.author.username);
    },
    favoriteArticleLabel: function favoriteArticleLabel() {
      return this.article.favorited ? "Unfavorite Article" : "Favorite Article";
    },
    favoriteCounter: function favoriteCounter() {
      return "(".concat(this.article.favoritesCount, ")");
    }
  }),
  methods: {
    toggleFavorite: function toggleFavorite() {
      if (!this.isAuthenticated) {
        this.$router.push({
          name: "login"
        });
        return;
      }

      var action = this.article.favorited ? _store_actions_type__WEBPACK_IMPORTED_MODULE_2__["FAVORITE_REMOVE"] : _store_actions_type__WEBPACK_IMPORTED_MODULE_2__["FAVORITE_ADD"];
      this.$store.dispatch(action, this.article.slug);
    },
    toggleFollow: function toggleFollow() {
      if (!this.isAuthenticated) {
        this.$router.push({
          name: "login"
        });
        return;
      }

      var action = this.article.following ? _store_actions_type__WEBPACK_IMPORTED_MODULE_2__["FETCH_PROFILE_UNFOLLOW"] : _store_actions_type__WEBPACK_IMPORTED_MODULE_2__["FETCH_PROFILE_FOLLOW"];
      this.$store.dispatch(action, {
        username: this.profile.username
      });
    },
    deleteArticle: function deleteArticle() {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return _this.$store.dispatch(_store_actions_type__WEBPACK_IMPORTED_MODULE_2__["ARTICLE_DELETE"], _this.article.slug);

              case 3:
                _this.$router.push("/");

                _context.next = 9;
                break;

              case 6:
                _context.prev = 6;
                _context.t0 = _context["catch"](0);
                console.error(_context.t0);

              case 9:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 6]]);
      }))();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/ArticleMeta.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./modules/App/Resources/Assets/js/app/components/ArticleMeta.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var _ArticleActions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ArticleActions */ "./modules/App/Resources/Assets/js/app/components/ArticleActions.vue");
/* harmony import */ var _store_actions_type__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../store/actions.type */ "./modules/App/Resources/Assets/js/app/store/actions.type.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: "RwvArticleMeta",
  components: {
    RwvArticleActions: _ArticleActions__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  props: {
    article: {
      type: Object,
      required: true
    },
    actions: {
      type: Boolean,
      required: false,
      "default": false
    }
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])(["currentUser", "isAuthenticated"])),
  methods: {
    isCurrentUser: function isCurrentUser() {
      if (this.currentUser.username && this.article.author.username) {
        return this.currentUser.username === this.article.author.username;
      }

      return false;
    },
    toggleFavorite: function toggleFavorite() {
      if (!this.isAuthenticated) {
        this.$router.push({
          name: "login"
        });
        return;
      }

      var action = this.article.favorited ? _store_actions_type__WEBPACK_IMPORTED_MODULE_2__["FAVORITE_REMOVE"] : _store_actions_type__WEBPACK_IMPORTED_MODULE_2__["FAVORITE_ADD"];
      this.$store.dispatch(action, this.article.slug);
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/ArticleActions.vue?vue&type=template&id=72a8344c&":
/*!****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./modules/App/Resources/Assets/js/app/components/ArticleActions.vue?vue&type=template&id=72a8344c& ***!
  \****************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.canModify
    ? _c(
        "span",
        [
          _c(
            "router-link",
            {
              staticClass: "btn btn-sm btn-outline-secondary",
              attrs: { to: _vm.editArticleLink }
            },
            [
              _c("i", { staticClass: "ion-edit" }),
              _vm._v(" "),
              _c("span", [_vm._v(" Edit Article")])
            ]
          ),
          _vm._v(" "),
          _c("span", [_vm._v("  ")]),
          _vm._v(" "),
          _c(
            "button",
            {
              staticClass: "btn btn-outline-danger btn-sm",
              on: { click: _vm.deleteArticle }
            },
            [
              _c("i", { staticClass: "ion-trash-a" }),
              _vm._v(" "),
              _c("span", [_vm._v(" Delete Article")])
            ]
          )
        ],
        1
      )
    : _c("span", [
        _c(
          "button",
          {
            staticClass: "btn btn-sm btn-outline-secondary",
            on: { click: _vm.toggleFollow }
          },
          [
            _c("i", { staticClass: "ion-plus-round" }),
            _vm._v(" "),
            _c("span", [_vm._v(" ")]),
            _vm._v(" "),
            _c("span", {
              domProps: { textContent: _vm._s(_vm.followUserLabel) }
            })
          ]
        ),
        _vm._v(" "),
        _c("span", [_vm._v("  ")]),
        _vm._v(" "),
        _c(
          "button",
          {
            staticClass: "btn btn-sm",
            class: _vm.toggleFavoriteButtonClasses,
            on: { click: _vm.toggleFavorite }
          },
          [
            _c("i", { staticClass: "ion-heart" }),
            _vm._v(" "),
            _c("span", [_vm._v(" ")]),
            _vm._v(" "),
            _c("span", {
              domProps: { textContent: _vm._s(_vm.favoriteArticleLabel) }
            }),
            _vm._v(" "),
            _c("span", [_vm._v(" ")]),
            _vm._v(" "),
            _c("span", {
              staticClass: "counter",
              domProps: { textContent: _vm._s(_vm.favoriteCounter) }
            })
          ]
        )
      ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/ArticleMeta.vue?vue&type=template&id=2a4bcb78&":
/*!*************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./modules/App/Resources/Assets/js/app/components/ArticleMeta.vue?vue&type=template&id=2a4bcb78& ***!
  \*************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "article-meta" },
    [
      _c(
        "router-link",
        {
          attrs: {
            to: {
              name: "profile",
              params: { username: _vm.article.author.username }
            }
          }
        },
        [_c("img", { attrs: { src: _vm.article.author.image } })]
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "info" },
        [
          _c(
            "router-link",
            {
              staticClass: "author",
              attrs: {
                to: {
                  name: "profile",
                  params: { username: _vm.article.author.username }
                }
              }
            },
            [
              _vm._v(
                "\n      " + _vm._s(_vm.article.author.username) + "\n    "
              )
            ]
          ),
          _vm._v(" "),
          _c("span", { staticClass: "date" }, [
            _vm._v(_vm._s(_vm._f("date")(_vm.article.createdAt)))
          ])
        ],
        1
      ),
      _vm._v(" "),
      _vm.actions
        ? _c("rwv-article-actions", {
            attrs: { article: _vm.article, canModify: _vm.isCurrentUser() }
          })
        : _c(
            "button",
            {
              staticClass: "btn btn-sm pull-xs-right",
              class: {
                "btn-primary": _vm.article.favorited,
                "btn-outline-primary": !_vm.article.favorited
              },
              on: { click: _vm.toggleFavorite }
            },
            [
              _c("i", { staticClass: "ion-heart" }),
              _vm._v(" "),
              _c("span", { staticClass: "counter" }, [
                _vm._v(" " + _vm._s(_vm.article.favoritesCount) + " ")
              ])
            ]
          )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);