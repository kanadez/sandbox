let app = {
    mix: require('laravel-mix'),
    modules: require('./storage/app/modules.json'),
    fs: require('fs'),
    scripts: [],
    styles: [],
    publicPath: "public/assets/app/",
    resourceRoot: "/assets/app/",
};

// Sort modules by order field
app.modules =
    Object
        .keys(app.modules)
        .map(function(key) {
            return app.modules[key];
        })
        .sort(function (a, b) {
            return a.order < b.order ? -1 : a.order > b.order ? 1 : 0;
        });

app.mix.setPublicPath(app.publicPath);
app.mix.setResourceRoot(app.resourceRoot);

app.modules.forEach(function (module) {
    //build modules
    
    module.paths = {};
    module.paths.base = 'modules/' + module.name + '/';
    module.paths.mix_file = './' + module.paths.base + 'app.mix.js';
    module.paths.assets = module.paths.base + 'Resources/Assets/';
    module.paths.fonts = module.paths.assets + 'fonts/';
    module.paths.js = module.paths.assets + 'js/';
    module.paths.css = module.paths.assets + 'css/';
    module.paths.img = module.paths.assets + 'img/';
    module.paths.sass = module.paths.assets + 'sass/';
    module.paths.styl = module.paths.assets + 'styl/';
    
    // Build js
    if (app.fs.existsSync(module.paths.js + 'app.js')) {
        app.scripts.push(module.paths.js + 'app.js')
    }


    // Build css
    if (app.fs.existsSync(module.paths.sass + 'app.scss')) {
        app.mix.sass(module.paths.sass + 'app.scss', 'css/' + module.name + '.css');
        app.styles.push("public/assets/app/css/" + module.name + '.css');
    }

    // Extra build options
    if (app.fs.existsSync(module.paths.mix_file)) {
        require(module.paths.mix_file)(app, module);
    }
});

// All in one
app.mix
    .js(app.scripts, 'js/app.js')
    .styles(app.styles, 'public/assets/app/css/app.css')
    .version();