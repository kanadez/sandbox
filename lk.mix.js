let mix = require('laravel-mix'),
    modules = require('./storage/app/modules.json'),
    fs = require('fs');

// Sort modules by order field
modules =
    Object
        .keys(modules)
        .map(function(key) {
            return modules[key];
        })
        .sort(function (a, b) {
            return a.order < b.order ? -1 : a.order > b.order ? 1 : 0;
        });

let scripts = [],
    styles = [];

mix.setPublicPath("public/assets/lk");
mix.setResourceRoot('/assets/lk/');

modules.forEach(function (module) {
    //build modules
    
    module.paths = {};
    module.paths.base = 'modules/' + module.name + '/';
    module.paths.mix_file = './' + module.paths.base + 'lk.mix.js';
    module.paths.assets = module.paths.base + 'Resources/Assets/';
    module.paths.fonts = module.paths.assets + 'fonts/';
    module.paths.js = module.paths.assets + 'js/';
    module.paths.css = module.paths.assets + 'css/';
    module.paths.sass = module.paths.assets + 'sass/';
    module.paths.styl = module.paths.assets + 'styl/';

    // Build js
    if (fs.existsSync(module.paths.js + 'lk.js')) {
        scripts.push(module.paths.js + 'lk.js')
    }

    // Build css
    if (fs.existsSync(module.paths.sass + 'lk.scss')) {
        mix.sass(module.paths.sass + 'lk.scss', 'css/' + module.name + '.css');
        styles.push("public/assets/lk/css/" + module.name + '.css');
    }

    // Extra build options
    if (fs.existsSync(module.paths.mix_file)) {
        require(module.paths.mix_file)(mix, module);
    }
});

// All in one
mix.disableSuccessNotifications();
mix
    .js(scripts, 'js/lk.js')
    .styles(styles, 'public/assets/lk/css/lk.css')
    .version();