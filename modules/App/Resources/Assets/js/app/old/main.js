/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// mixins
import {Utils} from './mixins/utils';

window.Vue          = require('vue');
window.axios        = require('axios');

Vue.prototype.$http = window.axios;

window.app = new Vue({
    el: '#search',
    mixins: [Utils],
    data: {
        // consts

        // vars
        urls: {

        },


    },

    computed: {

    },

    watch: {

    },

    mounted () {
        this.init();
    },

    methods: {

        init () {
            alert(123);
        },


    }
});