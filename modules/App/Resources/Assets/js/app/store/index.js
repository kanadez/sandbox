import Vue from "vue";
import Vuex from "vuex";

import home from "./home.module";
import test from "./test.module";
import auth from "./auth.module";
import article from "./article.module";
import profile from "./profile.module";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    home,
    test,
    auth,
    article,
    profile
  }
});
