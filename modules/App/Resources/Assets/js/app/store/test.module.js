import { QuestionsService } from "../common/api.service";
import { FETCH_TEST } from "./actions.type";
import {
  FETCH_TEST_START,
  FETCH_TEST_END,
} from "./mutations.type";

const state = {
  isLoading: true,
};

const getters = {
  
};

const actions = {
  [FETCH_TEST]({ commit }, params) {
    commit(FETCH_TEST_START);
    return QuestionsService.query([])
      .then(({ data }) => {
        commit(FETCH_TEST_END, data);
      })
      .catch(error => {
        throw new Error(error);
      });
  }
};

/* eslint no-param-reassign: ["error", { "props": false }] */
const mutations = {
  [FETCH_TEST_START](state) {
    state.isLoading = true;
  },
  [FETCH_TEST_END](state, { questions, questionsCount }) {
      
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
