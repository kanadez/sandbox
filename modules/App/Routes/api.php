<?php

use Illuminate\Http\Request;
use Illuminate\Routing\Router;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your module. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$router->group([
    'prefix'    => "questions",
    'as'        => 'questions.',
], function (Router $router) {
    $router->get('', 'QuestionsController@getQuestions')
        ->name('');
});
