(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["/js/app"],{

/***/ "./modules/App/Resources/Assets/js/app.js":
/*!************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ./app/main */ "./modules/App/Resources/Assets/js/app/main.js");

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/App.vue":
/*!*****************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/App.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _App_vue_vue_type_template_id_4a20d50a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./App.vue?vue&type=template&id=4a20d50a& */ "./modules/App/Resources/Assets/js/app/App.vue?vue&type=template&id=4a20d50a&");
/* harmony import */ var _App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./App.vue?vue&type=script&lang=js& */ "./modules/App/Resources/Assets/js/app/App.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _App_vue_vue_type_template_id_4a20d50a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _App_vue_vue_type_template_id_4a20d50a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "modules/App/Resources/Assets/js/app/App.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/App.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/App.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./App.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/App.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/App.vue?vue&type=template&id=4a20d50a&":
/*!************************************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/App.vue?vue&type=template&id=4a20d50a& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_4a20d50a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./App.vue?vue&type=template&id=4a20d50a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/App.vue?vue&type=template&id=4a20d50a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_4a20d50a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_4a20d50a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/common/api.service.js":
/*!*******************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/common/api.service.js ***!
  \*******************************************************************/
/*! exports provided: default, TagsService, ArticlesService, CommentsService, FavoriteService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TagsService", function() { return TagsService; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ArticlesService", function() { return ArticlesService; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommentsService", function() { return CommentsService; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FavoriteService", function() { return FavoriteService; });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.common.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var vue_axios__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue-axios */ "./node_modules/vue-axios/dist/vue-axios.min.js");
/* harmony import */ var vue_axios__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(vue_axios__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _jwt_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./jwt.service */ "./modules/App/Resources/Assets/js/app/common/jwt.service.js");
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./config */ "./modules/App/Resources/Assets/js/app/common/config.js");





var ApiService = {
  init: function init() {
    vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(vue_axios__WEBPACK_IMPORTED_MODULE_2___default.a, axios__WEBPACK_IMPORTED_MODULE_1___default.a);
    vue__WEBPACK_IMPORTED_MODULE_0___default.a.axios.defaults.baseURL = _config__WEBPACK_IMPORTED_MODULE_4__["API_URL"];
  },
  setHeader: function setHeader() {
    vue__WEBPACK_IMPORTED_MODULE_0___default.a.axios.defaults.headers.common["Authorization"] = "Token ".concat(_jwt_service__WEBPACK_IMPORTED_MODULE_3__["default"].getToken());
  },
  query: function query(resource, params) {
    return vue__WEBPACK_IMPORTED_MODULE_0___default.a.axios.get(resource, params)["catch"](function (error) {
      throw new Error("[RWV] ApiService ".concat(error));
    });
  },
  get: function get(resource) {
    var slug = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
    return vue__WEBPACK_IMPORTED_MODULE_0___default.a.axios.get("".concat(resource, "/").concat(slug))["catch"](function (error) {
      throw new Error("[RWV] ApiService ".concat(error));
    });
  },
  post: function post(resource, params) {
    return vue__WEBPACK_IMPORTED_MODULE_0___default.a.axios.post("".concat(resource), params);
  },
  update: function update(resource, slug, params) {
    return vue__WEBPACK_IMPORTED_MODULE_0___default.a.axios.put("".concat(resource, "/").concat(slug), params);
  },
  put: function put(resource, params) {
    return vue__WEBPACK_IMPORTED_MODULE_0___default.a.axios.put("".concat(resource), params);
  },
  "delete": function _delete(resource) {
    return vue__WEBPACK_IMPORTED_MODULE_0___default.a.axios["delete"](resource)["catch"](function (error) {
      throw new Error("[RWV] ApiService ".concat(error));
    });
  }
};
/* harmony default export */ __webpack_exports__["default"] = (ApiService);
var TagsService = {
  get: function get() {
    return ApiService.get("tags");
  }
};
var ArticlesService = {
  query: function query(type, params) {
    return ApiService.query("articles" + (type === "feed" ? "/feed" : ""), {
      params: params
    });
  },
  get: function get(slug) {
    return ApiService.get("articles", slug);
  },
  create: function create(params) {
    return ApiService.post("articles", {
      article: params
    });
  },
  update: function update(slug, params) {
    return ApiService.update("articles", slug, {
      article: params
    });
  },
  destroy: function destroy(slug) {
    return ApiService["delete"]("articles/".concat(slug));
  }
};
var CommentsService = {
  get: function get(slug) {
    if (typeof slug !== "string") {
      throw new Error("[RWV] CommentsService.get() article slug required to fetch comments");
    }

    return ApiService.get("articles", "".concat(slug, "/comments"));
  },
  post: function post(slug, payload) {
    return ApiService.post("articles/".concat(slug, "/comments"), {
      comment: {
        body: payload
      }
    });
  },
  destroy: function destroy(slug, commentId) {
    return ApiService["delete"]("articles/".concat(slug, "/comments/").concat(commentId));
  }
};
var FavoriteService = {
  add: function add(slug) {
    return ApiService.post("articles/".concat(slug, "/favorite"));
  },
  remove: function remove(slug) {
    return ApiService["delete"]("articles/".concat(slug, "/favorite"));
  }
};

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/common/config.js":
/*!**************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/common/config.js ***!
  \**************************************************************/
/*! exports provided: API_URL, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "API_URL", function() { return API_URL; });
var API_URL = "https://conduit.productionready.io/api";
/* harmony default export */ __webpack_exports__["default"] = (API_URL);

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/common/date.filter.js":
/*!*******************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/common/date.filter.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var date_fns_format__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! date-fns/format */ "./node_modules/date-fns/esm/format/index.js");

/* harmony default export */ __webpack_exports__["default"] = (function (date) {
  return Object(date_fns_format__WEBPACK_IMPORTED_MODULE_0__["default"])(new Date(date), "MMMM D, YYYY");
});

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/common/error.filter.js":
/*!********************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/common/error.filter.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (function (errorValue) {
  return "".concat(errorValue[0]);
});

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/common/jwt.service.js":
/*!*******************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/common/jwt.service.js ***!
  \*******************************************************************/
/*! exports provided: getToken, saveToken, destroyToken, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getToken", function() { return getToken; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "saveToken", function() { return saveToken; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "destroyToken", function() { return destroyToken; });
var ID_TOKEN_KEY = "id_token";
var getToken = function getToken() {
  return window.localStorage.getItem(ID_TOKEN_KEY);
};
var saveToken = function saveToken(token) {
  window.localStorage.setItem(ID_TOKEN_KEY, token);
};
var destroyToken = function destroyToken() {
  window.localStorage.removeItem(ID_TOKEN_KEY);
};
/* harmony default export */ __webpack_exports__["default"] = ({
  getToken: getToken,
  saveToken: saveToken,
  destroyToken: destroyToken
});

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/components/TheFooter.vue":
/*!**********************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/components/TheFooter.vue ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _TheFooter_vue_vue_type_template_id_61a02589___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TheFooter.vue?vue&type=template&id=61a02589& */ "./modules/App/Resources/Assets/js/app/components/TheFooter.vue?vue&type=template&id=61a02589&");
/* harmony import */ var _TheFooter_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./TheFooter.vue?vue&type=script&lang=js& */ "./modules/App/Resources/Assets/js/app/components/TheFooter.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _TheFooter_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _TheFooter_vue_vue_type_template_id_61a02589___WEBPACK_IMPORTED_MODULE_0__["render"],
  _TheFooter_vue_vue_type_template_id_61a02589___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "modules/App/Resources/Assets/js/app/components/TheFooter.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/components/TheFooter.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/components/TheFooter.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TheFooter_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./TheFooter.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/TheFooter.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TheFooter_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/components/TheFooter.vue?vue&type=template&id=61a02589&":
/*!*****************************************************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/components/TheFooter.vue?vue&type=template&id=61a02589& ***!
  \*****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TheFooter_vue_vue_type_template_id_61a02589___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./TheFooter.vue?vue&type=template&id=61a02589& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/TheFooter.vue?vue&type=template&id=61a02589&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TheFooter_vue_vue_type_template_id_61a02589___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TheFooter_vue_vue_type_template_id_61a02589___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/components/TheHeader.vue":
/*!**********************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/components/TheHeader.vue ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _TheHeader_vue_vue_type_template_id_fa5aed0a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TheHeader.vue?vue&type=template&id=fa5aed0a& */ "./modules/App/Resources/Assets/js/app/components/TheHeader.vue?vue&type=template&id=fa5aed0a&");
/* harmony import */ var _TheHeader_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./TheHeader.vue?vue&type=script&lang=js& */ "./modules/App/Resources/Assets/js/app/components/TheHeader.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _TheHeader_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _TheHeader_vue_vue_type_template_id_fa5aed0a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _TheHeader_vue_vue_type_template_id_fa5aed0a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "modules/App/Resources/Assets/js/app/components/TheHeader.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/components/TheHeader.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/components/TheHeader.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TheHeader_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./TheHeader.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/TheHeader.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TheHeader_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/components/TheHeader.vue?vue&type=template&id=fa5aed0a&":
/*!*****************************************************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/components/TheHeader.vue?vue&type=template&id=fa5aed0a& ***!
  \*****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TheHeader_vue_vue_type_template_id_fa5aed0a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./TheHeader.vue?vue&type=template&id=fa5aed0a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/TheHeader.vue?vue&type=template&id=fa5aed0a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TheHeader_vue_vue_type_template_id_fa5aed0a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TheHeader_vue_vue_type_template_id_fa5aed0a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/main.js":
/*!*****************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/main.js ***!
  \*****************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.common.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _App_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./App.vue */ "./modules/App/Resources/Assets/js/app/App.vue");
/* harmony import */ var _router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./router */ "./modules/App/Resources/Assets/js/app/router/index.js");
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./store */ "./modules/App/Resources/Assets/js/app/store/index.js");
/* harmony import */ var _registerServiceWorker__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./registerServiceWorker */ "./modules/App/Resources/Assets/js/app/registerServiceWorker.js");
/* harmony import */ var _store_actions_type__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./store/actions.type */ "./modules/App/Resources/Assets/js/app/store/actions.type.js");
/* harmony import */ var _common_api_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./common/api.service */ "./modules/App/Resources/Assets/js/app/common/api.service.js");
/* harmony import */ var _common_date_filter__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./common/date.filter */ "./modules/App/Resources/Assets/js/app/common/date.filter.js");
/* harmony import */ var _common_error_filter__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./common/error.filter */ "./modules/App/Resources/Assets/js/app/common/error.filter.js");









vue__WEBPACK_IMPORTED_MODULE_0___default.a.config.productionTip = false;
vue__WEBPACK_IMPORTED_MODULE_0___default.a.filter("date", _common_date_filter__WEBPACK_IMPORTED_MODULE_7__["default"]);
vue__WEBPACK_IMPORTED_MODULE_0___default.a.filter("error", _common_error_filter__WEBPACK_IMPORTED_MODULE_8__["default"]);
_common_api_service__WEBPACK_IMPORTED_MODULE_6__["default"].init(); // Ensure we checked auth before each page load.

_router__WEBPACK_IMPORTED_MODULE_2__["default"].beforeEach(function (to, from, next) {
  return Promise.all([_store__WEBPACK_IMPORTED_MODULE_3__["default"].dispatch(_store_actions_type__WEBPACK_IMPORTED_MODULE_5__["CHECK_AUTH"])]).then(next);
});
new vue__WEBPACK_IMPORTED_MODULE_0___default.a({
  router: _router__WEBPACK_IMPORTED_MODULE_2__["default"],
  store: _store__WEBPACK_IMPORTED_MODULE_3__["default"],
  render: function render(h) {
    return h(_App_vue__WEBPACK_IMPORTED_MODULE_1__["default"]);
  }
}).$mount("#app");

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/registerServiceWorker.js":
/*!**********************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/registerServiceWorker.js ***!
  \**********************************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var register_service_worker__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! register-service-worker */ "./node_modules/register-service-worker/index.js");
/* eslint-disable no-console */


if (false) {}

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/router/index.js":
/*!*************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/router/index.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.common.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-router */ "./node_modules/vue-router/dist/vue-router.esm.js");


vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(vue_router__WEBPACK_IMPORTED_MODULE_1__["default"]);
/* harmony default export */ __webpack_exports__["default"] = (new vue_router__WEBPACK_IMPORTED_MODULE_1__["default"]({
  routes: [{
    path: "/",
    component: function component() {
      return __webpack_require__.e(/*! import() */ 4).then(__webpack_require__.bind(null, /*! ../views/Home */ "./modules/App/Resources/Assets/js/app/views/Home.vue"));
    },
    children: [{
      path: "",
      name: "home",
      component: function component() {
        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e(5)]).then(__webpack_require__.bind(null, /*! ../views/HomeGlobal */ "./modules/App/Resources/Assets/js/app/views/HomeGlobal.vue"));
      }
    }, {
      path: "my-feed",
      name: "home-my-feed",
      component: function component() {
        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e(6)]).then(__webpack_require__.bind(null, /*! ../views/HomeMyFeed */ "./modules/App/Resources/Assets/js/app/views/HomeMyFeed.vue"));
      }
    }, {
      path: "tag/:tag",
      name: "home-tag",
      component: function component() {
        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e(7)]).then(__webpack_require__.bind(null, /*! ../views/HomeTag */ "./modules/App/Resources/Assets/js/app/views/HomeTag.vue"));
      }
    }]
  }, {
    name: "login",
    path: "/login",
    component: function component() {
      return __webpack_require__.e(/*! import() */ 8).then(__webpack_require__.bind(null, /*! ../views/Login */ "./modules/App/Resources/Assets/js/app/views/Login.vue"));
    }
  }, {
    name: "register",
    path: "/register",
    component: function component() {
      return __webpack_require__.e(/*! import() */ 12).then(__webpack_require__.bind(null, /*! ../views/Register */ "./modules/App/Resources/Assets/js/app/views/Register.vue"));
    }
  }, {
    name: "settings",
    path: "/settings",
    component: function component() {
      return __webpack_require__.e(/*! import() */ 13).then(__webpack_require__.bind(null, /*! ../views/Settings */ "./modules/App/Resources/Assets/js/app/views/Settings.vue"));
    }
  }, // Handle child routes with a default, by giving the name to the
  // child.
  // SO: https://github.com/vuejs/vue-router/issues/777
  {
    path: "/@:username",
    component: function component() {
      return __webpack_require__.e(/*! import() */ 9).then(__webpack_require__.bind(null, /*! ../views/Profile */ "./modules/App/Resources/Assets/js/app/views/Profile.vue"));
    },
    children: [{
      path: "",
      name: "profile",
      component: function component() {
        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e(10)]).then(__webpack_require__.bind(null, /*! ../views/ProfileArticles */ "./modules/App/Resources/Assets/js/app/views/ProfileArticles.vue"));
      }
    }, {
      name: "profile-favorites",
      path: "favorites",
      component: function component() {
        return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e(11)]).then(__webpack_require__.bind(null, /*! ../views/ProfileFavorited */ "./modules/App/Resources/Assets/js/app/views/ProfileFavorited.vue"));
      }
    }]
  }, {
    name: "article",
    path: "/articles/:slug",
    component: function component() {
      return Promise.all(/*! import() */[__webpack_require__.e(14), __webpack_require__.e(0), __webpack_require__.e(2)]).then(__webpack_require__.bind(null, /*! ../views/Article */ "./modules/App/Resources/Assets/js/app/views/Article.vue"));
    },
    props: true
  }, {
    name: "article-edit",
    path: "/editor/:slug?",
    props: true,
    component: function component() {
      return __webpack_require__.e(/*! import() */ 3).then(__webpack_require__.bind(null, /*! ../views/ArticleEdit */ "./modules/App/Resources/Assets/js/app/views/ArticleEdit.vue"));
    }
  }]
}));

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/store/actions.type.js":
/*!*******************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/store/actions.type.js ***!
  \*******************************************************************/
/*! exports provided: ARTICLE_PUBLISH, ARTICLE_DELETE, ARTICLE_EDIT, ARTICLE_EDIT_ADD_TAG, ARTICLE_EDIT_REMOVE_TAG, ARTICLE_RESET_STATE, CHECK_AUTH, COMMENT_CREATE, COMMENT_DESTROY, FAVORITE_ADD, FAVORITE_REMOVE, FETCH_ARTICLE, FETCH_ARTICLES, FETCH_COMMENTS, FETCH_PROFILE, FETCH_PROFILE_FOLLOW, FETCH_PROFILE_UNFOLLOW, FETCH_TAGS, LOGIN, LOGOUT, REGISTER, UPDATE_USER */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ARTICLE_PUBLISH", function() { return ARTICLE_PUBLISH; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ARTICLE_DELETE", function() { return ARTICLE_DELETE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ARTICLE_EDIT", function() { return ARTICLE_EDIT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ARTICLE_EDIT_ADD_TAG", function() { return ARTICLE_EDIT_ADD_TAG; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ARTICLE_EDIT_REMOVE_TAG", function() { return ARTICLE_EDIT_REMOVE_TAG; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ARTICLE_RESET_STATE", function() { return ARTICLE_RESET_STATE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CHECK_AUTH", function() { return CHECK_AUTH; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "COMMENT_CREATE", function() { return COMMENT_CREATE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "COMMENT_DESTROY", function() { return COMMENT_DESTROY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FAVORITE_ADD", function() { return FAVORITE_ADD; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FAVORITE_REMOVE", function() { return FAVORITE_REMOVE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_ARTICLE", function() { return FETCH_ARTICLE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_ARTICLES", function() { return FETCH_ARTICLES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_COMMENTS", function() { return FETCH_COMMENTS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_PROFILE", function() { return FETCH_PROFILE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_PROFILE_FOLLOW", function() { return FETCH_PROFILE_FOLLOW; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_PROFILE_UNFOLLOW", function() { return FETCH_PROFILE_UNFOLLOW; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_TAGS", function() { return FETCH_TAGS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LOGIN", function() { return LOGIN; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LOGOUT", function() { return LOGOUT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "REGISTER", function() { return REGISTER; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UPDATE_USER", function() { return UPDATE_USER; });
var ARTICLE_PUBLISH = "publishArticle";
var ARTICLE_DELETE = "deleteArticle";
var ARTICLE_EDIT = "editArticle";
var ARTICLE_EDIT_ADD_TAG = "addTagToArticle";
var ARTICLE_EDIT_REMOVE_TAG = "removeTagFromArticle";
var ARTICLE_RESET_STATE = "resetArticleState";
var CHECK_AUTH = "checkAuth";
var COMMENT_CREATE = "createComment";
var COMMENT_DESTROY = "destroyComment";
var FAVORITE_ADD = "addFavorite";
var FAVORITE_REMOVE = "removeFavorite";
var FETCH_ARTICLE = "fetchArticle";
var FETCH_ARTICLES = "fetchArticles";
var FETCH_COMMENTS = "fetchComments";
var FETCH_PROFILE = "fetchProfile";
var FETCH_PROFILE_FOLLOW = "fetchProfileFollow";
var FETCH_PROFILE_UNFOLLOW = "fetchProfileUnfollow";
var FETCH_TAGS = "fetchTags";
var LOGIN = "login";
var LOGOUT = "logout";
var REGISTER = "register";
var UPDATE_USER = "updateUser";

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/store/article.module.js":
/*!*********************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/store/article.module.js ***!
  \*********************************************************************/
/*! exports provided: state, actions, mutations, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "state", function() { return state; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "actions", function() { return actions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mutations", function() { return mutations; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.common.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _common_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../common/api.service */ "./modules/App/Resources/Assets/js/app/common/api.service.js");
/* harmony import */ var _actions_type__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./actions.type */ "./modules/App/Resources/Assets/js/app/store/actions.type.js");
/* harmony import */ var _mutations_type__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./mutations.type */ "./modules/App/Resources/Assets/js/app/store/mutations.type.js");


var _actions, _mutations;

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





var initialState = {
  article: {
    author: {},
    title: "",
    description: "",
    body: "",
    tagList: []
  },
  comments: []
};
var state = _objectSpread({}, initialState);
var actions = (_actions = {}, _defineProperty(_actions, _actions_type__WEBPACK_IMPORTED_MODULE_3__["FETCH_ARTICLE"], function (context, articleSlug, prevArticle) {
  return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
    var _yield$ArticlesServic, data;

    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            if (!(prevArticle !== undefined)) {
              _context.next = 2;
              break;
            }

            return _context.abrupt("return", context.commit(_mutations_type__WEBPACK_IMPORTED_MODULE_4__["SET_ARTICLE"], prevArticle));

          case 2:
            _context.next = 4;
            return _common_api_service__WEBPACK_IMPORTED_MODULE_2__["ArticlesService"].get(articleSlug);

          case 4:
            _yield$ArticlesServic = _context.sent;
            data = _yield$ArticlesServic.data;
            context.commit(_mutations_type__WEBPACK_IMPORTED_MODULE_4__["SET_ARTICLE"], data.article);
            return _context.abrupt("return", data);

          case 8:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }))();
}), _defineProperty(_actions, _actions_type__WEBPACK_IMPORTED_MODULE_3__["FETCH_COMMENTS"], function (context, articleSlug) {
  return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
    var _yield$CommentsServic, data;

    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return _common_api_service__WEBPACK_IMPORTED_MODULE_2__["CommentsService"].get(articleSlug);

          case 2:
            _yield$CommentsServic = _context2.sent;
            data = _yield$CommentsServic.data;
            context.commit(_mutations_type__WEBPACK_IMPORTED_MODULE_4__["SET_COMMENTS"], data.comments);
            return _context2.abrupt("return", data.comments);

          case 6:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }))();
}), _defineProperty(_actions, _actions_type__WEBPACK_IMPORTED_MODULE_3__["COMMENT_CREATE"], function (context, payload) {
  return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.next = 2;
            return _common_api_service__WEBPACK_IMPORTED_MODULE_2__["CommentsService"].post(payload.slug, payload.comment);

          case 2:
            context.dispatch(_actions_type__WEBPACK_IMPORTED_MODULE_3__["FETCH_COMMENTS"], payload.slug);

          case 3:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }))();
}), _defineProperty(_actions, _actions_type__WEBPACK_IMPORTED_MODULE_3__["COMMENT_DESTROY"], function (context, payload) {
  return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.next = 2;
            return _common_api_service__WEBPACK_IMPORTED_MODULE_2__["CommentsService"].destroy(payload.slug, payload.commentId);

          case 2:
            context.dispatch(_actions_type__WEBPACK_IMPORTED_MODULE_3__["FETCH_COMMENTS"], payload.slug);

          case 3:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }))();
}), _defineProperty(_actions, _actions_type__WEBPACK_IMPORTED_MODULE_3__["FAVORITE_ADD"], function (context, slug) {
  return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
    var _yield$FavoriteServic, data;

    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.next = 2;
            return _common_api_service__WEBPACK_IMPORTED_MODULE_2__["FavoriteService"].add(slug);

          case 2:
            _yield$FavoriteServic = _context5.sent;
            data = _yield$FavoriteServic.data;
            context.commit(_mutations_type__WEBPACK_IMPORTED_MODULE_4__["UPDATE_ARTICLE_IN_LIST"], data.article, {
              root: true
            });
            context.commit(_mutations_type__WEBPACK_IMPORTED_MODULE_4__["SET_ARTICLE"], data.article);

          case 6:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }))();
}), _defineProperty(_actions, _actions_type__WEBPACK_IMPORTED_MODULE_3__["FAVORITE_REMOVE"], function (context, slug) {
  return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee6() {
    var _yield$FavoriteServic2, data;

    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.next = 2;
            return _common_api_service__WEBPACK_IMPORTED_MODULE_2__["FavoriteService"].remove(slug);

          case 2:
            _yield$FavoriteServic2 = _context6.sent;
            data = _yield$FavoriteServic2.data;
            // Update list as well. This allows us to favorite an article in the Home view.
            context.commit(_mutations_type__WEBPACK_IMPORTED_MODULE_4__["UPDATE_ARTICLE_IN_LIST"], data.article, {
              root: true
            });
            context.commit(_mutations_type__WEBPACK_IMPORTED_MODULE_4__["SET_ARTICLE"], data.article);

          case 6:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6);
  }))();
}), _defineProperty(_actions, _actions_type__WEBPACK_IMPORTED_MODULE_3__["ARTICLE_PUBLISH"], function (_ref) {
  var state = _ref.state;
  return _common_api_service__WEBPACK_IMPORTED_MODULE_2__["ArticlesService"].create(state.article);
}), _defineProperty(_actions, _actions_type__WEBPACK_IMPORTED_MODULE_3__["ARTICLE_DELETE"], function (context, slug) {
  return _common_api_service__WEBPACK_IMPORTED_MODULE_2__["ArticlesService"].destroy(slug);
}), _defineProperty(_actions, _actions_type__WEBPACK_IMPORTED_MODULE_3__["ARTICLE_EDIT"], function (_ref2) {
  var state = _ref2.state;
  return _common_api_service__WEBPACK_IMPORTED_MODULE_2__["ArticlesService"].update(state.article.slug, state.article);
}), _defineProperty(_actions, _actions_type__WEBPACK_IMPORTED_MODULE_3__["ARTICLE_EDIT_ADD_TAG"], function (context, tag) {
  context.commit(_mutations_type__WEBPACK_IMPORTED_MODULE_4__["TAG_ADD"], tag);
}), _defineProperty(_actions, _actions_type__WEBPACK_IMPORTED_MODULE_3__["ARTICLE_EDIT_REMOVE_TAG"], function (context, tag) {
  context.commit(_mutations_type__WEBPACK_IMPORTED_MODULE_4__["TAG_REMOVE"], tag);
}), _defineProperty(_actions, _actions_type__WEBPACK_IMPORTED_MODULE_3__["ARTICLE_RESET_STATE"], function (_ref3) {
  var commit = _ref3.commit;
  commit(_mutations_type__WEBPACK_IMPORTED_MODULE_4__["RESET_STATE"]);
}), _actions);
/* eslint no-param-reassign: ["error", { "props": false }] */

var mutations = (_mutations = {}, _defineProperty(_mutations, _mutations_type__WEBPACK_IMPORTED_MODULE_4__["SET_ARTICLE"], function (state, article) {
  state.article = article;
}), _defineProperty(_mutations, _mutations_type__WEBPACK_IMPORTED_MODULE_4__["SET_COMMENTS"], function (state, comments) {
  state.comments = comments;
}), _defineProperty(_mutations, _mutations_type__WEBPACK_IMPORTED_MODULE_4__["TAG_ADD"], function (state, tag) {
  state.article.tagList = state.article.tagList.concat([tag]);
}), _defineProperty(_mutations, _mutations_type__WEBPACK_IMPORTED_MODULE_4__["TAG_REMOVE"], function (state, tag) {
  state.article.tagList = state.article.tagList.filter(function (t) {
    return t !== tag;
  });
}), _defineProperty(_mutations, _mutations_type__WEBPACK_IMPORTED_MODULE_4__["RESET_STATE"], function () {
  for (var f in state) {
    vue__WEBPACK_IMPORTED_MODULE_1___default.a.set(state, f, initialState[f]);
  }
}), _mutations);
var getters = {
  article: function article(state) {
    return state.article;
  },
  comments: function comments(state) {
    return state.comments;
  }
};
/* harmony default export */ __webpack_exports__["default"] = ({
  state: state,
  actions: actions,
  mutations: mutations,
  getters: getters
});

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/store/auth.module.js":
/*!******************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/store/auth.module.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _common_api_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../common/api.service */ "./modules/App/Resources/Assets/js/app/common/api.service.js");
/* harmony import */ var _common_jwt_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/jwt.service */ "./modules/App/Resources/Assets/js/app/common/jwt.service.js");
/* harmony import */ var _actions_type__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./actions.type */ "./modules/App/Resources/Assets/js/app/store/actions.type.js");
/* harmony import */ var _mutations_type__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./mutations.type */ "./modules/App/Resources/Assets/js/app/store/mutations.type.js");
var _actions, _mutations;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





var state = {
  errors: null,
  user: {},
  isAuthenticated: !!_common_jwt_service__WEBPACK_IMPORTED_MODULE_1__["default"].getToken()
};
var getters = {
  currentUser: function currentUser(state) {
    return state.user;
  },
  isAuthenticated: function isAuthenticated(state) {
    return state.isAuthenticated;
  }
};
var actions = (_actions = {}, _defineProperty(_actions, _actions_type__WEBPACK_IMPORTED_MODULE_2__["LOGIN"], function (context, credentials) {
  return new Promise(function (resolve) {
    _common_api_service__WEBPACK_IMPORTED_MODULE_0__["default"].post("users/login", {
      user: credentials
    }).then(function (_ref) {
      var data = _ref.data;
      context.commit(_mutations_type__WEBPACK_IMPORTED_MODULE_3__["SET_AUTH"], data.user);
      resolve(data);
    })["catch"](function (_ref2) {
      var response = _ref2.response;
      context.commit(_mutations_type__WEBPACK_IMPORTED_MODULE_3__["SET_ERROR"], response.data.errors);
    });
  });
}), _defineProperty(_actions, _actions_type__WEBPACK_IMPORTED_MODULE_2__["LOGOUT"], function (context) {
  context.commit(_mutations_type__WEBPACK_IMPORTED_MODULE_3__["PURGE_AUTH"]);
}), _defineProperty(_actions, _actions_type__WEBPACK_IMPORTED_MODULE_2__["REGISTER"], function (context, credentials) {
  return new Promise(function (resolve, reject) {
    _common_api_service__WEBPACK_IMPORTED_MODULE_0__["default"].post("users", {
      user: credentials
    }).then(function (_ref3) {
      var data = _ref3.data;
      context.commit(_mutations_type__WEBPACK_IMPORTED_MODULE_3__["SET_AUTH"], data.user);
      resolve(data);
    })["catch"](function (_ref4) {
      var response = _ref4.response;
      context.commit(_mutations_type__WEBPACK_IMPORTED_MODULE_3__["SET_ERROR"], response.data.errors);
      reject(response);
    });
  });
}), _defineProperty(_actions, _actions_type__WEBPACK_IMPORTED_MODULE_2__["CHECK_AUTH"], function (context) {
  if (_common_jwt_service__WEBPACK_IMPORTED_MODULE_1__["default"].getToken()) {
    _common_api_service__WEBPACK_IMPORTED_MODULE_0__["default"].setHeader();
    _common_api_service__WEBPACK_IMPORTED_MODULE_0__["default"].get("user").then(function (_ref5) {
      var data = _ref5.data;
      context.commit(_mutations_type__WEBPACK_IMPORTED_MODULE_3__["SET_AUTH"], data.user);
    })["catch"](function (_ref6) {
      var response = _ref6.response;
      context.commit(_mutations_type__WEBPACK_IMPORTED_MODULE_3__["SET_ERROR"], response.data.errors);
    });
  } else {
    context.commit(_mutations_type__WEBPACK_IMPORTED_MODULE_3__["PURGE_AUTH"]);
  }
}), _defineProperty(_actions, _actions_type__WEBPACK_IMPORTED_MODULE_2__["UPDATE_USER"], function (context, payload) {
  var email = payload.email,
      username = payload.username,
      password = payload.password,
      image = payload.image,
      bio = payload.bio;
  var user = {
    email: email,
    username: username,
    bio: bio,
    image: image
  };

  if (password) {
    user.password = password;
  }

  return _common_api_service__WEBPACK_IMPORTED_MODULE_0__["default"].put("user", user).then(function (_ref7) {
    var data = _ref7.data;
    context.commit(_mutations_type__WEBPACK_IMPORTED_MODULE_3__["SET_AUTH"], data.user);
    return data;
  });
}), _actions);
var mutations = (_mutations = {}, _defineProperty(_mutations, _mutations_type__WEBPACK_IMPORTED_MODULE_3__["SET_ERROR"], function (state, error) {
  state.errors = error;
}), _defineProperty(_mutations, _mutations_type__WEBPACK_IMPORTED_MODULE_3__["SET_AUTH"], function (state, user) {
  state.isAuthenticated = true;
  state.user = user;
  state.errors = {};
  _common_jwt_service__WEBPACK_IMPORTED_MODULE_1__["default"].saveToken(state.user.token);
}), _defineProperty(_mutations, _mutations_type__WEBPACK_IMPORTED_MODULE_3__["PURGE_AUTH"], function (state) {
  state.isAuthenticated = false;
  state.user = {};
  state.errors = {};
  _common_jwt_service__WEBPACK_IMPORTED_MODULE_1__["default"].destroyToken();
}), _mutations);
/* harmony default export */ __webpack_exports__["default"] = ({
  state: state,
  actions: actions,
  mutations: mutations,
  getters: getters
});

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/store/home.module.js":
/*!******************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/store/home.module.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _common_api_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../common/api.service */ "./modules/App/Resources/Assets/js/app/common/api.service.js");
/* harmony import */ var _actions_type__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./actions.type */ "./modules/App/Resources/Assets/js/app/store/actions.type.js");
/* harmony import */ var _mutations_type__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./mutations.type */ "./modules/App/Resources/Assets/js/app/store/mutations.type.js");
var _actions, _mutations;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




var state = {
  tags: [],
  articles: [],
  isLoading: true,
  articlesCount: 0
};
var getters = {
  articlesCount: function articlesCount(state) {
    return state.articlesCount;
  },
  articles: function articles(state) {
    return state.articles;
  },
  isLoading: function isLoading(state) {
    return state.isLoading;
  },
  tags: function tags(state) {
    return state.tags;
  }
};
var actions = (_actions = {}, _defineProperty(_actions, _actions_type__WEBPACK_IMPORTED_MODULE_1__["FETCH_ARTICLES"], function (_ref, params) {
  var commit = _ref.commit;
  commit(_mutations_type__WEBPACK_IMPORTED_MODULE_2__["FETCH_START"]);
  return _common_api_service__WEBPACK_IMPORTED_MODULE_0__["ArticlesService"].query(params.type, params.filters).then(function (_ref2) {
    var data = _ref2.data;
    commit(_mutations_type__WEBPACK_IMPORTED_MODULE_2__["FETCH_END"], data);
  })["catch"](function (error) {
    throw new Error(error);
  });
}), _defineProperty(_actions, _actions_type__WEBPACK_IMPORTED_MODULE_1__["FETCH_TAGS"], function (_ref3) {
  var commit = _ref3.commit;
  return _common_api_service__WEBPACK_IMPORTED_MODULE_0__["TagsService"].get().then(function (_ref4) {
    var data = _ref4.data;
    commit(_mutations_type__WEBPACK_IMPORTED_MODULE_2__["SET_TAGS"], data.tags);
  })["catch"](function (error) {
    throw new Error(error);
  });
}), _actions);
/* eslint no-param-reassign: ["error", { "props": false }] */

var mutations = (_mutations = {}, _defineProperty(_mutations, _mutations_type__WEBPACK_IMPORTED_MODULE_2__["FETCH_START"], function (state) {
  state.isLoading = true;
}), _defineProperty(_mutations, _mutations_type__WEBPACK_IMPORTED_MODULE_2__["FETCH_END"], function (state, _ref5) {
  var articles = _ref5.articles,
      articlesCount = _ref5.articlesCount;
  state.articles = articles;
  state.articlesCount = articlesCount;
  state.isLoading = false;
}), _defineProperty(_mutations, _mutations_type__WEBPACK_IMPORTED_MODULE_2__["SET_TAGS"], function (state, tags) {
  state.tags = tags;
}), _defineProperty(_mutations, _mutations_type__WEBPACK_IMPORTED_MODULE_2__["UPDATE_ARTICLE_IN_LIST"], function (state, data) {
  state.articles = state.articles.map(function (article) {
    if (article.slug !== data.slug) {
      return article;
    } // We could just return data, but it seems dangerous to
    // mix the results of different api calls, so we
    // protect ourselves by copying the information.


    article.favorited = data.favorited;
    article.favoritesCount = data.favoritesCount;
    return article;
  });
}), _mutations);
/* harmony default export */ __webpack_exports__["default"] = ({
  state: state,
  getters: getters,
  actions: actions,
  mutations: mutations
});

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/store/index.js":
/*!************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/store/index.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.common.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var _home_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./home.module */ "./modules/App/Resources/Assets/js/app/store/home.module.js");
/* harmony import */ var _auth_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./auth.module */ "./modules/App/Resources/Assets/js/app/store/auth.module.js");
/* harmony import */ var _article_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./article.module */ "./modules/App/Resources/Assets/js/app/store/article.module.js");
/* harmony import */ var _profile_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./profile.module */ "./modules/App/Resources/Assets/js/app/store/profile.module.js");






vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(vuex__WEBPACK_IMPORTED_MODULE_1__["default"]);
/* harmony default export */ __webpack_exports__["default"] = (new vuex__WEBPACK_IMPORTED_MODULE_1__["default"].Store({
  modules: {
    home: _home_module__WEBPACK_IMPORTED_MODULE_2__["default"],
    auth: _auth_module__WEBPACK_IMPORTED_MODULE_3__["default"],
    article: _article_module__WEBPACK_IMPORTED_MODULE_4__["default"],
    profile: _profile_module__WEBPACK_IMPORTED_MODULE_5__["default"]
  }
}));

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/store/mutations.type.js":
/*!*********************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/store/mutations.type.js ***!
  \*********************************************************************/
/*! exports provided: FETCH_END, FETCH_START, PURGE_AUTH, SET_ARTICLE, SET_AUTH, SET_COMMENTS, SET_ERROR, SET_PROFILE, SET_TAGS, TAG_ADD, TAG_REMOVE, UPDATE_ARTICLE_IN_LIST, RESET_STATE */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_END", function() { return FETCH_END; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_START", function() { return FETCH_START; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PURGE_AUTH", function() { return PURGE_AUTH; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_ARTICLE", function() { return SET_ARTICLE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_AUTH", function() { return SET_AUTH; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_COMMENTS", function() { return SET_COMMENTS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_ERROR", function() { return SET_ERROR; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_PROFILE", function() { return SET_PROFILE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_TAGS", function() { return SET_TAGS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TAG_ADD", function() { return TAG_ADD; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TAG_REMOVE", function() { return TAG_REMOVE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UPDATE_ARTICLE_IN_LIST", function() { return UPDATE_ARTICLE_IN_LIST; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RESET_STATE", function() { return RESET_STATE; });
var FETCH_END = "setArticles";
var FETCH_START = "setLoading";
var PURGE_AUTH = "logOut";
var SET_ARTICLE = "setArticle";
var SET_AUTH = "setUser";
var SET_COMMENTS = "setComments";
var SET_ERROR = "setError";
var SET_PROFILE = "setProfile";
var SET_TAGS = "setTags";
var TAG_ADD = "addTag";
var TAG_REMOVE = "removeTag";
var UPDATE_ARTICLE_IN_LIST = "updateArticleInList";
var RESET_STATE = "resetModuleState";

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/store/profile.module.js":
/*!*********************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/store/profile.module.js ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _common_api_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../common/api.service */ "./modules/App/Resources/Assets/js/app/common/api.service.js");
/* harmony import */ var _actions_type__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./actions.type */ "./modules/App/Resources/Assets/js/app/store/actions.type.js");
/* harmony import */ var _mutations_type__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./mutations.type */ "./modules/App/Resources/Assets/js/app/store/mutations.type.js");
var _actions;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




var state = {
  errors: {},
  profile: {}
};
var getters = {
  profile: function profile(state) {
    return state.profile;
  }
};
var actions = (_actions = {}, _defineProperty(_actions, _actions_type__WEBPACK_IMPORTED_MODULE_1__["FETCH_PROFILE"], function (context, payload) {
  var username = payload.username;
  return _common_api_service__WEBPACK_IMPORTED_MODULE_0__["default"].get("profiles", username).then(function (_ref) {
    var data = _ref.data;
    context.commit(_mutations_type__WEBPACK_IMPORTED_MODULE_2__["SET_PROFILE"], data.profile);
    return data;
  })["catch"](function () {// #todo SET_ERROR cannot work in multiple states
    // context.commit(SET_ERROR, response.data.errors)
  });
}), _defineProperty(_actions, _actions_type__WEBPACK_IMPORTED_MODULE_1__["FETCH_PROFILE_FOLLOW"], function (context, payload) {
  var username = payload.username;
  return _common_api_service__WEBPACK_IMPORTED_MODULE_0__["default"].post("profiles/".concat(username, "/follow")).then(function (_ref2) {
    var data = _ref2.data;
    context.commit(_mutations_type__WEBPACK_IMPORTED_MODULE_2__["SET_PROFILE"], data.profile);
    return data;
  })["catch"](function () {// #todo SET_ERROR cannot work in multiple states
    // context.commit(SET_ERROR, response.data.errors)
  });
}), _defineProperty(_actions, _actions_type__WEBPACK_IMPORTED_MODULE_1__["FETCH_PROFILE_UNFOLLOW"], function (context, payload) {
  var username = payload.username;
  return _common_api_service__WEBPACK_IMPORTED_MODULE_0__["default"]["delete"]("profiles/".concat(username, "/follow")).then(function (_ref3) {
    var data = _ref3.data;
    context.commit(_mutations_type__WEBPACK_IMPORTED_MODULE_2__["SET_PROFILE"], data.profile);
    return data;
  })["catch"](function () {// #todo SET_ERROR cannot work in multiple states
    // context.commit(SET_ERROR, response.data.errors)
  });
}), _actions);

var mutations = _defineProperty({}, _mutations_type__WEBPACK_IMPORTED_MODULE_2__["SET_PROFILE"], function (state, profile) {
  state.profile = profile;
  state.errors = {};
});

/* harmony default export */ __webpack_exports__["default"] = ({
  state: state,
  actions: actions,
  mutations: mutations,
  getters: getters
});

/***/ }),

/***/ "./modules/App/Resources/Assets/sass/app.scss":
/*!****************************************************!*\
  !*** ./modules/App/Resources/Assets/sass/app.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "./modules/Landing/Resources/Assets/sass/app.scss":
/*!********************************************************!*\
  !*** ./modules/Landing/Resources/Assets/sass/app.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/App.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./modules/App/Resources/Assets/js/app/App.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_TheHeader__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/TheHeader */ "./modules/App/Resources/Assets/js/app/components/TheHeader.vue");
/* harmony import */ var _components_TheFooter__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/TheFooter */ "./modules/App/Resources/Assets/js/app/components/TheFooter.vue");
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "App",
  components: {
    RwvHeader: _components_TheHeader__WEBPACK_IMPORTED_MODULE_0__["default"],
    RwvFooter: _components_TheFooter__WEBPACK_IMPORTED_MODULE_1__["default"]
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/TheFooter.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./modules/App/Resources/Assets/js/app/components/TheFooter.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "RwvFooter"
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/TheHeader.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./modules/App/Resources/Assets/js/app/components/TheHeader.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "RwvHeader",
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])(["currentUser", "isAuthenticated"]))
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/App.vue?vue&type=template&id=4a20d50a&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./modules/App/Resources/Assets/js/app/App.vue?vue&type=template&id=4a20d50a& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { attrs: { id: "app" } },
    [
      _c("RwvHeader"),
      _vm._v(" "),
      _c("router-view"),
      _vm._v(" "),
      _c("RwvFooter")
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/TheFooter.vue?vue&type=template&id=61a02589&":
/*!***********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./modules/App/Resources/Assets/js/app/components/TheFooter.vue?vue&type=template&id=61a02589& ***!
  \***********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("footer", [
    _c(
      "div",
      { staticClass: "container" },
      [
        _c(
          "router-link",
          {
            staticClass: "logo-font",
            attrs: { to: { name: "home", params: {} } }
          },
          [_vm._v("\n      conduit\n    ")]
        ),
        _vm._v(" "),
        _vm._m(0)
      ],
      1
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "attribution" }, [
      _vm._v("\n      An interactive learning project from\n      "),
      _c(
        "a",
        {
          attrs: {
            rel: "noopener noreferrer",
            target: "blank",
            href: "https://thinkster.io"
          }
        },
        [_vm._v("Thinkster")]
      ),
      _vm._v(". Code & design licensed under MIT.\n    ")
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/TheHeader.vue?vue&type=template&id=fa5aed0a&":
/*!***********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./modules/App/Resources/Assets/js/app/components/TheHeader.vue?vue&type=template&id=fa5aed0a& ***!
  \***********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("nav", { staticClass: "navbar navbar-light" }, [
    _c(
      "div",
      { staticClass: "container" },
      [
        _c(
          "router-link",
          { staticClass: "navbar-brand", attrs: { to: { name: "home" } } },
          [_vm._v("\n      conduit\n    ")]
        ),
        _vm._v(" "),
        !_vm.isAuthenticated
          ? _c("ul", { staticClass: "nav navbar-nav pull-xs-right" }, [
              _c(
                "li",
                { staticClass: "nav-item" },
                [
                  _c(
                    "router-link",
                    {
                      staticClass: "nav-link",
                      attrs: {
                        "active-class": "active",
                        exact: "",
                        to: { name: "home" }
                      }
                    },
                    [_vm._v("\n          Home\n        ")]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "li",
                { staticClass: "nav-item" },
                [
                  _c(
                    "router-link",
                    {
                      staticClass: "nav-link",
                      attrs: {
                        "active-class": "active",
                        exact: "",
                        to: { name: "login" }
                      }
                    },
                    [
                      _c("i", { staticClass: "ion-compose" }),
                      _vm._v("Sign in\n        ")
                    ]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "li",
                { staticClass: "nav-item" },
                [
                  _c(
                    "router-link",
                    {
                      staticClass: "nav-link",
                      attrs: {
                        "active-class": "active",
                        exact: "",
                        to: { name: "register" }
                      }
                    },
                    [
                      _c("i", { staticClass: "ion-compose" }),
                      _vm._v("Sign up\n        ")
                    ]
                  )
                ],
                1
              )
            ])
          : _c("ul", { staticClass: "nav navbar-nav pull-xs-right" }, [
              _c(
                "li",
                { staticClass: "nav-item" },
                [
                  _c(
                    "router-link",
                    {
                      staticClass: "nav-link",
                      attrs: {
                        "active-class": "active",
                        exact: "",
                        to: { name: "home" }
                      }
                    },
                    [_vm._v("\n          Home\n        ")]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "li",
                { staticClass: "nav-item" },
                [
                  _c(
                    "router-link",
                    {
                      staticClass: "nav-link",
                      attrs: {
                        "active-class": "active",
                        to: { name: "article-edit" }
                      }
                    },
                    [
                      _c("i", { staticClass: "ion-compose" }),
                      _vm._v(" New Article\n        ")
                    ]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "li",
                { staticClass: "nav-item" },
                [
                  _c(
                    "router-link",
                    {
                      staticClass: "nav-link",
                      attrs: {
                        "active-class": "active",
                        exact: "",
                        to: { name: "settings" }
                      }
                    },
                    [
                      _c("i", { staticClass: "ion-gear-a" }),
                      _vm._v(" Settings\n        ")
                    ]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _vm.currentUser.username
                ? _c(
                    "li",
                    { staticClass: "nav-item" },
                    [
                      _c(
                        "router-link",
                        {
                          staticClass: "nav-link",
                          attrs: {
                            "active-class": "active",
                            exact: "",
                            to: {
                              name: "profile",
                              params: { username: _vm.currentUser.username }
                            }
                          }
                        },
                        [
                          _vm._v(
                            "\n          " +
                              _vm._s(_vm.currentUser.username) +
                              "\n        "
                          )
                        ]
                      )
                    ],
                    1
                  )
                : _vm._e()
            ])
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ 0:
/*!****************************************************************************************************************************************************!*\
  !*** multi ./modules/App/Resources/Assets/js/app.js ./modules/Landing/Resources/Assets/sass/app.scss ./modules/App/Resources/Assets/sass/app.scss ***!
  \****************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /home/admin/web/pure-purple.metateg.pro/public_html/modules/App/Resources/Assets/js/app.js */"./modules/App/Resources/Assets/js/app.js");
__webpack_require__(/*! /home/admin/web/pure-purple.metateg.pro/public_html/modules/Landing/Resources/Assets/sass/app.scss */"./modules/Landing/Resources/Assets/sass/app.scss");
module.exports = __webpack_require__(/*! /home/admin/web/pure-purple.metateg.pro/public_html/modules/App/Resources/Assets/sass/app.scss */"./modules/App/Resources/Assets/sass/app.scss");


/***/ })

},[[0,"/js/manifest","vendors~/js/app~/js/vendor","vendors~/js/vendor","/js/vendor","vendors~/js/app"]]]);