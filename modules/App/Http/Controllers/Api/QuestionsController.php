<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 18.07.18
 * Time: 15:43
 */

namespace Modules\App\Http\Controllers\Api;

use Illuminate\Http\Request;

class QuestionsController extends BaseController
{
    
    public function __construct() 
    {
        
    }

    public function index()
    {
        
    }
    
    public function getQuestions()
    {
        return response()->json([
            1,2
        ]);
    }
        
}