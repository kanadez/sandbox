<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 18.07.18
 * Time: 15:43
 */

namespace Modules\App\Http\Controllers\Api;

use Illuminate\Http\Request;

use Modules\Core\Contracts\Collections\TestQuestionsCollectionContract;

class TestController extends BaseController
{
    /**
     *
     * @var TestQuestionsCollectionContract 
     */
    private $testQuestions;
    
    public function __construct(TestQuestionsCollectionContract $testQuestions) 
    {
        $this->testQuestions = $testQuestions;
    }


    public function index()
    {
        
    }
    
    public function getQuestions()
    {
        return $this->testQuestions->all();
    }
    
    
}