<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 18.07.18
 * Time: 15:43
 */

namespace Modules\App\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route; 

class AppController extends BaseController
{
    
    public function __construct() 
    {
        
    }


    public function index()
    {
        return view("app::web.index", []);
    }
    
    public function debugShowAllRoutes()
    {
        dd(Route::getRoutes());
    }
    
    
}