<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 24.07.18
 * Time: 16:35
 */

namespace Modules\App\Http\Controllers\Web;

use Modules\Core\Http\Controllers\CoreBaseController;

abstract class BaseController extends CoreBaseController
{
}