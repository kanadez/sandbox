const path = require('path');

module.exports = function (app, module) {
    // Copy fonts
    app.mix
        .copyDirectory(module.paths.assets + 'pRESIDENTPROXY/img', 'public/img')
        .extract([
            'popper.js',
            'vue',
            'bootstrap',
            'slick-carousel',
        ]);
        
    app.mix.webpackConfig({
        output: {
            //chunkFilename: 'js/[name].js',
            publicPath: '/assets/app/',
        },
        optimization: {
            splitChunks: {
      // include all types of chunks
      //chunks: 'async'
    }
        }

    });
};