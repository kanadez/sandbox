<?php

namespace Modules\Landing\Providers;

use Caffeinated\Modules\Support\ServiceProvider;
use Illuminate\Routing\Router;
use Modules\Landing\Http\Middleware\LocaleMiddleware;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot(Router $router)
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'landing');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'landing');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'landing');
        $this->loadConfigsFrom(__DIR__.'/../config');
        
        $router->pushMiddlewareToGroup("web", LocaleMiddleware::class);
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
