<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

/** @var  $router Illuminate\Routing\Router */
$router->get('', 'LandingController@index')
    ->name('index');

$router->get('/password/reset/{token}', 'LandingController@index')
    ->name('password.reset');

$router->get('/signin', 'LandingController@index')
    ->name('signin');

$router->get('/signup', 'LandingController@index')
    ->name('signup');


$router->group(["prefix" => "lang", "as" => "lang."], function (Illuminate\Routing\Router $router) {
    $router->get('{lang}', 'LanguageController@change')
        ->name('change')
        ->where("lang", "ru|en");
});
