<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 25.07.18
 * Time: 11:43
 */

namespace Modules\Landing\tests\Unit;

use Modules\Core\tests\TestCase;

class LandingControllerTest extends TestCase
{
    /**
     * @test
     */
    public function testIndex()
    {
        $response = $this->call('GET', '/');
        $response->assertSuccessful();
    }
}