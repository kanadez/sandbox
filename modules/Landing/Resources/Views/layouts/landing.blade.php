<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 23.07.18
 * Time: 14:38
 */
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title></title>
    <link rel="shortcut icon" href="#" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css">
    
    <link href="{{ mix("/css/Landing.css", 'assets/web') }}" rel="stylesheet">
    
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
<div class="wrapper">

    <header class="app-header navbar">
        <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="{{ route("admin.index") }}">
            <i></i>
        </a>
        
    </header>
    
    <main>
        <section class="head-block line">
            
            @yield('content')
                
        </section>
    </main>

    <footer  class="footer">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 order-2 order-md-1">
                    
                </div>
                
            </div>
        </div>
    </footer>

</div>
</div>

<script src="{{ mix("/js/manifest.js", 'assets/web') }}"></script>
<script src="{{ mix("/js/vendor.js", 'assets/web') }}"></script>
<script src="{{ mix("/js/web.js", 'assets/web') }}"></script>

</body>
</html>
