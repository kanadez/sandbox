<?php
/**
 * Created by NetBeans.
 * User: kanadez
 * Date: 10.07.20
 * Time: 09:34
 */
?>

@extends('landing::layouts.landing')

@section('content')

<div class="container no-sides-padding">
    <div id="property-review" class="h-100 align-items-center">
        
        {!! Form::hidden(null, "$property_id", 
            ["ref" => "property_id"]) 
        !!}
        
        <div class="property-details col-12 col-md-6 offset-md-3 col-lg-6 offset-lg-3 no-sides-padding">
            <div class="card property-card">
                <div class="card-header">
                    Оставить отзыв
                </div>
                <div class="card-body no-bottom-top-padding">
                    <form>
                        <div class="form-group add-review-wrapper">
                            <textarea v-model="add_review.review_text" placeholder="Напишите текст отзыва..." class="add-review form-control"></textarea>
                        </div>
                        <a v-on:click="submitReview()" href="javascript:void(0)" class="btn btn-success btn-block">Отправить</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
    
@endsection