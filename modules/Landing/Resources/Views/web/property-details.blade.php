<?php
/**
 * Created by NetBeans.
 * User: kanadez
 * Date: 10.07.20
 * Time: 09:34
 */
?>

@extends('landing::layouts.landing')

@section('content')

<div class="container no-sides-padding">
    <div id="property" class="h-100 align-items-center">
        <div class="property-details col-12 col-md-6 offset-md-3 col-lg-6 offset-lg-3 no-sides-padding">
            <div class="card property-card">
                <div class="card-body no-sides-padding no-bottom-top-padding">
                    <div class="image" style="background-image: {{ (isset($property["image"]) ? 'url(' . $property["image"]["src"] . ')' : '') }}"></div>
                    <div class="description">
                        <div class="title">
                            {{ $property["title"] }}
                        </div>
                        <div class="metro">
                            <i class="metro-icon"></i>
                            {{ $property["desc_metro"] }}
                        </div>
                        <div class="address">
                            {{ $property["desc_address"] }}
                        </div>
                        <div class="price">
                            {{ $property["price_value"] }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="card property-card">
                <div class="card-header">
                    Отзывы
                </div>
                <div class="card-body no-sides-padding no-bottom-top-padding">
                    <div class="add-review-wrapper sides-padding">
                        <div class="empty">
                            Пока нет отзывов
                        </div>
                        <a href="{{ route('web.add-review', $property["id"]) }}" class="btn btn-success btn-block">Оставить отзыв</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    
@endsection