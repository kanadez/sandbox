<?php
/**
 * Created by NetBeans.
 * User: kanadez
 * Date: 10.07.20
 * Time: 09:34
 */
?>

@extends('landing::layouts.landing')

@section('content')

<div class="container">
    <div id="search" class="h-100 align-items-center">
        <!--<div class="col-12 col-md-6 logo-wrapper"></div>-->
        <div class="autocomplete-wrapper col-12 col-md-6 offset-md-3 col-lg-6 offset-lg-3 no-sides-padding">
            <gmap-autocomplete 
                class="introInput form-control" 
                @place_changed="onAutocompleteChanged"
                :select-first-on-enter="true"
                :options="{
                    bounds: this.spb_bounds,
                    strictBounds: true,
                    types: ['geocode']
                }"
            >
                        <template v-slot:input="slotProps">
                            <v-text-field outlined
                                          prepend-inner-icon="place"
                                          placeholder="Location Of Event"
                                          ref="input"
                                          v-on:listeners="slotProps.listeners"
                                          v-on:attrs="slotProps.attrs">
                            </v-text-field>
                        </template>
            </gmap-autocomplete>
        </div>
        <div class="search-realty col-12 col-md-6 offset-md-3 col-lg-6 offset-lg-3 no-sides-padding">
            <a v-for="property in finded_properties" :href="getPropertyDetailsLink(property.id)" class="property-card-link">
                <div v-if="isset(property.image)" class="card property-card">
                    <div class="card-body no-sides-padding no-bottom-top-padding">
                        <div class="image" :style="'background-image:' + (isset(property.image) ? 'url(' + property.image.src + ')' : '') ">

                        </div>
                        <div class="description">
                            <div class="title">
                                @{{ property.title }}
                            </div>
                            <div class="metro">
                                <i class="metro-icon"></i>
                                @{{ property.desc_metro }}
                            </div>
                            <div class="address">
                                @{{ property.desc_address }}
                            </div>
                            <div class="price">
                                @{{ property.price_value }}
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>
    
@endsection