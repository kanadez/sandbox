<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 18.07.18
 * Time: 15:52
 */
?>
@extends('landing::layouts.landing')

@section('content')

<div id="search" class="h-100 align-items-center">
    <div class="col-md-3"></div>
    <div class="col-12 col-md-6">
        {{ Form::model(null, ['route' => ['landing.web.find-realty'], "class" => "user-edit-form form-horizontal form-border" ]) }}
        @include('core::admin.forms.errors')

        {!! Form::admTextSmall('query', 'Query', ["placeholder" => "Type to find..."]) !!}
        {{ Form::close() }}
    </div>
</div>

@endsection