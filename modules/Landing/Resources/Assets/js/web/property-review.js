/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// mixins
import {Utils} from './mixins/utils';
import Loading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';

window.Vue          = require('vue');
window.axios        = require('axios');

Vue.use(Loading);

Vue.prototype.$http = window.axios;

if (appDomExists("property-review")){
    window.app = new Vue({
        el: '#property-review',
        mixins: [Utils],
        data: {
            // consts

            // vars
            urls: {
                property_url:   "/property/details/",
                submit_review:  "/property/add-review",
            },
            
            property_id: null,
            
            add_review: {
                review_text: "",
            },

        },

        computed: {

        },

        watch: {

        },

        mounted () {
            this.init();
        },

        methods: {

            init () {
                this.property_id = this.$refs.property_id.value;
            },

            submitReview () {
                let loader = this.$loading.show();
                
                this.$http.post(this.urls.submit_review, {
                    property_id: this.property_id,
                    review_text: this.add_review.review_text
                })
                .then(response => { 
                    //loader.hide();
                    location.href = this.urls.property_url + this.property_id;
                })
                .catch(error => {
                    loader.hide();
                    this.$notify({
                        type: "error",
                        title: 'Ошибка!',
                        text: 'Что-то пошло не так. Попробуйте обновить страницу.'
                    });
                });
            },

        }
    });
}

function appDomExists(vue_app_id){
    if (document.getElementById(vue_app_id) != null){
        return true;
    }
    else{
        return false;
    }
}