/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import * as VueGoogleMaps from 'vue2-google-maps'
import {gmapApi} from 'vue2-google-maps'

// mixins
import {Utils} from './mixins/utils';

window.Vue          = require('vue');
window.axios        = require('axios');

Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyBsckC1p9dVClokEzfBOm5ozZ_F2ZP9cq8',
        libraries: 'places',
        region: 'RU',
        language: 'ru',
    },
    installComponents: false
});

Vue.prototype.$http = window.axios;

Vue.component('gmap-autocomplete', VueGoogleMaps.Autocomplete);

if (appDomExists("search")){
    window.app = new Vue({
        el: '#search',
        mixins: [Utils],
        data: {
            // consts

            // vars
            urls: {
                search_realty:      "/search/realty",
                property_details:   "/property/details/",
            },

            spb_bounds:                     null,
            autocomplete_selected_place:    null,
            finded_properties:              [],
        },

        computed: {
            google: gmapApi 
        },

        watch: {
            google: function (val) {
                this.init();
            },
        },

        mounted () {
            this.init();
        },

        methods: {

            init () {
                this.initGoogleAutocomlete();
            },

            initGoogleAutocomlete () {
                this.spb_bounds = {north: 60.399, south: 59.494, east: 31.312, west: 29.416};
            },

            onAutocompleteChanged (place) {
                if (!this.isset(place)){
                    return false;
                }

                this.parseAutocompletePlaceAddress(place);
                this.searchRealty();
            },

            parseAutocompletePlaceAddress (place) {
                let address_components_types_to_extract = [
                    "route",
                    "street_number"
                ];
                let parsed_address = {};

                place.address_components.forEach(function(address_component){
                    address_component.types.forEach(function(component_type){
                        if (address_components_types_to_extract.indexOf(component_type) !== -1){
                            parsed_address[component_type] = address_component.long_name;
                        }
                    });
                });

                this.autocomplete_selected_place = parsed_address;
            },

            searchRealty() {
                this.$http.post(this.urls.search_realty, {
                    address: this.autocomplete_selected_place
                })
                .then(response => { 
                    this.finded_properties = response.data.data;
                })
                .catch(error => {
                    /*this.$notify({
                        type: "error",
                        title: 'Failed!',
                        text: 'Something went wrong! Try to reload page'
                    });*/
                });
            },

            getPropertyDetailsLink (property_id) {
                return this.urls.property_details + property_id;
            },
        }
    });
}

function appDomExists(vue_app_id){
    if (document.getElementById(vue_app_id) != null){
        return true;
    }
    else{
        return false;
    }
}