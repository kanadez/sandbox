/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// mixins
import {Utils} from './mixins/utils';

window.Vue          = require('vue');
window.axios        = require('axios');

Vue.prototype.$http = window.axios;

if (appDomExists("property")){
    window.app = new Vue({
        el: '#property',
        mixins: [Utils],
        data: {
            // consts

            // vars
            urls: {

            },

        },

        computed: {

        },

        watch: {

        },

        mounted () {
            //this.init();
        },

        methods: {

            init () {

            },


        }
    });
}

function appDomExists(vue_app_id){
    if (document.getElementById(vue_app_id) != null){
        return true;
    }
    else{
        return false;
    }
}