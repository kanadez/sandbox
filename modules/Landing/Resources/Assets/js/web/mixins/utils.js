/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


let Utils = {
    
    data: function () {
        return {
            loader: null
        }
    },
    
    created: function () {
        //this.init()
    },
    
    methods: {
        getRef(ref, property) {
            if (ref != undefined){
                return ref[property];
            }
            else{
                return null;
            }
        },

        isset(variable) {
            if (variable === null || variable === undefined){
                return false;
            }
            else{
                return true;
            }
        },

        empty(variable) {
            if (
                    variable === "" 
                    || variable === [] 
                    || variable === {}
                    || variable === "[]"
                    || variable === "{}"
            ){
                return true;
            }
            else{
                return false;
            }
        },

        setLoading(loader_label) {
            this.loader = loader_label;
        },

        isLoaderShowing(where_to_show) {
            if (this.loader == where_to_show){
                return true;
            }
            else{
                return false;
            }
        },

        stopLoading() {
            this.loader = null;
        },

        getArrayLastElement(array) {
            if (array.length === 0){
                return null;
            }
            else{
                return array[array.length - 1];
            }
        },

        addClassToElement(element, classname) {
            var classString = element.className; 

            if (classString != undefined && classString.indexOf(classname) == -1){
                var newClass = classString.concat(" " + classname); 
                element.className = newClass;
            }
        },

        highlightText(element, text) {
            var innerHTML = element.innerHTML;
            var index = innerHTML.indexOf(text);

            if (index >= 0) { 
                innerHTML = 
                        innerHTML.substring(0, index) 
                        + '<span class="highlight-text">' 
                        + innerHTML.substring(index, index+text.length) 
                        + '</span>' 
                        + innerHTML.substring(index + text.length);
                element.innerHTML = innerHTML;
            }
        },

        unhighlightText() {
            var elements = document.getElementsByClassName('highlight-text');

            if (elements.length > 0){
                for (var key in elements){
                    var element = elements[key];
                    var parent  = element.parentNode;

                    if (parent != undefined){
                        parent.innerHTML = parent.innerHTML.replace('<span class="highlight-text">' , "");
                        parent.innerHTML = parent.innerHTML.replace('</span>'  , "");
                    }
                }
            }
        },

        removeClassFromElement(element, classname) {
            var classString = element.className; 

            if (classString != undefined){
                var newClass = classString.replace(classname, "");
                element.className = newClass;
            }
        },

        hideElementsByClass(classname) {
            var elements = document.getElementsByClassName(classname);

            if (elements.length > 0){
                for (var index = 0; index < elements.length; index++){
                    let element = elements[index];
                    element.style.display = "none";
                }
            }
        },

        isChecked(checkbox_event) {
            if (this.isset(checkbox_event.target)){
                return checkbox_event.target.checked;
            }
            else{
                return false;
            }
        },

        unsetById(array, item_id) {
            return array.filter(item => item.id != item_id);
        },

        copyToClipboard(text) {
            var wrapper       = document.createElement('span');
            wrapper.innerText = text;
            document.body.append(wrapper);

            var range = document.createRange();
            range.selectNodeContents(wrapper);

            window.getSelection().removeAllRanges();
            window.getSelection().addRange(range);

            var copied = document.execCommand('copy');

            window.getSelection().removeAllRanges();
            wrapper.remove();

            return copied;
        },

        convertKbToMb(value_in_kb) {
            return Math.round(value_in_kb / 1024);
        },

        showSumAsRubles(sum) {
            return (parseFloat(sum)).toLocaleString('ru') + " RUB";
        },

        reduceString(string, length) {
            return string.substr(0, length) + "...";
        },

        getRandomString(length = 8) {
            var result           = '';
            var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var charactersLength = characters.length;

            for (var i = 0; i < length; i++){
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }

            return  result;
        },
        
        csrfToken() {
            return document.querySelector('meta[name=csrf-token]').content;
        },
    }
    
}

export {Utils};