<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 19.09.18
 * Time: 20:45
 */

return [
    "for_what"                                  => "For what",
    "email_already_taken"                       => "E-mail already exists. Restore password or register another.",
    "thanks_for_request"                        => "Thanks for your request! We will contact you by E-mail.",
];