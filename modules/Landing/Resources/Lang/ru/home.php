<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 19.09.18
 * Time: 20:45
 */

return [
    "for_what"                                  => "Для чего",
    "email_already_taken"                       => "E-mail уже зарегистрирован. Восстановите пароль или попробуйте другой E-mail.",
    "thanks_for_request"                        => "Спасибо за Вашу заявку! Мы свяжемся с Вами через E-mail.",
];