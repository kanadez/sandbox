<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 19.09.18
 * Time: 16:35
 */

namespace Modules\Landing\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class LocaleMiddleware
{
    public function handle($request, Closure $next)
    {
        $sessionLocale = session()->get("locale");

        if(empty($sessionLocale)) {
            App::setLocale($this->getLocale($request));
        } else {
            App::setLocale($sessionLocale);
        }

        return $next($request);
    }

    private function getLocale(Request $request): string
    {
        $locale = strtolower(substr($request->server('HTTP_ACCEPT_LANGUAGE'), 0, 2));

        if (!in_array($locale, [
            "en",
            "ru",
        ])) {
            return 'en';
        }

        return $locale;
    }
}