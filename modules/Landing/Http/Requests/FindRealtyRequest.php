<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 24.07.18
 * Time: 8:56
 */

namespace Modules\Landing\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FindRealtyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address' => 'required|array|min:1',
        ];
    }

}