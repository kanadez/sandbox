<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 24.07.18
 * Time: 8:56
 */

namespace Modules\Landing\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class CreateProxyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:3',
            'email'  => 'required|string|email|max:255|unique:users,email',
            //'g-recaptcha-response'=>'required|recaptcha',
            'telegram'=>'nullable|string',
            'skype'=>'nullable|string',
        ];
    }

    public function messages()
    {
        return array_merge(
            parent::messages(),
            [
                "email.unique" => Lang::get("landing::home.email_already_taken"),
            ]
        );
    }
}