<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 28.09.18
 * Time: 11:12
 */

namespace Modules\Landing\Http\Controllers\Web;

use Illuminate\Support\Facades\App;

class LanguageController extends BaseController
{
    public function change(string $lang) {
        session()->put("locale", $lang);
        App::setLocale($lang);
        flash()->success(__("lk::lk.switch_lang"));
        return redirect()->back();
    }
}