<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 18.07.18
 * Time: 15:43
 */

namespace Modules\Landing\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use GoogleMaps\GoogleMaps;

use Modules\Core\Model\User;

use Modules\Landing\Http\Requests\FindRealtyRequest;
use Modules\Landing\Http\Requests\StorePropertyReviewRequest;

use Modules\Core\Repository\CollectedPropertyThumbRepository;
use Modules\Core\Repository\PropertiesReviewsRepository;

use Modules\Core\Criteria\FindRealtyCriteria;

class LandingController extends BaseController
{
    /*
     * @var CollectedPropertyThumbRepository
     */
    private $collectedPropertyThumbRepository;
    
    /*
     * @var PropertiesReviewsRepository
     */
    private $propertiesReviewsRepository;
    
    public function __construct(CollectedPropertyThumbRepository $collectedPropertyThumbRepository,
                                PropertiesReviewsRepository $propertiesReviewsRepository) 
    {
        $this->collectedPropertyThumbRepository = $collectedPropertyThumbRepository;
        $this->propertiesReviewsRepository      = $propertiesReviewsRepository;
    }


    public function index()
    {
        //return view("landing::web.index", []);
        return 123;
    }
    
    public function searchRealty()
    {
        /*$response = \GoogleMaps::load('geocoding')
                        ->setParam (['address' =>'santa cruz'])
                        ->get();
        */
        
        return view("landing::web.search-realty", []);
    }
    
    public function searchPeople()
    {
        return view("landing::web.search-people", []);
    }
    
    public function findRealty(FindRealtyRequest $request)
    {
        $data       = $request->validated();
        
        $properties = $this->collectedPropertyThumbRepository
                           ->with("image")
                           ->pushCriteria(new FindRealtyCriteria($data["address"]))
                           ->all();
        
        return response()->json([
            "status" => "success",
            "data"   => $properties
        ]);
    }
    
    public function propertyDetails(int $property_id)
    {
        $property =   $this->collectedPropertyThumbRepository
                           ->with("image")
                           ->find($property_id);
        
        return view("landing::web.property-details", [
            "property"  => $property->toArray()
        ]);
    }
    
    public function addPropertyReview(int $property_id)
    {
        if (!Auth::check()){ // если юзер не авторизован
            // в будущем - редирект на страницу авторизации, авторизация, оттуда редирект сюда
            // сейчас - автоматическая авторизация как аноним
            // через ларатраст
            // установить ларатраст, наследовать его трейт в UserRoleTrait, там сделать ф-ию authAsAnonym(), 
            // и уже там прописать ид аноноима (создав его) и сделать его авторизацию. Сделать статичным. 
            // здесь вызывать толкьо authAsAnonym()
            User::authAsAnonym();
        }
        
        return view("landing::web.add-property-review", [
            "property_id" => $property_id
        ]);
    }
    
    public function storePropertyReview(StorePropertyReviewRequest $request)
    {
        $data = $request->validated();
        $new_review_data = [
            "user_id"       => Auth::user()->id,
            "property_id"   => $data["property_id"],
            "review_text"   => $data["review_text"],
        ];
        $this->propertiesReviewsRepository->create($new_review_data);
        
        return response()->json([
            "status" => "success"
        ]);
    }
}