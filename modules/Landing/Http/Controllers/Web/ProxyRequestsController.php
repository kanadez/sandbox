<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 24.07.18
 * Time: 9:02
 */

namespace Modules\Landing\Http\Controllers\Web;

use Illuminate\Support\Facades\Mail;
use Modules\Core\Criteria\ClientRoleCriteria;
use Modules\Core\Mail\ProxyRequestCreated;
use Modules\Core\Model\ProxyRequest;
use Modules\Core\Model\Role;
use Modules\Core\Model\User;
use Modules\Core\Repository\ProxyRequestsRepository;
use Modules\Core\Repository\RolesRepository;
use Modules\Core\Repository\UsersRepository;
use Modules\Landing\Http\Requests\CreateProxyRequest;


class ProxyRequestsController extends BaseController
{
    /**
     * @var ProxyRequestsRepository
     */
    private $requestsRepository;
    /**
     * @var UsersRepository
     */
    private $usersRepository;
    /**
     * @var Role
     */
    private $role;

    /**
     * ProxyRequestsController constructor.
     * @param ProxyRequestsRepository $requestsRepository
     * @param UsersRepository $usersRepository
     * @param RolesRepository $rolesRepository
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function __construct(ProxyRequestsRepository $requestsRepository,
                                UsersRepository $usersRepository,
                                RolesRepository $rolesRepository)
    {
        $this->requestsRepository = $requestsRepository;
        $this->usersRepository = $usersRepository;
        $this->role = $rolesRepository->pushCriteria(ClientRoleCriteria::class)->all()->first();
    }

    /**
     * @param CreateProxyRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(CreateProxyRequest $request)
    {
        $data = $request->validated();

        $password = str_random(12);
        // Create user
        /** @var User $user */
        $user = $this->usersRepository->create(
            array_merge(
                ["password" => bcrypt($password)],
                $data
            )
        );
        $user->attachRole($this->role);

        // Register proxy request
        /** @var ProxyRequest $proxyRequest */
        $proxyRequest = $this->requestsRepository->create(
            array_merge([
                "user_id" => $user["id"],
            ], $data)
        );

        $message = (new ProxyRequestCreated($user["name"], $user["email"], $password))
            ->onQueue(config("mail.queue", "emails"));

        Mail::queue($message);

        return response()->json([
            "status" => 0,
        ]);
    }
}