module.exports = function (app, module) {
    // Copy fonts
    app.mix
        .copyDirectory(module.paths.assets + 'pRESIDENTPROXY/img', 'public/img')
        .extract([
            'popper.js',
            'vue',
            'bootstrap',
            'slick-carousel',
        ]);
};