<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 10.09.18
 * Time: 11:52
 */
?>
@extends('lk::layouts.lk')

@section('content')
    @include('core::admin.forms.errors')
    <div class="row">
        @foreach($products as $product)
            <div class="col-md-6 col-xl-4">
                <div class="card">
                    {{ Form::open(["url" => route("lk.cart.add", [$product])]) }}
                    <div class="card-header">{{ $product["name"] }}</div>
                    <div class="card-body">{!! $product["description"] !!}</div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-7">
                                <div class="input-group">
                                    <span class="input-group-prepend">
                                        <a class="btn btn-warning catalog-minus" href="#" title="Edit">
                                            <i class="fa fa-minus "></i>
                                        </a>
                                    </span>
                                    <input type="number" class="form-control" name="quantity" value="1" min="1">
                                    <span class="input-group-append">
                                        <a class="btn btn-info catalog-plus" href="#" title="Edit">
                                            <i class="fa fa-plus "></i>
                                        </a>
                                    </span>
                                </div>
                            </div>
                            <div class="col-5">
                                <div class="float-right">
                                    <button class="btn btn-success" type="submit"><i class="fa fa-cart-plus"></i> @lang("lk::catalog.add")</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>

        @endforeach
    </div>
@endsection
