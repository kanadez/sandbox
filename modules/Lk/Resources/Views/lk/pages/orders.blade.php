@extends('lk::layouts.lk')


@section('content')
    <div class="row">
        <div class="col-md-12">
            <table class="table" id="orders">
                <thead>
                    <th>#</th>
                    <th>{{ trans('lk::tables.name') }}</th>
                    <th>{{ trans('lk::tables.network_type') }}</th>
                    <th>{{ trans('lk::tables.mounthly_traffic') }}</th>
                    <th>{{ trans('lk::tables.geolocation') }}</th>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
@overwrite
