@extends('lk::layouts.lk')


@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <button class="btn btn-primary" id="add-proxy-port"><i class="fa fa-plus mr-2"></i> {{ trans('lk::buttons.add_proxy_port') }}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">{{ trans('lk::forms.proxy_ports') }}</div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table" id="ports">
                        <thead>
                            <tr>
                              <th>#</th>
                              <th>{{ trans('lk::tables.name') }}</th>
                              <th>{{ trans('lk::tables.network_type') }}</th>
                              <th>{{ trans('lk::tables.traffic_volume') }}</th>
                              <th>{{ trans('lk::tables.created_at') }}</th>
                              <th></th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @include('lk::lk.modals.proxy.port')
@overwrite
