<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 17.09.18
 * Time: 16:46
 */
?>
@extends('lk::layouts.lk')

@section('content')
    <div class="card">
        <div class="card-header">
            Success
        </div>
        <div class="card-body">
            You have successfully paid order №{{$order["number"]}}.
        </div>
    </div>
@endsection
