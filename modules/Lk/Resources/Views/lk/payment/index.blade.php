<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 17.09.18
 * Time: 14:44
 */
?>
@extends('lk::layouts.lk')

@section('content')
    <div class="card">
        <div class="card-header">
            Your payments
        </div>
        <div class="card-body">
            <table class="table table-responsive-sm">
                <thead>
                <tr>
                    <th>Order №</th>
                    <th>Date creation</th>
                    <th>Sum</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                @foreach($payments as $payment)
                    <tr>
                        <td>{{ $payment->order["number"] }}</td>
                        <td>{{ $payment["created_at"] }}</td>
                        <td>{{ $payment->order["price"] }}{!! $payment->order->currency["short_name"] !!} </td>
                        <td>
                            @paymentStatus($payment->status)
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
