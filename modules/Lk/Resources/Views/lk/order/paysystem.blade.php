<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 14.09.18
 * Time: 12:32
 */
?>
@extends('lk::layouts.lk')

@section('content')
    {{ Form::open(["url" => route("lk.order.create")]) }}
    @foreach($paysystems as $paysystem)
        <div class="card">
            <div class="card-header">@lang(sprintf("lk::paysystem.%s", $paysystem["name"]))</div>
            <div class="card-body">
                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                    <ul>
                        @foreach($paysystem->methods as $method)
                        <li class="btn btn-light">
                            <input type="radio" name="paysystem_method_id" autocomplete="off" value="{{ $method["id"] }}">
                            @if (!empty($img = $method->getFirstMedia('paysystem_methods')))
                                <img class="payment-method-logo" src="{{ $img->getUrl() }}" alt="">
                            @else
                                <span>{{ $method["name"] }}</span>
                            @endif
                        </li>
                        @endforeach
                    </ul>
                </div>

            </div>
        </div>
    @endforeach
    <div class="clearfix">
        <div class="float-right">
            <input class="btn btn-success" type="submit" value="@lang("lk::paysystem.pay", [ "price" => $cart->getPrice() . $cart->getCurrency()->short_name ])">
        </div>
    </div>
    {{ Form:: close() }}
@endsection
