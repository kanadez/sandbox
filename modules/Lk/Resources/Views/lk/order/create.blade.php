<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 11.09.18
 * Time: 11:57
 */
?>
@extends('lk::layouts.lk')

@section('content')
<!--<h1>@lang("lk::order.create_h1")</h1>-->
<div class="row">
    <div class="col-md-12">
    <div class="card">
        @if ($cart->getItems()->count() > 0)
        <div class="card-body">
            <table class="table table-responsive-sm table-sm">
                <thead>
                <tr>
                    <th>@lang("lk::order.product")</th>
                    <th>@lang("lk::order.price")</th>
                    <th>@lang("lk::order.quantity")</th>
                    <th class="text-center">@lang("lk::order.subtotal")</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($cart->getItems() as $item)
                    <tr>
                        <td data-th="Product">{{ $item->getProduct()->name }}</td>
                        <td data-th="Price">{{ $item->getProduct()->getPrice() }}{!! $item->getCurrency()->short_name !!}</td>
                        <td data-th="Quantity">{{ $item->getQuantity() }}</td>
                        <td data-th="Subtotal" class="text-center">{{ $item->getPrice() }}{!! $item->getCurrency()->short_name !!}</td>
                        <td>
                            <a class="text-danger" href="{{ route("lk.cart.remove", [$item->getProduct()]) }}"><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <td></td>
                    <td></td>
                    <td>

                    </td>
                    <td class="text-center"><strong>@lang("lk::order.total"): {{ $cart->getPrice() }}{!! $cart->getCurrency()->short_name !!}</strong></td>
                    <td></td>
                </tr>
                </tfoot>
            </table>
        </div>
        <div class="card-footer">
            <div class="clearfix">
                <a class="btn btn-success float-right" href="{{ route("lk.order.paysystem") }}">@lang("lk::paysystem.paysystem") <i class="fa fa-angle-right"></i></a>
            </div>
        </div>
        @else
        <div class="card-body text-center">
            <p>
                @lang("lk::order.cart_empty_for_now")
            </p>
            <div class="col-12">
                <div>
                    <a href="{{ route("lk.catalog.index") }}" class="btn btn-success"><i class="fa fa-plus "></i>@lang("lk::order.add_proxy")</a>
                </div>
            </div>
        </div>
        @endif
</div>
</div>
</div>
@endsection
