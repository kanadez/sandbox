<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 19.09.18
 * Time: 9:52
 */
?>
@extends('lk::layouts.lk')

@section('content')
    <div class="card">
        <div class="card-header">
            @lang("lk::order.index_h1")
        </div>
        <div class="card-body">
            <table class="table table-responsive-sm">
                <thead>
                <tr>
                    <th>№</th>
                    <th>@lang("lk::order.created_ad")</th>
                    <th>@lang("lk::order.price")</th>
                    <th>@lang("lk::order.status")</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $order)
                    <tr>
                        <td><a href="{{ route("lk.order.details", [$order]) }}">@lang("lk::order.order") №{{ $order["number"] }}</a></td>
                        <td>{{ $order["created_at"] }}</td>
                        <td>{{ $order["price"] }}{!! $order->currency["short_name"] !!} </td>
                        <td>
                            @status($order->getStatusId())
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
