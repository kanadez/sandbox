<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 19.09.18
 * Time: 14:39
 */
?>
@extends('lk::layouts.lk')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    @lang("lk::order.details")
                    <div class="float-right">
                        @status($order->getStatusId())
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-responsive-sm">
                        <thead>
                        <tr>
                            <th>@lang("lk::product.id")</th>
                            <th>@lang("lk::product.name")</th>
                            <th>@lang("lk::product.price")</th>
                            <th>@lang("lk::product.quantity")</th>
                            <th>@lang("lk::product.sum")</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($order->items as $item)
                            <tr>
                                <td>{{ $item->product["id"] }}</td>
                                <td>{{ $item->product["name"] }}</td>
                                <td>{{ $item->product->getPrice() }}{!! $item->product->price->currency["short_name"] !!}</td>
                                <td>{{ $item["quantity"] }}</td>
                                <td>{{ $item["price"] }}{!! $item->currency["short_name"] !!} </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    <b class="float-right">@lang("lk::product.total"): {{ $order["price"] }}{!! $order->currency["short_name"] !!}</b>
                </div>
            </div>
        </div>
    </div>
@endsection
