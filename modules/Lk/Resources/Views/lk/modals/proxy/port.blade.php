<div class="modal" id="port" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <form method="POST" action="#" data-action-store="{{ route('api.ports.store') }}" data-action-update="{{ route('api.ports.update', ['port' => '[id]']) }}">
            <div class="modal-header">
              <h5 class="modal-title">{{ trans('lk::forms.port_info') }}</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="modal-body">
                <div class="alert alert-primary request-error d-none"></div>
                <div class="row mb-4">
                    <div class="col-md-12">
                        <div class="row mb-2">
                            <div class="form-group col-sm-12">
                                <label for="name">{{ trans('lk::forms.name') }}</label>
                                <input type="text" name="name" class="form-control" id="name" >
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="geo_type">{{ trans('lk::forms.country') }}</label>
                                <select class="form-control" name="country_id" data-default-value="182">
                                    @foreach(\Modules\Lk\Model\Country::get() as $country)
                                        <option value="{{ $country->id }}">{{ $country->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="geo_type">{{ trans('lk::forms.city') }}</label>
                                <select class="form-control" name="city_id" data-default-value="99972">

                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="network_type">{{ trans('lk::forms.network_type') }}</label>
                                <select class="form-control" name="network_type_id" data-default-value="3">
                                    @foreach(\Modules\Core\Model\Network\Type::get() as $type)
                                        <option value="{{ $type->id }}">{{ $type->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="ip_type1">{{ trans('lk::forms.traffic_volumes') }}</label>
                                <select class="form-control" name="traffic_volume_id" data-default-value="1">
                                    @foreach(\Modules\Core\Model\Traffic\Volume::get() as $volume)
                                        <option value="{{ $volume->id }}">{{ $volume->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <!--div class="row">
                            <div class="form-group col-sm-12">
                                <div class="col-sm-4 col-form-label"></div>
                                <div class="col-sm-8">
                                    <input type="checkbox" name="use_unlimited_traffic" class="mr-2" id="use_unlimited_traffic" >
                                    <label for="use_unlimited_traffic" class="col-form-label">{{ trans('lk::forms.use_unlimited_traffic') }}</label>
                                </div>
                            </div>
                        </div-->
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="pull_size" >{{ trans('lk::forms.pull_size') }}</label>
                                <input type="text" name="pull_size" class="form-control" id="pull_size">
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="pull_size" >{{ trans('lk::forms.rotation') }}</label>
                                <input type="text" name="rotation" class="form-control" id="rotation" data-default-value="10">
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="pull_size" >-</label>
                                <select class="form-control" name="rotation_time_type" data-default-value="1">
                                    <option value="1">{{ trans('lk::forms.min') }}</option>
                                    <option value="2">{{ trans('lk::forms.h') }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mb-2">

                            <div class="form-group col-sm-6">
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        <label for="name">{{ trans('lk::forms.login') }}</label>
                                        <input type="text" name="username" class="form-control" id="name" >
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-12 mb-0">
                                        <label for="name">{{ trans('lk::forms.password') }}</label>
                                        <input type="text" name="password" class="form-control" id="name" >
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="allowed_ip">{{ trans('lk::forms.allowed_ip') }}</label>
                                <textarea name="allowed_ip" class="form-control" id="allowed_ip" rows="4"></textarea>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-sm-3">
                                <label for="uptime" >{{ trans('lk::forms.uptime') }}</label>
                                <input type="text" name="uptime" class="form-control" id="uptime">
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="latency" >{{ trans('lk::forms.latency') }}</label>
                                <input type="text" name="latency" class="form-control" id="latency">
                            </div>
                            <div class="form-group col-sm-2">
                                <label for="max_connections" >{{ trans('lk::forms.max_connections') }}</label>
                                <input type="text" name="max_connections" class="form-control" id="max_connections" data-default-value="50">
                            </div>
                            <div class="form-group col-sm-2">
                                <label for="bandwidth_in" >{{ trans('lk::forms.bandwidth_in') }}</label>
                                <input type="text" name="bandwidth_in" class="form-control" id="bandwidth_in" data-default-value="2048">
                            </div>
                            <div class="form-group col-sm-2">
                                <label for="bandwidth_out" >{{ trans('lk::forms.bandwidth_out') }}</label>
                                <input type="text" name="bandwidth_out" class="form-control" id="bandwidth_out" data-default-value="2048">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-danger">
                                Стоимость: <span class="price">0</span> р.
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" name="save"><i class="fa fa-save mr-2"></i>{{ trans('lk::forms.save') }}</button>
            </div>
        </form>
    </div>
  </div>
</div>
