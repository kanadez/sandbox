
@extends('lk::layouts.lk')

@section('content')
<!--<h1>@lang("lk::order.create_h1")</h1>-->
        <div class="card">
            <div class="card-header">
                @lang("lk::order.create_h1")
            </div>
            <div class="card-body">
                {{ Form::model($item, ['route' => ['lk.proxy-port.store', $item], "class" => "user-edit-form form-horizontal form-border" ]) }}
                @include('core::admin.forms.errors')

                {!! Form::admText('name', Lang::get('lk::proxy_port.Name')) !!}

                {!! Form::admSelect('type', Lang::get('lk::proxy_port.Type'), ['residental' => 'Residental', 'mobile' => 'Mobile', 'datacenter'=> 'Datacenter'], ['placeholder' => Lang::get('lk::proxy_port.pick_type')]); !!}

                {!! Form::admSelect('ip_type', Lang::get('lk::proxy_port.ip_type'), ['static' => 'Static', 'dinamic' => 'Dinamic'], ['placeholder' => Lang::get('lk::proxy_port.pick_type')]); !!}

                {!! Form::admText('time_ip_change', Lang::get('lk::proxy_port.time_ip_change')) !!}

                {!! Form::admText('pull_size', Lang::get('lk::proxy_port.pull_size')) !!}

                {!! Form::admSelect('node_id', Lang::get('lk::proxy_port.node'), \Modules\Core\Model\Node::pluck('name','id'), ['placeholder' => Lang::get('lk::proxy_port.pick_node'), 'data-live-search' => "true"]); !!}

                <div class="form-group">
                    <label for="trafic" class="col-sm-3 control-label">{{ Lang::get('lk::proxy_port.trafic') }} <span class="selectedTrafic"></span></label>
                    <div class="col-sm-6">
                        <input value="<?php if($item->exists) {echo$item['trafic'];}else{echo 20;}?>" class="form-control" min="20" max="1000" name="trafic" type="range" id="trafic">
                    </div>
                </div>            

                {!! Form::admSelect('country_id', Lang::get('lk::proxy_port.country'), \Modules\Core\Model\CountryForProxyPort::pluck('name','id'), ['placeholder' => Lang::get('lk::proxy_port.pick_country'), 'data-live-search' => "true"]); !!}

                @if($item->exists)
                 <div class="city-selector-wrapper">
                {!! Form::admSelect('city_id', Lang::get('lk::proxy_port.city'), \Modules\Core\Model\CityForProxyPort::where('country_id', $item['country_id'])->pluck('name','id'), ['placeholder' => Lang::get('lk::proxy_port.pick_city'), 'data-live-search' => "true"]); !!}
                <div style="display: none;" class="loading-cities"></div>
                </div>
                @else
                <div class="city-selector-wrapper">
                {!! Form::admSelect('city_id', Lang::get('lk::proxy_port.city'), [], ['placeholder' => Lang::get('lk::proxy_port.pick_city'), 'data-live-search' => "true"]); !!}
                <div class="loading-cities"></div>
                </div>

                @endif

                {!! Form::admText('username','Username') !!}
                {!! Form::admText('password', 'Password') !!}
                {!! Form::admText('peer_rotate', 'Peer rotate') !!}
                {!! Form::admText('fingerprint', 'Fingerprint') !!}
                {!! Form::admText('asn', 'ASN') !!}
                {!! Form::admText('uptime', 'Uptime') !!}
                {!! Form::admNumber('latency', 'Latency') !!}
                {!! Form::admText('speed_download', 'Speed download') !!}
                {!! Form::admText('speed_upload', 'Speed upload') !!}
                {!! Form::admText('allowed_ip', 'Allowed IPs(list divide by ;)') !!}
                {!! Form::admNumber('max_connections', 'Max connections') !!}
                {!! Form::admText('bandwidth_in', 'Bandwidth In') !!}
                {!! Form::admText('bandwidth_out', 'Bandwidth Out') !!}
                {!! Form::admText('time_limit', 'Time Limit') !!}

                {!! Form::admSubmit($item->exists ? 'Update' : 'Create') !!}
                {{ Form::close() }}
            </div>
        </div>
@endsection
