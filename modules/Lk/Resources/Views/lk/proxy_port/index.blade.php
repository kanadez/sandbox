
@extends('lk::layouts.lk')

@section('content')

    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 top-panel">
        <a class="btn btn-success" href="{{ route("lk.proxy-port.create") }}">
            <i class="fa fa-plus"></i> <span>@lang("lk::proxy_port.add_new")</span>
        </a>
    </div>
    <div class="card">
        <div class="card-header">
            @lang("lk::proxy_port.index_h1")
        </div>
        <div class="card-body">
            @if($errors->any())
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                    {{ $errors->first() }}
                </div>
            @endif
            <table class="table table-responsive-sm">
                <thead>
                <tr>
                    <th>@lang("lk::proxy_port.Name")</th>
                    <th>@lang("lk::proxy_port.Type")</th>
                    <th>@lang("lk::proxy_port.trafic")</th>
                    <th>@lang("lk::proxy_port.created_at")</th>
                    <th><!--@lang("lk::proxy_port.actions")--></th>
                </tr>
                </thead>
                <tbody>
                @foreach($proxyPorts as $proxyPort)
                    <tr>
                        <td><a href="{{ route("lk.proxy-port.details", [$proxyPort]) }}">{{ $proxyPort["name"] }}</a></td>
                        <td>{{ $proxyPort["type"] }}</td>
                        <td>{{ $proxyPort["trafic"] }}</td>
                        <td>{{ $proxyPort["created_at"] }}</td>
                        <td>
                             <a class="btn btn-as-link" href="{{ route("lk.proxy-port.edit", $proxyPort) }}" title="Edit">
                                        <i class="fa fa-pencil "></i>
                            </a>
                            <a class="btn btn-as-link" href="{{ route("lk.proxy-port.delete", $proxyPort) }}" title="Remove" data-confirm="@lang("lk::proxy_port.confirm_remove") {{ $proxyPort["name"] }}?">
                                <i class="fa fa-trash-o "></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
