<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 24.07.18
 * Time: 10:49
 */
?>
@extends('lk::layouts.lk')

@section('content')
    @include('core::admin.forms.errors')
    <div class="row">
        <div class="col-md-12 col-xl-12">
            <div class="card">
                <div class="card-body text-center">
                    <p>
                        @lang("lk::dashboard.no_purchased_proxy_yet")
                    </p>
                    <div class="col-12">
                        <div>
                            <a href="{{ route("lk.catalog.index") }}" class="btn btn-success"><i class="fa fa-cart-plus"></i> @lang("lk::dashboard.buy_proxy")</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    {{--<h2>Test robokassa</h2>--}}
    {{--{{ Form::open(["route" => "lk.pay.robokassa"]) }}--}}
    {{--{{ Form::admNumber("sum", "Price", ["step" => "0.01", "required" => "required"]) }}--}}
    {{--{{ Form::admSubmit("Proceed") }}--}}
    {{--{{ Form:: close() }}--}}

    {{--<h2>Test interkassa</h2>--}}
    {{--{{ Form::open(["route" => "lk.pay.interkassa"]) }}--}}
    {{--{{ Form::admNumber("sum", "Price", ["step" => "0.01", "required" => "required"]) }}--}}
    {{--{{ Form::admSubmit("Proceed") }}--}}
    {{--{{ Form:: close() }}--}}
@endsection