<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 24.07.18
 * Time: 8:25
 */
?>
        <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="Client area">
    <meta name="keyword" content="Client area">
    <title>Client area</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link href="{{ mix("/css/lk.css", 'assets/lk') }}" rel="stylesheet">
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
<header class="app-header navbar">

    <a href="{{ route("web.index") }}" class="logo">
        <span class="logo--upcase"><span class="logo--blue">ASTRO</span>PROXY</span>.COM
    </a>
    <button class="navbar-toggler sidebar-toggler mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <button class="navbar-toggler sidebar-toggler-mobile" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>


    <div class="navbar-toggler cursor-default">
    </div>
    <ul class="nav navbar-nav px-3 ml-auto">
        <!--<li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <i class="font-lg cui-cart">
                    <span class="badge badge-pill {{ count($cart) > 0 ? 'badge-warning' : 'badge-danger' }}"> {{ count($cart) }}</span>
                </i>
            </a>
            @if(count($cart) > 0 )
                <div class="dropdown-menu dropdown-menu-right">
                    <div class="dropdown-header text-center">
                        <strong>@lang("lk::order.total") - {{ $cart->getPrice() }}{!! $cart->getCurrency()->short_name !!}</strong>
                    </div>
                    @foreach($cart->getItems() as $item)
                        <div class="dropdown-item" >
                            <a href="{{ route("lk.cart.remove", [$item->getProduct()]) }}" title="Remove">
                                <i class="text-danger fa fa-times" aria-hidden="true"></i>
                            </a>
                            {{ $item->getProduct()->name }} - {{ $item->getQuantity() }} - {{ $item->getPrice() }}{!! $item->getCurrency()->short_name !!}
                        </div>
                    @endforeach
                    <a class="dropdown-item text-center" href=" {{ route("lk.order.make") }}">
                        <strong><i class="fa fa-cart-plus"></i>@lang('lk::order.checkout')</strong>
                    </a>
                </div>
            @endif
        </li>-->
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                {{--<img class="img-avatar" src="img/avatars/6.jpg" alt="admin@bootstrapmaster.com">--}}
                <i class="fa fa-user"></i>
                <span>{{ $user["name"] }}</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="{{ route("lk.logout") }}">
                    <i class="fa fa-lock"></i> Logout</a>
            </div>
        </li>
        <li class="nav-item with-select">
            <i class="fa fa-globe near-select"></i>
            <select class="language-picker" data-width="fit">
                <option data-content='English' value="en" @if(App::getLocale() == "en") selected @endif>English</option>
                <option data-content='Русский' value="ru" @if(App::getLocale() == "ru") selected @endif>Русский</option>
            </select>
        </li>
    </ul>
</header>
<div class="app-body">
    <div class="sidebar">
        <!-- ToDO: refactor -->
        @include('core::admin.menu.menu', ['items' => Menu::get('LkMenu')])
        {{--<button class="sidebar-minimizer brand-minimizer" type="button"></button>--}}
    </div>
    <main class="main">
        <!--{{ Breadcrumbs::render() }}-->
        @include('core::admin.flash.message')
        <div class="container-fluid">
            <div class="animated fadeIn">
                @yield('content')
            </div>
        </div>
    </main>
</div>
<footer class="app-footer">
    <div>
        <a href="https://astroproxy.com">astroproxy.com</a>
        <span>&copy; 2018 astroproxy.com</span>
    </div>
    <div class="ml-auto">
        <span>Powered by</span>
        <a href="https://astroproxy.com">astroproxy.com</a>
    </div>
</footer>
<!-- CoreUI and necessary plugins-->
<script src="{{ mix("/js/manifest.js", 'assets/lk') }}"></script>
<script src="{{ mix("/js/vendor.js", 'assets/lk') }}"></script>

<script src="{{ mix("/js/lk.js", 'assets/lk') }}"></script>


</body>
</html>


