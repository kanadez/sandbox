$(document).ready(function() {
    $('.language-picker').selectpicker();

    $('.language-picker').on('changed.bs.select', function (e, clickedIndex, newValue, oldValue) {
        var selected = $(e.currentTarget).val();
        switch (selected) {
            case "ru":
                window.location.href = "/lk/lang/ru";
                break;
            case "en":
                window.location.href = "/lk/lang/en";
                break;
        }
    });
});


