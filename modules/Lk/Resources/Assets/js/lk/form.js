class Form {
    constructor(selector) {
        this.selector(selector);
    }

    selector(selector) {
        if(typeof selector == 'undefined') {
            return this._selector;
        }
        this._selector = selector;
        return this;
    }

    id(id) {
        if(typeof id == 'undefined') {
            return this._id;
        }
        this._id = id;
        return this;
    }

    lock(state) {
        $(this.selector()).find('input, textarea, select').prop('disabled', state == false ? false : true);
    }

    reset(fill_default_values) {
        $(this.selector()).attr('data-processing', 1);

        $(this.selector()).find('input, select, textarea').val(null);
        $(this.selector()).find('input[type="checkbox"]').prop('checked', false);

        if(fill_default_values) {
            $(this.selector()).find('[data-default-value]').each(function() {
                $(this).val($(this).attr('data-default-value')).trigger('change');
            });
        }

        this.id(null);

        $(this.selector()).attr('data-processing', 0);
        return this;
    }

    inputs(values) {
        console.log(values);
        if(!values) {
            let result = {};
            $(this.selector()).find('input[name], select[name], textarea[name]').each(function() {
                result[$(this).attr('name')] = $(this).val();
                if($(this).attr('type') == 'checkbox') {
                    result[$(this).attr('name')] = $(this).prop('checked') ? 1 : 0;
                }
            });
            return result;
        }
        this.reset();
        if(values['id']) {
            this.id(values['id']);
        }
        $(this.selector()).attr('data-processing', 1);
        for(var name in values) {
            console.log('NAME', name);
            let input = $(this.selector()).find('[name="' + name + '"]');
            if(!input.length || input.attr('type') == 'file') {
                continue;
            }
            if(input.attr('type') == 'checkbox') {
                input.prop('checked', values[name] != 0).trigger('change');
                continue;
            }
            input.val(values[name]);
        }
        $(this.selector()).attr('data-processing', 0);
        return Promise.resolve(values);
    }

    load(data) {
        return Promise.resolve(data).then(data => {
            this.inputs(data);
        });
    }

    save() {
        this.lock();
        let data = this.inputs();
        $(this.selector()).find('.validation-error').remove();
        $(this.selector()).find('.request-error').addClass('d-none');
        if(this.id()) {
            return api(decodeURI($(this.selector()).attr('data-action-update')).replace(/\[id\]/, this.id())).update(data).then(this.onSaveSuccess.bind(this)).catch(this.onSaveFail.bind(this));
        } {
            return api($(this.selector()).attr('data-action-store')).create(data).then(this.onSaveSuccess.bind(this)).catch(this.onSaveFail.bind(this));
        }
    }

    onSaveSuccess(data) {
        this.lock(false);
        return Promise.resolve(data);
    }

    onSaveFail(error) {
        this.lock(false);

        if(typeof error == 'string') {
            let div = $(this.selector()).find('.request-error');
            if(div.length) {
                div.html(error);
                div.removeClass('d-none');
            } else {
                notify().error('Ошибка', error);
            }
        } else {
            for(var key in error) {
                let name = key.replace(/\.(.*?)\.(.*)/, '[$1][$2]');
                $(this.selector()).find('[name="' + name + '"]').parent().append(`
                    <p class="text-danger text-small m-0 ml-3 mt-1 validation-error">${error[key].shift()}</p>
                `);
            }
            $(this.selector()).find('.request-error').html('Проверьте все поля').removeClass('d-none')
        }
        return Promise.reject(error);
    }
}

window['forms'] = function(selector) {
    if(!repository()['forms'] || !repository()['forms'][selector]) {
        repository()['forms'][selector] = new Form(selector);
    }
    return repository()['forms'][selector];
}
