class Api {

    constructor(path) {
        this.path(path);
    }

    path(path) {
        if(typeof path == 'undefined') {
            return this._path;
        }
        this._path = path;
        return this;
    }

    resource(resource) {
        if(typeof resource == 'undefined') {
            return this._resource;
        }
        this._resource = resource;
        return this;
    }

    method(method, url, data) {
        return new Promise(function(resolve, reject) {
            $.ajax( {
                    method: method,
              url: url,
              data: data,
                    cache: false,
              success: function(json) {
                  if(json['errors']) {
                      return reject(json['errors']);
                  }
                  if(json['code'] && json['code'] == 200) {
                      return resolve(json['data'], json);
                  }
                  if(json['code'] && json['code'] == 302) {
                      window.location = json['data']['url'];
                      return resolve({});
                  }
                  if(json['code']) {
                      return reject(json['text'], json);
                  }
                  return reject('не удалось выполнить запрос', json);
              },
              dataType: 'json'
            } ).fail(function(xhr, error) {
                    if(xhr.responseJSON && xhr.responseJSON['errors']) {
                        return reject(xhr.responseJSON['errors']);
                    } else if(xhr.responseJSON && xhr.responseJSON['exception']) {
                        return reject(xhr.responseJSON['message']);
                    }
                    return reject(error)
            }).always(function() {
            });
        });
    }

    upload(data) {
        var that = this;
        let url = that.path().indexOf('http://') === -1 ? '/api/' + that.path().replace('.', '/') : that.path();
        return new Promise(function(resolve, reject) {
            $.ajax({
                url: url, // point to server-side PHP script
                dataType: 'json',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data,
                type: 'post',
                success: function(json) {
                    if(!json) {
                        return reject('Не удалось выполнить запрос');
                    }
                    if(json['errors']) {
                        return reject(json['errors'][Object.keys(json['errors']).shift()][0]);
                    }
                    if(json['code'] && json['code'] == 200) {
                        return resolve(json['data'], json);
                    }
                    if(json['code'] && json['code'] == 302) {
                        window.location = json['data']['url'];
                        return;
                    }
                    if(json['code']) {
                        return reject(json['text'], json);
                    }
                    return reject('не удалось выполнить запрос', json);
    			},
             }).fail(function(xhr, error) {
                 reject(error)
             });
        });
    }

    create(data) {
        return this.method('POST', (this.resource().indexOf('http://') === -1 ? this.path() : '') + this.resource(), data);
    }

    update(data) {
        data = data || {};
        data['_method'] = 'PATCH';
        return this.method('POST', (this.resource().indexOf('http://') === -1 ? this.path() : '') + this.resource(), data);
    }

    delete(data) {
        data = data || {};
        data['_method'] = 'DELETE';
        return this.method('POST', (this.resource().indexOf('http://') === -1 ? this.path() : '') + this.resource(), data);
    }

    get(data) {
        return this.method('GET', (this.resource().indexOf('http://') === -1 ? this.path() : '') + this.resource(), data);
    }

}

window['api'] = function(resource) {
    if(!repository()['api']) {
        repository()['api'] = new Api(config.api.path);
    }
    if(!resource) {
        return repository()['api'];
    }
    return repository()['api'].resource(resource);
}
