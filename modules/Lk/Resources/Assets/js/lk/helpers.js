window['repository'] = function() {
    if(!window['app_repository']) {
        window['app_repository'] = {
            'api': null,
            'forms': {},
            'modals': {},
        };
    }
    return window['app_repository'];
}
