class Modal {
    constructor(selector) {
        this.selector(selector);

        this.listeners(null);

        $(this.selector()).find('[name="save"]').on('click', this.onClickSave.bind(this));
    }

    selector(selector) {
        if(typeof selector == 'undefined') {
            return this._selector;
        }
        this._selector = selector;
        return this;
    }

    listeners() {
        if(!this['_listeners']) {
            this._listeners = {};
        }
        return this._listeners;
    }

    on(event, cb) {
        if(!this.listeners()[event]) {
            this.listeners()[event] = [];
        }
        this.listeners()[event].push(cb);
    }

    emit(event, params) {
        if(!this.listeners()[event]) {
            return;
        }
        for(var i in this.listeners()[event]) {
            this.listeners()[event][i](params);
        }
        return;
    }

    form() {
        return forms($(this.selector()).find('form'));
    }

    open(data) {
        console.log(this.selector());
        this.form().reset(!data);
        return Promise.resolve(data).then(data => {
            $(this.selector()).modal('show');
            return this.form().inputs(data);
        });
    }

    close() {
        $(this.selector()).modal('hide');
    }

    save() {
        return this.form().save().then(result => {
            this.close();
            this.emit('save.success', result);
            return Promise.resolve(result);
        }).catch(error => {
            this.emit('save.fail', error);
            return Promise.reject(error);
        });
    }

    onClickSave() {
        return this.save();
    }
}


window['modals'] = function(selector) {
    if(!repository()['modals'] || !repository()['modals'][selector]) {
        repository()['modals'][selector] = new Modal(selector);
    }
    return repository()['modals'][selector];
}
