$(document).ready(() => {
    $('table[id="orders"]').DataTable( {
        searching: false,
        processing: true,
        serverSide: true,
        destroy: true,
        autoWidth: false,
        ordering: false,
        language: {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Russian.json"
        },
        ajax: {
            url: api().path() + 'orders',
        },
        columns: [
            {data: 'id'},
            {data: 'first_name'},
            {data: 'last_name'},
            {data: 'country.name'},
            {data: 'city.name'},
            {data: 'role.name'},
            {data: 'created_at'},
            {data: 'id'},
        ],
        columnDefs: [ {
                targets: 7,
                render: function(data, type, row) {
                    return `
                      <a href="#" class="edit fa fa-edit mr-2 text-info" data-id="${row.id}"></a>
                      <a href="#" class="remove fa fa-trash text-danger" data-id="${row.id}"></a>
                    `;
                }
            }
        ]
    } );
});
