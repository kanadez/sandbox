<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 19.09.18
 * Time: 20:22
 */

return [
    "buy_proxy"                             => "Купить прокси",
    "no_purchased_proxy_yet"                => "Вы пока что не приобрели ни одного прокси."
];