<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 20.09.18
 * Time: 8:31
 */

return [
    "paysystem" => "Способ оплаты",
    "robokassa" => "Робокасса",
    "interkassa" => "Интеркасса",
    "pay" => "Оплатить :price",
];