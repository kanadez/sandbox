<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 20.09.18
 * Time: 9:05
 */

return [
    "id" => "ID товара",
    "name" => "Название товара",
    "price" => "Цена",
    "quantity" => "Количество",
    "sum" => "Сумма",
    "total" => "Всего",
];