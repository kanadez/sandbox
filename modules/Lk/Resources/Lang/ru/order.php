<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 19.09.18
 * Time: 16:39
 */

return [
    "order" => "Заказ",
    "checkout" => "Перейти в корзину",
    "details" => "Детали заказа",
    "create" => "Создание заказ",
    "create_h1" => "Создать заказ",
    "total" => "Всего",
    "product" => "Товар",
    "price" => "Цена",
    "quantity" => "Количество",
    "subtotal" => "Сумма",
    "index_h1" => "Ваши заказы",
    "created_ad" => "Дата создания",
    "status" => "Статус",
    "cart_empty_for_now" => "Корзина пока пуста.",
    "add_proxy" => "Добавить прокси"
];