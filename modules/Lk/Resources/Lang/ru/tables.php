<?php

return [
    'name' => 'Название',
    'network_type' => 'Тип сети',
    'mounthly_traffic' => 'Месячный трафик',
    'geolocation' => 'Гео',
    'ip_type' => 'Тип IP',
    'created_at' => 'Создано',
    'traffic_volume' => 'Объем трафика',
];
