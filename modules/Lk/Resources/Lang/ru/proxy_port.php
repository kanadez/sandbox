<?php

return [ 
    "index_h1" => "Ваши прокси-порты",
    "add_new" => "Добавить новый прокси порт",
    "Name" => "Название",
    "Type" => "Тип",
    "pick_type" => "Выберите тип",
    "ip_type" => "Тип IP",
    "time_ip_change" => "Время смены IP",
    "pull_size" => "Размер пулла",
    "trafic" => "Трафик",
    "trafic_limits" => "мин - 20GB, макс - 1TB",
    "country" => "Страна",
    "pick_country" => "Выберите страну",
    "city" => "Город",
    "pick_city" => "Выберите город",
    "created_at" => "Дата создания",
    "proxy_port" => "Прокси порт",
    "edit" => "Изменить",
    "actions" => "Действия",
    "confirm_remove" => "Вы действительно хотите удалить прокси порт",
    "node" => "Нода",
    "pick_node" => "Выберите ноду",
    "month_costs" => "Ежемесячные расходы",
    "traffic_costs" => "Стоимость трафика",
    "ip_costs" => "Стоимость IP"
];