<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 19.09.18
 * Time: 20:33
 */

return [
    "dashboard" => "My proxies",
    "orders" => "Orders",
    "buy" => "Buy proxies",
    "cart" => "Cart",
    "proxy-ports" => "Proxy ports",
];