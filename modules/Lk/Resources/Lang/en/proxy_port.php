<?php

return [ 
    "index_h1" => "Your proxy ports",
    "add_new" => "Add new proxy port",
    "Name" => "Name",
    "Type" => "Type",
    "pick_type" => "Pick a type",
    "ip_type" => "IP type",
    "time_ip_change" => "IP change time",
    "pull_size" => "Size of pull",
    "trafic" => "Trafic",
    "trafic_limits" => "min - 20GB, max - 1TB",
    "country" => "Country",
    "pick_country" => "Pick a country",
    "city" => "City",
    "pick_city" => "Pick a city",
    "created_at" => "Created at",
    "proxy_port" => "Proxy port",
    "edit" => "Edit",
    "actions" => "Actions",
    "confirm_remove" => "Are you sure, that you want to remove proxy port",
    "node" => "Node",
    "pick_node" => "Pick a node",
    "month_costs" => "Costs per month",
    "traffic_costs" => "Trafic costs",
    "ip_costs" => "IP costs"
];