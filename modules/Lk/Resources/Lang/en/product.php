<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 20.09.18
 * Time: 9:05
 */

return [
    "id" => "Product ID",
    "name" => "Product name",
    "price" => "Price",
    "quantity" => "Quantity",
    "sum" => "Sum",
    "total" => "Total",
];