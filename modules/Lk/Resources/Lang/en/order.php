<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 19.09.18
 * Time: 16:39
 */

return [
    "order" => "Order",
    "checkout" => "Checkout",
    "details" => "Details",
    "create" => "Create order",
    "create_h1" => "Create order",
    "total" => "Total",
    "product" => "Product",
    "price" => "Price",
    "quantity" => "Quantity",
    "subtotal" => "Subtotal",
    "index_h1" => "Your orders",
    "created_ad" => "Date creation",
    "status" => "Status",
    "cart_empty_for_now" => "Cart is empty for now.",
    "add_proxy" => "Add proxy"
];