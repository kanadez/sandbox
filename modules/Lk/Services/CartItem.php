<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 10.09.18
 * Time: 13:35
 */

namespace Modules\Lk\Services;

use Illuminate\Contracts\Support\Jsonable;
use Modules\Core\Model\Product;
use Modules\Lk\Contracts\CartItemContract;
use RuntimeException;

class CartItem implements CartItemContract
{
    /**
     * @var Product
     */
    private $product;
    /**
     * @var int
     */
    private $quantity;

    /**
     * CartItem constructor.
     * @param Product $product
     * @param int $quantity
     */
    public function __construct(Product $product, int $quantity = 1)
    {
        if ($quantity < 1) {
            throw new RuntimeException("Wrong item count");
        }
        $this->product = $product;
        $this->quantity = $quantity;
    }

    public function getProductId(): int
    {
        return $this->product["id"];
    }

    public function increment(int $quantity = 1)
    {
        $this->quantity += $quantity;
    }

    public function decrement(int $quantity = 1)
    {
        $this->quantity -= $quantity;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    public function getPrice()
    {
        return $this->product->price["value"] * $this->quantity;
    }

    public function getCurrency()
    {
        return $this->product->price->currency;
    }

    public function getCurrencyId(): int
    {
        return $this->getCurrency()["id"];
    }

}