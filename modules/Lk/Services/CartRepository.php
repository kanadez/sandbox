<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 10.09.18
 * Time: 14:29
 */

namespace Modules\Lk\Services;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Session\SessionManager;
use Modules\Lk\Contracts\CartContract;
use Modules\Lk\Contracts\CartRepositoryContract;
use SuperClosure\Serializer;

class CartRepository implements CartRepositoryContract
{
    /**
     * @var SessionManager
     */
    private $sessionManager;
    /**
     * @var Dispatcher
     */
    private $events;
    /**
     * @var string
     */
    private $instance = "default";
    /**
     * @var Serializer
     */
    private $serializer;

    public function __construct(SessionManager $sessionManager, Dispatcher $events, Serializer $serializer)
    {
        $this->sessionManager = $sessionManager;
        $this->events = $events;
        $this->serializer = $serializer;
    }

    public function get(string $name = "default"): CartContract
    {
        $instance = $this->getInstanceName($name);

        if (empty($cartString = $this->sessionManager->get($instance))) {

            $cart = app(CartContract::class);

            $this->save($cart, $name);
        } else {
            $cart = unserialize($cartString);
        }

        return $cart;
    }

    public function save(Cart $cart, string $name = "default"): CartRepositoryContract
    {
        $this->sessionManager->put(
            $this->getInstanceName($name),
            serialize($cart)
        );

        return $this;
    }

    private function getInstanceName(string $name)
    {
        return sprintf('%s.%s', 'cart', $name);
    }
}