<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 10.09.18
 * Time: 13:34
 */

namespace Modules\Lk\Services;

use Countable;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\Collection;
use Iterator;
use Modules\Core\Model\Currency;
use Modules\Core\Model\Product;
use Modules\Lk\Contracts\CartContract;
use Modules\Lk\Events\CartItemAdded;
use Modules\Lk\Events\CartItemRemoved;
use Serializable;

class Cart implements CartContract
{
    /**
     * @var Collection
     */
    private $items;

    public function __construct()
    {
        $this->items = collect();
    }

    public function clear()
    {

    }

    public function add(Product $item, int $count = 1)
    {
        /** @var CartItem $existItem */
        if (!empty($existItem = $this->items->first(function (CartItem $existItem) use ($item, $count) {
            return $existItem->getProductId() === $item["id"];
        }))) {
            $existItem->increment($count);
            event(new CartItemAdded($this, $existItem));
        } else {
            $cartItem = new CartItem($item, $count);
            $this->items->push($cartItem);
            event(new CartItemAdded($this, $cartItem));
        }
    }

    public function remove(Product $item)
    {
        $cartItem = $this->items->first(function (CartItem $removeItem) use ($item) {
            return $removeItem->getProductId() === $item["id"];
        });

        if(empty($cartItem)) {
            // ToDO: not exists exception
            return ;
        }

        $this->items->forget(
            $this->items->search($cartItem)
        );
        event(new CartItemRemoved($this, $cartItem));
    }

    public function count()
    {
        return $this->items->count();
    }

    /**
     * @return Collection
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function getPrice()
    {
        return $this->items->sum(function (CartItem $cartItem) {
            return $cartItem->getPrice();
        });
    }

    public function getCurrency(): Currency
    {
        return $this->items->first()->getCurrency();
    }
}