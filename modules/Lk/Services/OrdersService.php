<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 11.09.18
 * Time: 16:55
 */

namespace Modules\Lk\Services;

use Modules\Core\Contracts\PaysystemContract;
use Modules\Core\Model\Order;
use Modules\Core\Model\Payment;
use Modules\Core\Model\Paysystem;
use Modules\Core\Model\PaysystemMethod;
use Modules\Core\Model\User;
use Modules\Core\Paysystems\Robokassa;
use Modules\Core\Repository\OrderItemsRepository;
use Modules\Core\Repository\OrdersRepository;
use Modules\Core\Repository\PaymentsRepository;
use Modules\Core\Traits\Strings;
use Modules\Lk\Contracts\CartContract;
use Modules\Lk\Contracts\CartItemContract;

class OrdersService
{
    use Strings;
    /**
     * @var OrdersRepository
     */
    private $ordersRepository;
    /**
     * @var PaymentsRepository
     */
    private $paymentsRepository;
    /**
     * @var OrderItemsRepository
     */
    private $orderItemsRepository;

    /**
     * OrdersService constructor.
     * @param OrdersRepository $ordersRepository
     * @param OrderItemsRepository $orderItemsRepository
     * @param PaymentsRepository $paymentsRepository
     */
    public function __construct(OrdersRepository $ordersRepository,
                                OrderItemsRepository $orderItemsRepository,
                                PaymentsRepository $paymentsRepository)
    {
        $this->ordersRepository = $ordersRepository;
        $this->paymentsRepository = $paymentsRepository;
        $this->orderItemsRepository = $orderItemsRepository;
    }

    /**
     * @param CartContract $cart
     * @param PaysystemMethod $paysystemMethod
     * @param Paysystem $paysystem
     * @param User $user
     * @return Order
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function createOrder(CartContract $cart,
                                User $user): Order
    {
        
        $lastOrderNumber = Order::orderBy('created_at', 'desc')->first()->number;
        $lastOrderNumber++;
        $order = $this->ordersRepository->create([
            "number" => $lastOrderNumber,
            "user_id" => $user["id"],
            "currency_id" => $cart->getCurrency()->id, // ToDo: refactor to getCurrencyId()
            "price" => $cart->getPrice(),
        ]);

        /** @var CartItemContract $cartItem */
        foreach ($cart->getItems() as $cartItem) {
            $this->orderItemsRepository->create([
                "order_id" => $order["id"],
                "product_id" => $cartItem->getProductId(),
                "currency_id" => $cartItem->getCurrencyId(),
                "quantity" => $cartItem->getQuantity(),
                "price" => $cartItem->getPrice(),
            ]);
        }

        return $order;
    }

    /**
     * @param Order $order
     * @param PaysystemMethod $paysystemMethod
     * @return Payment
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function createPayment(Order $order, PaysystemMethod $paysystemMethod): Payment
    {
        $driver = $paysystemMethod->paysystem->getDriver();

        $comment = sprintf("Payment for order ID %s", $order["id"]);

        $link = $driver->getPaymentLink(
            $order["price"],
            $paysystemMethod->paysystem->getCurrencyName($order->currency),
            $paysystemMethod["name"],
            $order["id"],
            $comment
        );

        /** @var Payment $payment */
        $payment = $this->paymentsRepository->create([
            "order_id" => $order["id"],
            "paysystem_id" => $paysystemMethod->paysystem["id"],
            "paysystem_method_id" => $paysystemMethod["id"],
            "paysystem_status_id" => $driver::CREATED_STATUS_ID,
            "comment" => $comment,
            "link" => $link,
            "is_test" => $driver->isTest(),
        ]);

        return $payment;
    }
}