<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 10.09.18
 * Time: 15:09
 */

namespace Modules\Lk\Listeners;


use Modules\Lk\Contracts\CartRepositoryContract;
use Modules\Lk\Events\ModifyCart;

class PersistCart
{
    /**
     * @var CartRepositoryContract
     */
    private $cartRepository;

    public function __construct(CartRepositoryContract $cartRepository)
    {
        $this->cartRepository = $cartRepository;
    }

    public function handle(ModifyCart $event)
    {
        $this->cartRepository->save($event->cart);
    }
}