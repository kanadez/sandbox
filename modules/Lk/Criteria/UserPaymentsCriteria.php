<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 17.09.18
 * Time: 14:49
 */

namespace Modules\Lk\Criteria;

use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class UserPaymentsCriteria implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereHas('order', function($query) {
            $query->where('user_id', Auth::user()->id);
        });
    }

}