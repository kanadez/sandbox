<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 20.07.18
 * Time: 13:33
 */

use Illuminate\Routing\Router;

/*
 *  Заказы
 */
$router->namespace('Routes')->group(function() use($router) {
    $router->get('proxy/ports', 'ProxyController@ports')->name('proxy.ports');
});

/** @var  $router Illuminate\Routing\Router */
$router->get('/', function() {
    return redirect(route('lk.proxy.ports'));
})->name('index');

$router->group(["prefix" => "pay", "as" => "pay."], function (Router $router) {
    $router->post('robokassa', 'DashboardController@pay')
        ->name('robokassa');

    $router->post('interkassa', 'DashboardController@payInterkassa')
        ->name('interkassa');
});

$router->group(["prefix" => "catalog", "as" => "catalog."], function (Router $router) {
    $router->get('', 'CatalogController@index')
        ->name('index');
});

$router->group(["prefix" => "cart", "as" => "cart."], function (Router $router) {
    $router->post('add/{itemId}', 'CartController@add')
        ->name('add');

    $router->get('remove/{itemId}', 'CartController@remove')
        ->name('remove');
});

$router->group(["prefix" => "order", "as" => "order."], function (Router $router) {
    $router->get('', 'OrdersController@index')
        ->name('index');

    $router->get('make', 'OrdersController@make')
        ->name('make');

    $router->get('paysystem', 'OrdersController@paysystem')
        ->name('paysystem');

    $router->post('create', 'OrdersController@create')
        ->name('create');

    $router->get('{itemId}', 'OrdersController@details')
        ->name('details')
        ->where("itemId", "'[0-9]+'");
});


$router->group(["prefix" => "proxy-port", "as" => "proxy-port."], function (Router $router) {
    $router->get('', 'ProxyPortsController@index')
        ->name('index');

    $router->get('make', 'ProxyPortsController@make')
        ->name('create');

    $router->post('create', 'ProxyPortsController@store')
        ->name('store');

    $router->get('{proxyPortId}', 'ProxyPortsController@details')
        ->name('details');

    $router->get('edit/{proxyPortId}', 'ProxyPortsController@edit')
        ->name('edit');

    $router->post('edit/{proxyPortId?}', 'ProxyPortsController@store')
        ->name('store');

    $router->get('delete/{proxyPortId?}', 'ProxyPortsController@delete')
        ->name('delete');
});

$router->group(["prefix" => "payment", "as" => "payment."], function (Router $router) {

    $router->get('index', 'PaymentsController@index')
        ->name('index');

    $router->group(["prefix" => "success", "as" => "success."], function (Router $router) {
        $router->get('robokassa', 'PaymentsController@successRobokassa')
            ->name('robokassa');

        $router->get('interkassa', 'PaymentsController@successInterkassa')
            ->name('interkassa');
    });

});

$router->group(["prefix" => "lang", "as" => "lang."], function (Router $router) {
    $router->get('{lang}', 'LanguageController@change')
        ->name('change')
        ->where("lang", "ru|en");
});
