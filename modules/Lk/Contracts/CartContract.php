<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 10.09.18
 * Time: 13:45
 */

namespace Modules\Lk\Contracts;

use Countable;
use Illuminate\Support\Collection;
use Modules\Core\Model\Currency;
use Modules\Core\Model\Product;

interface CartContract extends Countable
{
    public function add(Product $item, int $count = 1);
    public function remove(Product $item);
    public function getPrice();
    public function getCurrency(): Currency;
    public function getItems(): Collection;
}