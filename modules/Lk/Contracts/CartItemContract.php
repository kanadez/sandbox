<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 10.09.18
 * Time: 15:05
 */

namespace Modules\Lk\Contracts;


interface CartItemContract
{
    public function getProductId(): int;
    public function getCurrencyId(): int;
    public function getPrice();
    public function getQuantity(): int;
}