<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 10.09.18
 * Time: 14:29
 */

namespace Modules\Lk\Contracts;

use Modules\Lk\Services\Cart;

interface CartRepositoryContract
{
    public function get(string $name = "default"): CartContract;
    public function save(Cart $cart, string $name = "default"): CartRepositoryContract;
}