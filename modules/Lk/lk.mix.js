module.exports = function (mix, module) {
    mix
        .webpackConfig({
            resolve: {
                alias: {
                    pace: 'pace-progress'
                }
            }
        })
        .autoload({

        })
        .extract([
            'lodash',
            'vue',
            'popper.js',
            'jquery',
            'bootstrap',
            'axios',
            '@coreui/coreui',
            'pace-progress',
            'uuid',
        ])
};