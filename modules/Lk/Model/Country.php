<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 02.08.18
 * Time: 16:55
 */

namespace Modules\Lk\Model;

use Modules\Core\Model\BaseModel;

class Country extends BaseModel
{
    protected $table = 'all_countries';

    protected $fillable = [
        "name",
    ];
}
