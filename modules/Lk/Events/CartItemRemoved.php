<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 11.09.18
 * Time: 11:08
 */

namespace Modules\Lk\Events;

use Modules\Lk\Contracts\CartContract;
use Modules\Lk\Contracts\CartItemContract;

class CartItemRemoved extends ModifyCart
{
    /**
     * @var CartItemContract
     */
    public $cartItem;

    /**
     * CartItemAdded constructor.
     * @param CartContract $cart
     * @param CartItemContract $cartItem
     */
    public function __construct(CartContract $cart,
                                CartItemContract $cartItem)
    {
        parent::__construct($cart);
        $this->cartItem = $cartItem;
    }
}