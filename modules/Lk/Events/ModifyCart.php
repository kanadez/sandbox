<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 10.09.18
 * Time: 15:07
 */

namespace Modules\Lk\Events;

use Modules\Lk\Contracts\CartContract;

abstract class ModifyCart
{
    /**
     * @var CartContract
     */
    public $cart;

    public function __construct(CartContract $cart)
    {
        $this->cart = $cart;
    }
}