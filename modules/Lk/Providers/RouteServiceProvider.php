<?php

namespace Modules\Lk\Providers;

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use Modules\Core\Providers\BaseRouteServiceProvider;

class RouteServiceProvider extends BaseRouteServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'Modules\Lk\Http\Controllers';
    /**
     * @var string
     */
    protected $module = "lk";

    protected function mapLkRoutes()
    {
        Route::group([
            'namespace'  => $this->namespace.'\Lk',
            'middleware' => [ 'web' ],
            'prefix'     => env('APP_LK_PREFIX', 'dashboard'),
            'as' => 'lk.',
        ], function (Router $router) {
            // Login routes
            $router->get('login', 'Auth\LoginController@showLoginForm')
                ->name('login');
            $router->post('login', 'Auth\LoginController@login');
            $router->get('logout', 'Auth\LoginController@logout')
                ->name('logout');

            // Password routes
            $router->post('password/forgot', 'Auth\ForgotPasswordController@sendResetLinkEmail')
                ->name('password.forgot');
            $router->post('password/reset', 'Auth\ResetPasswordController@reset')
                ->name('password.reset');
            
            $router->group([
                'middleware' => [ 'lk' ],
            ], function ($router) {
                require module_path($this->module, 'Routes/lk.php');
            });
        });
    }
}
