<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 10.09.18
 * Time: 12:04
 */

namespace Modules\Lk\Providers;

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Support\ServiceProvider;

class BreadCrumbsServiceProvider extends ServiceProvider
{
    public function register()
    {
        Breadcrumbs::for('lk.index', function ($trail) {
            $trail->push(__('lk::lk.lk'), route('lk.index'));
        });

        // Catalog
        Breadcrumbs::for('lk.catalog.index', function ($trail) {
            $trail->parent('lk.index');
            $trail->push(__('lk::menu.buy'), route('lk.index'));
        });

        // Order
        Breadcrumbs::for('lk.order.index', function ($trail) {
            $trail->parent('lk.index');
            $trail->push(__('lk::menu.orders'), route('lk.order.index'));
        });

        Breadcrumbs::for('lk.order.details', function ($trail, $itemId) {
            $trail->parent('lk.order.index');
            $trail->push(__('lk::order.details'), route('lk.order.details', [$itemId]));
        });

        Breadcrumbs::for('lk.order.make', function ($trail) {
            $trail->parent('lk.index');
            $trail->push(__('lk::order.create'), route('lk.order.make'));
        });

        Breadcrumbs::for('lk.order.paysystem', function ($trail) {
            $trail->parent('lk.order.make');
            $trail->push(__('lk::paysystem.paysystem'), route('lk.order.paysystem'));
        });

        // Payment
        Breadcrumbs::for('lk.payment.index', function ($trail) {
            $trail->parent('lk.index');
            $trail->push('Payments', route('lk.payment.index'));
        });

        Breadcrumbs::for('lk.payment.success.robokassa', function ($trail) {
            $trail->parent('lk.payment.index');
            $trail->push('Success', route('lk.payment.success.robokassa'));
        });

        Breadcrumbs::for('lk.payment.success.interkassa', function ($trail) {
            $trail->parent('lk.payment.index');
            $trail->push('Success', route('lk.payment.success.interkassa'));
        });

    }
}