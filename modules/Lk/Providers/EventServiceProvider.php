<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 10.09.18
 * Time: 16:29
 */

namespace Modules\Lk\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Modules\Lk\Events\CartItemAdded;
use Modules\Lk\Events\CartItemRemoved;
use Modules\Lk\Listeners\PersistCart;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        CartItemAdded::class => [
            PersistCart::class,
        ],
        CartItemRemoved::class => [
            PersistCart::class,
        ],
    ];
}