<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 10.09.18
 * Time: 13:46
 */

namespace Modules\Lk\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Lk\Contracts\CartContract;
use Modules\Lk\Contracts\CartRepositoryContract;
use Modules\Lk\Services\Cart;
use Modules\Lk\Services\CartRepository;

class CartServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind(CartContract::class, Cart::class);
        $this->app->bind(CartRepositoryContract::class, CartRepository::class);
    }
}