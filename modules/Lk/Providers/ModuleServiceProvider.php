<?php

namespace Modules\Lk\Providers;

use Caffeinated\Modules\Support\ServiceProvider;
use Illuminate\Routing\Router;
use Modules\Lk\Http\Middleware\LkAccessMiddleware;
use Modules\Lk\Http\Middleware\LkMenuMiddleware;
use Modules\Lk\Http\Middleware\LocaleMiddleware;
use Modules\Lk\Http\Middleware\RedirectClientIfAuthenticated;
use Modules\Lk\Http\Middleware\ShareCart;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot(Router $router)
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'lk');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'lk');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'lk');
        $this->loadConfigsFrom(__DIR__.'/../config');
        $router->pushMiddlewareToGroup("lk", LocaleMiddleware::class);
        $router->pushMiddlewareToGroup("lk", LkAccessMiddleware::class);
        $router->pushMiddlewareToGroup("lk", LkMenuMiddleware::class);
        $router->pushMiddlewareToGroup("lk", ShareCart::class);
        $router->pushMiddlewareToGroup("guest-lk", RedirectClientIfAuthenticated::class);
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(CartServiceProvider::class);
        $this->app->register(EventServiceProvider::class);
        $this->app->register(BreadCrumbsServiceProvider::class);
        $this->app->register(RouteServiceProvider::class);
    }
}
