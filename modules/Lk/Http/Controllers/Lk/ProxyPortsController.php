<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 11.09.18
 * Time: 11:46
 */

namespace Modules\Lk\Http\Controllers\Lk;


use Illuminate\Http\Request;
use Modules\Core\Repository\ProxyPortsRepository;
use Modules\Lk\Criteria\UserProxyPortsCriteria;
use Modules\Core\Http\Requests\StoreProxyPortRequest as StoreProxyPortRequest;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Modules\Core\Model\CountryForProxyPort;
use Modules\Core\Model\CityForProxyPort;
use Modules\Core\Model\Node;
use Modules\Core\Model\ProxyPort;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class ProxyPortsController extends BaseController
{

  /**
     * @var ProxyPortsRepository
     */
    private $proxyPortsRepository;

    /**
     * ProxyPortsController constructor.
     * @param ProxyPortsRepository $proxyPortsRepository
     */
    public function __construct( ProxyPortsRepository $proxyPortsRepository)
    {

        $this->proxyPortsRepository = $proxyPortsRepository;

    }

    public function index()
    {
        $proxyPorts = $this->proxyPortsRepository
            ->resetCriteria()
            ->pushCriteria(UserProxyPortsCriteria::class)
            ->with([
                "country",
                "city",
                "node",
            ])
            ->all();

        return view("lk::lk.proxy_port.index", [
            "proxyPorts" => $proxyPorts,
        ]);
    }


    public function make()
    {
        $item = $this->proxyPortsRepository->makeModel();
        $item->username = strstr(Auth::user()->email, '@', true) . $this->generateRandomString(5);
        $item->password = $this->generateRandomString(6, true);
        $item->peer_rotate = "10m";
        $item->fingerprint = "";
        $item->asn = "";
        $item->uptime = 0;
        $item->latency = 1500;
        $item->speed_download = "100KB";
        $item->speed_upload = "100KB";
        $item->allowed_ip = '';
        $item->max_connections = 1000;
        $item->bandwidth_in = '10MB';
        $item->bandwidth_out = '10MB';
        $item->time_limit = '0m';
        return view("lk::lk.proxy_port.create", [
            "item" => $item,
        ]);

    }

    public function edit(int $proxyPortId)
    {
        $item = $this->proxyPortsRepository->find($proxyPortId);

        if(!is_null($item->allowed_ip) && $item->allowed_ip != '') {
            $item->allowed_ip = implode(';', json_decode($item->allowed_ip, true));
        }

        return view("lk::lk.proxy_port.create", [
            "item" => $item,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(int $itemId = null,StoreProxyPortRequest $request)
    {
        $data = $request->validated();

        if( !isset($data['country_id']) ){
            $data['country_id'] = null;
        }
        if( !isset($data['city_id']) ){
            $data['city_id'] = null;
        }
        
        if( $itemId == null || $this->proxyPortsRepository->find($itemId)->node_id != $data['node_id'] ) {
            $data['http'] = $this->getUniquePort($data['node_id'], 'http');
            $data['socks'] = $data['http']+50;
        }

        if( !is_null($data['allowed_ip']) && $data['allowed_ip'] != '' ) {
            $data['allowed_ip'] = json_encode(explode(';', $data['allowed_ip']));
        }

        $data['uptime'] = intval($data['uptime']);
        $data['latency'] = intval($data['latency']);

        $proxyPort = $this->proxyPortsRepository->updateOrCreate(["id" => $itemId], $data);

        if( $itemId == null || $this->proxyPortsRepository->find($itemId)->node_id != $data['node_id'] ) {
            $isExportedByAPI = $this->proxyGenerateApiRequest($data);

            if( $isExportedByAPI['Result'] == 'OK' ) {
                $r = $this->proxyPortsRepository->updateOrCreate(["id" => $proxyPort->id], ['is_exported_by_api' => 1]);
            } else { 
                if( isset($isExportedByAPI['error']) ) {
                    return redirect()->route("lk.proxy-port.index")->withErrors([$isExportedByAPI['error']]);
                } else {
                    return redirect()->route("lk.proxy-port.index")->withErrors([$isExportedByAPI['Error']]);
                }
                
            }
        }
        
        return redirect()->route("lk.proxy-port.index");
           
    }


    public function details( int $proxyPortId )
    {
        
        $proxyPort = $this->proxyPortsRepository
            ->resetCriteria()
            ->pushCriteria(UserProxyPortsCriteria::class)
            ->with([
                "country",
                "city",
                "node",
            ])
            ->find($proxyPortId);

        return view("lk::lk.proxy_port.details", [
            "proxyPort" => $proxyPort,
            "mounthCostsIP" => Config::get('prices.ipMonthPrice'),
            "mounthCostsTrafic" => Config::get('prices.GbMonthPrice')*$proxyPort->trafic
        ]);
    }


    public function delete (int $proxyPortId)
    {
        $proxyPort = $this->proxyPortsRepository->find($proxyPortId);
        $delete = $this->proxyDeleteApiRequest( $proxyPort );
        if( $delete['Result'] == 'OK' ||  $proxyPort->is_exported_by_api != 1 ) {
            $this->proxyPortsRepository->delete($proxyPortId);
            flash("Proxy port successfully removed!")->success();
            return redirect()->back();
        } else {
            return redirect()->route("lk.proxy-port.index")->withErrors([$delete['error']]);
        }
    }


    public function getCities( Request $request ) 
    {
        $countryId = $request->input('country_id');

        $cities = DB::table('all_cities')->where('country_id',$countryId)->pluck("name","id")->all();
        $data = view('lk::lk.proxy_port.ajax-select',compact('cities'))->render();
        return response()->json(['options'=>$data]);
    }


    public function getUniquePort( $nodeId, $portType = 'http' )
    {
        $port = rand(10000, 30000);
        $isUnique = DB::table('proxy_ports')->where('node_id', $nodeId)->where('http', $port)->get()->count();
        if( $isUnique < 1 ) {
            return $port;
        } else {
            $this->getUniquePort( $nodeId, 'http' );
        }
    }


    private function proxyGenerateApiRequest($data)
    {
        if( !is_null($data['country_id']) && $data['country_id'] != '' ) {
            $country = CountryForProxyPort::find($data['country_id']);
            $countryName = $country->name;
        } else {
            $countryName = '';
        }
        if( !is_null($data['city_id']) && $data['city_id'] != '' ) {
            $city = CityForProxyPort::find($data['city_id']);
            $cityName = $city->name;
        } else {
            $cityName = '';
        }
        
        $node = Node::find( $data['node_id'] );
        $allowedIps = array();
        if( $data['allowed_ip'] != '' ) {
            $allowedIps = explode(';', $data['allowed_ip']);
        }
        $request = [
            "Email" => Auth::user()->email,
            "Config" => [
                "PeerRotate" => $data['peer_rotate'],
                "PeerInfo" => [
                    "Country"       => $countryName,
                    "City"          => $cityName,
                    "Fingerprint"   => (string)$data['fingerprint'],
                    "Type"          => $data['type'],
                    "Asn"           => (string)$data['asn'],
                    "Uptime"        => (int)$data['uptime'],
                    "Latency"       => (int)$data['latency'],
                    "SpeedDownload" => $data['speed_download'],
                    "SpeedUpload"   => $data['speed_upload'],
                ],
                "HTTP"      => $data['http'],
                "Socks"     => $data['socks'],
                "AllowedIP" => $allowedIps,
                "Auth" => [
                    "Username" => $data['username'],
                    "Password" => $data['password'],
                ],
                "MaxConnections" => (int)$data['max_connections'],
                "Bandwidth"      => [
                    "In" => $data['bandwidth_in'],
                    "Out" => $data['bandwidth_out'],
                ],
                "TrafficLimit" => $data['trafic'],
                "TimeLimit"    => $data['time_limit']
            ],
        ];

        $data = json_encode($request);

        $client = new Client([
            'headers' => ['Content-Type' => 'application/json']
        ]);
        try {
            $response = $client->post('http://'.$node->ip.':8103/api/cmd/proxyGenerate', 
                ['body' => $data]
            );
            return json_decode($response->getBody(), true);
        } catch (GuzzleException $e) {
            return ['Result'=> 'error', 'error'=>$e->getMessage()];
        }
        
    }

    public function proxyDeleteApiRequest( $proxyPort )
    {
        $node = Node::find( $proxyPort->node_id );
        $client = new Client();
        try {
            $response = $client->post('http://'.$node->ip.':8103/api/cmd/proxyRemove?email='.Auth::user()->email.'&httpPort='.$proxyPort->http.'&socksPort='.$proxyPort->socks);
            return json_decode($response->getBody(), true);
        }  catch (GuzzleException $e) {
            return ['Result'=> 'error', 'error'=>$e->getMessage()];
        }
        
    }

    private function generateRandomString($length = 5, $password = false) {
        if( $password == false ) {
            $characters = '0123456789!@#$%^&*()';
        } else {
            $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        }
        
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}