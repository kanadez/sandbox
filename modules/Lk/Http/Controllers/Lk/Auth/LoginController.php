<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 24.07.18
 * Time: 11:07
 */

namespace Modules\Lk\Http\Controllers\Lk\Auth;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Modules\Lk\Http\Controllers\Lk\BaseController;

class LoginController extends BaseController
{
    use AuthenticatesUsers {
        logout as protected laravelLogout;
    }
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->redirectTo = route("lk.index");
        $this->middleware('guest-lk')->except('logout');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm()
    {
        //return view('lk::lk.auth.login');
        return redirect(route("web.index"));
    }
    
    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        return response()->json([
            "status"    => "success",
            "redirect"  => $this->redirectTo
        ]);
    }

    public function logout(Request $request)
    {
        $this->laravelLogout($request);
        return redirect(route("web.index"));
    }
}