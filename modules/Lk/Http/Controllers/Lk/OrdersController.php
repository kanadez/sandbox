<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 11.09.18
 * Time: 11:46
 */

namespace Modules\Lk\Http\Controllers\Lk;


use Illuminate\Http\Request;
use Modules\Core\Contracts\PaymentServiceContract;
use Modules\Core\Model\Paysystem;
use Modules\Core\Model\PaysystemMethod;
use Modules\Core\Repository\OrdersRepository;
use Modules\Core\Repository\PaysystemMethodsRepository;
use Modules\Core\Repository\PaysystemsRepository;
use Modules\Core\Services\Payments\InterkassaService;
use Modules\Core\Services\Payments\RobokassaService;
use Modules\Lk\Contracts\CartContract;
use Modules\Lk\Contracts\CartRepositoryContract;
use Modules\Lk\Criteria\UserOrdersCriteria;
use Modules\Lk\Http\Middleware\EmptyCartRedirectMiddleware;
use Modules\Lk\Http\Requests\CreatePaymentRequest;
use Modules\Lk\Services\OrdersService;

class OrdersController extends BaseController
{
    /**
     * @var CartRepositoryContract
     */
    private $cartRepository;
    /**
     * @var PaysystemsRepository
     */
    private $paysystemsRepository;
    /**
     * @var OrdersService
     */
    private $ordersService;
    /**
     * @var PaysystemMethodsRepository
     */
    private $paysystemMethodsRepository;
    /**
     * @var OrdersRepository
     */
    private $ordersRepository;

    /**
     * OrdersController constructor.
     * @param CartRepositoryContract $cartRepository
     * @param PaysystemsRepository $paysystemsRepository
     * @param PaysystemMethodsRepository $paysystemMethodsRepository
     * @param OrdersService $ordersService
     * @param OrdersRepository $ordersRepository
     */
    public function __construct(CartRepositoryContract $cartRepository,
                                PaysystemsRepository $paysystemsRepository,
                                PaysystemMethodsRepository $paysystemMethodsRepository,
                                OrdersService $ordersService,
                                OrdersRepository $ordersRepository)
    {
        $this->cartRepository = $cartRepository;
        $this->paysystemsRepository = $paysystemsRepository;
        $this->ordersService = $ordersService;
        $this->paysystemMethodsRepository = $paysystemMethodsRepository;
        $this->ordersRepository = $ordersRepository;

        $this->middleware(EmptyCartRedirectMiddleware::class);
    }

    public function index()
    {
        $orders = $this->ordersRepository
            ->resetCriteria()
            ->pushCriteria(UserOrdersCriteria::class)
            ->with([
                "currency",
                "payment",
                "payment.status",
            ])
            ->all();

        return view("lk::lk.order.index", [
            "orders" => $orders,
        ]);
    }

    public function details(int $itemId)
    {
        $order = $this->ordersRepository
            ->resetCriteria()
            ->pushCriteria(UserOrdersCriteria::class)
            ->with([
                "currency",
                "payment",
                "payment.status",
            ])
            ->find($itemId);

        return view("lk::lk.order.details", [
            "order" => $order,
        ]);
    }

    public function make()
    {
        return view("lk::lk.order.create", [
        ]);
    }

    public function paysystem()
    {
        $paysystems = $this->paysystemsRepository->all();
        $cart = $this->cartRepository->get();

        return view("lk::lk.order.paysystem", [
            "paysystems" => $paysystems,
            "cart" => $cart,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function create(CreatePaymentRequest $request)
    {
        /** @var PaysystemMethod $paysystemMethod */
        $paysystemMethod = $this->paysystemMethodsRepository->find($request->input("paysystem_method_id"));

        $order = $this->ordersService->createOrder(
            $this->cartRepository->get(),
            $request->user()
        );

        $payment = $this->ordersService->createPayment($order, $paysystemMethod);

        return redirect()->to($payment["link"]);
    }
}