<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 17.09.18
 * Time: 11:38
 */

namespace Modules\Lk\Http\Controllers\Lk;


use Illuminate\Http\Request;
use Modules\Core\Repository\OrdersRepository;
use Modules\Core\Repository\PaymentsRepository;
use Modules\Lk\Criteria\UserPaymentsCriteria;
use Modules\Lk\Http\Requests\InterkassaSuccessfulPaymentRequest;
use Modules\Lk\Http\Requests\RobokassaSuccessfulPaymentRequest;

class PaymentsController extends BaseController
{
    /**
     * @var OrdersRepository
     */
    private $ordersRepository;
    /**
     * @var PaymentsRepository
     */
    private $paymentsRepository;

    /**
     * PaymentsController constructor.
     * @param OrdersRepository $ordersRepository
     * @param PaymentsRepository $paymentsRepository
     */
    public function __construct(OrdersRepository $ordersRepository,
                                PaymentsRepository $paymentsRepository)
    {
        $this->ordersRepository = $ordersRepository;
        $this->paymentsRepository = $paymentsRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index()
    {
        $payments = $this->paymentsRepository
            ->resetCriteria()
            ->pushCriteria(UserPaymentsCriteria::class)
            ->all();

        return view("lk::lk.payment.index", [
            "payments" => $payments,
        ]);
    }
    public function successRobokassa(RobokassaSuccessfulPaymentRequest $request)
    {
        $orderId = $request->input("InvId");
        $order = $this->ordersRepository->find($orderId);

        return view("lk::lk.payment.success", [
            "order" => $order,
        ]);
    }

    public function successInterkassa(InterkassaSuccessfulPaymentRequest $request)
    {
        $orderId = $request->input("ik_pm_no");
        $order = $this->ordersRepository->find($orderId);

        return view("lk::lk.payment.success", [
            "order" => $order,
        ]);
    }

}