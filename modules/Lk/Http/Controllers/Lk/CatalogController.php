<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 10.09.18
 * Time: 11:52
 */

namespace Modules\Lk\Http\Controllers\Lk;


use Modules\Core\Repository\ProductsRepository;

class CatalogController extends BaseController
{
    /**
     * @var ProductsRepository
     */
    private $productsRepository;

    /**
     * CatalogController constructor.
     * @param ProductsRepository $productsRepository
     */
    public function __construct(ProductsRepository $productsRepository)
    {
        $this->productsRepository = $productsRepository;
    }

    public function index()
    {
        $products = $this->productsRepository->all();

        return view("lk::lk.catalog.index", [
            "products" => $products,
        ]);
    }
}