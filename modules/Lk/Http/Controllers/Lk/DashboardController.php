<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 23.07.18
 * Time: 20:30
 */

namespace Modules\Lk\Http\Controllers\Lk;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Contracts\RobokassaContract;
use Modules\Core\Repository\OrdersRepository;
use Modules\Core\Repository\PaysystemsRepository;
use Modules\Core\Services\Payments\InterkassaService;
use Modules\Core\Services\Payments\RobokassaService;

class DashboardController extends BaseController
{
    /**
     * @var RobokassaService
     */
    private $robokassaService;
    /**
     * @var InterkassaService
     */
    private $interkassaService;

    /**
     * DashboardController constructor.
     * @param RobokassaService $robokassaService
     * @param InterkassaService $interkassaService
     */
    public function __construct(RobokassaService $robokassaService,
                                InterkassaService $interkassaService)
    {
        $this->robokassaService = $robokassaService;
        $this->interkassaService = $interkassaService;
    }

    public function index()
    {
        return view("lk::lk.dashboard.index", [

        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function pay(Request $request)
    {
        $link = $this->robokassaService->process($request->input("sum"), $request->user());

        return redirect($link);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function payInterkassa(Request $request)
    {
        $link = $this->interkassaService->process($request->input("sum"), $request->user());

        return redirect($link);
    }
}