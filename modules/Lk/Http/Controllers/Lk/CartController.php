<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 10.09.18
 * Time: 13:20
 */

namespace Modules\Lk\Http\Controllers\Lk;

use Modules\Core\Model\Product;
use Modules\Core\Repository\ProductsRepository;
use Modules\Lk\Contracts\CartContract;
use Modules\Lk\Contracts\CartRepositoryContract;
use Modules\Lk\Http\Requests\AddProductToCartRequest;


class CartController extends BaseController
{
    /**
     * @var ProductsRepository
     */
    private $productsRepository;
    /**
     * @var CartRepositoryContract
     */
    private $cartRepository;

    /**
     * CartController constructor.
     * @param ProductsRepository $productsRepository
     * @param CartRepositoryContract $cartRepository
     */
    public function __construct(ProductsRepository $productsRepository,
                                CartRepositoryContract $cartRepository)
    {
        $this->productsRepository = $productsRepository;
        $this->cartRepository = $cartRepository;
    }

    public function add(int $productId, AddProductToCartRequest $request)
    {
        /** @var Product $product */
        $product = $this->productsRepository->find($productId);

        /** @var CartContract $cart */
        $cart = $this->cartRepository->get();

        $cart->add($product, $request->input("quantity"));

        flash()->success("Item successfully added");

        return redirect()->back();
    }

    public function remove(int $productId)
    {
        /** @var Product $product */
        $product = $this->productsRepository->find($productId);

        /** @var CartContract $cart */
        $cart = $this->cartRepository->get();

        $cart->remove($product);

        flash()->success("Item successfully removed");

        return redirect()->back();

    }
}