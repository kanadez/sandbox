<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 24.07.18
 * Time: 10:46
 */

namespace Modules\Lk\Http\Controllers\Lk\Routes;

use Modules\Lk\Http\Controllers\Controller;

class ProxyController extends Controller
{

    public function ports() {
        return view('lk::lk.pages.proxy.ports');
    }

}
