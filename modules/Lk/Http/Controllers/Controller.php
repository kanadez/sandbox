<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 24.07.18
 * Time: 10:46
 */

namespace Modules\Lk\Http\Controllers;

use Modules\Lk\Http\Controllers\Lk\BaseController;

class Controller extends BaseController
{
    public function view($path, $params = []) {
        return view('pages.' . $path, $params);
    }

    public function fail($message = 'error', $data = [], $code = 500) {
        $data = is_object($message) ? $message : $data;
        $message = is_string($message) ? $message : 'error';
        return $this->json($message, $data, $code);
    }

    public function ok($message = 'ok', $data = [], $code = 200) {
        $data = !is_string($message) ? $message : $data;
        $message = is_string($message) ? $message : 'ok';
        return $this->json($message, $data, $code);
    }

    public function json($message, $data, $code) {
        return response()->json([
            'code' =>  $code,
            'text' => $message,
            'data' => $data
        ]);
    }

    public function redirect($url) {
        if(request()->ajax()) {
            return $this->json('redirect', [
                'url' => $url,
            ], 302);
        }
    }

    public function catch($callback, $transaction = false) {
        if($transaction) {
            DB::beginTransaction();
        }
        try {
            $result = (Closure::bind($callback, $this))();
            if($transaction) {
                DB::commit();
            }
            return $result;
        } catch(Exception $e) {
            if($transaction) {
                DB::rollBack();
            }
            return request()->ajax() ? $this->fail($e->getMessage() . '<br/>' . $e->getFile() . '<br/>' . $e->getLine()) : $e;
        }
    }

    public function compileList($list, $fields = []) {
        $result = [];
        if($fields) {
            foreach ($list as $key => $value) {
                $item = [];
                foreach($fields as $field) {
                    $field_name = $field;
                    $field_path = $field;
                    if(!is_string($field)) {
                        $keys = array_keys($field);
                        $field_name = $keys[0];
                        $field_path = $field[$keys[0]];
                    }
                    data_set($item, $field_name, data_get($value, $field_path));
                }
                $result[] = $item;
            }
        } else {
            $result = $list;
        }
        return $result;
    }

    public function table($list, $fields = []) {
        $count = is_array($list) ? count($list) : $list->count();

        $list = is_array($list) ? $list : $list->skip(request()->get('start'))->take(request()->get('length'))->get();

        $result = $this->compileList($list, $fields);

        return [
            'draw' => request()->get('draw'),
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $result,
        ];
    }

    public function errors($errors) {
        return response([
            'errors' =>  $errors,
            'message' => 'The given data was invalid.'
        ], 422);
    }
}
