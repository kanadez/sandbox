<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 24.07.18
 * Time: 10:46
 */

namespace Modules\Lk\Http\Controllers\Api\Proxy;

use Modules\Lk\Http\Controllers\Controller;
use Modules\Lk\Http\Requests\Port as PortRequest;
use Modules\Core\Model\ProxyPort;
use Modules\Core\Model\Node;

class PortsController extends Controller
{

    public function index() {
        return $this->table(
            user()->proxyPorts()->orderBy('id', 'desc'),
            [
                'id',
                'user_id',
                'name',
                ['network_type.name' => 'networkType.name'],
                ['ip_type.name' => 'ipType.name'],
                ['traffic_volume.name' => 'trafficVolume.name'],
                'created_at',
            ]
        );
    }

    public function store(PortRequest $request) {
        $node = $this->getNodeWithMinimalPorts();

        $port_id = ProxyPort::insertGetId(array_merge(
            $request->only(
                'name',
                'network_type_id',
                'country_id',
                'city_id',
                'use_unlimited_traffic',
                'pull_size',
                'traffic_volume_id',
                'rotation',
                'rotation_time_type',
                'username',
                'password',
                'allowed_ip',
                'uptime',
                'latency',
                'max_connections',
                'bandwidth_in',
                'bandwidth_out'
            ),
            [
                'user_id' => user()->id,
                'created_at' => date('Y-m-d H:i:s'),
                'type' => 'residental',
                'ip_type' => 'static',
                'time_ip_change' => 0,
                'trafic' => 0,
                'node_id' => $node->id,
                'ip_type_id' => -1,
            ]
        ));

        if(!$node->createPort(ProxyPort::find($port_id))) {
            return $this->fail('Не удалось получить HTTP/Socks прокси');
        }

        return $this->ok(['id' => $port_id]);
    }

    public function update(PortRequest $request, $id) {
        $port = user()->proxyPorts()->findOrFail($id);

        ProxyPort::where('id', $id)->update(array_merge(
            $request->only(
                'name',
                'network_type_id',
                'country_id',
                'city_id',
                'use_unlimited_traffic',
                'pull_size',
                'traffic_volume_id',
                'rotation',
                'rotation_time_type',
                'username',
                'password',
                'allowed_ip',
                'uptime',
                'latency',
                'max_connections',
                'bandwidth_in',
                'bandwidth_out'
            ),
            [
                'user_id' => user()->id,
                'created_at' => date('Y-m-d H:i:s'),
                'type' => 'residental',
                'ip_type' => 'static',
                'time_ip_change' => 0,
                'trafic' => 0,
                //'node_id' => $this->getNodeIdWithMinimalPorts(),
                'ip_type_id' => -1
            ]
        ));

        return $this->ok();
    }

    public function show($id) {
        $port = user()->proxyPorts()->findOrFail($id);

        return $this->ok($port->only(
            'id',
            'name',
            'network_type_id',
            'created_at',
            'use_unlimited_traffic',
            'country_id',
            'city_id',
            'pull_size',
            'traffic_volume_id',
            'rotation',
            'rotation_time_type',
            'username',
            'password',
            'allowed_ip',
            'uptime',
            'latency',
            'max_connections',
            'bandwidth_in',
            'bandwidth_out'
        ));
    }

    public function destroy($id) {
        $port = user()->proxyPorts()->findOrFail($id);

        $port->delete();

        return $this->ok();
    }


    public function getNodeWithMinimalPorts() {
        $list = ProxyPort::selectRaw('node_id as id, count(1) as count')->groupBy('node_id')->where('node_id', '>', 0)->orderBy('count', 'asc')->first();
        return $list ? Node::find($list->id) : Node::first();
    }

}
