<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 24.07.18
 * Time: 10:46
 */

namespace Modules\Lk\Http\Controllers\Api;

use Modules\Lk\Http\Controllers\Controller;
use \Modules\Lk\Model\City;
use Illuminate\Support\Str;

class CalculatorController extends Controller
{
      public function index() {
          $method = 'calc' . Str::studly(request()->get('type'));
          return $this->$method();
      }

      public function calcProxyPortPrice() {
          $amount = 0;
          $amount += request()->get('traffic_volume_id', 0);

          $amount += request()->get('city_id', 0);
          $amount += request()->get('pull_size', 0);

          $amount += request()->filled('allowed_ip') ? 145 : 0;

          return $this->ok([
              'amount' => round($amount),
          ]);
      }
}
