<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 24.07.18
 * Time: 10:46
 */

namespace Modules\Lk\Http\Controllers\Api;

use Modules\Lk\Http\Controllers\Controller;
use \Modules\Lk\Model\City;

class CitiesController extends Controller
{
    public function index() {
        return $this->ok(City::where('country_id', request()->get('country_id'))->get());
    }
}
