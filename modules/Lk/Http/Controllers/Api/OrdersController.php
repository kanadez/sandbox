<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 24.07.18
 * Time: 10:46
 */

namespace Modules\Lk\Http\Controllers\Api;

use Modules\Lk\Http\Controllers\Controller;

class OrdersController extends Controller
{

    public function index() {
        return $this->table(
            user()->orders()
        );
    }

}
