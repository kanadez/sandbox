<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 25.07.18
 * Time: 16:48
 */

namespace Modules\Lk\Http\Middleware;


use Closure;
use Lavary\Menu\Builder;
use Lavary\Menu\Facade as Menu;
use Modules\Lk\Contracts\CartRepositoryContract;

class LkMenuMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
        // CreateAdminMenu instance
        /*Menu::make('LkMenu', function(Builder $menu) {
            $menu
                ->add(__('lk::menu.dashboard'), route("lk.index"))
                ->data('order', 100)
                ->data("icon", "nav-icon icon-speedometer");
        });*/

        /*Menu::make('LkMenu', function(Builder $menu) {
            $menu
                ->add(__('lk::menu.buy'), route("lk.catalog.index"))
                ->data('order', 200)
                ->data("icon", "nav-icon icon-list");
        });*/


        Menu::make('LkMenu', function(Builder $menu) {

            $menu->add(__('lk::menu.proxy-ports'), route("lk.proxy.ports"))
                ->data("icon", "nav-icon icon-docs");
        });

        /*Menu::make('LkMenu', function(Builder $menu) {
            $menu
                ->add(__('lk::menu.orders'), route("lk.order.index"))
                ->data('order', 300)
                ->data("icon", "nav-icon icon-docs");
        });*/
        /*Menu::make('LkMenu', function(Builder $menu) {
            $cart = app(CartRepositoryContract::class)->get();
            $menu
                ->add(__('lk::menu.cart'), route("lk.order.make"))
                ->data('order', 400)
                ->data("icon", "nav-icon cui-cart")
                ->data("badge", [
                    "icon" => "badge " . ( count($cart) > 0 ? 'badge-warning' : 'badge-danger' ),
                    "text" => count($cart),
                ])
                ;
        });*/

//        Menu::make('LkMenu', function(Builder $menu) {
//            $menu
//                ->add("Payments", route("lk.payment.index"))
//                ->data('order', 400)
//                ->data("icon", "nav-icon icon-wallet");
//        });

        return $next($request);
    }
}
