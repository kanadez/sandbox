<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 25.09.18
 * Time: 10:31
 */

namespace Modules\Lk\Http\Middleware;

use Closure;
use Modules\Lk\Contracts\CartRepositoryContract;

class EmptyCartRedirectMiddleware
{
    /**
     * @var CartRepositoryContract
     */
    private $cartRepository;

    /**
     * EmptyCartRedirectMiddleware constructor.
     * @param CartRepositoryContract $cartRepository
     */
    public function  __construct(CartRepositoryContract $cartRepository)
    {
        $this->cartRepository = $cartRepository;
    }

    public function handle($request, Closure $next, $guard = null)
    {
        $cart = $this->cartRepository->get();

        if (empty(count($cart))) {
            //flash()->error("Cart is empty");
            //return redirect()->route("lk.catalog.index");
        }

        return $next($request);
    }
}