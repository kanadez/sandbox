<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 11.09.18
 * Time: 9:16
 */

namespace Modules\Lk\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\View;
use Modules\Lk\Contracts\CartRepositoryContract;

class ShareCart
{
    /**
     * @var CartRepositoryContract
     */
    private $cartRepository;

    /**
     * ShareCart constructor.
     * @param CartRepositoryContract $cartRepository
     */
    public function __construct(CartRepositoryContract $cartRepository)
    {
        $this->cartRepository = $cartRepository;
    }

    public function handle($request, Closure $next, $guard = null)
    {
        View::share('cart', $this->cartRepository->get());
        return $next($request);
    }
}