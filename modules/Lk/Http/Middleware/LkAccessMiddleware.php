<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 20.07.18
 * Time: 12:09
 */

namespace Modules\Lk\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class LkAccessMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
        if (($user = Auth::user()) && $user->can("lk-access")) {
            View::share('user', $user);
            return $next($request);
        }

        return redirect(route("lk.login"));
    }
}