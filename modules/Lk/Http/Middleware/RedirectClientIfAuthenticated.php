<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 24.07.18
 * Time: 11:23
 */

namespace Modules\Lk\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectClientIfAuthenticated
{
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            return redirect(route("lk.index"));
        }

        return $next($request);
    }
}