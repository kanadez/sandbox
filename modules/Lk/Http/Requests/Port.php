<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 17.09.18
 * Time: 10:37
 */

namespace Modules\Lk\Http\Requests;


class Port extends Request
{

    public function rules()
    {
        return [
            'name' => 'required',
            'network_type_id' => 'required|exists:network_types,id',
            'country_id' => 'required|exists:all_countries,id',
            'city_id' => 'nullable|exists:all_cities,id',
            'rotation' => 'required|integer|min:1',
            'rotation_time_type' => 'required|integer|between:1,2',
            //'ip_type_id' => 'required|exists:ip_types,id',
            //'use_unlimited_traffic' => 'required|integer|between:0,1',
            'traffic_volume_id' => 'required',
            'pull_size' => 'required|integer|min:1',
            'uptime' => 'required|integer|min:0',
            'latency' => 'required|integer|min:0',
            'max_connections' => 'required|integer|min:0',
            'bandwidth_in' => 'required|integer|min:0',
            'bandwidth_out' => 'required|integer|min:0',


        ];
    }
}
