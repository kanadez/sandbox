<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 17.09.18
 * Time: 10:37
 */

namespace Modules\Lk\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class CreatePaymentRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'paysystem_method_id' => 'required|integer|exists:paysystem_methods,id',
        ];
    }
}