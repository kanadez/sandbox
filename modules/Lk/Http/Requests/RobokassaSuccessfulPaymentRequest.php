<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 17.09.18
 * Time: 14:26
 */

namespace Modules\Lk\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RobokassaSuccessfulPaymentRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'InvId' => 'required|integer|exists:orders,id',
            'OutSum' => 'required|numeric',
            'SignatureValue' => 'required|alpha_num',
        ];
    }
}