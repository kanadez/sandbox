<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 18.09.18
 * Time: 17:32
 */

namespace Modules\Lk\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InterkassaSuccessfulPaymentRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'ik_co_id' => 'required|alpha_num',
            'ik_pm_no' => 'required|integer|exists:orders,id',
            'ik_inv_id' => 'required|integer',
            'ik_inv_st' => 'required|string',
        ];
    }
}
