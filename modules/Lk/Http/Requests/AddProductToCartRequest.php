<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 11.09.18
 * Time: 11:18
 */

namespace Modules\Lk\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddProductToCartRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'quantity' => 'required|integer|min:1',
        ];
    }
}