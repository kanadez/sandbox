<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 24.07.18
 * Time: 10:46
 */

namespace Modules\Core\Http\Controllers\Api\Nodes;

use Modules\Core\Http\Controllers\Controller;
use Modules\Core\Model\Node;
use Modules\Core\Http\Requests\Node as NodeRequest;
use Modules\Core\Model\ProxyPort;

class PortsController extends Controller
{

    function index($node_id) {
        $query = ProxyPort::selectRaw('id, concat(name, " (HTTP: ", http, ", Socks: ", socks, ")") as name, user_id, created_at')->where('node_id', $node_id)->orderBy('id', 'desc');

        return $this->table($query, [
            'id',
            'name',
            ['user.name' => 'user.email'],
            'created_at',
        ]);
    }



}
