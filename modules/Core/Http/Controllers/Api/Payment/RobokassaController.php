<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 26.07.18
 * Time: 12:10
 */

namespace Modules\Core\Http\Controllers\Api\Payment;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Modules\Core\Http\Controllers\Api\BaseController;
use Modules\Core\Http\Requests\Api\RobokassaSuccessRequest;
use Modules\Core\Services\Payments\RobokassaService;

class RobokassaController extends BaseController
{
    /**
     * @var RobokassaService
     */
    private $service;

    /**
     * RobokassaController constructor.
     * @param RobokassaService $service
     */
    public function __construct(RobokassaService $service)
    {
        $this->service = $service;
    }

    /**
     * @param RobokassaSuccessRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function success(RobokassaSuccessRequest $request)
    {
        $data = $request->validated();

        if ($this->service->successPayment($data["InvId"], $data["SignatureValue"])) {
            return response('Ok', 200);
        }
        return response('Error', 500);
    }
}