<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 30.07.18
 * Time: 11:50
 */

namespace Modules\Core\Http\Controllers\Api\Payment;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Modules\Core\Http\Controllers\Api\BaseController;
use Modules\Core\Services\Payments\InterkassaService;

class InterkassaController extends BaseController
{
    /**
     * @var InterkassaService
     */
    private $service;

    /**
     * InterkassaController constructor.
     * @param InterkassaService $service
     */
    public function __construct(InterkassaService $service)
    {
        $this->service = $service;
    }

    public function success(Request $request)
    {
        if ($this->service->successPayment($request)) {
            return response('Ok', 200);
        }
        return response('Error', 500);
    }
}