<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 24.07.18
 * Time: 10:46
 */

namespace Modules\Core\Http\Controllers\Api;

use Modules\Core\Http\Controllers\Controller;
use Modules\Core\Model\Node;
use Modules\Core\Http\Requests\Node as NodeRequest;

class NodesController extends Controller
{

    function index() {
        $query = Node::selectRaw('*');

        return $this->table($query, [
            'id',
            'slug',
            'name',
            'ip',
            'created_at'
        ]);
    }

    function show($id) {
        $node = Node::findOrFail($id);

        return $this->ok($node->only('id', 'name', 'ip', 'slug'));
    }

    function store(NodeRequest $request) {
        $node = Node::create(array_merge(
            $request->only('slug'),
            [
                'name' => $request->get('slug') . '.' . env('APP_DOMAIN', 'astroproxy.com'),
                'domain' => env('APP_DOMAIN', 'astroproxy.com'),
                'ip' => $this->ip($request),
                'created_at' => date('Y-m-d H:i:s'),
            ]
        ));

        return $this->ok();
    }

    function update(NodeRequest $request, $id) {
        $node = Node::findOrFail($id);

        $node->update(array_merge(
            $request->only('slug'),
            [
                'name' => $request->get('slug') . '.' . env('APP_DOMAIN', 'astroproxy.com'),
                'ip' => $this->ip($request),
            ]
        ));

        return $this->ok();
    }

    function ip($request) {
        $host = $request->get('slug') . "." . env("APP_DOMAIN", 'astroproxy.com');
        $ip = gethostbyname($host);
        return $ip == $host ? null : $ip;
    }

    function destroy($id) {
        $node = Node::findOrFail($id);
        $node->delete();

        return $this->ok();
    }

}
