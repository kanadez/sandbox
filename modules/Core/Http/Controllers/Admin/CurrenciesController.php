<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 05.09.18
 * Time: 11:58
 */

namespace Modules\Core\Http\Controllers\Admin;


use Modules\Core\Criteria\DefaultSortCriteria;
use Modules\Core\Http\Requests\Admin\StoreCurrencyRequest;
use Modules\Core\Repository\CurrenciesRepository;

class CurrenciesController extends BaseController
{
    /**
     * @var CurrenciesRepository
     */
    private $repository;

    /**
     * CurrenciesController constructor.
     * @param CurrenciesRepository $repository
     */
    public function __construct(CurrenciesRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $items = $this->repository
            ->pushCriteria(DefaultSortCriteria::class)
            ->paginate($this->pageSize);

        return view("core::admin.currency.index", [
            "items" => $items,
        ]);
    }

    public function create()
    {
        $item = $this->repository->makeModel();

        return view("core::admin.currency.edit", [
            "item" => $item,
        ]);
    }

    public function edit(int $itemId)
    {
        $item = $this->repository->find($itemId);

        return view("core::admin.currency.edit", [
            "item" => $item,
        ]);
    }

    public function store(int $itemId = null, StoreCurrencyRequest $request)
    {
        $data = $request->validated();

        /** @var Node $item */
        $item = $this->repository->updateOrCreate(["id" => $itemId], $data);

        flash("Item successfully saved!")->success();

        return redirect()->route("admin.currency.edit", $item);
    }

    public function delete(int $itemId)
    {
        $this->repository->delete($itemId);

        flash("Item successfully removed!")->success();

        return redirect()->back();
    }
}