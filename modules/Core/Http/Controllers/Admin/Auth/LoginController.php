<?php

namespace Modules\Core\Http\Controllers\Admin\Auth;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Modules\Core\Http\Controllers\Admin\BaseController;

class LoginController extends BaseController
{
    use AuthenticatesUsers {
        logout as protected laravelLogout;
    }
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->redirectTo = route("admin.index");
        $this->middleware('guest-admin')->except('logout');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('core::admin.auth.login');
    }

    public function logout(Request $request)
    {
        $this->laravelLogout($request);
        return redirect(route("admin.login"));
    }
}
