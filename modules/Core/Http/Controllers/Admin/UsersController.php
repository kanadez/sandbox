<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 19.07.18
 * Time: 13:59
 */

namespace Modules\Core\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Route;
use Modules\Core\Criteria\DcNodes;
use Modules\Core\Criteria\DefaultSortCriteria;
use Modules\Core\Criteria\NodeWithFreePorts;
use Modules\Core\Criteria\RsdNodes;
use Modules\Core\Events\UserPortAdded;
use Modules\Core\Events\UserPortRemoved;
use Modules\Core\Http\Requests\Admin\StoreUserDcPorts;
use Modules\Core\Http\Requests\CreateUserRequest;
use Modules\Core\Http\Requests\StoreUserRequest;
use Modules\Core\Http\Requests\UpdateUserRequest;
use Modules\Core\Model\User;
use Modules\Core\Repository\NodePortsRepository;
use Modules\Core\Repository\NodesRepository;
use Modules\Core\Repository\RolesRepository;
use Modules\Core\Repository\UsersRepository;

class UsersController extends BaseController
{
    /**
     * @var UsersRepository
     */
    private $repository;
    /**
     * @var array
     */
    private $roles;
    /**
     * @var Collection
     */
    private $dcNodes;
    /**
     * @var Collection
     */
    private $rsdNodes;
    /**
     * @var NodesRepository
     */
    private $nodesRepository;
    /**
     * @var NodePortsRepository
     */
    private $nodePortsRepository;


    /**
     * UsersController constructor.
     * @param UsersRepository $repository
     * @param RolesRepository $rolesRepository
     * @param NodesRepository $nodesRepository
     * @param NodePortsRepository $nodePortsRepository
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function __construct(UsersRepository $repository,
                                RolesRepository $rolesRepository,
                                NodesRepository $nodesRepository,
                                NodePortsRepository $nodePortsRepository)
    {
        $this->repository = $repository;
        $this->roles = $rolesRepository
            ->all(["id", "name"])
            ->toArray();

        Route::model('user', User::class);
        $this->dcNodes = $nodesRepository
            ->resetCriteria()
            ->pushCriteria(DcNodes::class)
//            ->pushCriteria(NodeWithFreePorts::class)
            ->all();
        $this->rsdNodes = $nodesRepository
            ->resetCriteria()
            ->pushCriteria(RsdNodes::class)
//            ->pushCriteria(NodeWithFreePorts::class)
            ->all();

        $this->nodesRepository = $nodesRepository;
        $this->nodePortsRepository = $nodePortsRepository;
    }

    public function index()
    {
        $items = $this->repository
            ->with(["roles"])
            ->pushCriteria(DefaultSortCriteria::class)
            ->paginate($this->pageSize);

        return view("core::admin.user.index", [
            "items" => $items,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function create()
    {
        $item = $this->repository->makeModel();

        return view("core::admin.user.edit", [
            "item" => $item,
            "roles" => $this->roles,
            "item_roles" => $item->roles->pluck("id")->toArray(),
            /*"dc_nodes" => $this->dcNodes,
            "rsd_nodes" => $this->rsdNodes,*/
        ]);
    }

    public function edit(int $itemId)
    {
        $item = $this->repository->find($itemId);

        return view("core::admin.user.edit", [
            "item" => $item,
            "roles" => $this->roles,
            "item_roles" => $item->roles->pluck("id")->toArray(),
            /*"dc_nodes" => $this->dcNodes,
            "rsd_nodes" => $this->rsdNodes,*/
        ]);
    }

    /**
     * @param int $itemId
     * @param StoreUserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(int $itemId = null, StoreUserRequest $request)
    {
        $data = $request->validated();

        if (!empty($data["password"])) {
            $data["password"] = bcrypt($data["password"]);
        }

        /** @var User $item */
        $item = $this->repository->updateOrCreate(["id" => $itemId], $request->validated());

        $item->roles()->sync($request->input("roles"));

        $synced = $item->dcPorts()->sync(array_get($data, "ports", []));

        $this->sendCommands($item, $synced);

        flash("Item successfully saved!")->success();

        return redirect()->route("admin.user.edit", $item);
    }

    public function delete(int $itemId)
    {
        $this->repository->delete($itemId);

        flash("Item successfully removed!")->success();

        return redirect()->back();
    }

    private function sendCommands(User $user, array $synced)
    {
        // Send gen user command
        foreach ($synced["attached"] as $attached) {
            $port = $this->nodePortsRepository->with(["node"])->find($attached);
            event(new UserPortAdded($port["node"], $port, auth()->user(), $user));
        }

        // Send del user command
        foreach ($synced["detached"] as $detached) {
            $port = $this->nodePortsRepository->with(["node"])->find($detached);
            event(new UserPortRemoved($port["node"], $port, auth()->user(), $user));
        }
    }
}