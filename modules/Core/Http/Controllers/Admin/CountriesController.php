<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 06.08.18
 * Time: 10:57
 */

namespace Modules\Core\Http\Controllers\Admin;


use Modules\Core\Criteria\DefaultSortCriteria;
use Modules\Core\Http\Requests\Admin\StoreCountryRequest;
use Modules\Core\Repository\CountriesRepository;

class CountriesController extends BaseController
{
    /**
     * @var CountriesRepository
     */
    private $repository;

    /**
     * CountriesController constructor.
     * @param CountriesRepository $repository
     */
    public function __construct(CountriesRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index()
    {
        $items = $this->repository
            ->pushCriteria(DefaultSortCriteria::class)
            ->paginate($this->pageSize);

        return view("core::admin.country.index", [
            "items" => $items,
        ]);
    }

    public function create()
    {
        $item = $this->repository->makeModel();

        return view("core::admin.country.edit", [
            "item" => $item,
        ]);
    }

    public function edit(int $itemId)
    {
        $item = $this->repository->find($itemId);

        return view("core::admin.country.edit", [
            "item" => $item,
        ]);
    }

    public function store(int $itemId = null, StoreCountryRequest $request)
    {
        $data = $request->validated();

        /** @var Node $item */
        $item = $this->repository->updateOrCreate(["id" => $itemId], $data);

        flash("Item successfully saved!")->success();

        return redirect()->route("admin.country.edit", $item);
    }

    public function delete(int $itemId)
    {
        $this->repository->delete($itemId);

        flash("Item successfully removed!")->success();

        return redirect()->back();
    }
}