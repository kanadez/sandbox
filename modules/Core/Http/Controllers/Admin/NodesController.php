<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 30.07.18
 * Time: 13:54
 */

namespace Modules\Core\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Modules\Core\Contracts\DnsContract;
use Modules\Core\Criteria\DefaultSortCriteria;
use Modules\Core\Criteria\NodePortByNode;
use Modules\Core\Criteria\NodePortWithNode;
use Modules\Core\Criteria\NodeWithAll;
use Modules\Core\Events\NodeSaved;
use Modules\Core\Http\Requests\Admin\StoreNodePortRequest;
use Modules\Core\Http\Requests\Admin\StoreNodeRequest;
use Modules\Core\Model\Node;
use Modules\Core\Repository\DcsRepository;
use Modules\Core\Repository\FingerPrintTypesRepository;
use Modules\Core\Repository\NodeAssignmentsRepository;
use Modules\Core\Repository\NodeDomainsRepository;
use Modules\Core\Repository\NodePortsRepository;
use Modules\Core\Repository\NodeProvidersRepository;
use Modules\Core\Repository\NodeRotationPeriodsRepository;
use Modules\Core\Repository\NodesRepository;
use Modules\Core\Repository\ProxiesRepository;
use Modules\Core\Repository\ResidentialsRepository;
use Illuminate\Support\Facades\Redirect;

class NodesController extends BaseController
{
    /**
     * @var NodesRepository
     */
    private $repository;
    /**
     * @var array
     */
    private $dcs;
    /**
     * @var NodePortsRepository
     */
    private $nodePortsRepository;
    /**
     * @var DnsContract
     */
    private $dns;
    /**
     * @var Collection
     */
    private $residentials;
    /**
     * @var NodeAssignmentsRepository
     */
    private $nodeAssignmentsRepository;
    /**
     * @var NodeRotationPeriodsRepository
     */
    private $nodeRotationPeriodsRepository;
    /**
     * @var DcsRepository
     */
    private $dcsRepository;
    /**
     * @var ResidentialsRepository
     */
    private $residentialsRepository;

    /**
     * NodesController constructor.
     * @param NodesRepository $repository
     * @param DcsRepository $dcsRepository
     * @param NodePortsRepository $nodePortsRepository
     * @param NodeAssignmentsRepository $nodeAssignmentsRepository
     * @param NodeRotationPeriodsRepository $nodeRotationPeriodsRepository
     * @param ResidentialsRepository $residentialsRepository
     * @param DnsContract $dns
     */
    public function __construct(NodesRepository $repository,
                                DcsRepository $dcsRepository,
                                NodePortsRepository $nodePortsRepository,
                                NodeAssignmentsRepository $nodeAssignmentsRepository,
                                NodeRotationPeriodsRepository $nodeRotationPeriodsRepository,
                                ResidentialsRepository $residentialsRepository,
                                DnsContract $dns)
    {
        $this->repository = $repository;
        $this->dcs = $dcsRepository->all()->pluck("name", "id")->toArray();
        $this->nodePortsRepository = $nodePortsRepository;
        $this->dns = $dns;
        $this->residentials = $residentialsRepository->all()->pluck("name", "id")->toArray();
        $this->nodeAssignmentsRepository = $nodeAssignmentsRepository;
        $this->nodeRotationPeriodsRepository = $nodeRotationPeriodsRepository;
        $this->dcsRepository = $dcsRepository;
        $this->residentialsRepository = $residentialsRepository;
    }

    public function index()
    {
        $items = $this->repository
            ->pushCriteria(NodeWithAll::class)
            ->pushCriteria(DefaultSortCriteria::class)
            ->paginate($this->pageSize);

        return view("core::admin.node.index", [
            "items" => $items,
        ]);
    }

    public function create()
    {
        $item = $this->repository->makeModel();
        $assignments =  $this->nodeAssignmentsRepository->all();
        $rotationPeriods =  $this->nodeRotationPeriodsRepository->all();
        $dcs =  $this->dcsRepository->all();
        $residentials =  $this->residentialsRepository->all();

        return view("core::admin.node.edit", [
            "item" => $item,
            "assignments" => $assignments,
            "rotationPeriods" => $rotationPeriods,
            "dcs" => $dcs,
            "residentials" => $residentials,
        ]);
    }

    public function edit(int $itemId)
    {
        $item = $this->repository->find($itemId);
        $assignments =  $this->nodeAssignmentsRepository->all();
        $rotationPeriods =  $this->nodeRotationPeriodsRepository->all();
        $dcs =  $this->dcsRepository->all();
        $residentials =  $this->residentialsRepository->all();

        return view("core::admin.node.edit", [
            "item" => $item,
            "assignments" => $assignments,
            "rotationPeriods" => $rotationPeriods,
            "dcs" => $dcs,
            "residentials" => $residentials,
        ]);
    }

    /**
     * @param int|null $itemId
     * @param StoreNodeRequest $request
     * @param NodeAssignmentsRepository $nodeAssignmentsRepository
     * @param NodeRotationPeriodsRepository $nodeRotationPeriodsRepository
     * @param NodeProvidersRepository $nodeProvidersRepository
     * @param NodeDomainsRepository $nodeDomainsRepository
     * @return void
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(int $itemId = null,
                          StoreNodeRequest $request)
    {
        
        $request->request->add(['name' => $request->input('slug') . "." . env("APP_DOMAIN")]);
        $validator = Validator::make($request->all(), [
            'name' => 'string|unique:nodes,name,'.$itemId,
            'slug' => 'required|string',
        ]);
        
        if ($validator->fails()) {
            return Redirect::to('/admin/node/create')
                ->withErrors($validator)
                ->withInput();
        }

        $data = $request->all();

        // ToDO: remove hardcode
        $data["domain"] = env("APP_DOMAIN");
        $data["is_test"] = false;

        /** @var Node $item */
        $item = $this->repository->updateOrCreate(["id" => $itemId], $data);

        event(new NodeSaved($item));

        flash("Item successfully saved!")->success();

        return redirect()->route("admin.node.index");
    }

    public function delete(int $itemId)
    {
        $this->repository->delete($itemId);

        flash("Item successfully removed!")->success();

        return redirect()->back();
    }

    public function proxy(int $itemId)
    {
        $item = $this->repository->with(["proxies"])->find($itemId);

        return view("core::admin.proxy.index", [
            "item" => $item,
        ]);
    }

    /**
     * @param int $itemId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function port(int $itemId)
    {
        $item = $this->repository->find($itemId);

        /** @var Collection $items */
        $items = $this->nodePortsRepository
            ->pushCriteria(NodePortWithNode::class)
            ->pushCriteria(new NodePortByNode($itemId))
            ->pushCriteria(DefaultSortCriteria::class)
            ->paginate($this->pageSize);

        return view("core::admin.node.port", [
            "items" => $items,
            "item" => $item,
        ]);
    }

    public function portEdit(int $nodeId, int $itemId)
    {
        return app()->call('Modules\Core\Http\Controllers\Admin\NodePortsController@edit', ['itemId' => $itemId, 'nodeId' => $nodeId]);
    }

    public function portStore(int $nodeId, int $itemId = null, StoreNodePortRequest $request)
    {
        $response = app()->call('Modules\Core\Http\Controllers\Admin\NodePortsController@store', ['itemId' => $itemId, $request]);

        return redirect()->route("admin.node.port", [$nodeId]);
    }

    public function fingerPrint(int $nodeId, int $itemId)
    {
        $response = app()->call('Modules\Core\Http\Controllers\Admin\NodePortsController@fingerPrint', ['itemId' => $itemId]);

        return redirect()->route("admin.node.port", [$nodeId]);
    }

    public function fingerPrintApply(int $nodeId, int $itemId)
    {
        $response = app()->call('Modules\Core\Http\Controllers\Admin\NodePortsController@fingerPrintApply', ['itemId' => $itemId]);

        return redirect()->route("admin.node.port", [$nodeId]);
    }


    public function portDelete(int $nodeId, int $itemId)
    {
        return app()->call('Modules\Core\Http\Controllers\Admin\NodePortsController@delete', ['itemId' => $itemId]);
    }

    public function syncDns(int $itemId)
    {
        $item = $this->repository->find($itemId);

        $this->repository->update(["ip" => $this->dns->getIp($item["name"])], $itemId);

        flash("Dns successfully updated!")->success();

        return redirect()->back();
    }

    private function getValidationString($param)
    {
        return $param ?? 'null';
    }

    public function lookup(Request $request)
    {
        $validator = Validator::make($request->all(), [
           'host' => 'required'
        ]);
        
        if ($validator->fails()) {
            $validator->messages()->first();
            return $response = response()->json([
                "status" => "error",
                "responce" => $validator->messages()->first(),
            ]);
        }

        $host = $request->input('host');
        //$SuchNodeMaxNumber = intval(Node::where('name', 'like', '%' . $host . '%')->max('number')) + 1;
        //$nodeName = $host . "-" . $SuchNodeMaxNumber . "." . env("APP_DOMAIN");
        $nodeName = $host . "." . env("APP_DOMAIN");
        $ip = gethostbyname($nodeName);
        if( $ip == $host . "." . env("APP_DOMAIN") ) {
            return $response = response()->json([
                "status" => "success",
                "responce" => 'no resolve / check DNS',
            ]);
        } else {
            return $response = response()->json([
                "status" => "success",
                "responce" => $ip,
            ]);
        }
    }
}