<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 11.09.18
 * Time: 11:46
 */

namespace Modules\Core\Http\Controllers\Admin;


use Illuminate\Http\Request;
use Modules\Core\Repository\ProxyPortsRepository;
use Modules\Core\Criteria\AdminProxyPortsCriteria;
use Modules\Core\Http\Requests\StoreProxyPortRequest as StoreProxyPortRequest;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Modules\Core\Model\DefaultProxyPortSettings;
use Modules\Core\Model\CountryForProxyPort;
use Modules\Core\Model\CityForProxyPort;
use Modules\Core\Model\Node;
use Modules\Core\Model\ProxyPort;
use Modules\Core\Model\User;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class ProxyPortsController extends BaseController
{

  /**
     * @var ProxyPortsRepository
     */
    private $proxyPortsRepository;

    /**
     * ProxyPortsController constructor.
     * @param ProxyPortsRepository $proxyPortsRepository
     */
    public function __construct( ProxyPortsRepository $proxyPortsRepository)
    {

        $this->proxyPortsRepository = $proxyPortsRepository;

    }

    public function index(int $userId)
    {
        $proxyPorts = $this->proxyPortsRepository
            ->resetCriteria()
            ->pushCriteria(new AdminProxyPortsCriteria($userId))
            ->with([
                "country",
                "city",
                "node",
            ])
            ->all();

        return view("core::admin.proxy_port.index", [
            "proxyPorts" => $proxyPorts,
            "userId" => $userId
        ]);
    }


    public function make( int $userId )
    {
        $item = $this->proxyPortsRepository->makeModel();

        return view("core::admin.proxy_port.create", [
            "item" => $item,
            "userId" => $userId
        ]);

    }

    public function edit(int $proxyPortId)
    {
        $item = $this->proxyPortsRepository->find($proxyPortId);

        return view("core::admin.proxy_port.create", [
            "item" => $item,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(int $itemId = null,StoreProxyPortRequest $request)
    {
        $data = $request->validated();

        if( !isset($data['country_id']) ){
            $data['country_id'] = null;
        }
        if( !isset($data['city_id']) ){
            $data['city_id'] = null;
        }

        if( $itemId == null || $this->proxyPortsRepository->find($itemId)->node_id != $data['node_id'] ) {
            $data['http'] = $this->getUniquePort($data['node_id'], 'http');
            $data['socks'] = $data['http']+50;
        }

        $proxyPort = $this->proxyPortsRepository->updateOrCreate(["id" => $itemId], $data);

        if( $itemId == null || $this->proxyPortsRepository->find($itemId)->node_id != $data['node_id'] ) {
            $isExportedByAPI = $this->proxyGenerateApiRequest($data);
            if( $isExportedByAPI['Result'] == 'OK' ) {
                $r = $this->proxyPortsRepository->updateOrCreate(["id" => $proxyPort->id], ['is_exported_by_api' => 1]);
            } else {
                return redirect()->route("admin.proxy-port.index", $data['user_id'])->withErrors([$isExportedByAPI['error']]);
            }
        }
        
        return redirect()->route("admin.proxy-port.index");
           
    }

    public function details( int $proxyPortId )
    {
        $proxyPort = $this->proxyPortsRepository
            ->resetCriteria()
            ->with([
                "country",
                "city",
                "node",
            ])
            ->find($proxyPortId);

        return view("core::admin.proxy_port.details", [
            "proxyPort" => $proxyPort,
            "mounthCostsIP" => Config::get('prices.ipMonthPrice'),
            "mounthCostsTrafic" => Config::get('prices.GbMonthPrice')*$proxyPort->trafic
        ]);
    }

    public function delete (int $proxyPortId)
    {
        $proxyPort = $this->proxyPortsRepository->find($proxyPortId);
        $delete = $this->proxyDeleteApiRequest( $proxyPort );
        if( $delete['Result'] == 'OK' || $proxyPort->is_exported_by_api != 1 ) {
            $this->proxyPortsRepository->delete($proxyPortId);
            flash("Proxy port successfully removed!")->success();
            return redirect()->back();
        } else {
            return redirect()->route("admin.proxy-port.index", $proxyPort->user_id)->withErrors([$delete['error']]);
        }

    }


    public function getCities( Request $request ) 
    {
        $countryId = $request->input('country_id');

        $cities = DB::table('all_cities')->where('country_id',$countryId)->pluck("name","id")->all();
        $data = view('core::admin.proxy_port.ajax-select',compact('cities'))->render();
        return response()->json(['options'=>$data]);
    }


    public function getUniquePort( $nodeId, $portType = 'http' )
    {
        $port = rand(10000, 30000);
        $isUnique = DB::table('proxy_ports')->where('node_id', $nodeId)->where('http', $port)->get()->count();
        if( $isUnique < 1 ) {
            return $port;
        } else {
            $this->getUniquePort( $nodeId, 'http' );
        }
    }

    private function proxyGenerateApiRequest($data)
    {
        $defaultSettings = DefaultProxyPortSettings::all();
        $defaultSettings = $defaultSettings[0];
        if( !is_null($data['country_id']) && $data['country_id'] != '' ) {
            $country = CountryForProxyPort::find($data['country_id']);
            $countryName = $country->name;
        } else {
            $countryName = '';
        }
        if( !is_null($data['city_id']) && $data['city_id'] != '' ) {
            $city = CityForProxyPort::find($data['city_id']);
            $cityName = $city->name;
        } else {
            $cityName = '';
        }

        $user =  User::find($data['user_id']);

        $node = Node::find( $data['node_id'] );
       
        $request = [
            "Email" => $user->email,
            "Config" => [
                "PeerRotate" => $defaultSettings->peer_rotate,
                "PeerInfo" => [
                    "Country"       => $countryName,
                    "City"          => $cityName,
                    "Fingerprint"   => (string)$defaultSettings->fingerprint,
                    "Type"          => $data['type'],
                    "Asn"           => (string)$defaultSettings->asn,
                    "Uptime"        => (int)$defaultSettings->uptime,
                    "Latency"       => $defaultSettings->latency,
                    "SpeedDownload" => $defaultSettings->speed_download,
                    "SpeedUpload"   => $defaultSettings->speed_upload,
                ],
                "HTTP"      => $defaultSettings->http,
                "Socks"     => $defaultSettings->socks,
                "AllowedIP" => [],
                "Auth" => [
                    "Username" => $defaultSettings->username,
                    "Password" => $defaultSettings->password,
                ],
                "MaxConnections" => $defaultSettings->max_connections,
                "Bandwidth"      => [
                    "In" => $defaultSettings->bandwidth_in,
                    "Out" => $defaultSettings->bandwidth_out,
                ],
                "TrafficLimit" => $data['trafic'],
                "TimeLimit"    => $defaultSettings->time_limit
            ],
        ];

        $data = json_encode($request);

        $client = new Client([
            'headers' => ['Content-Type' => 'application/json']
        ]);
        try {
            $response = $client->post('http://'.$node->ip.':8103/api/cmd/proxyGenerate', 
                ['body' => $data]
            );
            return json_decode($response->getBody(), true);
        } catch (GuzzleException $e) {
            return ['Result'=> 'error', 'error'=>$e->getMessage()];
        } 
    }


    public function proxyDeleteApiRequest( $proxyPort )
    {
        $node = Node::find( $proxyPort->node_id );
        $client = new Client();
        try {
            $response = $client->post('http://'.$node->ip.':8103/api/cmd/proxyRemove?email='.Auth::user()->email.'&httpPort='.$proxyPort->http.'&socksPort='.$proxyPort->socks);
            return json_decode($response->getBody(), true);
        }  catch (GuzzleException $e) {
            return ['Result'=> 'error', 'error'=>$e->getMessage()];
        }
        
    }
}