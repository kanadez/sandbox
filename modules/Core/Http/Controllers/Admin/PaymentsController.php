<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 26.07.18
 * Time: 15:12
 */

namespace Modules\Core\Http\Controllers\Admin;


use Modules\Core\Repository\OrdersRepository;

class PaymentsController extends BaseController
{
    /**
     * @var OrdersRepository
     */
    private $repository;

    /**
     * PaymentsController constructor.
     * @param OrdersRepository $repository
     */
    public function __construct(OrdersRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $items = $this->repository
            ->with([
                "payment",
                "payment.paysystem",
                "payment.status",
            ])
            ->orderBy("created_at", "desc")
            ->paginate($this->pageSize);

        return view("core::admin.payment.index", [
            "items" => $items,
        ]);
    }
}