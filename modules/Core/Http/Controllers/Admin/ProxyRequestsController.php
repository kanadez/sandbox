<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 24.07.18
 * Time: 8:43
 */

namespace Modules\Core\Http\Controllers\Admin;

use Modules\Core\Repository\ProxyRequestsRepository;
use Illuminate\Support\Facades\Response;

class ProxyRequestsController extends BaseController
{
    /**
     * @var ProxyRequestsRepository
     */
    private $repository;

    /**
     * ProxyRequestsController constructor.
     * @param ProxyRequestsRepository $repository
     */
    public function __construct(ProxyRequestsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $items = $this->repository
            ->with(["user"])
            ->orderBy("created_at", "desc")
            ->paginate($this->pageSize);

        return view("core::admin.proxy_request.index", [
            "items" => $items,
        ]);
    }

    public function export()
    {

        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=file.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );
        $items = $this->repository
            ->with(["user"])
            ->orderBy("created_at", "desc")->get();
        $columns = array('RequestId', 'UserEmail', 'Telegram', 'Skype', 'UserNotified', 'IsProcessed', 'CreatedAt');

        $callback = function() use ($items, $columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach($items as $item) {

                fputcsv($file, array($item["id"], $item->user["email"], $item["telegram"], $item["skype"], $item["user_notified"], $item["is_processed"], $item["created_at"]));
            }
            fclose($file);
        };

        return Response::stream($callback, 200, $headers);

    }

    /**
     * @param int $userId
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function create(int $userId)
    {
        $this->repository->updateOrCreate([
            "user_id" => $userId,
        ], [
            "user_id" => $userId,
        ]);

        flash("Proxy request successfully created!")->success();

        return redirect()->back();
    }

    /**
     * @param int $itemId
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function approve(int $itemId)
    {
        $this->repository->update(["is_processed" => true], $itemId);

        flash("Item marked as processed!")->success();

        return redirect()->back();
    }

    /**
     * @param int $itemId
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function unapprove(int $itemId)
    {
        $this->repository->update(["is_processed" => false], $itemId);

        flash("Item marked as un processed!")->success();

        return redirect()->back();
    }

    /**
     * @param int $itemId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(int $itemId)
    {
        $this->repository->delete($itemId);

        flash("Item successfully removed!")->success();

        return redirect()->back();
    }
}