<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 07.08.18
 * Time: 14:50
 */

namespace Modules\Core\Http\Controllers\Admin;


use Illuminate\Support\Collection;
use Modules\Core\Contracts\NodePortsCollectionContract;
use Modules\Core\Criteria\DefaultSortCriteria;
use Modules\Core\Criteria\NodePortWithNode;
use Modules\Core\Criteria\NodeWithAll;
use Modules\Core\Events\FingerPrintApplied;
use Modules\Core\Events\FingerPrintProposed;
use Modules\Core\Events\NodePortSaved;
use Modules\Core\Http\Requests\Admin\StoreNodePortRequest;
use Modules\Core\Model\NodePort;
use Modules\Core\Repository\FingerPrintTypesRepository;
use Modules\Core\Repository\NodePortsRepository;
use Modules\Core\Repository\NodesRepository;

class NodePortsController extends BaseController
{
    /**
     * @var NodePortsRepository
     */
    private $repository;
    /**
     * @var NodesRepository
     */
    private $nodesRepository;
    /**
     * @var array
     */
    private $nodes;
    /**
     * @var NodePortsCollectionContract
     */
    private $collection;
    /**
     * @var Collection
     */
    private $fingerPrints;

    /**
     * NodePortsController constructor.
     * @param NodePortsRepository $repository
     * @param NodesRepository $nodesRepository
     * @param NodePortsCollectionContract $collection
     * @param FingerPrintTypesRepository $fingerPrintTypesRepository
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function __construct(NodePortsRepository $repository,
                                NodesRepository $nodesRepository,
                                NodePortsCollectionContract $collection,
                                FingerPrintTypesRepository $fingerPrintTypesRepository)
    {
        $this->repository = $repository;
        $this->nodesRepository = $nodesRepository;
        $this->nodes = $nodesRepository->pushCriteria(NodeWithAll::class)->all()->pluck("name", "id")->toArray();
        $this->collection = $collection;
        $this->fingerPrints = $fingerPrintTypesRepository->all()->pluck("description", "id")->toArray();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index()
    {
        $items = $this->repository
            ->pushCriteria(NodePortWithNode::class)
            ->pushCriteria(DefaultSortCriteria::class)
            ->paginate($this->pageSize);

        return view("core::admin.node_port.index", [
            "items" => $items,
        ]);
    }

    public function create(int $itemId = null)
    {
        $item = $this->repository->makeModel();
        $node = null;
        $numbers = collect();
        if ($itemId) {
            $node = $this->nodesRepository->with(["ports"])->find($itemId);
            $numbers = $node->ports->pluck("number");
        }


        return view("core::admin.node_port.edit", [
            "item" => $item,
            "node" => $node,
            "nodes" => $this->nodes,
            "fingerPrints" => $this->fingerPrints,
            "numbers" => $this->collection->diff($numbers),
        ]);
    }

    public function edit(int $itemId, int $nodeId = null)
    {
        $item = $this->repository->find($itemId);
        $node = null;
        $numbers = collect();
        if ($nodeId) {
            $node = $this->nodesRepository->with(["ports"])->find($nodeId);
            $numbers = $node->ports->pluck("number", "number")->forget($item["number"]);
        }

        return view("core::admin.node_port.edit", [
            "item" => $item,
            "node" => $node,
            "nodes" => $this->nodes,
            "fingerPrints" => $this->fingerPrints,
            "numbers" => $this->collection->diff($numbers),
        ]);
    }

    public function store(int $itemId = null, StoreNodePortRequest $request)
    {
        $data = $request->validated();

        /** @var NodePort $item */
        $item = $this->repository->updateOrCreate(["id" => $itemId], $data);

        event(new NodePortSaved($item->node, $item, auth()->user()));

        flash("Item successfully saved!")->success();

        return redirect()->route("admin.node_port.edit", $item);
    }

    public function delete(int $itemId)
    {
        $this->repository->delete($itemId);

        flash("Item successfully removed!")->success();

        return redirect()->back();
    }

    public function fingerPrint(int $itemId)
    {
        $item = $this->repository->find($itemId);

        event(new FingerPrintProposed($item["node"], $item));

        flash("Fingerprint was processed!")->success();

        return redirect()->back();
    }

    public function fingerPrintApply(int $itemId)
    {
        $item = $this->repository->find($itemId);

        event(new FingerPrintApplied($item["node"], $item));

        flash("Fingerprint was applied!")->success();

        return redirect()->back();
    }

}