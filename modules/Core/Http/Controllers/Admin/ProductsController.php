<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 20.09.18
 * Time: 15:46
 */

namespace Modules\Core\Http\Controllers\Admin;


use Modules\Core\Criteria\DefaultSortCriteria;
use Modules\Core\Http\Requests\Admin\StoreProductRequest;
use Modules\Core\Model\Product;
use Modules\Core\Repository\CurrenciesRepository;
use Modules\Core\Repository\PricesRepository;
use Modules\Core\Repository\ProductsRepository;

class ProductsController extends BaseController
{
    /**
     * @var ProductsRepository
     */
    private $productsRepository;
    /**
     * @var CurrenciesRepository
     */
    private $currenciesRepository;
    /**
     * @var PricesRepository
     */
    private $pricesRepository;

    /**
     * ProductsController constructor.
     * @param ProductsRepository $productsRepository
     * @param PricesRepository $pricesRepository
     * @param CurrenciesRepository $currenciesRepository
     */
    public function __construct(ProductsRepository $productsRepository,
                                PricesRepository $pricesRepository,
                                CurrenciesRepository $currenciesRepository)
    {
        $this->productsRepository = $productsRepository;
        $this->currenciesRepository = $currenciesRepository;
        $this->pricesRepository = $pricesRepository;
    }

    public function index()
    {
        $items = $this->productsRepository
            ->resetCriteria()
            ->pushCriteria(DefaultSortCriteria::class)
            ->paginate($this->pageSize);

        return view("core::admin.product.index", [
            "items" => $items,
        ]);
    }

    public function create()
    {
        $item = $this->productsRepository->makeModel();
        $currencies = $this->currenciesRepository->all();

        return view("core::admin.product.edit", [
            "item" => $item,
            "currencies" => $currencies,
        ]);
    }

    public function edit(int $itemId)
    {
        $item = $this->productsRepository->find($itemId);
        $currencies = $this->currenciesRepository->all();

        return view("core::admin.product.edit", [
            "item" => $item,
            "currencies" => $currencies,
        ]);
    }

    public function store(int $itemId = null, StoreProductRequest $request)
    {
        $data = $request->validated();

        /** @var Product $item */
        $item = $this->productsRepository->updateOrCreate(["id" => $itemId], $data);

        $price = $this->pricesRepository->updateOrCreate(["id" => $item->price["id"]], [
            "product_id" => $item["id"],
            "price_type_id" => Product::PRICE_TYPE,
            "currency_id" => $data["currency_id"],
            "value" => $data["price"],
        ]);

        flash("Item successfully saved!")->success();

        return redirect()->route("admin.product.edit", $item);
    }

    public function delete(int $itemId)
    {
        $this->productsRepository->delete($itemId);

        flash("Item successfully removed!")->success();

        return redirect()->back();
    }
}