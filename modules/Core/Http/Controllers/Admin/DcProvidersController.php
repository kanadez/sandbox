<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 06.08.18
 * Time: 11:12
 */

namespace Modules\Core\Http\Controllers\Admin;

use Modules\Core\Criteria\DefaultSortCriteria;
use Modules\Core\Http\Requests\Admin\StoreDcProviderRequest;
use Modules\Core\Repository\DcProvidersRepository;

class DcProvidersController extends BaseController
{
    /**
     * @var DcProvidersRepository
     */
    private $repository;

    public function __construct(DcProvidersRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index()
    {
        $items = $this->repository
            ->pushCriteria(DefaultSortCriteria::class)
            ->paginate($this->pageSize);

        return view("core::admin.dc_provider.index", [
            "items" => $items,
        ]);
    }

    public function create()
    {
        $item = $this->repository->makeModel();

        return view("core::admin.dc_provider.edit", [
            "item" => $item,
        ]);
    }

    public function edit(int $itemId)
    {
        $item = $this->repository->find($itemId);

        return view("core::admin.dc_provider.edit", [
            "item" => $item,
        ]);
    }

    public function store(int $itemId = null, StoreDcProviderRequest $request)
    {
        $data = $request->validated();

        /** @var Node $item */
        $item = $this->repository->updateOrCreate(["id" => $itemId], $data);

        flash("Item successfully saved!")->success();

        return redirect()->route("admin.dc_provider.edit", $item);
    }

    public function delete(int $itemId)
    {
        $this->repository->delete($itemId);

        flash("Item successfully removed!")->success();

        return redirect()->back();
    }

}