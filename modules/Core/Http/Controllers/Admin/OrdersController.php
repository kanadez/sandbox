<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 26.09.18
 * Time: 12:10
 */

namespace Modules\Core\Http\Controllers\Admin;


use Modules\Core\Criteria\DefaultSortCriteria;
use Modules\Core\Repository\OrdersRepository;

class OrdersController extends BaseController
{
    /**
     * @var OrdersRepository
     */
    private $ordersRepository;

    /**
     * OrdersController constructor.
     * @param OrdersRepository $ordersRepository
     */
    public function __construct(OrdersRepository $ordersRepository)
    {
        $this->ordersRepository = $ordersRepository;
    }

    public function index()
    {
        $orders = $this->ordersRepository
            ->resetCriteria()
            ->pushCriteria(DefaultSortCriteria::class)
            ->with(["payment", "items"])
            ->paginate($this->pageSize);

        return  view("core::admin.order.index", [
            "items" => $orders,
        ]);
    }
}