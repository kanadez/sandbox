<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 06.08.18
 * Time: 11:28
 */

namespace Modules\Core\Http\Controllers\Admin;

use Modules\Core\Criteria\DefaultSortCriteria;
use Modules\Core\Http\Requests\Admin\StoreDcRequest;
use Modules\Core\Repository\CountriesRepository;
use Modules\Core\Repository\DcProvidersRepository;
use Modules\Core\Repository\DcsRepository;

class DcsController extends BaseController
{
    /**
     * @var DcsRepository
     */
    private $repository;
    /**
     * @var array
     */
    private $providers;
    /**
     * @var array
     */
    private $countries;

    /**
     * DcsController constructor.
     * @param DcsRepository $repository
     * @param DcProvidersRepository $dcProvidersRepository
     * @param CountriesRepository $countriesRepository
     */
    public function __construct(DcsRepository $repository,
                                DcProvidersRepository $dcProvidersRepository,
                                CountriesRepository $countriesRepository)
    {
        $this->repository = $repository;
        $this->providers = $dcProvidersRepository->all()->pluck("name", "id")->toArray();
        $this->countries = $countriesRepository->all()->pluck("name", "id")->toArray();
    }

    public function index()
    {
        $items = $this->repository
            ->with([
                "provider",
                "country",
            ])
            ->pushCriteria(DefaultSortCriteria::class)
            ->paginate($this->pageSize);

        return view("core::admin.dc.index", [
            "items" => $items,
        ]);
    }

    public function create()
    {
        $item = $this->repository->makeModel();

        return view("core::admin.dc.edit", [
            "item" => $item,
            "providers" => $this->providers,
            "countries" => $this->countries,
        ]);
    }

    public function edit(int $itemId)
    {
        $item = $this->repository->find($itemId);

        return view("core::admin.dc.edit", [
            "item" => $item,
            "providers" => $this->providers,
            "countries" => $this->countries,
        ]);
    }

    public function store(int $itemId = null, StoreDcRequest $request)
    {
        $data = $request->validated();

        /** @var Node $item */
        $item = $this->repository->updateOrCreate(["id" => $itemId], $data);

        flash("Item successfully saved!")->success();

        return redirect()->route("admin.dc.edit", $item);
    }

    public function delete(int $itemId)
    {
        $this->repository->delete($itemId);

        flash("Item successfully removed!")->success();

        return redirect()->back();
    }
}