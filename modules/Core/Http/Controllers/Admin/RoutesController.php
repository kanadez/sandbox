<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 24.07.18
 * Time: 10:46
 */

namespace Modules\Core\Http\Controllers\Admin;

use Modules\Core\Http\Controllers\Controller;

class RoutesController extends Controller
{

    public function nodes() {
        return view('core::admin.pages.nodes');
    }

}
