<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 23.08.18
 * Time: 11:38
 */

namespace Modules\Core\Http\Controllers\Admin;


use Modules\Core\Criteria\DefaultSortCriteria;
use Modules\Core\Http\Requests\Admin\StoreRemoteCommandRequest;
use Modules\Core\Repository\RemoteCommandsRepository;

class RemoteCommandsController extends BaseController
{
    /**
     * @var RemoteCommandsRepository
     */
    private $repository;

    /**
     * RemoteCommandsController constructor.
     * @param RemoteCommandsRepository $repository
     */
    public function __construct(RemoteCommandsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $items = $this->repository
            ->pushCriteria(DefaultSortCriteria::class)
            ->paginate($this->pageSize);

        return view("core::admin.remote_command.index", [
            "items" => $items,
        ]);
    }

    public function create()
    {
        $item = $this->repository->makeModel();

        return view("core::admin.remote_command.edit", [
            "item" => $item,
        ]);
    }

    public function edit(int $itemId)
    {
        $item = $this->repository->find($itemId);

        return view("core::admin.remote_command.edit", [
            "item" => $item,
        ]);
    }

    public function store(int $itemId = null, StoreRemoteCommandRequest $request)
    {
        $data = $request->validated();

        /** @var Node $item */
        $item = $this->repository->updateOrCreate(["id" => $itemId], $data);

        flash("Item successfully saved!")->success();

        return redirect()->route("admin.remote_command.edit", $item);
    }

    public function delete(int $itemId)
    {
        $this->repository->delete($itemId);

        flash("Item successfully removed!")->success();

        return redirect()->back();
    }
}