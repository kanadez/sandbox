<?php


namespace Modules\Core\Http\Controllers\Admin;

use Modules\Core\Http\Requests\Admin\DefaultProxyPortSettingsRequest;
use Modules\Core\Model\DefaultProxyPortSettings;

class DefaultProxyPortSettingsController extends BaseController
{

    private $defaultSettings;

    public function __construct()
    {
        //$defaultSettings = DefaultProxyPortSettings::all();
        //$this->defaultSettings = $defaultSettings[0];
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index()
    {
        if(!is_null($this->defaultSettings->allowed_ip) && $this->defaultSettings->allowed_ip != '') {
            $this->defaultSettings->allowed_ip = implode(';', json_decode($this->defaultSettings->allowed_ip, true));
        }
        return view("core::admin.default_proxy_port_settings.index", [
            "defaultSettings" => $this->defaultSettings,
        ]);
    }


    public function save(int $itemId = null,  DefaultProxyPortSettingsRequest $request )
    {
        $data = $request->validated();
        if( !is_null($data['allowed_ip']) && $data['allowed_ip'] != '' ) {
            $data['allowed_ip'] = json_encode(explode(';', $data['allowed_ip']));
        }
        $this->defaultSettings->update($data);
        return redirect()->route("admin.default-proxy-ports-settings.index");
    }


}
