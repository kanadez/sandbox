<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 22.08.18
 * Time: 16:05
 */

namespace Modules\Core\Http\Controllers\Admin;

use Modules\Core\Http\Requests\Admin\StoreResidentialParentRequest;
use Modules\Core\Repository\ResidentialParentsRepository;

class ResidentialParentsController extends BaseController
{
    /**
     * @var ResidentialParentsRepository
     */
    private $repository;

    /**
     * ResidentialParentsController constructor.
     * @param ResidentialParentsRepository $repository
     */
    public function __construct(ResidentialParentsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function store(StoreResidentialParentRequest $request)
    {
        $data = $request->validated();

        /** @var DcPool $item */
        $item = $this->repository->create(array_filter($data));

        flash("Item successfully saved!")->success();

        return redirect(url()->previous() . '#parents');
    }

    public function delete(int $itemId)
    {
        $this->repository->delete($itemId);

        flash("Item successfully removed!")->success();

        return redirect(url()->previous() . '#parents');
    }
}