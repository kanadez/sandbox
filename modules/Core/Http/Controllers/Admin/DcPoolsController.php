<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 22.08.18
 * Time: 13:09
 */

namespace Modules\Core\Http\Controllers\Admin;

use Illuminate\Support\Facades\URL;
use Modules\Core\Http\Requests\StoreDcPoolRequest;
use Modules\Core\Model\DcPool;
use Modules\Core\Repository\DcPoolsRepository;

class DcPoolsController extends BaseController
{
    /**
     * @var DcPoolsRepository
     */
    private $repository;

    /**
     * DcPoolsController constructor.
     * @param DcPoolsRepository $repository
     */
    public function __construct(DcPoolsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function store(StoreDcPoolRequest $request)
    {
        $data = $request->validated();

        /** @var DcPool $item */
        $item = $this->repository->create(array_filter($data));

        flash("Item successfully saved!")->success();

        return redirect(url()->previous() . '#pools');
    }

    public function delete(int $itemId)
    {
        $this->repository->delete($itemId);

        flash("Item successfully removed!")->success();

        return redirect(url()->previous() . '#pools');
    }
}