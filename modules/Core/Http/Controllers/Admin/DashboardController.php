<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 18.07.18
 * Time: 12:11
 */

namespace Modules\Core\Http\Controllers\Admin;

class DashboardController extends BaseController
{
    public function index()
    {
        return view("core::admin.dashboard.index", [

        ]);
    }
}