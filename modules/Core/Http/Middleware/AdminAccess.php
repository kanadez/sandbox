<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 18.07.18
 * Time: 9:55
 */

namespace Modules\Core\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class AdminAccess
{
    public function handle($request, Closure $next, $guard = null)
    {
        if (($user = Auth::user()) && $user->can("admin-access")) {
            View::share('user', $user);
            return $next($request);
        }

        return redirect(route("admin.login"));
    }
}