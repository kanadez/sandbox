<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 19.07.18
 * Time: 13:29
 */

namespace Modules\Core\Http\Middleware;

use Closure;
use Lavary\Menu\Builder;
use Lavary\Menu\Facade as Menu;
use Lavary\Menu\Item;

class AdminMenuMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
        // CreateAdminMenu instance
        Menu::make('AdminMenu', function(Builder $menu) {
            $menu
                ->add("Dashboard", route("admin.index"))
                ->data('order', 100)
                ->data("icon", "fa fa-fw fa-tachometer");
            $menu
                ->add('Users', route("admin.user.index"))
                ->data('order', 200)
                ->data("icon", "fa fa-fw fa-user")
                ->data("mask", "admin\.user.*");

            $menu
                ->add('Default Proxy Port Settings', route("admin.default-proxy-ports-settings.index"))
                ->data('order', 300)
                ->data("icon", "fa fa-fw fa-gear")
                ->data("mask", "admin\.default-proxy-ports-settings.index.*");

            /*$menu
                ->add('Proxy requests', route("admin.proxy-request.index"))
                ->data('order', 300)
                ->data("icon", "nav-icon icon-envelope-letter")
                ->data("mask", "admin\.proxy-request.*");*/

            /*tap($menu
                ->add('Billing')
                ->data('order', 400)
                ->data("icon", "nav-icon icon-wallet")
                ->data("mask", "admin\.payment|order.*"), function (Item $menu) {
                    $menu
                        ->add('Orders', route("admin.order.index"))
                        ->data('order', 100)
                        ->data("icon", "nav-icon icon-puzzle")
                        ->data("mask", "admin\.order.*");
                    $menu
                        ->add('Payments', route("admin.payment.index"))
                        ->data('order', 200)
                        ->data("icon", "nav-icon icon-puzzle")
                        ->data("mask", "admin\.payment.*");
            });*/


            $menu
                ->add('Nodes', route("admin.nodes"))
                ->data('order', 500)
                ->data("icon", "fa fa-fw fa-server")
                ->data("mask", "admin\.node|node_port.*");

//            tap($menu
//                ->add('Proxies')
//                ->data('order', 500)
//                ->data("icon", "nav-icon icon-list")
//                ->data("mask", "admin\.node|node_port.*"), function (Item $menu) {
//                $menu
//                    ->add('Nodes', route("admin.node.index"))
//                    ->data('order', 100)
//                    ->data("icon", "nav-icon icon-puzzle")
//                    ->data("mask", "admin\.node.*");
//                $menu
//                    ->add('Node ports', route("admin.node_port.index"))
//                    ->data('order', 200)
//                    ->data("icon", "nav-icon icon-puzzle")
//                    ->data("mask", "admin\.node_port.*");
//            });
//
//            $menu
//                ->add('Countries', route("admin.country.index"))
//                ->data('order', 700)
//                ->data("icon", "nav-icon icon-map")
//                ->data("mask", "admin\.country.*");

            /*tap($menu
                ->add('Providers')
                ->data('order', 800)
                ->data("icon", "nav-icon icon-cloud-download")
                ->data("mask", "admin\.dc|residential.*"), function (Item $menu) {
                    $menu
                        ->add("Data centers", route("admin.dc.index"))
                        ->data('order', 100)
                        ->data("icon", "nav-icon icon-puzzle")
                        ->data("mask", "admin\.dc\..*");

                    $menu
                        ->add("Residential", route("admin.residential.index"))
                        ->data('order', 200)
                        ->data("icon", "nav-icon icon-puzzle")
                        ->data("mask", "admin\.residential                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       \..*");


            });*/

            /*$menu
                ->add("Settings", route("admin.remote_command.index"))
                ->data('order', 900)
                ->data("icon", "nav-icon icon-settings");

            $menu
                ->add("Currencies", route("admin.currency.index"))
                ->data('order', 1000)
                ->data("icon", "nav-icon icon-options");

            $menu
                ->add("Products", route("admin.product.index"))
                ->data('order', 1100)
                ->data("icon", "nav-icon cui-cart");*/
        });

        return $next($request);
    }
}
