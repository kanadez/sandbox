<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 18.07.18
 * Time: 14:57
 */

namespace Modules\Core\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Model\User;

class RedirectAdminIfAuthenticated
{
    public function handle($request, Closure $next, $guard = null)
    {
        /** @var User $user */
        if (($user = Auth::user()) && $user->can("admin-access")) {
            return redirect(route("admin.index"));
        }

        return $next($request);
    }
}