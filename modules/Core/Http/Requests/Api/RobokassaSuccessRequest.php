<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 30.07.18
 * Time: 10:29
 */

namespace Modules\Core\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class RobokassaSuccessRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'InvId' => 'required|string',
            'SignatureValue' => 'required|string',
        ];
    }
}