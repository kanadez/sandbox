<?php

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string|min:3',
            //'dc_bandwidth_in' => 'required|integer',
            //'dc_bandwidth_out' => 'required|integer',
            //'dc_max_connections' => 'required|integer',
            //'rsd_bandwidth_in' => 'required|integer',
            //'rsd_bandwidth_out' => 'required|integer',
            //'rsd_max_connections' => 'required|integer',
            //'ports' => '',
        ];

        if (!empty($this->userId)) {
            $rules['email']  = 'required|string|email|max:255|unique:users,email,' . $this->userId;
            if (!empty($this->request->get('password')) ||
                !empty($this->request->get('password_confirmation'))) {
                $rules['password'] = 'required|string|min:6|confirmed';
            }
        } else {
            $rules['email']  = 'required|string|email|max:255|unique:users,email';
            $rules['password'] = 'required|string|min:6|confirmed';
        }

        return $rules;

    }
}
