<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 27.08.18
 * Time: 12:29
 */

namespace Modules\Core\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserDcPorts extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'dc_ports' => 'required',
        ];
    }
}