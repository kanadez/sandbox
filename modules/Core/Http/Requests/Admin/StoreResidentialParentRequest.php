<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 22.08.18
 * Time: 16:08
 */

namespace Modules\Core\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreResidentialParentRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'residential_id' => 'required|integer',
            'ip' => 'required|ip',
            'port' => 'required|integer|min:1|max:65000',
        ];
    }
}