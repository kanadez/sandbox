<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 08.08.18
 * Time: 12:14
 */

namespace Modules\Core\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreNodePortRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'node_id' => 'required|integer',
            'provider_point' => '',
//            'interval' => 'required|integer',
            'finger_print_id' => 'required|integer',
            'mtu' => 'required|integer',
            'ip_geo' => '',
            'link' => '',
            'os_fingerprint' => '',
            'comment' => '',
            'number' => 'required|integer|unique:node_ports,number,'.$this->getValidationString($this->itemId) . ',id' .
                ',node_id,'.$this->getValidationString($this->request->get("node_id")),
        ];
    }

    private function getValidationString($param)
    {
        return $param ?? 'null';
    }
}