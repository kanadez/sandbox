<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 30.07.18
 * Time: 14:41
 */

namespace Modules\Core\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Core\Model\Node;

class StoreNodeRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'integer|unique,nodes',
            'slug' => 'required|string',
            /*'server' => 'required|string|min:3',
            'dc_id' => 'nullable|integer',
            'is_test' => 'required|boolean',
            'hypid' => 'required|integer',
            'assignment_id' => 'required|integer|exists:node_assignments,id',
            'rotation_period_id' => 'required|integer|exists:node_rotation_periods,id',
            'dcable_id' => 'required|integer',
            'dcable_type' => 'required|string',
            'ip' => 'nullable|ipv4',*/
            
         ];

    }

    private function getValidationString($param)
    {
        return $param ?? 'null';
    }
}