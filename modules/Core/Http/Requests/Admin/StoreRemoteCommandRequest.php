<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 23.08.18
 * Time: 11:47
 */

namespace Modules\Core\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreRemoteCommandRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string',
            'command' => 'required|string',
        ];
    }
}