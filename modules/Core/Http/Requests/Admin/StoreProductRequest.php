<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 20.09.18
 * Time: 16:40
 */

namespace Modules\Core\Http\Requests\Admin;


use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string',
            'description' => 'required|string',
            'price' => 'required|numeric',
            'currency_id' => 'required|integer|exists:currencies,id'
        ];
    }
}