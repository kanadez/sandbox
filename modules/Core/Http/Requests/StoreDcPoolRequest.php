<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 22.08.18
 * Time: 13:10
 */

namespace Modules\Core\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class StoreDcPoolRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'dc_id' => 'required|integer',
            'ip' => 'required|ip',
            'mask' => 'nullable|ip',
            'gw' => 'nullable|ip',
            'dns' => 'required|ip',
        ];
    }
}