<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 17.09.18
 * Time: 10:37
 */

namespace Modules\Core\Http\Requests;


class Node extends Request
{

    public function rules()
    {
        return [
            'slug' => 'required|regex:/^[a-zA-Z\-0-9]+$/',
        ];
    }
}
