<?php

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreProxyPortRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string|min:3',
            'user_id' => 'required|integer',
            'type' => 'required|string',
            'ip_type' => 'required|string',
            'time_ip_change' => 'required|string',
            'pull_size' => 'required|string',
            'trafic' => 'required|integer',
            'country_id' => 'integer|nullable',
            'city_id' => 'integer|nullable',
            'node_id' => 'integer|required',
            "http" => "integer",
            "socks" => "integer",
            "username" => "string|required",
            "password" => "string|required",
            "peer_rotate" => "string",
            "fingerprint" => "string|nullable",
            "asn" => "string|nullable",
            "uptime" => "string|required",
            "latency" => "integer|required",
            "speed_download" => "string|required",
            "speed_upload" => "string|required",
            "allowed_ip" => "string|nullable",
            "max_connections" => "integer|required",
            "bandwidth_in" => "string|required",
            "bandwidth_out" => "string|required",
            "time_limit" => "string|required",
        ];


        return $rules;

    }


    public function getValidatorInstance()
    {
        if( is_null($this->request->get('user_id')) ) {
            $this->merge(['user_id'=>Auth::user()->id]);
        }
        
        return parent::getValidatorInstance();
    }
}
