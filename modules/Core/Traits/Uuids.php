<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 26.07.18
 * Time: 11:26
 */

namespace Modules\Core\Traits;

use Ramsey\Uuid\Uuid;

trait Uuids
{
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->{$model->getKeyName()} = Uuid::uuid4()->toString();
        });
    }
}