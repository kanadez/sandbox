<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 26.07.18
 * Time: 16:15
 */

namespace Modules\Core\Traits;

trait Strings
{
    public function formatPrice(string $price)
    {
        return number_format(
            filter_var( $price, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION),
            2,
            ".",
            ""
        );
    }
}