<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 26.07.18
 * Time: 16:15
 */

namespace Modules\Core\Traits;

use Illuminate\Support\Facades\Auth;

trait UserRolesTrait
{
    
    public static function authAsAnonym()
    {
        $anonym_user = self::where("name", env("ANONYM_USER_NAME"))->first();
        Auth::login($anonym_user, true);
    }
    
}