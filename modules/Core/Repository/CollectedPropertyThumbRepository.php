<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 30.07.18
 * Time: 9:59
 */

namespace Modules\Core\Repository;

use Modules\Core\Model\CollectedPropertyThumb;

class CollectedPropertyThumbRepository extends BaseRepository
{
    public function model()
    {
        return CollectedPropertyThumb::class;
    }
}