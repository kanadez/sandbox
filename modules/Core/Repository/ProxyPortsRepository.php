<?php

namespace Modules\Core\Repository;

use Modules\Core\Model\ProxyPort;

class ProxyPortsRepository extends BaseRepository
{
    public function model()
    {
        return ProxyPort::class;
    }
}