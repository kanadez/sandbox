<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 26.07.18
 * Time: 9:58
 */

namespace Modules\Core\Repository;

use Modules\Core\Model\PaysystemStatus;

class PaysystemStatusesRepository extends BaseRepository
{
    public function model()
    {
        return PaysystemStatus::class;
    }
}