<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 13.09.18
 * Time: 14:00
 */

namespace Modules\Core\Repository;

use Modules\Core\Model\PaysystemMethod;

class PaysystemMethodsRepository extends BaseRepository
{
    public function model()
    {
        return PaysystemMethod::class;
    }
}