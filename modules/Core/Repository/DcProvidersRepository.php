<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 02.08.18
 * Time: 16:21
 */

namespace Modules\Core\Repository;

use Modules\Core\Model\DcProvider;

class DcProvidersRepository extends BaseRepository
{
    public function model()
    {
        return DcProvider::class;
    }
}