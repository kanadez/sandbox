<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 19.09.18
 * Time: 9:17
 */

namespace Modules\Core\Repository;

use Modules\Core\Model\OrderItem;

class OrderItemsRepository extends BaseRepository
{
    public function model()
    {
        return OrderItem::class;
    }
}