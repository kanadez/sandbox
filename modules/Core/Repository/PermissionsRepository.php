<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 18.07.18
 * Time: 10:55
 */

namespace Modules\Core\Repository;

use Modules\Core\Model\Permission;

class PermissionsRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Permission::class;
    }
}