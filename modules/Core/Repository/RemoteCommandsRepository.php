<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 23.08.18
 * Time: 11:06
 */

namespace Modules\Core\Repository;

use Modules\Core\Model\RemoteCommand;

class RemoteCommandsRepository extends BaseRepository
{
    public function model()
    {
        return RemoteCommand::class;
    }
}