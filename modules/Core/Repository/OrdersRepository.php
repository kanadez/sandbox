<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 26.07.18
 * Time: 9:39
 */

namespace Modules\Core\Repository;

use Modules\Core\Model\Order;

class OrdersRepository extends BaseRepository
{
    public function model()
    {
        return Order::class;
    }
}