<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 30.07.18
 * Time: 9:59
 */

namespace Modules\Core\Repository;

use Modules\Core\Model\PropertyReview;

class PropertiesReviewsRepository extends BaseRepository
{
    public function model()
    {
        return PropertyReview::class;
    }
}