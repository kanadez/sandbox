<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 10.09.18
 * Time: 11:07
 */

namespace Modules\Core\Repository;

use Modules\Core\Model\Price;

class PricesRepository extends BaseRepository
{
    public function model()
    {
        return Price::class;
    }
}