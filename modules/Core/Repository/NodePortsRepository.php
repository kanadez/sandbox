<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 07.08.18
 * Time: 14:32
 */

namespace Modules\Core\Repository;

use Modules\Core\Model\NodePort;

class NodePortsRepository extends BaseRepository
{
    public function model()
    {
        return NodePort::class;
    }
}