<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 24.07.18
 * Time: 8:43
 */

namespace Modules\Core\Repository;

use Modules\Core\Model\ProxyRequest;

class ProxyRequestsRepository extends BaseRepository
{
    public function model()
    {
        return ProxyRequest::class;
    }
}