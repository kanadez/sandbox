<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 02.08.18
 * Time: 13:53
 */

namespace Modules\Core\Repository;

use Modules\Core\Model\NodeRotationPeriod;

class NodeRotationPeriodsRepository extends BaseRepository
{
    public function model()
    {
        return NodeRotationPeriod::class;
    }
}