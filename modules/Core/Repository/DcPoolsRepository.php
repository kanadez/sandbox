<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 22.08.18
 * Time: 13:29
 */

namespace Modules\Core\Repository;

use Modules\Core\Model\DcPool;

class DcPoolsRepository extends BaseRepository
{
    public function model()
    {
        return DcPool::class;
    }
}