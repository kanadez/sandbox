<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 09.08.18
 * Time: 10:33
 */

namespace Modules\Core\Repository;

use Modules\Core\Model\NodeDomain;

class NodeDomainsRepository extends BaseRepository
{
    public function model()
    {
        return NodeDomain::class;
    }
}