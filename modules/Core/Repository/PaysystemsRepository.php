<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 25.07.18
 * Time: 17:06
 */

namespace Modules\Core\Repository;

use Modules\Core\Model\Paysystem;

class PaysystemsRepository extends BaseRepository
{
    public function model()
    {
        return Paysystem::class;
    }
}