<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 02.08.18
 * Time: 16:17
 */

namespace Modules\Core\Repository;

use Modules\Core\Model\Dc;

class DcsRepository extends BaseRepository
{
    public function model()
    {
        return Dc::class;
    }
}