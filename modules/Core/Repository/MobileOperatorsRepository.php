<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 30.07.18
 * Time: 9:59
 */

namespace Modules\Core\Repository;

use Modules\Core\Model\MobileOperator;

class MobileOperatorsRepository extends BaseRepository
{
    public function model()
    {
        return MobileOperator::class;
    }
}