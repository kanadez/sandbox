<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 30.07.18
 * Time: 10:27
 */

namespace Modules\Core\Repository;

use Modules\Core\Model\Node;
use Modules\Core\Validators\NodeValidator;

class NodesRepository extends BaseRepository
{
    public function model()
    {
        return Node::class;
    }
}