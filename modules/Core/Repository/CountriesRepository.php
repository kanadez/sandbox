<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 02.08.18
 * Time: 16:55
 */

namespace Modules\Core\Repository;

use Modules\Core\Model\Country;

class CountriesRepository extends BaseRepository
{
    public function model()
    {
        return Country::class;
    }
}