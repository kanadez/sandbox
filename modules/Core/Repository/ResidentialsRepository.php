<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 22.08.18
 * Time: 15:45
 */

namespace Modules\Core\Repository;

use Modules\Core\Model\Residential;

class ResidentialsRepository extends BaseRepository
{
    public function model()
    {
        return Residential::class;
    }
}