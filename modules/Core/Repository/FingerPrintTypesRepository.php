<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 30.08.18
 * Time: 11:18
 */

namespace Modules\Core\Repository;

use Modules\Core\Model\FingerPrintType;

class FingerPrintTypesRepository extends BaseRepository
{
    public function model()
    {
        return FingerPrintType::class;
    }
}