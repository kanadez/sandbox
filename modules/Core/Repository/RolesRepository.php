<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 18.07.18
 * Time: 10:52
 */

namespace Modules\Core\Repository;

use Modules\Core\Model\Role;

class RolesRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Role::class;
    }
}