<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 10.09.18
 * Time: 11:04
 */

namespace Modules\Core\Repository;

use Modules\Core\Model\Product;

class ProductsRepository extends BaseRepository
{
    public function model()
    {
        return Product::class;
    }
}