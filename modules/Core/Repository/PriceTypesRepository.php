<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 10.09.18
 * Time: 10:43
 */

namespace Modules\Core\Repository;

use Modules\Core\Model\PriceType;

class PriceTypesRepository extends BaseRepository
{
    public function model()
    {
        return PriceType::class;
    }
}