<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 02.08.18
 * Time: 13:21
 */

namespace Modules\Core\Repository;

use Modules\Core\Model\NodeAssignment;

class NodeAssignmentsRepository extends BaseRepository
{
    public function model()
    {
        return NodeAssignment::class;
    }
}