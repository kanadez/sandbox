<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 02.08.18
 * Time: 14:11
 */

namespace Modules\Core\Repository;

use Modules\Core\Model\NodeProvider;

class NodeProvidersRepository extends BaseRepository
{
    public function model()
    {
        return NodeProvider::class;
    }
}