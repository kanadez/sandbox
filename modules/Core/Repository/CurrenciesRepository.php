<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 05.09.18
 * Time: 11:36
 */

namespace Modules\Core\Repository;

use Modules\Core\Model\Currency;

class CurrenciesRepository extends BaseRepository
{
    public function model()
    {
        return Currency::class;
    }
}