<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 30.07.18
 * Time: 15:17
 */

namespace Modules\Core\Repository;

use Modules\Core\Model\Proxy;

class ProxiesRepository extends BaseRepository
{
    public function model()
    {
        return Proxy::class;
    }
}