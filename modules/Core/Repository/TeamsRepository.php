<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 18.07.18
 * Time: 10:56
 */

namespace Modules\Core\Repository;

use Modules\Core\Model\Team;

class TeamsRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Team::class;
    }
}