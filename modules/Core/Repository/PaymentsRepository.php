<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 26.07.18
 * Time: 11:21
 */

namespace Modules\Core\Repository;

use Modules\Core\Model\Payment;

class PaymentsRepository extends BaseRepository
{
    public function model()
    {
        return Payment::class;
    }
}