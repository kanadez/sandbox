<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 07.08.18
 * Time: 15:09
 */

namespace Modules\Core\Collection;

use Illuminate\Support\Collection;
use Modules\Core\Contracts\Collections\TestQuestionsCollectionContract;

class TestQuestionsCollection extends Collection implements TestQuestionsCollectionContract
{

}