<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 18.07.18
 * Time: 10:33
 */

namespace Modules\Core\Console;

use Caffeinated\Modules\Facades\Module;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'Modules\Core\Console\Commands\LoadCountries',
        'Modules\Core\Console\Commands\LoadCities',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     * @throws \Caffeinated\Modules\Exceptions\ModuleNotFoundException
     */
    protected function commands()
    {
        // Load module console commands
        foreach (Module::all() as $module) {
            $this->load(module_path($module["slug"], 'Console/Commands'));
        }

        require base_path('routes/console.php');
    }
}