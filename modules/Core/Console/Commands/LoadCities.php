<?php

namespace Modules\Core\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class LoadCities extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cities:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        ); 
        $countriesJson = file_get_contents(url('/').'/cities.json', false, stream_context_create($arrContextOptions));
        $cities = json_decode($countriesJson, true);
        $bar = $this->output->createProgressBar(count($cities));
        $bar->start();
        foreach ($cities as $key => $city) {
            $cityExist = DB::table('all_cities')->where('id', $city['id'])->get()->toArray();

            if( count($cityExist) == 0 ) {
                DB::table('all_cities')->insert(
                    [
                        'id' => $city['id'], 
                        'name' => $city['name'],
                        'country_id' => $city['country_id'],
                    ]
                );
            }

            $bar->advance();
        }
        $bar->finish();
    }
}
