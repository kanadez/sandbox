<?php

namespace Modules\Core\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class LoadCountries extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'countries:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );  
        $countriesJson = file_get_contents(url('/').'/countries.json', false, stream_context_create($arrContextOptions));
        $countries = json_decode($countriesJson, true);
        $bar = $this->output->createProgressBar(count($countries));
        $bar->start();
        foreach ($countries as $key => $country) {
            $countryExist = DB::table('all_countries')->where('id', $country['id'])->get()->toArray();

            if( count($countryExist) == 0 ) {
                DB::table('all_countries')->insert(
                    [
                        'id' => $country['id'], 
                        'name' => $country['name'],
                        'iso3' => $country['iso3'],
                        'iso2' => $country['iso2'],
                    ]
                );
            }

            $bar->advance();
        }
        $bar->finish();
    }
}
