import Echo from "laravel-echo";
window.asd = Echo;
window.io = require('socket.io-client');

if (typeof io !== 'undefined') {
    window.Echo = new Echo({
        broadcaster: 'socket.io',
        host: window.location.hostname,
        path: '/ws'
    });
}

window.Echo.private('admin_notifications.' + laravel.user.id)
// ToDO: refactor
    .listen('.admin.notification.user_gen', (e) => {
        window.toastr[e.type]('<b>Command:</b> ' + e.command + '<br /><b>Result:</b> ' + e.log, e.title)
    })
    .listen('.admin.notification.user_del', (e) => {
        window.toastr[e.type]('<b>Command:</b> ' + e.command + '<br /><b>Result:</b> ' + e.log, e.title)
    })
    .listen('.admin.notification.middle_gen', (e) => {
        window.toastr[e.type]('<b>Command:</b> ' + e.command + '<br /><b>Result:</b> ' + e.log, e.title)
    })
    .listen('.admin.notification.finger_print_info', (e) => {
        window.toastr[e.type]('<b>Command:</b> ' + e.command + '<br /><b>Result:</b> ' + e.log, e.title)
    })
    .listen('.admin.notification.finger_print_applied', (e) => {
        window.toastr[e.type]('<b>Command:</b> ' + e.command + '<br /><b>Result:</b> ' + e.log, e.title)
    })
;
