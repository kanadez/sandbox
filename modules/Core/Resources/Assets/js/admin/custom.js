 $(document).ready(function() {
 	$('[name="country_id"]').selectpicker();
 	$('[name="city_id"]').selectpicker({
 		width: 'auto',
 	});
 	$('[name="country_id"]').on('change', function() {
 		var country_id = $(this).val();
 		$.ajax({
            type: "POST",
            url: "/get-cities/",
            data: {
                _token:  $('meta[name="csrf-token"]').attr('content'),
                country_id: country_id
            },
            beforeSend: function (msg) {
            	$('.loading-cities').show();
              	$('.loading-cities').html('<img style="width:50px" src="/img/load.gif">');
            },
            success: function(data){
                $("select[name='city_id'").html(data.options);
                $('select[name="city_id"]').selectpicker('refresh');
                $('.loading-cities').hide();
                $('.loading-cities').html('');
            },
            error: function(){
                
            }
        }).fail(function(){
           
        });
 	});


    function showTraficOutput(trafic) {
        var output = '';
        if( Number(trafic) < 1000 ) {
            output = trafic + ' GB'; 
        } else {
            output =   '1 TB';
        }
        $('.selectedTrafic').html(output);
    }

    var trafic = $('[name="trafic"]').val();
    showTraficOutput(trafic);

    $('[name="trafic"]').on('change', function() {
        showTraficOutput($(this).val());
    });


    $('[name="slug"]').on("keypress", function(evt) {
      var keycode = evt.charCode || evt.keyCode;
      if (keycode == 46) { // forbit to type dots <.>
        return false;
      }
    });

    $('#getIp').click(function (e) {
        var empty = true;
        var assignment = $('.assignment_button:checked').parent().text().trim();
        var rotation = $('.rotation_period_button:checked').parent().text().trim();
        var dcable = $('.dcable_button:checked').parent().text().trim();
        var slug = $('#slug').val();

        if( slug != '' ) {
            empty = false;
            var host = slug;
        } else if ( assignment != '' && rotation != '' && dcable != '') {
            empty = false;
            var host = assignment + '.' + rotation + '.' + dcable; 
        }
        if( empty != true ) {

            //$('[name="ip"]').attr("placeholder", "no resolve / check DNS");
            //$('[name="ip"]').val("no resolve / check DNS");
            var url = $(this).attr('data-lookup-url');
            $.ajax({
                type: 'POST',
                url: url,
                data: {host:host},
                headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
                success: function(result){
                    $('[name="ip"]').attr("placeholder", result.responce);
                    $('[name="ip"]').val(result.responce);
                },
                error: function(result){
                    
                }
            });
        } else {
            alert('Please fill hostname field')
        }


        return false;
    });


});
