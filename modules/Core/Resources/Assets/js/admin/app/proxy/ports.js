$(document).ready(() => {
    $(document).on('click', 'button#add-proxy-port', onClickAddProxyButton);
    $(document).on('click', '.edit-port', onClickEditPortButton);
    $(document).on('click', '.remove-port', onClickRemovePortButton);

    $(document).on('change', '[name="country_id"]', onChangeCountry);

    loadProxyPortsTable();

    modals('#port').on('save.success', function() {
        refreshProxyPortsTable();
    });

    $(document).on('change', modals('#port').selector() + ' input, ' + modals('#port').selector() + ' textarea,' + modals('#port').selector() + ' select', onFormChangeValue)
});

function refreshProxyPortsTable() {
    $('table#ports').DataTable().ajax.reload();
}

function loadProxyPortsTable() {
    $('table[id="ports"]').DataTable( {
        searching: false,
        processing: true,
        serverSide: true,
        destroy: true,
        autoWidth: false,
        ordering: false,
        bSort: false,
        language: {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Russian.json"
        },
        ajax: {
            url: api().path() + 'proxy/ports',
        },
        columns: [
            {data: 'id'},
            {data: 'name'},
            {data: 'network_type.name'},
            {data: 'traffic_volume.name'},
            {data: 'created_at'},
            {data: 'id'},
        ],
        columnDefs: [ {
                targets: 5,
                render: function(data, type, row) {
                    return `
                      <a href="#" class="edit-port btn btn-as-link fa fa-pencil mr-2 text-info" data-id="${row.id}"></a>
                      <a href="#" class="remove-port btn btn-as-link fa fa-trash text-danger" data-id="${row.id}"></a>
                    `;
                }
            }
        ]
    } );
}

function loadCities(country_id) {
    return api('cities').get({
        country_id: country_id
    }).then(cities => {
        console.log(cities);
        $('[name="city_id"]').html('<option value="">Не выбрано</option>' + cities.map(item => {
            return `<option value="${item.id}">${item.name}</option>`;
        }).join(''));
        $('[name="city_id"]').val($('[name="city_id"]').attr('data-default-value'));
        return Promise.resolve(cities);
    });
}

function onClickAddProxyButton() {
    modals('#port').open().then(result => {
        $(modals('#port').selector()).find('[name="name"]').val('New Port');
        $(modals('#port').selector()).find('[name="username"]').val(Math.random().toString(36).slice(2));
        $(modals('#port').selector()).find('[name="password"]').val(Math.random().toString(36).slice(2));

        onFormChangeValue();
    });

}

function onChangeCountry(e) {
    let country_id = $(e.target).val();
    return loadCities(country_id);
}

function onClickEditPortButton(e) {
    let id = $(e.currentTarget).attr('data-id');
    modals('#port').open(api('proxy/ports/' + id).get()).then(port => {
        return loadCities(port.country_id).then(cities => {
            $('[name="city_id"]').val(port.city_id);
            return Promise.resolve();
        });
    }).then(result => {
        onFormChangeValue();
    });
    return false;
}

function onClickRemovePortButton(e) {
    let id = $(e.currentTarget).attr('data-id');
    api('proxy/ports/' + id).delete().then(result => {
        refreshProxyPortsTable();
    });
    return false;
}

function onFormChangeValue() {
    let form = modals('#port').form();
    if($(form.selector()).attr('data-processing') == 1) {
        return;
    }
    let data = form.inputs();
    data['type'] = 'proxy_port_price';
    api('calculator').get(data).then(result => {
        $(form.selector()).find('.price').html(result.amount);
    });
}
