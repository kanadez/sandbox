$(document).ready(function() {
    $(document).on('click', 'button#add-node', onClickAddNodeButton);
    $(document).on('click', '.edit-node', onClickEditNodeButton);
    $(document).on('click', '.remove-node', onClickRemoveNodeButton);
    $(document).on('click', '.node-ports', onClickNodePortsButton);

    modals('#node').on('save.success', function() {
        refreshNodesTable();
    });

    loadNodesTable();
});

function refreshNodesTable() {
    $('table#nodes').DataTable().ajax.reload();
}


function loadNodesTable() {
    $('table[id="nodes"]').DataTable( {
        searching: false,
        processing: true,
        serverSide: true,
        destroy: true,
        autoWidth: false,
        ordering: false,
        bSort: false,
        language: {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Russian.json"
        },
        ajax: {
            url: api().path() + 'nodes',
        },
        columns: [
            {data: 'id'},
            {data: 'name'},
            {data: 'ip'},
            {data: 'created_at'},
            {data: 'id'},
        ],
        columnDefs: [ {
                targets: 4,
                render: function(data, type, row) {
                    return `
                      <a href="#" class="node-ports btn btn-as-link fa fa-list mr-2 text-info" data-id="${row.id}"></a>
                      <a href="#" class="edit-node btn btn-as-link fa fa-pencil mr-2 text-info" data-id="${row.id}"></a>
                      <a href="#" class="remove-node btn btn-as-link fa fa-trash text-danger" data-id="${row.id}"></a>
                    `;
                }
            }
        ]
    } );
}

function onClickAddNodeButton() {
    modals('#node').open().then(result => {
        //onFormChangeValue();
    });
}

function onClickEditNodeButton(e) {
    let id = $(e.currentTarget).attr('data-id');
    modals('#node').open(api('nodes/' + id).get()).then(result => {
        //onFormChangeValue();
    });
    return false;
}

function onClickRemoveNodeButton(e) {
    let id = $(e.currentTarget).attr('data-id');
    api('nodes/' + id).delete().then(result => {
        refreshNodesTable();
    });
    return false;
}

function onClickNodePortsButton(e) {
    let id = $(e.currentTarget).attr('data-id');
    modals('#node-ports').open({}).then(result => {
        $(modals('#node-ports').selector() + ' table').DataTable( {
            searching: false,
            processing: true,
            serverSide: true,
            destroy: true,
            autoWidth: false,
            ordering: false,
            bSort: false,
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Russian.json"
            },
            ajax: {
                url: api().path() + 'nodes/' + id + '/ports',
            },
            columns: [
                {data: 'id'},
                {data: 'name'},
                {data: 'user.name'},
                {data: 'created_at'},
            ],
            /*columnDefs: [ {
                    targets: 4,
                    render: function(data, type, row) {
                        return `
                          <a href="#" class="node-ports btn btn-as-link fa fa-list mr-2 text-info" data-id="${row.id}"></a>
                          <a href="#" class="edit-node btn btn-as-link fa fa-pencil mr-2 text-info" data-id="${row.id}"></a>
                          <a href="#" class="remove-node btn btn-as-link fa fa-trash text-danger" data-id="${row.id}"></a>
                        `;
                    }
                }
            ]*/
        } );
    });
    return false;
}
