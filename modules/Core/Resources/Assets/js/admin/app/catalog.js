$(document).ready(function() {
    $('.catalog-plus').click(function () {
        let input = $(this).closest("div").find("input[name='quantity']");
        input.val(parseInt(input.val()) + 1);

    });

    $('.catalog-minus').click(function () {
        let input = $(this).closest("div").find("input[name='quantity']");
        let num = parseInt(input.val()) - 1;
        input.val(num < 1 ? 1 : num);
    });
});