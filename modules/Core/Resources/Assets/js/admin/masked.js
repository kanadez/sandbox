$(document).ready(function() {
    var options =  {
        translation: {
            'Z': {
                pattern: /[0-9]/, optional: true
            }
        },
        onChange: function(cep, event, currentField, options){
            if(cep){
                let ipArray = cep.split(".");
                for (i in ipArray){
                    if(ipArray[i] != "" && parseInt(ipArray[i]) > 255){
                        ipArray[i] =  '255';
                    }
                }
                var resultingValue = ipArray.join(".");
                $(currentField).val(resultingValue);
            }
        }
    };

    $('.ip_address').attr("pattern", '((^|\\.)((25[0-5])|(2[0-4]\\d)|(1\\d\\d)|([1-9]?\\d))){4}$').mask("0ZZ.0ZZ.0ZZ.0ZZ", options);
});