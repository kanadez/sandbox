window._ = require('lodash');
window.Popper = require('popper.js').default;

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.$ = window.jQuery = require('jquery');
    require('jquery-mask-plugin');
    require('bootstrap');
} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/**
 * Progresss bar loader
 */
let pace = require("pace-progress");
pace.start();

/**
 * Coreui admin js
 */
require("@coreui/coreui");

/**
 * Uuid
 */
window.uuidv4 = require('uuid/v4');

/**
 * Forms validation
 */
require('jquery-validation');

require('bootstrap-select');

/**
 * Codemirror
 */

import * as CodeMirror from 'codemirror';
window.CodeMirror = CodeMirror;
require('codemirror/mode/shell/shell');
require('./jquery.codemirror');

$('.bash-text').codemirror({
    mode: 'shell',
    matchBrackets: true,
    indentUnit: 4 ,
    lineNumbers: true,
});

window.toastr = require('toastr');

window.toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": true,
    "progressBar": true,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "10000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}

window.formEmulate = (action, method, values) => {
    const token = document.head.querySelector('meta[name="csrf-token"]');
    if (token) {
        const form = $('<form/>', {
            action: action,
            method: method
        });

        $.each(values, (index, item) => {
            form.append(
                $('<input/>', {
                    type: 'hidden',
                    name: item.name,
                    value: item.value,
                })
            );
        });

        form.append(
            $('<input/>', {
                type: 'hidden',
                name: '_token',
                value: token.content,
            })
        );
        form.appendTo('body').submit();
    }
};
