/**
 * Всплывающее окно подверждения на ссылках
 */
$(document).ready(function() {
    $('a[data-confirm]').click(function(ev) {
        let href = $(this).attr('href'),
            id = uuidv4(),
            text = $(this).attr('data-confirm'),
            html = '<div class="modal fade" id="' + id + '" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">\n' +
                '<div class="modal-dialog modal-danger" role="document">\n' +
                '<div class="modal-content">\n' +
                '<div class="modal-header">\n' +
                '<h4 class="modal-title">Warning!</h4>\n' +
                '<button type="button" class="close" data-dismiss="modal" aria-label="Close">\n' +
                '<span aria-hidden="true">×</span>\n' +
                '</button>\n' +
                '</div>\n' +
                '<div class="modal-body">\n' +
                '<p>' + text + '</p>\n' +
                '</div>\n' +
                '<div class="modal-footer">\n' +
                '<button type="button" class="btn btn-secondary" data-dismiss="modal">Нет</button>\n' +
                '<a class="btn btn-danger" href="' + href + '">Да</a>\n' +
                '</div>\n' +
                '</div>\n' +
                '\n' +
                '</div>\n' +
                '\n' +
                '</div>';
        $('body').append(html);
        $(html).modal({show:true});
        return false;
    });
});