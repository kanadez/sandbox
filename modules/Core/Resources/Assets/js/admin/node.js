// Govnokod
// ToDo: refactor
$(document).ready(function() {
    let assignment = $('.assignment_button:checked');
    let type = assignment.attr("data-type");
    $('.' + type).show();

    $('.assignment_button').change(function (e) {
        $('.dcable').hide()
        let type = $(this).attr("data-type");
        $('.' + type).show();
        setType(type);
        hideNotSelected(type, $(this).attr("data-opposite"));
    });

    let setType = function (type) {
        switch (type) {
            case 'dc':
                $('.dcable_type').val('Modules\\Core\\Model\\Dc');
                break;
            case 'rs':
                $('.dcable_type').val('Modules\\Core\\Model\\Residential');
                break;
        }
    };
    let hideNotSelected = function(type) {

        let dc = $('.dcable.dc input:radio[name="dcable_id"]'),
            rs = $('.dcable.rs input:radio[name="dcable_id"]');
        switch (type) {
            case 'dc':
                rs.attr("disabled", true).removeAttr("checked").hide().parent().removeClass("active");
                dc.removeAttr("disabled").show();
                break;
            case 'rs':
                dc.attr("disabled", true).removeAttr("checked").hide().parent().removeClass("active");
                rs.removeAttr("disabled").show();
                break;
        }

    };

    $('.assignment_button, .rotation_period_button, .dcable_button').change(function (e) {
        let assignment = $('.assignment_button:checked').parent().text().trim();
        let rotation = $('.rotation_period_button:checked').parent().text().trim();
        let dcable = $('.dcable_button:checked').parent().text().trim();
        $('.node_name').val(assignment + '.' + rotation + '.' + dcable);
    });
});