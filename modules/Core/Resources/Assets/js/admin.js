require('datatables.net');

require('./admin/bootstrap');

require('./admin/confirm');

require('./admin/tabs');

require('./admin/masked');

require('./admin/node');

require('./admin/user');

require('./admin/notifications');

require('./admin/ckeditor');

require('./admin/custom');

require('./admin/app/helpers');

require('./admin/app/config');

require('./admin/app/api');

require('./admin/app/form');

require('./admin/app/modal');

require('./admin/app/catalog');

require('./admin/app/lang');

require('./admin/app/custom');

require('./admin/app/nodes');
