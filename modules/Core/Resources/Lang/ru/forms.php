<?php

return [
    'nodes' => 'Ноды',
    'node_info' => 'Параметры ноды',
    'node_ports' => 'Порты ноды',
];
