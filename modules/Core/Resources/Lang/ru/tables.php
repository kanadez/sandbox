<?php

return [
    'ip' => 'IP',
    'country' => 'Страна',
    'name' => 'Название',
    'created_at' => 'Создано',
    'port' => 'Порт',
    'user' => 'Пользователь',
];
