<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 23.07.18
 * Time: 18:42
 */
?>
@component('mail::message')
# Hello, {{ $name }}

You are receiving this email because we received a proxy request for your this mail account.

@component('mail::table')
||               |                       ||
|| ------------- |:---------------------:||
|| Client area   |{{ route("lk.login") }}||
|| Login         | {{ $email }}  ||
|| Password      | {{ $password }}       ||
@endcomponent

@component('mail::button', ['url' => route("lk.login")])
Client area
@endcomponent

If you did not request a proxy, no further action is required.

Thanks,
{{ config('app.name') }}
@endcomponent
