<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 23.07.18
 * Time: 18:42
 */
?>

@component('mail::message')
    # Hello!

    You are receiving this email because we received a password reset request for this mail account.

    @component('mail::button', ['url' => route("lk.password.reset") . $email])
    Reset password
    @endcomponent

    If you did not request a password reset, no further action is required.

    Thanks,
    {{ config('app.name') }}
@endcomponent
