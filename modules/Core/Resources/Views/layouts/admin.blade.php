<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 18.07.18
 * Time: 11:03
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="Admin panel">
    <meta name="keyword" content="Admin panel">
    <title>Admin panel</title>
    <link href="{{ mix("/css/admin.css", 'assets/admin') }}" rel="stylesheet">
    <script>
        let laravel = {
            user: {!! auth()->user()->toJson() !!} ,
        }
    </script>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
<header class="app-header navbar">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="{{ route("admin.index") }}">
        <span>Admin</span>
    </a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <button class="navbar-toggler sidebar-toggler-mobile" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <ul class="nav navbar-nav d-md-down-none">
        <li class="nav-item px-3">
            <a class="nav-link" href="{{ route("web.index") }}" target="_blank">Landing</a>
        </li>
        <li class="nav-item px-3">
            <a class="nav-link" href="{{ route("lk.index") }}" target="_blank">User's dashboard</a>
        </li>
    </ul>
    <ul class="nav navbar-nav px-3 ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                {{--<img class="img-avatar" src="img/avatars/6.jpg" alt="admin@bootstrapmaster.com">--}}
                <span>{{ $user["name"] }}</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="{{ route("admin.logout") }}">
                    <i class="fa fa-lock"></i> Logout</a>
            </div>
        </li>
    </ul>
</header>
<div class="app-body">
    <div class="sidebar"> 
        @include('core::admin.menu.menu', ['items' => Menu::get('AdminMenu')])
        <button class="sidebar-minimizer brand-minimizer" type="button"></button>
    </div>
    <main class="main">
        {{ Breadcrumbs::render() }}
        @include('core::admin.flash.message')
        <div class="container-fluid">
            <div class="animated fadeIn">
                @yield('content')
            </div>
        </div>
    </main>
</div>
<footer class="app-footer">
    <div>
        <a href="https://astroproxy.com">astroproxy.com</a>
        <span>&copy; 2018 astroproxy.com</span>
    </div>
    <div class="ml-auto">
        <span>Powered by</span>
        <a href="https://astroproxy.com">astroproxy.com</a>
    </div>
</footer>
<!-- CoreUI and necessary plugins-->
<script src="{{ mix("/js/manifest.js", 'assets/admin') }}"></script>
<script src="{{ mix("/js/vendor.js", 'assets/admin') }}"></script>
<script src="{{ mix("/js/admin.js", 'assets/admin') }}"></script>
</body>
</html>
