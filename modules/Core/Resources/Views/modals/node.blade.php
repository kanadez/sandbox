<div class="modal" id="node" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form method="POST" action="#" data-action-store="{{ route('api.nodes.store') }}" data-action-update="{{ route('api.nodes.update', ['node' => '[id]']) }}">
            <div class="modal-header">
              <h5 class="modal-title">{{ trans('core::forms.node_info') }}</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="modal-body">
                <div class="alert alert-primary request-error d-none"></div>
                <div class="row mb-4">
                    <div class="col-md-12">
                        <div class="row mb-2">
                            <div class="form-group col-sm-12">
                                <label for="name">{{ trans('lk::forms.name') }}</label>
                                <input type="text" name="slug" class="form-control" id="slug" style="padding-right: 115px; text-align: right;">
                                <b for="name" style="position: absolute; right: 25px; top: 36px;">.{{ env("APP_DOMAIN", "astroproxy.com") }}</b>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" name="save"><i class="fa fa-save mr-2"></i>{{ trans('lk::forms.save') }}</button>
            </div>
        </form>
    </div>
  </div>
</div>

<div class="modal" id="node-ports" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <form method="POST" action="#" data-action-store="{{ route('api.nodes.store') }}" data-action-update="{{ route('api.nodes.update', ['node' => '[id]']) }}">
            <div class="modal-header">
              <h5 class="modal-title">{{ trans('core::forms.node_ports') }}</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="modal-body">
                <div class="alert alert-primary request-error d-none"></div>
                <table class="table">
                    <thead>
                        <th>#</th>
                        <th>{{ trans('core::tables.port') }}</th>
                        <th>{{ trans('core::tables.user') }}</th>
                        <th>{{ trans('core::tables.created_at') }}</th>
                    </thead>
                </table>
            </div>
        </form>
    </div>
  </div>
</div>
