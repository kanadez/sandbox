<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 26.09.18
 * Time: 12:39
 */
?>
@extends('core::layouts.admin')

@section('content')
    <!-- /.row-->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> <strong>Orders list</strong>
                    <a class="btn btn-success float-right" href="{{ route("admin.order.create") }}">
                        <i class="fa fa-plus"></i> <span>Create</span>
                    </a>
                </div>
                <div class="card-body">
                    <table class="table table-responsive-sm table-bordered table-striped table-sm">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th>id</th>
                            <th>Number</th>
                            <th>Items</th>
                            <th>Price</th>
                            {{--<th>Actions</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($items as $item)
                            <tr>
                                <td scope="row">{{ $loop->iteration }}</td>
                                <td>{{ $item["id"] }}</td>
                                <td>
                                    <a href="{{ route("admin.order.edit", $item) }}">Order №{{ $item["number"] }}</a>
                                </td>
                                <td>
                                    <ul class="list-group">
                                        @foreach($item->items as $subItem)
                                            <li class="list-group-item small-item list-group-item-action">{{ $subItem->product["name"] }}</li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td>{{ $item->price }}{!! $item->currency["short_name"] !!}</td>
                                {{--<td>--}}
                                    {{--<a class="btn btn-danger" href="{{ route("admin.order.delete", $item) }}" title="Remove" data-confirm="Are you sure, that you want to remove order {{ $item["name"] }}?">--}}
                                        {{--<i class="fa fa-trash-o "></i>--}}
                                    {{--</a>--}}

                                {{--</td>--}}
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $items->links() }}
                </div>
            </div>
        </div>
        <!-- /.col-->
    </div>
@endsection
