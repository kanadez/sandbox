@extends('core::layouts.admin')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <button class="btn btn-primary" id="add-node"><i class="fa fa-plus mr-2"></i> {{ trans('core::buttons.add_node') }}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">{{ trans('core::forms.nodes') }}</div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table" id="nodes">
                        <thead>
                            <tr>
                              <th>#</th>
                              <th>{{ trans('core::tables.name') }}</th>
                              <th>{{ trans('core::tables.ip') }}</th>
                              <th>{{ trans('core::tables.created_at') }}</th>
                              <th></th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @include('core::modals.node')
@overwrite
