<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 18.07.18
 * Time: 14:09
 */
?>
@foreach($items as $item)
    <li class="nav-item
        @if($item->hasChildren())
            nav-dropdown
        @endif
    @if(!empty($item->data("mask")) && preg_match('/'.$item->data("mask").'/i', Route::currentRouteName()))
            open
        @endif">

        <a class="nav-link
            @if($item->hasChildren())
                nav-dropdown-toggle
            @endif
        @if(!$item->hasChildren() && !empty($item->data("mask")) && preg_match('/'.$item->data("mask").'/i', Route::currentRouteName()))
                active
            @endif"
           title="{!! $item->title !!}"
           href="{!! $item->url() !!}">
            @if(!empty($item->data("icon")))<i class="{{ $item->data("icon") }}"></i> <span class="title">@endif{!! $item->title !!}</span>
            @if(!empty($item->data("badge")))<span class="{{ $item->data("badge")["icon"] }}">{{ $item->data("badge")["text"] }}</span>@endif
        </a>
        @if($item->hasChildren())
            <ul class="nav-dropdown-items">
                @include('core::admin.menu.items', ['items' => $item->children()])
            </ul>
        @endif
    </li>
@endforeach
