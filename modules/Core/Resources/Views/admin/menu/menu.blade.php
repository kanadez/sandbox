<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 18.07.18
 * Time: 14:09
 */
?>
<!--nav-->
<nav class="sidebar-nav">
    <ul class="nav">
        @if(!empty($items))
            @include('core::admin.menu.items', ['items' => $items->sortBy('order', 'asc')->roots()])
        @endif
    </ul>
</nav>
<!--/nav-->
