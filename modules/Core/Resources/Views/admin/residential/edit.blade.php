<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 22.08.18
 * Time: 15:58
 */
?>
@extends('core::layouts.admin')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> <strong> {{ $item->exists ? 'Edit' : 'Create' }} residential{{ $item["email"] }}</strong>
                </div>
                <div class="card-body">

                    @include('core::admin.forms.errors')
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="true">Information</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#parents" role="tab" aria-controls="info" aria-selected="true">Parent ports</a>
                        </li>
                    </ul>
                    <div class="tab-content admin-tab-content">
                        <div class="tab-pane active" id="info" role="tabpanel">
                            {{ Form::model($item, ['route' => ['admin.residential.store', $item], "class" => "residential-edit-form" ]) }}
                            {!! Form::admText('name', 'Name', ["placeholder" => "Residential"]) !!}
                            {!! Form::admSubmit($item->exists ? 'Update' : 'Create') !!}
                            {{ Form::close() }}
                        </div>
                        <div class="tab-pane" id="parents" role="tabpanel">
                            <table class="table table-responsive-sm table-bordered table-striped table-sm">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th>id</th>
                                    <th>IP</th>
                                    <th>Port</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($item->parents as $port)
                                    <tr>
                                        <td scope="row">{{ $loop->iteration }}</td>
                                        <td>{{ $port->id }}</td>
                                        <td>{{ $port->ip }}</td>
                                        <td>{{ $port->port }}</td>
                                        <td>
                                            <a class="btn btn-danger" href="{{ route("admin.residential_parent.delete", $port) }}" title="Remove" data-confirm="Are you sure, that you want to remove parent port {{ $port["ip"] }}?">
                                                <i class="fa fa-trash-o "></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ Form::open(["route" => ["admin.residential_parent.store"], "class" => "residential_parent-store-form" ]) }}
                            {!! Form::admHidden('residential_id', $item["id"]) !!}
                            <div class="row">
                                <div class="col-md-11">
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="ip" class="form-control-label">IP</label>
                                            <input id="ip" name="ip" type="text" class="form-control ip_address" placeholder="Ip" required="required" >
                                        </div>
                                        <div class="col-md-6">
                                            <label for="port" class="form-control-label">Mask</label>
                                            <input id="port" name="port" type="number" class="form-control" placeholder="Port" min="1" max="65000" required="required">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1 pool-save">
                                    <div class="form-group">
                                        <input class="btn btn-success" type="submit" value="Create">
                                    </div>
                                </div>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
