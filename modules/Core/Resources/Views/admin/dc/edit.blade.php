<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 06.08.18
 * Time: 11:52
 */
?>
@extends('core::layouts.admin')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> <strong> {{ $item->exists ? 'Edit' : 'Create' }} data center {{ $item["email"] }}</strong>
                </div>
                <div class="card-body">

                    @include('core::admin.forms.errors')
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="true">Information</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#pools" role="tab" aria-controls="info" aria-selected="true">IP pools</a>
                        </li>
                    </ul>
                    <div class="tab-content admin-tab-content">
                        <div class="tab-pane active" id="info" role="tabpanel">
                            {{ Form::model($item, ['route' => ['admin.dc.store', $item], "class" => "dc-edit-form" ]) }}
                            {!! Form::admText('name', 'Name', ["placeholder" => "Dc"]) !!}
                            {{--{!! Form::admSelect('provider_id', 'Provider', $providers) !!}--}}
                            {!! Form::admSelect('country_id', 'Country', $countries) !!}
                            {!! Form::admSubmit($item->exists ? 'Update' : 'Create') !!}
                            {{ Form::close() }}
                        </div>
                        <div class="tab-pane" id="pools" role="tabpanel">
                            <table class="table table-responsive-sm table-bordered table-striped table-sm">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th>id</th>
                                    <th>IP</th>
                                    <th>Mask</th>
                                    <th>Gateway</th>
                                    <th>DNS</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($item->pools as $pool)
                                        <tr>
                                            <td scope="row">{{ $loop->iteration }}</td>
                                            <td>{{ $pool->id }}</td>
                                            <td>{{ $pool->ip }}</td>
                                            <td>{{ $pool->mask }}</td>
                                            <td>{{ $pool->gw }}</td>
                                            <td>{{ $pool->dns }}</td>
                                            <td>
                                                <a class="btn btn-danger" href="{{ route("admin.dc_pool.delete", $pool) }}" title="Remove" data-confirm="Are you sure, that you want to remove dc pool {{ $pool["ip"] }}?">
                                                    <i class="fa fa-trash-o "></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ Form::open(["route" => ["admin.dc_pool.store"], "class" => "dc_pool-store-form" ]) }}
                            {!! Form::admHidden('dc_id', $item["id"]) !!}
                            <div class="row">
                                <div class="col-md-11">
                                    <div class="form-group row">
                                        <div class="col-md-3">
                                            <label for="ip" class="form-control-label">IP</label>
                                            <input id="ip" name="ip" type="text" class="form-control ip_address" placeholder="ip" required="required" >
                                        </div>
                                        <div class="col-md-3">
                                            <label for="mask" class="form-control-label">Mask</label>
                                            <input id="mask" name="mask" type="text" class="form-control ip_address" placeholder="Mask" value="255.255.255.0">
                                        </div>
                                        <div class="col-md-3">
                                            <label for="gw" class="form-control-label">Gateway</label>
                                            <input id="gw" name="gw" type="text" class="form-control ip_address" placeholder="GW">
                                        </div>
                                        <div class="col-md-3">
                                            <label for="dns" class="form-control-label">DNS</label>
                                            <input id="dns" name="dns" type="text" class="form-control ip_address" placeholder="DNS" value='8.8.8.8'>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1 pool-save">
                                    <div class="form-group">
                                        <input class="btn btn-success" type="submit" value="Create">
                                    </div>
                                </div>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
