<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 20.07.18
 * Time: 10:47
 */
?>
@extends('core::layouts.admin')

@section('content')
    <!-- /.row-->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> <strong>Default Proxy Ports Settings (for API request)</strong>
                </div>
                <div class="card-body">
                    {{ Form::model($defaultSettings, ['route' => ['admin.default-proxy-ports-settings.save', $defaultSettings], "class" => "user-edit-form" ]) }}
                        @include('core::admin.forms.errors')

                        {!! Form::admText('username', 'Username') !!}
                        {!! Form::admText('password', 'Password') !!}
                        {!! Form::admText('peer_rotate', 'Peer rotate') !!}
                        {!! Form::admText('fingerprint', 'Fingerprint') !!}
                        {!! Form::admText('asn', 'ASN') !!}
                        {!! Form::admText('uptime', 'Uptime') !!}
                        {!! Form::admNumber('latency', 'Latency') !!}
                        {!! Form::admText('speed_download', 'Speed download') !!}
                        {!! Form::admText('speed_upload', 'Speed upload') !!}
                        {!! Form::admText('allowed_ip', 'Allowed IPs(list divide by ;)') !!}
                        {!! Form::admNumber('max_connections', 'Max connections') !!}
                        {!! Form::admText('bandwidth_in', 'Bandwidth In') !!}
                        {!! Form::admText('bandwidth_out', 'Bandwidth Out') !!}
                        {!! Form::admText('time_limit', 'Time Limit') !!}

                        {!! Form::admSubmit($defaultSettings->exists ? 'Save' : 'Create') !!}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
        <!-- /.col-->
    </div>
    <!-- /.row-->
@endsection
