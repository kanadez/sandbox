<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 23.08.18
 * Time: 11:41
 */
?>
@extends('core::layouts.admin')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> <strong> {{ $item->exists ? 'Edit' : 'Create' }} remote command {{ $item["email"] }}</strong>
                </div>
                <div class="card-body">
                    {{ Form::model($item, ['route' => ['admin.remote_command.store', $item], "class" => "remote_command-edit-form" ]) }}
                    @include('core::admin.forms.errors')
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="true">Information</a>
                        </li>
                    </ul>
                    <div class="tab-content admin-tab-content">
                        <div class="tab-pane active" id="info" role="tabpanel">
                            {!! Form::admText('name', 'Name', ["placeholder" => "Command name"]) !!}
                            {!! Form::admTextarea('command', 'Command', ["placeholder" => "Command", "class" => 'form-control bash-text']) !!}
                            {!! Form::admTextarea('comment', 'Comment', ["placeholder" => "Comment"]) !!}
                        </div>
                    </div>
                    {!! Form::admSubmit($item->exists ? 'Update' : 'Create') !!}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection
