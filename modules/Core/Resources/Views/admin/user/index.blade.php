<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 19.07.18
 * Time: 14:16
 */
?>
@extends('core::layouts.admin')

@section('content')
    <!-- /.row-->
    <div class="row">
        <div class="col-lg-12">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 top-panel">
                <a class="btn btn-success" href="{{ route("admin.user.create") }}">
                    <i class="fa fa-plus"></i> <span>Create</span>
                </a>
            </div>
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> <strong>Users list</strong>
                </div>
                <div class="card-body">
                    <table class="table table-responsive-sm table-bordered table-striped table-sm">
                        <thead>
                        <tr>
                            <!--<th scope="col">#</th>
                            <th>id</th>-->
                            <th>Email</th>
                            <th>Registraion date</th>
                            <th>Role</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($items as $item)
                            <tr>
                                <!--<td scope="row">{{ $loop->iteration }}</td>
                                <td>{{ $item["id"] }}</td>-->
                                <td>
                                    <a href="{{ route("admin.user.edit", $item) }}">{{ $item["email"] }}</a>
                                </td>
                                <td>{{ $item["created_at"] }}</td>
                                <td>
                                    @foreach($item->roles as $role)
                                        {{ $role["name"]." " }}
                                    @endforeach
                                </td>
                                <td>
                                    <!--<a class="btn btn-link" href="{{ route("admin.proxy-request.create", $item) }}" title="Create proxy request">
                                        <i class="fa fa-envelope-o "></i>
                                    </a>
                                    <a class="btn btn-link" href="{{ route("admin.user.edit", $item) }}" title="Edit">
                                        <i class="fa fa-edit "></i>
                                    </a>-->
                                    <a title="Proxy ports" class="btn btn-link" href="{{ route("admin.proxy-port.index", $item) }}">
                                        <i class="fa fa-server "></i>
                                    </a>
                                    <a class="btn btn-link" href="{{ route("admin.user.delete", $item) }}" title="Remove" data-confirm="Are you sure, that you want to remove user {{ $item["name"] }}?">
                                        <i class="fa fa-trash-o "></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $items->links() }}
                </div>
            </div>
        </div>
        <!-- /.col-->
    </div>
    <!-- /.row-->
@endsection
