<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 19.07.18
 * Time: 14:53
 */
?>
@extends('core::layouts.admin')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 top-panel">
                @if($item->exists)
                    <a class="btn btn-success" href="{{ route("admin.proxy-port.create", $item) }}">
                        <i class="fa fa-plus"></i> <span>Create proxy port</span>
                    </a>

                    <a class="btn btn-success" href="{{ route("admin.proxy-port.index", $item) }}">
                        <i class="fa fa-server"></i> <span>View proxy ports</span>
                    </a>
                @endif
            </div>
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-user"></i> <strong> {{ $item->exists ? 'Edit' : 'Create' }} user {{ $item["email"] }}</strong>
                </div>
                <div class="card-body">
                    {{ Form::model($item, ['route' => ['admin.user.store', $item], "class" => "user-edit-form form-horizontal form-border" ]) }}
                    @include('core::admin.forms.errors')
                    
                    {!! Form::admText('name', 'Name', ["placeholder" => "Enter your name"]) !!}
                    {!! Form::admEmail('email', 'Email', ["placeholder" => "Enter email"]) !!}
                    {!! Form::admPassword('password', 'Password', ["placeholder" => "Password"]) !!}
                    {!! Form::admPassword('password_confirmation', 'Password confirmation', ["placeholder" => "Password confirmation"]) !!}
                    {!! Form::admCheckboxInput('roles', 'Roles', $roles, $item_roles ) !!}
                    {!! Form::admSubmit($item->exists ? 'Update' : 'Create') !!}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection