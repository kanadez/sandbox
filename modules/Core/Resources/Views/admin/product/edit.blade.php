<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 20.09.18
 * Time: 16:00
 */
?>
@extends('core::layouts.admin')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> <strong> {{ $item->exists ? 'Edit' : 'Create' }} product {{ $item["name"] }}</strong>
                </div>
                <div class="card-body">
                    {{ Form::model($item, ['route' => ['admin.product.store', $item], "class" => "product-edit-form" ]) }}
                    @include('core::admin.forms.errors')
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="true">Information</a>
                        </li>
                    </ul>
                    <div class="tab-content admin-tab-content">
                        <div class="tab-pane active" id="info" role="tabpanel">
                            {!! Form::admText('name', 'Name', ["placeholder" => "Enter currency"]) !!}
                            <div class="form-group">
                                <label class="col-form-label" for="price">Price</label>
                                <div class="controls">
                                    <div class="input-group">
                                        <input class="form-control" type="number" id="price" min="0" name="price" step="0.01" value="{{ $item->price["value"] ?? old("price")}}">
                                        <div class="input-group-append">
                                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                @foreach($currencies as $currency)
                                                    <label class="btn btn-light @if($item->getCurrencyId() == $currency["id"] && empty(old("currency_id")) || old("currency_id") ==  $currency["id"]) active @endif">
                                                        <input type="radio" name="currency_id" autocomplete="off" value="{{ $currency["id"] }}" @if($item->getCurrencyId() == $currency["id"] && empty(old("currency_id")) || old("currency_id") ==  $currency["id"]) checked @endif> {!! $currency["short_name"] !!}
                                                    </label>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {!! Form::admHtmlArea('description', 'Description', ["placeholder" => "Description"]) !!}
                        </div>
                    </div>
                    {!! Form::admSubmit($item->exists ? 'Update' : 'Create') !!}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection
