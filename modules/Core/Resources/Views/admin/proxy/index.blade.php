<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 01.08.18
 * Time: 9:26
 */
?>
@extends('core::layouts.admin')

@section('content')
    <!-- /.row-->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> <strong>Proxy list of node {{ $item["name"] }}</strong>
                </div>
                <div class="card-body">
                    <table class="table table-responsive-sm table-bordered table-striped table-sm">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th>id</th>
                            <th>Port</th>
                            <th>Login</th>
                            <th>Pass</th>
                            <th>Created at</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($item->proxies as $item)
                            <tr>
                                <td scope="row">{{ $loop->iteration }}</td>
                                <td>{{ $item["id"] }}</td>
                                <td>
                                    {{ $item["port"] }}
                                </td>
                                <td>
                                    {{ $item["login"] }}
                                </td>
                                <td>
                                    {{ $item["pass"] }}
                                </td>
                                <td>
                                    {{ $item["created_at"] }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{--{{ $items->links() }}--}}
                </div>
            </div>
        </div>
        <!-- /.col-->
    </div>
    <!-- /.row-->
@endsection
