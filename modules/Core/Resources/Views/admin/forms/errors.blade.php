<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 18.07.18
 * Time: 14:49
 */
?>
@if(!empty($errors = session("errors")))
    @foreach($errors->all() as $error)
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
            {!! $error !!}
        </div>
    @endforeach
@endif
