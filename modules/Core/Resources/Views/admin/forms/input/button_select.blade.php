<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 01.10.18
 * Time: 11:41
 */
?>
<div class="form-group">
    {{ Form::label($name, $label_name, ['class' => 'control-label']) }}
    <div>
        <div class="btn-group btn-group-toggle" data-toggle="buttons">
            @foreach($values as $value)
                <label class="btn btn-info {{ Form::getValueAttribute($name) == $value["id"] ? "active" : ""  }}">
                    {!! Form::radio($name, $value["id"], Form::getValueAttribute($name) == $value["id"], $attributes) !!} {{ $value["name"] }}
                </label>
            @endforeach
        </div>
    </div>
</div>
