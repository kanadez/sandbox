<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 26.07.18
 * Time: 9:27
 */
if (empty(Form::getValueAttribute($name)) && !empty($attributes["default"])) {
    $value = $attributes["default"];
}
?>
<div class='form-group'>
    {!! Form::label($name, $label_name, ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
    {!! Form::number($name, $value, array_merge(['class' => 'form-control'], $attributes)) !!}
    </div>
</div>
