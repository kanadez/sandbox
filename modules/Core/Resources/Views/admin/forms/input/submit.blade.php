<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 19.07.18
 * Time: 15:06
 */
?>
<div class="form-group col-sm-12 text-center">
    {{ Form::submit($value, array_merge(['class' => 'btn btn-primary'], $attributes)) }}
</div>
