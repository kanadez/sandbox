<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 19.07.18
 * Time: 15:31
 */
?>
<div class="form-group">
    {{ Form::label($name, $label_name, ['class' => 'col-sm-3 control-label']) }}
    <div class="col-sm-6">
    {{ Form::password($name, array_merge(['class' => 'form-control'], $attributes)) }}
    </div>
</div>
