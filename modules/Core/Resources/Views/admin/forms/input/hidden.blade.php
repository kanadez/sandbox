<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 08.08.18
 * Time: 11:06
 */
if (empty(Form::getValueAttribute($name)) && !empty($attributes["default"])) {
    $value = $attributes["default"];
}
?>
{{ Form::hidden($name, $value, array_merge(['class' => 'form-control'], $attributes)) }}
