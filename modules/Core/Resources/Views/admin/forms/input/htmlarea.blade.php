<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 20.09.18
 * Time: 16:13
 */
?>
<div class="form-group">
    {{ Form::label($name, $label_name, ['class' => 'control-label']) }}
    {{ Form::textarea($name, $value, array_merge(['class' => 'form-control html-area'], $attributes)) }}
</div>
