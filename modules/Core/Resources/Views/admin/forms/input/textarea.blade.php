<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 08.08.18
 * Time: 11:36
 */
?>
<div class="form-group">
    {{ Form::label($name, $label_name, ['class' => 'control-label']) }}
    {{ Form::textarea($name, $value, array_merge(['class' => 'form-control'], $attributes)) }}
</div>
