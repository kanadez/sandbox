<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 19.07.18
 * Time: 16:10
 */
?>
<div class="form-group">
    <label for="{{$name}}" class="control-label col-sm-3">{{ $label_name }}</label>
    <div class="col-sm-6">
    @foreach($values as $value)
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="{{ $value[$id_filed] }}" id="{{ $name.$loop->index }}" name="{{ $name.'[]' }}" @if(in_array($value[$id_filed], $selected)) checked="checked" @endif>
            <label class="form-check-label" for="{{ $name.$loop->index }}">{{ $value[$name_field] }}</label>
        </div>
    @endforeach
    </div>
</div>
