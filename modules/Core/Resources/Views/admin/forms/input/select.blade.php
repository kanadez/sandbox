<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 02.08.18
 * Time: 15:16
 */
?>
<div class="form-group">
    <label for="{{$name}}" class="col-sm-3 control-label">{{ $label_name }}</label>
    <div class="col-sm-6">
    {{ Form::select($name, $values, $default, array_merge($attributes, ['class' => 'form-control'])) }}
    </div>
</div>
