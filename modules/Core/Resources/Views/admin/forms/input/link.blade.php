<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 23.08.18
 * Time: 10:25
 */
?>
<div class="form-group">
    {{ Form::label($name, $label_name, ['class' => 'control-label']) }}
    <a class="form-control" href="{{ $value }}">{{ $name }}</a>
</div>
