<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 19.07.18
 * Time: 15:22
 */
if (empty(Form::getValueAttribute($name)) && !empty($attributes["default"])) {
    $value = $attributes["default"];
}
?>

<div class="form-group">
{{ Form::text($name, $value, array_merge(['class' => 'form-control'], $attributes)) }}
</div>