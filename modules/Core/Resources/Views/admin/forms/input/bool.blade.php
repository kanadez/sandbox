<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 27.09.18
 * Time: 9:06
 */
?>
<div class='form-group'>
    {!! Form::label($name, $label_name) !!}
    <div>
        {!! Form::hidden($name, 0) !!}
        <label class="inline_label">{!! Form::checkbox($name, 1) !!} Yes/No</label>
    </div>
</div>