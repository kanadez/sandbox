<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 08.08.18
 * Time: 10:41
 */
?>
@extends('core::layouts.admin')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> <strong> {{ $item->exists ? 'Edit' : 'Create' }} port {{ $item["name"] }} @if(!empty($node)) for node {{ $node["name"] }} @endif </strong>
                </div>
                <div class="card-body">
                    {{ Form::model($item, ['route' => empty($node) ? ['admin.node_port.store', $item] : ['admin.node.port.store', $node, $item], "class" => "node-port-edit-form" ]) }}
                    @include('core::admin.forms.errors')
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="true">Information</a>
                        </li>
                    </ul>
                    <div class="tab-content admin-tab-content">
                        <div class="tab-pane active" id="info" role="tabpanel">
                            {!! Form::admText('number', 'Number', ["disabled" => "disabled", "default" => $item["next_number"]]) !!}
                            {!! Form::admHidden('number', $item->exists ? $item["number"] : $item["next_number"]) !!}
                            {!! Form::admSelect('finger_print_id', 'Finger print', $fingerPrints) !!}
                            @if (empty($node))
                                {!! Form::admSelect('node_id', 'Node', $nodes) !!}
                            @else
                                {!! Form::admText('node_name', 'Node', ["placeholder" => $node["name"], "disabled" => "disabled"], $node["name"]) !!}
                                {!! Form::admHidden('node_id', $node["id"]) !!}
                            @endif
                            @php($route = $node["dcable_type"] == \Modules\Core\Model\Dc::class ? 'dc' : 'residential')
                            {!! Form::admLink($node->dcable["name"], 'Data center', [], route("admin.".$route.".edit", [$node->dcable])) !!}
                            @if($item["next_number"] == 1 || $item["number"] == 1)
                                {!! Form::admText('provider_point', 'External IP', ["disabled" => "disabled", "default" => $node["ip"]]) !!}
                                {!! Form::admHidden('provider_point', $node["ip"]) !!}
                            @else
                                {!! Form::admSelect('provider_point', 'External IP', $node->dcable->points()) !!}
                            @endif
                            {!! Form::admText('interval', 'Interval', ["disabled" => "disabled", "default" => ($node["rotationPeriod"]["name"] == 'stc' ? 'static' : $node["rotationPeriod"]["name"])]) !!}
                            {!! Form::admNumber('mtu', 'MTU', ["placeholder" => "Mtu", "default" => 1493]) !!}
                            {!! Form::admText('ip_geo', 'IP Geo', ["placeholder" => "IP Geo"]) !!}
                            {!! Form::admText('link', 'Link', ["placeholder" => "Network link"]) !!}
                            {!! Form::admTextarea('comment', 'Comment', ["placeholder" => "Comment"]) !!}
                        </div>
                    </div>
                    {!! Form::admSubmit($item->exists ? 'Update' : 'Create') !!}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection
