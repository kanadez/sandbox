<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 06.08.18
 * Time: 11:02
 */
?>
@extends('core::layouts.admin')

@section('content')
    <!-- /.row-->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> <strong>Countries list</strong>
                    <a class="btn btn-success float-right" href="{{ route("admin.country.create") }}">
                        <i class="fa fa-plus"></i> <span>Create</span>
                    </a>
                </div>
                <div class="card-body">
                    <table class="table table-responsive-sm table-bordered table-striped table-sm">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th>id</th>
                            <th>Name</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($items as $item)
                            <tr>
                                <td scope="row">{{ $loop->iteration }}</td>
                                <td>{{ $item["id"] }}</td>
                                <td>
                                    <a href="{{ route("admin.country.edit", $item) }}">{{ $item["name"] }}</a>
                                </td>
                                <td>
                                    <a class="btn btn-info" href="{{ route("admin.country.edit", $item) }}" title="Edit">
                                        <i class="fa fa-edit "></i>
                                    </a>
                                    <a class="btn btn-danger" href="{{ route("admin.country.delete", $item) }}" title="Remove" data-confirm="Are you sure, that you want to remove country {{ $item["name"] }}?">
                                        <i class="fa fa-trash-o "></i>
                                    </a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $items->links() }}
                </div>
            </div>
        </div>
        <!-- /.col-->
    </div>
    <!-- /.row-->
@endsection
