<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 20.07.18
 * Time: 10:47
 */
?>
@extends('core::layouts.admin')

@section('content')
    <!-- /.row-->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> <strong>Proxy requests list</strong>
                </div>
                <div class="card-body">
                    <table class="table table-responsive-sm table-bordered table-striped table-sm">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th>id</th>
                            <th>User</th>
                            <th>Tgm</th>
                            <th>Skype</th>
                            <th>Proceed</th>
                            <th>Created at</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($items as $item)
                            <tr>
                                <td scope="row">{{ $loop->iteration }}</td>
                                <td>{{ $item["id"] }}</td>
                                <td>
                                    <a href="{{ route("admin.user.edit", $item) }}">{{ $item->user["email"] }}</a>
                                </td>
                                <td>{{ $item["telegram"] }}</td>
                                <td>{{ $item["skype"] }}</td>
                                <td>@yes_or_no($item["is_processed"])</td>
                                <td>{{ $item["created_at"] }}</td>
                                <td>
                                    @if($item["is_processed"])
                                        <a class="btn btn-secondary" href="{{ route("admin.proxy-request.unapprove", $item) }}" title="Uncheck approved">
                                            <i class="fa fa-square-o  "></i>
                                        </a>
                                    @else
                                        <a class="btn btn-success" href="{{ route("admin.proxy-request.approve", $item) }}" title="Mark checked">
                                            <i class="fa fa-check-square-o "></i>
                                        </a>
                                    @endif
                                    <a class="btn btn-danger" href="{{ route("admin.proxy-request.delete", $item) }}" title="remove" data-confirm="Are you sure, that you want to remove request {{ $item["id"] }}?">
                                        <i class="fa fa-trash-o "></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $items->links() }}
                </div>
            </div>
        </div>
        <!-- /.col-->
    </div>
    <!-- /.row-->
@endsection
