
@extends('core::layouts.admin')

@section('content')
<!--<h1>@lang("lk::order.create_h1")</h1>-->
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                @lang("lk::proxy_port.proxy_port") {{ $proxyPort['name'] }}
                <div class="float-right">
                <a class="btn btn-success float-right" href="{{ route("admin.proxy-port.edit", $proxyPort) }}">
                    <i class="fa fa-edit"></i> <span>@lang("lk::proxy_port.edit")</span>
                </a>
                </div>
            </div>
            <div class="card-body">
                @if($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                        {{ $errors->first() }}
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-6">
                        <ul>
                            <li>
                                @lang("lk::proxy_port.Type") - {{ $proxyPort['type'] }}
                            </li>
                            <li>
                                @lang("lk::proxy_port.ip_type") - {{ $proxyPort['ip_type'] }}
                            </li>
                            <li>
                                @lang("lk::proxy_port.time_ip_change") - {{ $proxyPort['time_ip_change'] }}
                            </li>
                            <li>
                                @lang("lk::proxy_port.pull_size") - {{ $proxyPort['pull_size'] }} 
                            </li>
                            <li>
                                @lang("lk::proxy_port.trafic") - {{ $proxyPort['trafic'] }} GB
                            </li>
                            @if( $proxyPort->country )
                            <li>
                                @lang("lk::proxy_port.country") - {{ $proxyPort->country->name }}
                            </li>
                            @endif
                            @if( $proxyPort->city )
                            <li>
                                @lang("lk::proxy_port.city") - {{ $proxyPort->city->name }}
                            </li>
                            @endif
                        </ul>
                    </div>
                    <div class="col-md-6">
                        @lang("lk::proxy_port.month_costs") - {{$mounthCostsIP + $mounthCostsTrafic}} RUR<br>
                        @lang("lk::proxy_port.traffic_costs") - {{$mounthCostsTrafic}} RUR<br>
                        @lang("lk::proxy_port.ip_costs") - {{$mounthCostsIP}} RUR<br>
                    </div>
                </div>
                                
            </div>
        </div>
    </div>
</div>
@endsection
