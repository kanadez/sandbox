
@extends('core::layouts.admin')

@section('content')
<!--<h1>@lang("lk::order.create_h1")</h1>-->
<div class="row">
    <div class="col-md-12">
        <div class="card">

            {{ Form::model($item, ['route' => ['admin.proxy-port.store', $item], "class" => "user-edit-form" ]) }}
            @include('core::admin.forms.errors')

            {!! Form::admText('name', Lang::get('lk::proxy_port.Name')) !!}

            {!! Form::admSelect('type', Lang::get('lk::proxy_port.Type'), ['residental' => 'Residental', 'mobile' => 'Mobile', 'datacenter'=> 'Datacenter'], ['placeholder' => Lang::get('lk::proxy_port.pick_type')]); !!}

            {!! Form::admSelect('ip_type', Lang::get('lk::proxy_port.ip_type'), ['static' => 'Static', 'dinamic' => 'Dinamic'], ['placeholder' => Lang::get('lk::proxy_port.pick_type')]); !!}

            {!! Form::admText('time_ip_change', Lang::get('lk::proxy_port.time_ip_change')) !!}

            {!! Form::admText('pull_size', Lang::get('lk::proxy_port.pull_size')) !!}

            @if($item->exists)
            {!! Form::hidden('user_id', $item['user_id']) !!}
            @else
            {!! Form::hidden('user_id', $userId) !!}
            @endif
            
            {!! Form::admSelect('node_id', Lang::get('lk::proxy_port.node'), \Modules\Core\Model\Node::pluck('name','id'), ['placeholder' => Lang::get('lk::proxy_port.pick_node'), 'data-live-search' => "true"]); !!}

            <div class="form-group">
                <label for="trafic" class="control-label">{{ Lang::get('lk::proxy_port.trafic') }} <span class="selectedTrafic"></span></label>
                <input value="<?php if($item->exists) {echo$item['trafic'];}else{echo 20;}?>" class="form-control" min="20" max="1000" name="trafic" type="range" id="trafic">
            </div>

            {!! Form::admSelect('country_id', Lang::get('lk::proxy_port.country'), \Modules\Core\Model\CountryForProxyPort::pluck('name','id'), ['placeholder' => Lang::get('lk::proxy_port.pick_country'), 'data-live-search' => "true"]); !!}

            @if($item->exists)
             <div class="city-selector-wrapper">
            {!! Form::admSelect('city_id', Lang::get('lk::proxy_port.city'), \Modules\Core\Model\CityForProxyPort::where('country_id', $item['country_id'])->pluck('name','id'), ['placeholder' => Lang::get('lk::proxy_port.pick_city'), 'data-live-search' => "true"]); !!}
            <div style="display: none;" class="loading-cities"></div>
            </div>
            @else
            <div class="city-selector-wrapper">
            {!! Form::admSelect('city_id', Lang::get('lk::proxy_port.city'), [], ['placeholder' => Lang::get('lk::proxy_port.pick_city'), 'data-live-search' => "true"]); !!}
            <div class="loading-cities"></div>
            </div>
            
            @endif
            {!! Form::admSubmit($item->exists ? 'Update' : 'Create') !!}
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection
