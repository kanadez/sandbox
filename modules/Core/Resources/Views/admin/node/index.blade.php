<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 30.07.18
 * Time: 13:57
 */
?>
@extends('core::layouts.admin')

@section('content')
    <!-- /.row-->
    <div class="row">
        <div class="col-lg-12">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 top-panel">
                <a class="btn btn-success" href="{{ route("admin.node.create") }}">
                    <i class="fa fa-plus"></i> <span>Create</span>
                </a>
            </div>
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> <strong>Nodes list</strong>
                </div>
                <div class="card-body">
                    <table class="table table-responsive-sm table-bordered table-striped table-sm">
                        <thead>
                        <tr>
                            <!--<th scope="col">#</th>
                            <th>id</th>-->
                            <th>Name</th>
                            <!--<th>Datacenter</th>-->
                            {{--<th>Country</th>--}}
                            <th>IP address</th>
                            <!--<th>Server name</th>-->
                            <!--<th>Hypervisor id</th>-->
                            <!--<th>Created at</th>-->
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($items as $item)
                            <tr>
                                <!--<td scope="row">{{ $loop->iteration }}</td>
                                <td>{{ $item["id"] }}</td>-->
                                <td>
                                    <a href="{{ route("admin.node.edit", $item) }}">{{ $item["name"] }}</a>
                                </td>
                                <!--<td>{{ $item["dcable"]["name"] }}</td>-->
                                {{--<td>{{ $item["dc"]["country"]["name"] }}</td>--}}

                                <td>
                                    @na($item["ip"])
                                    <a class="btn btn-as-link" href="{{ route("admin.node.syncDns", $item) }}" title="Sync dns">
                                        <i class="fa fa-refresh "></i>
                                    </a>
                                </td>
                                <!--<td>
                                    {{ $item["server"] }}
                                </td>
                                <td>{{ $item["hypid"] }}</td>-->
                                <!--<td>{{ $item["created_at"] }}</td>-->
                                <td>
                                    <a class="btn btn-as-link" href="{{ route("admin.node.delete", $item) }}" title="Remove" data-confirm="Are you sure, that you want to remove user {{ $item["name"] }}?">
                                        <i class="fa fa-trash-o "></i>
                                    </a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $items->links() }}
                </div>
            </div>
        </div>
        <!-- /.col-->
    </div>
    <!-- /.row-->
@endsection
