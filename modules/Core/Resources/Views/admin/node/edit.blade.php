<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 30.07.18
 * Time: 14:36
 */
// ToDO; kostyl blyat'!!!
/**
 * @param string $type
 * @return string
 */
function opType(string $type): string
{
    return $type == 'dc' ? "rs" : "dc";
}
?>
@extends('core::layouts.admin')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> <strong> {{ $item->exists ? 'Edit' : 'Create' }} node {{ $item->exists ? $item["name"] : '' }}</strong>
                </div>
                <div class="card-body">
                    {{ Form::model($item, ['route' => ['admin.node.store', $item], "class" => "node-edit-form form-horizontal form-border" ]) }}
                    @include('core::admin.forms.errors')
                    <!--<ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="true">Information</a>
                        </li>
                    </ul>-->
                    <!--<div class="tab-content admin-tab-content">
                        <div class="tab-pane active" id="info" role="tabpanel">
                            {!! Form::admText('name', 'Name', ["placeholder" => "Name", "disabled" => true, "class" => "form-control node_name"]) !!}
                            
                            <div class="form-group">
                                {{ Form::label("assignment_id", "Assignment type", ['class' => 'control-label']) }}
                                <div>
                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                        @foreach($assignments as $value)
                                            <label class="btn btn-info {{ Form::getValueAttribute("assignment_id") == $value["id"] ? "active" : ""  }}">
                                                {!! Form::radio("assignment_id", $value["id"],
                                                Form::getValueAttribute("assignment_id") == $value["id"],
                                                ["class" => "assignment_button", "data-type" => $value["type"], "data-opposite" => opType($value["type"])]) !!} {{ $value["name"] }}
                                            </label>
                                        @endforeach
                                    </div>
                                </div>
                            </div>-->
                            <!--{!! Form::admButtonSelect('rotation_period_id', 'Rotation period', $rotationPeriods, ["class" => "rotation_period_button"]) !!}-->
                            <!--{!! Form::hidden("dcable_type", null, ["class" => "dcable_type"]) !!}-->
                            <!--<div class="form-group">
                                {{ Form::label("dcable_id", "Datacenter", ['class' => 'control-label']) }}
                                <div class="dcable dc">
                                    <div>
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            @foreach($dcs as $value)
                                                <label class="btn btn-info {{ Form::getValueAttribute('dcable_id') == $value["id"] && Form::getValueAttribute('dcable_type') == \Modules\Core\Model\Dc::class ? "active" : ""  }}">
                                                    <input name="dcable_id" type="radio"
                                                           class="dcable_button"
                                                           value="{{ $value["id"] }}"
                                                           @if(Form::getValueAttribute('dcable_id') == $value["id"] && Form::getValueAttribute('dcable_type') == \Modules\Core\Model\Dc::class)
                                                           checked="checked"
                                                           @endif> {{ $value["name"] }}
                                                </label>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="dcable rs">
                                    <div>
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            @foreach($residentials as $value)
                                                <label class="btn btn-info {{ Form::getValueAttribute('dcable_id') == $value["id"] && Form::getValueAttribute('dcable_type') == \Modules\Core\Model\Residential::class ? "active" : ""  }}">
                                                    <input name="dcable_id" type="radio"
                                                           value="{{ $value["id"] }}"
                                                           class="dcable_button"
                                                           @if(Form::getValueAttribute('dcable_id') == $value["id"] && Form::getValueAttribute('dcable_type') == \Modules\Core\Model\Residential::class)
                                                           checked="checked"
                                                           @endif> {{ $value["name"] }}
                                                </label>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>-->
                            
                            <!--{!! Form::admText('server', 'Server', ["placeholder" => "Server"]) !!}
                            {!! Form::admNumber('hypid', 'HYPID', ["placeholder" => "Hypervisor id", "min" => 1]) !!}
                            {!! Form::admBool('is_test', 'Test node') !!}
                        </div>
                    </div>-->

                    <div class="form-group">
                        {!! Form::admTextWithPostfix('slug', 'Hostname', "." . env("APP_DOMAIN"), ["placeholder" => "Hostname", "disabled" => false, "class" => "form-control hostname"]) !!}
                        
                    </div>
                    <div class="form-group">
                        {!! Form::admText('ip', 'Ip address', ["placeholder" => "N/A", "class" => "form-control"]) !!}
                        @if( !($item->exists) )
                            <div class="col-sm-3">
                                <a class="btn lookup-button" data-lookup-url="{{route('admin.node.lookup')}}" id="getIp" href="#">Lookup</a>
                            </div>
                        @endif
                    </div>
                    <div class="form-group text-center">
                        {{ Form::submit($item->exists ? 'Update' : 'Create', ['class' => 'btn btn-primary']) }}
                        @if($item->exists)
                            <a class="btn btn-as-link" href="{{ route("admin.node.syncDns", $item) }}">
                                <i class="fa fa-refresh"></i>
                                Sync Dns
                            </a>
                        @endif
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection
