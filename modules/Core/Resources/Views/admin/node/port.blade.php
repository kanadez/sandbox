<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 07.08.18
 * Time: 16:27
 */
?>
@extends('core::layouts.admin')

@section('content')
    <!-- /.row-->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> <strong>{{ $item["name"] }} port list</strong>
                    <a class="btn btn-success float-right" href="{{ route("admin.node.port.create", $item) }}">
                        <i class="fa fa-plus"></i> <span>Create</span>
                    </a>
                </div>
                <div class="card-body">
                    <table class="table table-responsive-sm table-bordered table-striped table-sm">
                        <thead>
                        <tr>
                            <th>Number</th>
                            <th>Fingerprint</th>
                            <th>Mtu</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($items as $item)
                            <tr>
                                <td>
                                    <a href="{{ route("admin.node.port.edit", [$item["node"], $item]) }}">{{ $item["number"] }}</a>
                                </td>
                                <td>
                                    {{ $item["fingerprint"]["name"] }}
                                </td>
                                <td>
                                    {{ $item["mtu"] }}
                                </td>
                                <td>
                                    <a class="btn btn-secondary" href="{{ route("admin.node.port.finger_print", [ $item["node"], $item]) }}" title="Finger check">
                                        <i class="fa fa-refresh "></i>
                                    </a>
                                    <a class="btn btn-warning" href="{{ route("admin.node.port.finger_print.apply", [ $item["node"], $item]) }}" title="Finger apply">
                                        <i class="fa fa-cloud-upload "></i>
                                    </a>
                                    <a class="btn btn-info" href="{{ route("admin.node.port.edit", [$item["node"], $item]) }}" title="Edit">
                                        <i class="fa fa-edit "></i>
                                    </a>
                                    <a class="btn btn-danger" href="{{ route("admin.node_port.delete", $item) }}" title="Remove" data-confirm="Are you sure, that you want to remove node port {{ $item["number"] }}?">
                                        <i class="fa fa-trash-o "></i>
                                    </a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $items->links() }}
                </div>
            </div>
        </div>
        <!-- /.col-->
    </div>
    <!-- /.row-->
@endsection
