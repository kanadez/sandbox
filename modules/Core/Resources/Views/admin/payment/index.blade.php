<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 26.07.18
 * Time: 15:15
 */
?>
@extends('core::layouts.admin')

@section('content')
    <!-- /.row-->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> <strong>Order payments list</strong>
                    {{--<a class="btn btn-success float-right" href="{{ route("admin.user.create") }}">--}}
                        {{--<i class="fa fa-plus"></i> <span>Create</span>--}}
                    {{--</a>--}}
                </div>
                <div class="card-body">
                    <table class="table table-responsive-sm table-bordered table-striped table-sm">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th>id</th>
                            <th>Order №</th>
                            <th>Sum</th>
                            <th>Paysystem</th>
                            <th>Status</th>
                            <th>Test payment</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($items as $item)
                            <tr>
                                <td scope="row">{{ $loop->iteration }}</td>
                                <td>{{ $item["id"] }}</td>
                                <td>{{ $item["number"] }}</td>
                                <td>{{ $item["price"] }}</td>
                                <td>{{ $item["payment"]["paysystem"]["name"] }}</td>
                                <td>{{ $item["payment"]["status"]["name"] }}</td>
                                <td>@yes_or_no($item["payment"]["is_test"])</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $items->links() }}
                </div>
            </div>
        </div>
        <!-- /.col-->
    </div>
    <!-- /.row-->
@endsection
