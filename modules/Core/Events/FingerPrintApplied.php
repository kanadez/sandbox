<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 30.08.18
 * Time: 12:12
 */

namespace Modules\Core\Events;


use Modules\Core\Model\Node;
use Modules\Core\Model\NodePort;

class FingerPrintApplied extends CommandEvent
{
    /**
     * @var NodePort
     */
    public $nodePort;

    public function __construct(Node $node, NodePort $nodePort)
    {
        parent::__construct($node);
        $this->nodePort = $nodePort;
    }
}