<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 28.08.18
 * Time: 15:51
 */

namespace Modules\Core\Events;

use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;

abstract class AdminNotification implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var string
     */
    public $channel;
    /**
     * @var string
     */
    public $type = 'success';
    /**
     * @var string
     */
    public $title = 'Title';
    /**
     * @var string
     */
    public $command = 'Command';
    /**
     * @var string
     */
    public $log = 'Log';

    public function broadcastOn()
    {
        return new PrivateChannel($this->channel);
    }
}