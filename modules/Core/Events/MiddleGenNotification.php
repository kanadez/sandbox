<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 28.08.18
 * Time: 17:37
 */

namespace Modules\Core\Events;


class MiddleGenNotification extends AdminNotification
{
    public function broadcastAs()
    {
        return 'admin.notification.middle_gen';
    }
}