<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 29.08.18
 * Time: 13:05
 */

namespace Modules\Core\Events;

use Modules\Core\Model\Node;
use Modules\Core\Model\NodePort;

class FingerPrintProposed extends CommandEvent
{
    /**
     * @var NodePort
     */
    public $nodePort;

    public function __construct(Node $node, NodePort $nodePort)
    {
        parent::__construct($node);
        $this->nodePort = $nodePort;
    }
}