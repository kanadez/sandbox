<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 30.08.18
 * Time: 10:50
 */

namespace Modules\Core\Events;


class FingerPrintNotification extends AdminNotification
{
    public function broadcastAs()
    {
        return 'admin.notification.finger_print_info';
    }
}