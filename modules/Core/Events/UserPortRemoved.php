<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 27.08.18
 * Time: 18:00
 */

namespace Modules\Core\Events;

use Modules\Core\Model\Node;
use Modules\Core\Model\NodePort;
use Modules\Core\Model\User;

class UserPortRemoved extends CommandEvent
{
    /**
     * @var User
     */
    public $user;
    /**
     * @var User
     */
    public $admin;

    /**
     * @var NodePort
     */
    public $port;

    public function __construct(Node $node, NodePort $port, User $admin, User $user)
    {
        parent::__construct($node);

        $this->port = $port;
        $this->admin = $admin;
        $this->user = $user;
    }
}