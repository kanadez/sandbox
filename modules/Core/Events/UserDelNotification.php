<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 28.08.18
 * Time: 17:21
 */

namespace Modules\Core\Events;


class UserDelNotification extends AdminNotification
{
    public function broadcastAs()
    {
        return 'admin.notification.user_del';
    }
}