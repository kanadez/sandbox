<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 24.08.18
 * Time: 12:00
 */

namespace Modules\Core\Events;

use Modules\Core\Model\Node;
use Modules\Core\Model\NodePort;

class NodePortSaved extends CommandEvent
{
    /**
     * @var NodePort
     */
    public $nodePort;
    public $admin;

    /**
     * NodePortSaved constructor.
     * @param Node $node
     * @param NodePort $nodePort
     * @param $admin
     */
    public function __construct(Node $node, NodePort $nodePort, $admin)
    {
        parent::__construct($node);
        $this->nodePort = $nodePort;
        $this->admin = $admin;
    }
}