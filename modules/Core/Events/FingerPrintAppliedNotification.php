<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 30.08.18
 * Time: 12:42
 */

namespace Modules\Core\Events;


class FingerPrintAppliedNotification extends AdminNotification
{
    public function broadcastAs()
    {
        return 'admin.notification.finger_print_applied';
    }
}