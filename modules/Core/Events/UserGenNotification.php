<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 28.08.18
 * Time: 15:55
 */

namespace Modules\Core\Events;


class UserGenNotification extends AdminNotification
{
    public function broadcastAs()
    {
        return 'admin.notification.user_gen';
    }
}