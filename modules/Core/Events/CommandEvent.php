<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 24.08.18
 * Time: 10:22
 */

namespace Modules\Core\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Modules\Core\Model\Node;
use Modules\Core\Model\User;

abstract class CommandEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var Node
     */
    public $node;
    /**
     * @var string
     */
    public $log;
    /**
     * @var User
     */
    public $admin;

    /**
     * Create a new event instance.
     *
     * @param Node $node
     */
    public function __construct(Node $node)
    {
        $this->node = $node;
        $this->admin = auth()->user();
    }
}