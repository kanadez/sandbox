<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 24.07.18
 * Time: 8:47
 */

namespace Modules\Core\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PasswordForgotten extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    /**
     * @var string
     */
    private $token;
    /**
     * @var string
     */
    private $email;

    /**
     * Create a new message instance.
     *
     * @param string $token
     * @param string $email
     * @param string $password
     */
    public function __construct(string $token,
                                string $email)
    {
        $this->token = $token;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from(env('APP_SUPPORT_EMAIL', 'support@astroproxy.com'))
            ->to($this->email)
            ->markdown('core::emails.password.forgotten', [
                "token" => $this->token,
                "email" => $this->email
            ]);
    }
}