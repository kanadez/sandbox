<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 24.07.18
 * Time: 8:47
 */

namespace Modules\Core\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProxyRequestCreated extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $password;

    /**
     * Create a new message instance.
     *
     * @param string $name
     * @param string $email
     * @param string $password
     */
    public function __construct(string $name,
                                string $email,
                                string $password)
    {
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from(env('APP_SUPPORT_EMAIL', 'support@astroproxy.com'))
            ->to($this->email)
            ->markdown('core::emails.proxy_request.created', [
                "name" => $this->name,
                "email" => $this->email,
                "password" => $this->password,
            ]);
    }
}