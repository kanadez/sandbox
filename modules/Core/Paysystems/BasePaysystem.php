<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 30.07.18
 * Time: 12:01
 */

namespace Modules\Core\Paysystems;

use GuzzleHttp\Client;

abstract class BasePaysystem
{
    /**
     * @var string
     */
    protected $baseUrl;
    /**
     * @var bool
     */
    protected $debug = false;
    /**
     * @var Client
     */
    protected $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => $this->baseUrl,
        ]);
    }

    /**
     * @param bool $debug
     * @return BasePaysystem
     */
    public function setDebug(bool $debug): BasePaysystem
    {
        $this->debug = $debug;
        return $this;
    }

    public function isTest(): bool
    {
        return $this->debug;
    }
}