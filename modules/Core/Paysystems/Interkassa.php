<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 30.07.18
 * Time: 11:48
 */

namespace Modules\Core\Paysystems;

use GuzzleHttp\Exception\ServerException;
use Illuminate\Support\Facades\Log;
use Modules\Core\Contracts\InterkassaContract;

class Interkassa extends BasePaysystem implements InterkassaContract
{
    public const CREATED_STATUS_ID = 2;
    public const PAID_STATUS_ID = 4;
    public const ERROR_STATUS_ID = 6;
    public const FAIL_STATUS_ID = 8;

    /**
     * @var string
     */
    protected $baseUrl = "https://sci.interkassa.com";
    /**
     * @var string
     */
    private $checkoutId;
    /**
     * @var string
     */
    private $currency;
    /**
     * @var string
     */
    private $key;
    /**
     * @var string
     */
    private $testKey;

    /**
     * Interkassa constructor.
     * @param string $checkoutId
     * @param string $currency
     * @param string $key
     * @param string $testKey
     */
    public function __construct(string $checkoutId,
                                string $currency,
                                string $key,
                                string $testKey)
    {
        parent::__construct();
        $this->checkoutId = $checkoutId;
        $this->currency = $currency;
        $this->key = $key;
        $this->testKey = $testKey;
    }

    public function getPaymentLink(string $sum, string $currency, string $method, string $invId, string  $desc): string
    {
        $data = [
            "ik_am" => $sum,
            "ik_pm_no" => $invId,
            "ik_desc" => $desc,
            "ik_co_id" => $this->checkoutId,
            "ik_cur" => $this->currency,
            "ik_pw_via" => $method,
//            "ik_act" => "payways",
//            "ik_int" => "json",
        ];
        $sign = $this->signature($data);
        $data["ik_sign"] = $sign;

        return $this->baseUrl. '?' . http_build_query($data);
    }

    /**
     * https://www.interkassa.com/documentation-sci/#341proverka-informacii-o-platezhe
     * @param array $data
     * @return string
     */
    private function signature(array $data)
    {
        ksort($data, SORT_STRING);

        array_push($data, $this->key);

        $signString = implode(':', $data);

        return base64_encode(md5($signString, true));
    }

    public function checkSignature(array $data): bool
    {
        if (array_key_exists('ik_sign', $data)) {
            // Get sign from request
            $sign = $data['ik_sign'];
            unset($data['ik_sign']);

            // Set key to check crc
            $signKey = $data["ik_pw_via"] == "test_interkassa_test_xts" ? $this->testKey : $this->key;

            // Compute signature
            ksort($data, SORT_STRING);

            array_push($data, $signKey);

            $signString = implode(':', $data);

            return $sign === base64_encode(md5($signString, true));
        }

        return false;
    }
}