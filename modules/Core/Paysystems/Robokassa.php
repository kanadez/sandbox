<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 25.07.18
 * Time: 16:05
 */

namespace Modules\Core\Paysystems;

use Modules\Core\Contracts\RobokassaContract;
use Modules\Core\Model\Order;
use Modules\Core\Traits\Strings;

class Robokassa implements RobokassaContract
{
    use Strings;
    public const CREATED_STATUS_ID = 1;
    public const PAID_STATUS_ID = 3;
    public const ERROR_STATUS_ID = 5;
    public const FAIL_STATUS_ID = 7;
    /**
     * Url template
     * 1. mrh_login
     * 2. sum
     * 3. invoice_id (unique for shop)
     * 4. inv_desc (description?)
     * 5. crc
     * @var string
     */
    private $base_url = "https://auth.robokassa.ru/Merchant/Index.aspx?".
                        "MrchLogin=%s&OutSum=%s&OutSumCurrency=%s&IncCurrLabel=%s&InvId=%s&Desc=%s&SignatureValue=%s&IsTest=%s";
    /**
     * @var string
     */
    private $mrhId;
    /**
     * @var string
     */
    private $pw1;
    /**
     * @var string
     */
    private $pw2;
    /**
     * @var bool
     */
    private $debug = false;

    public function __construct(string $mrhId, string $pw1, string $pw2)
    {
        $this->mrhId = $mrhId;
        $this->pw1 = $pw1;
        $this->pw2 = $pw2;
    }

    /**
     * @param bool $debug
     * @return Robokassa
     */
    public function setDebug(bool $debug): Robokassa
    {
        $this->debug = $debug;
        return $this;
    }

    public function getPaymentLink(string $sum, string $currency, string $method, string $invId, string  $desc): string
    {
        return sprintf($this->base_url,
            $this->mrhId,
            $this->formatPrice($sum),
            $currency,
            $method,
            $invId,
            $desc,
            $this->crc($sum, $invId, $currency),
            intval($this->debug)
        );
    }



    private function crc(string $sum, string $invId, string $currency): string
    {
        return md5(
            sprintf("%s:%s:%s:%s:%s",
                $this->mrhId, $this->formatPrice($sum), $invId, $currency, $this->pw1
            )
        );
    }

    public function checkSignature(string $invId, string $sum, string $signature): bool
    {
        return $signature === md5(sprintf("%s:%s:%s", $this->formatPrice($sum), $invId, $this->pw1));
    }

    public function isTest(): bool
    {
        return $this->debug;
    }
}