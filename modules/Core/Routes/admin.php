<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 18.07.18
 * Time: 10:25
 */

use Illuminate\Routing\Router;

/** @var  $router Router */
$router->get('', 'DashboardController@index')->name('index');

$router->get('/nodes', 'RoutesController@nodes')->name('nodes');

$router->group([
    'prefix'     => "user",
    'as' => 'user.',
], function (Router $router) {
    $router->get('', 'UsersController@index')
        ->name('index');

    $router->get('create', 'UsersController@create')
        ->name('create');

    $router->get('{userId}', 'UsersController@edit')
        ->name('edit');

    $router->post('{userId?}', 'UsersController@store')
        ->name('store');

    $router->get('delete/{userId}', 'UsersController@delete')
        ->name('delete');

    $router->post('dc_ports/{userId}', 'UsersController@addDcPorts')
        ->name('add_dc_ports');

    $router->get('dc_ports/delete/{userId}/{portId}', 'UsersController@deleteDcPort')
        ->name('delete_dc_ports');

    $router->post('rsd_ports/{userId}', 'UsersController@addRsdPorts')
        ->name('add_rsd_ports');
});


$router->group([
    'prefix'     => "default-proxy-ports-settings",
    'as' => 'default-proxy-ports-settings.',
], function (Router $router) {
    $router->get('', 'DefaultProxyPortSettingsController@index')
        ->name('index');

    $router->post('save', 'DefaultProxyPortSettingsController@save')
        ->name('save');
});

$router->group([
    'prefix'     => "proxy-request",
    'as' => 'proxy-request.',
], function (Router $router) {
    $router->get('export', 'ProxyRequestsController@export')
        ->name('import');

    $router->get('', 'ProxyRequestsController@index')
        ->name('index');

    $router->get('create/{userId}', 'ProxyRequestsController@create')
        ->name('create');

    $router->get('delete/{itemId}', 'ProxyRequestsController@delete')
        ->name('delete');

    $router->get('approve/{itemId}', 'ProxyRequestsController@approve')
        ->name('approve');

    $router->get('unapprove/{itemId}', 'ProxyRequestsController@unapprove')
        ->name('unapprove');
});

$router->group(["prefix" => "proxy-port", "as" => "proxy-port."], function (Router $router) {
    $router->get('{userId}', 'ProxyPortsController@index')
        ->name('index');

    $router->get('make/{userId}', 'ProxyPortsController@make')
        ->name('create');

    $router->post('create', 'ProxyPortsController@store')
        ->name('store');

    $router->get('view/{proxyPortId}', 'ProxyPortsController@details')
        ->name('details');

    $router->get('edit/{proxyPortId}', 'ProxyPortsController@edit')
        ->name('edit');

    $router->post('edit/{proxyPortId?}', 'ProxyPortsController@store')
        ->name('store');

    $router->get('delete/{proxyPortId?}', 'ProxyPortsController@delete')
        ->name('delete');
});

$router->group([
    'prefix'     => "payment",
    'as' => 'payment.',
], function (Router $router) {
    $router->get('', 'PaymentsController@index')
        ->name('index');
});

/*$router->group([
    'prefix'     => "node",
    'as' => 'node.',
], function (Router $router) {
    $router->get('', 'NodesController@index')
        ->name('index');

    $router->get('create', 'NodesController@create')
        ->name('create');

    $router->get('{itemId}', 'NodesController@edit')
        ->name('edit');

    $router->post('{itemId?}', 'NodesController@store')
        ->name('store');

    $router->get('delete/{itemId}', 'NodesController@delete')
        ->name('delete');

    $router->get('{itemId}/proxy', 'NodesController@proxy')
        ->name('proxy');

    $router->get('{itemId}/syncDns', 'NodesController@syncDns')
        ->name('syncDns');

    $router->get('{itemId}/port', 'NodesController@port')
        ->name('port');

    $router->get('{itemId}/port/create', 'NodePortsController@create')
        ->name('port.create');

    $router->get('{nodeId}/port/{itemId}', 'NodesController@portEdit')
        ->name('port.edit');

    $router->post('{nodeId}/port/{itemId?}', 'NodesController@portStore')
        ->name('port.store');

    $router->get('{nodeId}/port/delete/{itemId}', 'NodesController@portDelete')
        ->name('port.delete');

    $router->get('{nodeId}/port/finger_print/{itemId}', 'NodesController@fingerPrint')
        ->name('port.finger_print');
    $router->get('{nodeId}/port/finger_print/{itemId}/apply', 'NodesController@fingerPrintApply')
        ->name('port.finger_print.apply');

    $router->post('lookup/get-ip', 'NodesController@lookup')
        ->name('lookup');
});*/

$router->group([
    'prefix'     => "node_port",
    'as' => 'node_port.',
], function (Router $router) {
    $router->get('', 'NodePortsController@index')
        ->name('index');

    $router->get('create', 'NodePortsController@create')
        ->name('create');

    $router->get('{itemId}', 'NodePortsController@edit')
        ->name('edit');

    $router->post('{itemId?}', 'NodePortsController@store')
        ->name('store');

    $router->get('delete/{itemId}', 'NodePortsController@delete')
        ->name('delete');

    $router->get('{itemId}/proxy', 'NodePortsController@proxy')
        ->name('proxy');

    $router->get('finger_print/{itemId}', 'NodePortsController@fingerPrint')
        ->name('finger_print');
});

$router->group([
    'prefix'     => "country",
    'as' => 'country.',
], function (Router $router) {
    $router->get('', 'CountriesController@index')
        ->name('index');

    $router->get('create', 'CountriesController@create')
        ->name('create');

    $router->get('{userId}', 'CountriesController@edit')
        ->name('edit');

    $router->post('{userId?}', 'CountriesController@store')
        ->name('store');

    $router->get('delete/{userId}', 'CountriesController@delete')
        ->name('delete');
});

$router->group([
    'prefix'     => "dc_provider",
    'as' => 'dc_provider.',
], function (Router $router) {
    $router->get('', 'DcProvidersController@index')
        ->name('index');

    $router->get('create', 'DcProvidersController@create')
        ->name('create');

    $router->get('{itemId}', 'DcProvidersController@edit')
        ->name('edit');

    $router->post('{itemId?}', 'DcProvidersController@store')
        ->name('store');

    $router->get('delete/{itemId}', 'DcProvidersController@delete')
        ->name('delete');
});

$router->group([
    'prefix'     => "dc",
    'as' => 'dc.',
], function (Router $router) {
    $router->get('', 'DcsController@index')
        ->name('index');

    $router->get('create', 'DcsController@create')
        ->name('create');

    $router->get('{itemId}', 'DcsController@edit')
        ->name('edit');

    $router->post('{itemId?}', 'DcsController@store')
        ->name('store');

    $router->get('delete/{itemId}', 'DcsController@delete')
        ->name('delete');

});

$router->group([
    'prefix'     => "dc_pool",
    'as' => 'dc_pool.',
], function (Router $router) {
    $router->post('', 'DcPoolsController@store')
        ->name('store');
    $router->get('delete/{itemId}', 'DcPoolsController@delete')
        ->name('delete');
});

$router->group([
    'prefix'     => "residential",
    'as' => 'residential.',
], function (Router $router) {

    $router->get('', 'ResidentialsController@index')
        ->name('index');

    $router->get('create', 'ResidentialsController@create')
        ->name('create');

    $router->get('{itemId}', 'ResidentialsController@edit')
        ->name('edit');

    $router->post('{itemId?}', 'ResidentialsController@store')
        ->name('store');

    $router->get('delete/{itemId}', 'ResidentialsController@delete')
        ->name('delete');
});

$router->group([
    'prefix'     => "residential_parent",
    'as' => 'residential_parent.',
], function (Router $router) {
    $router->post('', 'ResidentialParentsController@store')
        ->name('store');
    $router->get('delete/{itemId}', 'ResidentialParentsController@delete')
        ->name('delete');
});

$router->group([
    'prefix'     => "remote_command",
    'as' => 'remote_command.',
], function (Router $router) {

    $router->get('', 'RemoteCommandsController@index')
        ->name('index');

    $router->get('create', 'RemoteCommandsController@create')
        ->name('create');

    $router->get('{itemId}', 'RemoteCommandsController@edit')
        ->name('edit');

    $router->post('{itemId?}', 'RemoteCommandsController@store')
        ->name('store');

    $router->get('delete/{itemId}', 'RemoteCommandsController@delete')
        ->name('delete');
});


$router->group([
    'prefix'     => "currency",
    'as' => 'currency.',
], function (Router $router) {
    $router->get('', 'CurrenciesController@index')
        ->name('index');

    $router->get('create', 'CurrenciesController@create')
        ->name('create');

    $router->get('{userId}', 'CurrenciesController@edit')
        ->name('edit');

    $router->post('{userId?}', 'CurrenciesController@store')
        ->name('store');

    $router->get('delete/{userId}', 'CurrenciesController@delete')
        ->name('delete');
});

$router->group([
    'prefix'     => "product",
    'as' => 'product.',
], function (Router $router) {
    $router->get('', 'ProductsController@index')
        ->name('index');

    $router->get('create', 'ProductsController@create')
        ->name('create');

    $router->get('{itemId}', 'ProductsController@edit')
        ->name('edit');

    $router->post('{itemId?}', 'ProductsController@store')
        ->name('store');

    $router->get('delete/{itemId}', 'ProductsController@delete')
        ->name('delete');
});

$router->group([
    'prefix'     => "order",
    'as' => 'order.',
], function (Router $router) {
    $router->get('', 'OrdersController@index')
        ->name('index');

    $router->get('create', 'OrdersController@create')
        ->name('create');

    $router->get('{itemId}', 'OrdersController@edit')
        ->name('edit');

    $router->post('{itemId?}', 'OrdersController@store')
        ->name('store');

    $router->get('delete/{itemId}', 'OrdersController@delete')
        ->name('delete');
});
