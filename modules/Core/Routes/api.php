<?php

use Illuminate\Routing\Router;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your module. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/** @var \Illuminate\Routing\Router $router */

Route::resource('nodes', 'NodesController');
Route::resource('nodes/{id}/ports', 'Nodes\PortsController');

$router->group([
    'prefix'     => "payment",
    'namespace' => "Payment",
    'as' => 'payment.',
], function (Router $router) {
    $router->group([
        'prefix'     => "robokassa",
        'as' => 'robokassa.',
    ], function (Router $router) {
        $router->get('success', 'RobokassaController@success');
    });

    $router->group([
        'prefix'     => "interkassa",
        'as' => 'interkassa.',
    ], function (Router $router) {
        $router->post('success', 'InterkassaController@success');
    });
});
