<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 25.07.18
 * Time: 16:22
 */

namespace Modules\Core\tests\Unit;


use Modules\Core\Paysystems\Robokassa;
use Modules\Core\tests\TestCase;

class RobokassaTest extends TestCase
{
    public function testBlah()
    {
        $kassa = (new Robokassa(env("ROBOKASSA_MERCHANT"), env("ROBOKASSA_PW_1"), env("ROBOKASSA_PW_2")))
            ->setDebug(true);
        dump($kassa->getPaymentLink(10, 1));
    }
}