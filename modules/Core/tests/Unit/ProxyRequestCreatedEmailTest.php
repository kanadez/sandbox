<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 24.07.18
 * Time: 16:23
 */

namespace Modules\Core\tests\Unit;

use Exception;
use Illuminate\Support\Facades\Mail;
use Modules\Core\Mail\ProxyRequestCreated;
use Modules\Core\tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProxyRequestCreatedEmailTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        try {
            $job_id = Mail::queue(
                (new ProxyRequestCreated("Test email", "a6y@xakep.ru", "TestPw"))
                    ->onQueue(config("mail.queue", "emails"))
            );
            $this->assertNotNull($job_id);
        } catch (Exception $exception) {
            $this->fail();
        }
    }
}