<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 30.07.18
 * Time: 15:15
 */

namespace Modules\Core\Model;


class Proxy extends BaseModel
{
    public function node()
    {
        return $this->belongsTo(Node::class);
    }
}