<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 02.08.18
 * Time: 16:55
 */

namespace Modules\Core\Model;

class Country extends BaseModel
{
    protected $fillable = [
        "name",
    ];
}
