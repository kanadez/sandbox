<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 07.08.18
 * Time: 14:31
 */

namespace Modules\Core\Model;

class NodePort extends BaseModel
{
    protected $fillable = [
        "number",
        "node_id",
        "provider_point",
        "finger_print_id",
        "interval",
        "mtu",
        "ip_geo",
        "link",
        "os_fingerprint",
        "comment",
    ];

    public function node()
    {
        return $this->belongsTo(Node::class);
    }

    public function getNextNumberAttribute()
    {
        return $this->max("number") + 1;
    }

    public function fingerPrint()
    {
        return $this->belongsTo(FingerPrintType::class, "finger_print_id");
    }
}