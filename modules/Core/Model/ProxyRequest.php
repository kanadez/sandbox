<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 24.07.18
 * Time: 8:42
 */

namespace Modules\Core\Model;

class ProxyRequest extends BaseModel
{
    protected $fillable = [
        "user_id",
        "is_processed",
        "telegram",
        "skype",
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}