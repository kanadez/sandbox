<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 02.08.18
 * Time: 14:11
 */

namespace Modules\Core\Model;

class NodeProvider extends BaseModel
{
    protected $fillable = [
        "name",
    ];
}