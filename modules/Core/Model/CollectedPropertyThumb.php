<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 26.07.18
 * Time: 10:54
 */

namespace Modules\Core\Model;

class CollectedPropertyThumb extends BaseModel
{
    
    protected $table = "collected_properties_thumbs";

    protected $fillable = [
        "id",
        "type",
        "title",
        "link",
        "price_value",
        "price_comission",
        "desc_metro",
        "desc_address",
        "date",
    ];
    
    public function image()
    {
        return $this->hasOne(CollectedPropertyThumbImage::class, "thumb_id", "id");
    }
    
}