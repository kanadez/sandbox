<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 02.08.18
 * Time: 16:17
 */

namespace Modules\Core\Model;

use Exception;
use Illuminate\Support\Collection;
use IPTools\IP;
use IPTools\Network;

class Dc extends BaseModel
{
    protected $fillable = [
        "name",
        "country_id",
    ];

    public function provider()
    {
        return$this->belongsTo(DcProvider::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function pools()
    {
        return $this->hasMany(DcPool::class);
    }

    public function points(): Collection
    {
        $return = collect();
        $nets = $this->pools->map(function (DcPool $pool) {
            try  {
                return (new Network(new IP($pool["ip"]), new IP($pool["mask"])))->getHosts();
            } catch (Exception $exception) {
                return collect();
            }
        });
        foreach($nets as $hosts) {
            foreach($hosts as $ip) {
                $return->put((string)$ip, (string)$ip);
            }
        }
        return $return;
    }
}