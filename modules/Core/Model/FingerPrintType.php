<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 30.08.18
 * Time: 11:18
 */

namespace Modules\Core\Model;


class FingerPrintType extends BaseModel
{
    protected $fillable = [
        "name",
        "description",
    ];
}