<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 22.08.18
 * Time: 13:11
 */

namespace Modules\Core\Model;


class DcPool extends BaseModel
{
    protected $fillable = [
        "dc_id",
        "ip",
        "mask",
        "gw",
        "dns",
    ];
    public function dc()
    {
        return $this->belongsTo(Dc::class);
    }
}