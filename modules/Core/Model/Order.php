<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 26.07.18
 * Time: 9:38
 */

namespace Modules\Core\Model;

class Order extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = [
        "number",
        "user_id",
        "currency_id",
        "price",
    ];


    public function payment()
    {
        return $this->hasOne(Payment::class);
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = intval($value * $this->currency["multiplier"]);
    }

    public function getPriceAttribute($value)
    {
        return $value / $this->currency["multiplier"];
    }

    public function items()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function getStatusId():? int
    {
        return !empty($payment = $this->payment) ? (
            !empty($status = $payment->status) ? ($status["id"]) : null
        ) : null;
    }
}