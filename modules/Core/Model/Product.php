<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 10.09.18
 * Time: 11:04
 */

namespace Modules\Core\Model;


class Product extends BaseModel
{
    public const PRICE_TYPE = 1;
    protected $fillable = [
        "name",
        "description",
    ];

    public function price()
    {
        return $this->hasOne(Price::class);
    }

    public function getPrice()
    {
        return $this->price["value"];
    }

    public function getCurrencyId()
    {
        return !empty($price = $this->price) ? (
            !empty($currency = $price->currency) ? (
                $currency["id"]
        ) : null
        ) : null;
    }
}