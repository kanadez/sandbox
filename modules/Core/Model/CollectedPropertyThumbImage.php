<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 26.07.18
 * Time: 10:54
 */

namespace Modules\Core\Model;

class CollectedPropertyThumbImage extends BaseModel
{
    
    protected $table = "collected_properties_thumbs_images";

    protected $fillable = [
        "id",
        "thumb_id",
        "src",
        "added",
    ];
    
}