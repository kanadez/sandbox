<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 05.09.18
 * Time: 11:35
 */

namespace Modules\Core\Model;

class Currency extends BaseModel
{
    protected $fillable = [
        "multiplier",
        "name",
        "short_name",
        "rk_name",
        "ik_name",
    ];

    public function prices()
    {
        return $this->hasMany(Price::class);
    }
}