<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 02.08.18
 * Time: 13:19
 */

namespace Modules\Core\Model;

class NodeAssignment extends BaseModel
{
    protected $fillable = [
        "name",
    ];
}