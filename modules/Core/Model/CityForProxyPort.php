<?php

namespace Modules\Core\Model;

class CityForProxyPort extends BaseModel
{
	protected $table = 'all_cities';


	public function country()
	{
		return $this->belongsTo('Modules\Core\Model\CountryForProxyPort');
	}
}