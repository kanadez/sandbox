<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 02.08.18
 * Time: 16:55
 */

namespace Modules\Core\Model\Network;

use Modules\Core\Model\BaseModel;

class Type extends BaseModel
{
    protected $table = 'network_types';
}
