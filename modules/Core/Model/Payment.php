<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 26.07.18
 * Time: 10:54
 */

namespace Modules\Core\Model;

use Modules\Core\Paysystems\Interkassa;
use Modules\Core\Paysystems\Robokassa;

class Payment extends BaseModel
{
    protected $fillable = [
        "order_id",
        "paysystem_id",
        "paysystem_method_id",
        "paysystem_status_id",
        "is_test",
        "link",
        "comment",
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function paysystem()
    {
        return $this->belongsTo(Paysystem::class);
    }

    public function status()
    {
        return $this->belongsTo(PaysystemStatus::class, "paysystem_status_id");
    }

    public function setPaid()
    {
        switch ($this["paysystem_id"]) {
            case Paysystem::ROBOKASSA_ID:
                $status = Robokassa::PAID_STATUS_ID;
                break;
            case Paysystem::INTERKASSA_ID:
                $status = Interkassa::PAID_STATUS_ID;
                break;
            default:
                $status = null;
                break;
        }
        $this->update([
            "paysystem_status_id" => $status,
        ]);
    }
}