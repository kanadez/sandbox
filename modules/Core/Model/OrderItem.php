<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 11.09.18
 * Time: 17:24
 */

namespace Modules\Core\Model;


class OrderItem extends BaseModel
{
    protected $fillable = [
        "order_id",
        "product_id",
        "currency_id",
        "quantity",
        "price",
    ];

    public function product()
    {
        return$this->belongsTo(Product::class);
    }

    public function order()
    {
        return$this->belongsTo(Order::class);
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = intval($value * $this->currency["multiplier"]);
    }

    public function getPriceAttribute($value)
    {
        return $value / $this->currency["multiplier"];
    }
}