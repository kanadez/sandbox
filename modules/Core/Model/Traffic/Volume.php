<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 02.08.18
 * Time: 16:55
 */

namespace Modules\Core\Model\Traffic;

use Modules\Core\Model\BaseModel;

class Volume extends BaseModel
{
    protected $table = 'traffic_volumes';
}
