<?php

namespace Modules\Core\Model;

class CountryForProxyPort extends BaseModel
{
	protected $table = 'all_countries';

	public function cities()
	{
		return $this->hasMany('Modules\Core\Model\CityForProxyPort', 'country_id');
	}

}