<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 26.07.18
 * Time: 9:57
 */

namespace Modules\Core\Model;

class PaysystemStatus extends BaseModel
{
    protected $fillable = [
        "paysystem_id",
        "name",
    ];

    public function payments()
    {
        return $this->belongsTo(Payment::class);
    }
}