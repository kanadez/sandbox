<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 26.07.18
 * Time: 10:54
 */

namespace Modules\Core\Model;

class PropertyReview extends BaseModel
{
    
    protected $table = "properties_reviews";

    protected $fillable = [
        "id",
        "user_id",
        "property_id",
        "review_text",
        "created_at",
        "updated_at",
    ];
    
}