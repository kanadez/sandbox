<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 25.07.18
 * Time: 17:05
 */

namespace Modules\Core\Model;

use Modules\Core\Contracts\InterkassaContract;
use Modules\Core\Contracts\PaymentServiceContract;
use Modules\Core\Contracts\PaysystemContract;
use Modules\Core\Contracts\RobokassaContract;
use Modules\Core\Paysystems\Interkassa;
use Modules\Core\Paysystems\Robokassa;
use Modules\Core\Services\Payments\InterkassaService;
use Modules\Core\Services\Payments\RobokassaService;

class Paysystem extends BaseModel implements PaysystemContract
{
    protected $fillable = [
        "name",
    ];

    public function methods()
    {
        return $this->hasMany(PaysystemMethod::class);
    }

    // ToDO: remove
    public function getService(): PaymentServiceContract
    {
        switch ($this["id"]) {
            case Paysystem::ROBOKASSA_ID:
                return app(RobokassaService::class);
            case Paysystem::INTERKASSA_ID:
                return app(InterkassaService::class);
            default:
                return app(RobokassaService::class);
        }
    }

    public function getDriver()
    {
        switch ($this["id"]) {
            case Paysystem::ROBOKASSA_ID:
                return app(RobokassaContract::class);
            case Paysystem::INTERKASSA_ID:
                return app(InterkassaContract::class);
            default:
                return app(RobokassaContract::class);
        }
    }

    public function getCurrencyName(Currency $currency): string
    {
        switch ($this["id"]) {
            case Paysystem::ROBOKASSA_ID:
                return $currency["rk_name"];
            case Paysystem::INTERKASSA_ID:
                return $currency["ik_name"];
            default:
                return $currency["rk_name"];
        }
    }
}