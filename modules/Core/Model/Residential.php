<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 22.08.18
 * Time: 15:44
 */

namespace Modules\Core\Model;


use Illuminate\Support\Collection;

class Residential extends BaseModel
{
    protected $fillable = [
        "name",
    ];

    public function parents()
    {
        return $this->hasMany(ResidentialParent::class, 'residential_id', "id");
    }

    public function points(): Collection
    {
        return $this->parents
            ->map(function (ResidentialParent $residentialParent) {
                return $residentialParent["ip"] . ":" . $residentialParent["port"];
            })
            ->keyBy(function ($item) {
                return $item;
            });
    }
}