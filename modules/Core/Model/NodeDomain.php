<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 09.08.18
 * Time: 10:33
 */

namespace Modules\Core\Model;

class NodeDomain extends BaseModel
{
    protected $fillable = [
        "name",
    ];
}