<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 10.09.18
 * Time: 11:08
 */

namespace Modules\Core\Model;

class Price extends BaseModel
{
    protected $fillable = [
        "product_id",
        "price_type_id",
        "currency_id",
        "value",
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function setValueAttribute($value)
    {
        $this->attributes['value'] = intval($value * $this->currency["multiplier"]);
    }

    public function getValueAttribute($value)
    {
        return $value / $this->currency["multiplier"];
    }
}