<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 30.07.18
 * Time: 10:06
 */

namespace Modules\Core\Model;

use Modules\Core\Contracts\RemoteHostContract;
use Modules\Core\Criteria\NodeNumberCriteria;
use Modules\Core\Repository\NodesRepository;
use GuzzleHttp\Client;

class Node extends BaseModel implements RemoteHostContract
{
    public const NAME_REXEP = '/^([\w]+)\-([\w]+)\-([\w]+)\-([\d]+)\.(.+)$/';

    protected $casts = [
		    'created_at' => 'string',
		];

    protected $fillable = [
        "name",
        "slug",
        "server",
        "ip",
        "hypid",
        "domain",
        "assignment_id",
        "rotation_period_id",
        "dcable_id",
        "dcable_type",
        "number",
        "is_test",
    ];

//    /**
//     * @return string|null
//     */
//    public function getAssignmentNameAttribute()
//    {
//        return $this->splitName($this["name"],1);
//    }
//
//    /**
//     * @return string|null
//     */
//    public function getRotationPeriodNameAttribute()
//    {
//        return $this->splitName($this["name"], 2);
//    }
//
//    /**
//     * @return string|null
//     */
//    public function getProviderNameAttribute()
//    {
//        return $this->splitName($this["name"], 3);
//    }
//
//    /**
//     * @return string|null
//     */
//    public function getNumberNameAttribute()
//    {
//        return $this->splitName($this["name"], 4);
//    }
//
//    /**
//     * @return string|null
//     */
//    public function getDomainNameAttribute()
//    {
//        return $this->splitName($this["name"], 5);
//    }

    protected static function boot()
    {
        parent::boot();
        /** @var NodesRepository $repository */
        $repository = app(NodesRepository::class);
        static::creating(static::getNodeName($repository));
        static::saving(static::getNodeName($repository));
    }

    private static function getNodeName(NodesRepository $repository)
    {
        return function (Node $model) use ($repository) {
            $number = static::getNumber($model, $repository);
            $model["number"] = $number;
            if(is_null( $model->slug )) {
                $model["name"] = $model->assignment["name"] . "-" .
                    $model->rotationPeriod["name"] . "-" .
                    $model->dcable["name"] . "-" .
                    $number . "." .
                    env("APP_DOMAIN");
            } else {
                //$model["name"] = $model->slug . "-" .
                    $number . "." .
                    env("APP_DOMAIN");
                $model["name"] = $model->slug . "." .
                    env("APP_DOMAIN");
            }

        };
    }

    /**
     * @param Node $model
     * @param NodesRepository $repository
     * @return int
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private static function getNumber(Node $model, NodesRepository $repository)
    {
        if ($model->exists &&
            $model->getOriginal("assignment_id") == $model["assignment_id"] &&
            $model->getOriginal("rotation_period_id") == $model["rotation_period_id"] &&
            $model->getOriginal("dcable_id") == $model["dcable_id"] &&
            $model->getOriginal("dcable_type") == $model["dcable_type"]) {
            return $model["number"];
        }
        return $repository
                ->resetCriteria()
                ->pushCriteria(new NodeNumberCriteria($model))
                ->get()
                ->max("number") + 1;
    }
    /**
     * @param string $name
     * @param int $part
     * @return string|null
     */
    public static function splitName(string $name, int $part)
    {
        if (preg_match(self::NAME_REXEP, $name, $matches)) {
            return $matches[$part];
        }
        return null;
    }

    public function proxies()
    {
        return $this->hasMany(Proxy::class);
    }

    public function ports()
    {
        return $this->hasMany(NodePort::class);
    }


    public function proxyPorts()
    {
        return $this->hasMany('Modules\Core\Model\ProxyPort');
    }

    public function assignment()
    {
        return $this->belongsTo(NodeAssignment::class);
    }

//    public function fingerPrint()
//    {
//        return $this->belongsTo(FingerPrintType::class, "finger_print_id");
//    }

    public function rotationPeriod()
    {
        return $this->belongsTo(NodeRotationPeriod::class);
    }

    public function provider()
    {
        return $this->belongsTo(NodeProvider::class);
    }

    public function dcable()
    {
        return $this->morphTo();
    }

    public function getConfig(): array
    {
        return [
            'host'      => sprintf("%s:%s", $this->getHost(), $this->getPort()),
            'username'  => $this->getUser(),
            'password'  => '',
            'key'       => storage_path('app/remote.key'),
            'keytext'   => '',
            'keyphrase' => '',
            'agent'     => '',
            'timeout'   => 10,
        ];
    }

    public function getHost(): string
    {
        return $this["name"];
    }

    public function getUser(): string
    {
        return "root";
    }

    public function getPort(): int
    {
        return 25240;
    }

    public function createPort($port) {
      $client = new Client();

      $rotation = $port->rotation > 0 ? $port->rotation . ($port->rotation_time_type == 1 ? 'm' : 'h') : 0;
      $response = json_decode((string)$client->post($this->name . ':8103/api/cmd/proxyGenerate', [
          \GuzzleHttp\RequestOptions::JSON => [
              "Email" => $port->user->email,
              "Config" => [
                  "PeerRotate" => $rotation,
                  "PeerInfo" => [
                      "Country" => $port->country->name,
                      "City" => $port->city->name,
                      "Fingerprint" => "",
                      "Type" => $port->networkType->slug,
                      "Asn" => "",
                      "Uptime" => $port->uptime,
                      "Latency" => $port->latency,
                      "SpeedDownload" => "100KB",
                      "SpeedUpload" => "100KB"
                  ],
                  //"HTTP": 8888,
                  //"Socks": 9999,
                  "AllowedIP" => [],
                  "Auth" => [
                      "Username" => $port->username,
                      "Password" => $port->password,
                  ],
                  "MaxConnections" => $port->max_connections,
                  "TrafficLimit" => "100GB",
                  "TimeLimit" => "12h"
                ]
            ]
        ])->getBody()->getContents(), true);

        if(!isset($response['Result']) || $response['Result'] != 'OK') {
            return  false;
        }

        ProxyPort::where('id', $port->id)->update([
            'http' => $response['Data']['Ports']['HTTP'],
            'socks' => $response['Data']['Ports']['Socks'],
        ]);

        return true;
    }
}
