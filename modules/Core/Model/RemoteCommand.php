<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 23.08.18
 * Time: 11:05
 */

namespace Modules\Core\Model;


class RemoteCommand extends BaseModel
{
    protected $fillable = [
        "name",
        "command",
    ];
}