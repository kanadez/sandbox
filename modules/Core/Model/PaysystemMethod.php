<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 13.09.18
 * Time: 12:23
 */

namespace Modules\Core\Model;


use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class PaysystemMethod extends BaseModel implements HasMedia
{
    use HasMediaTrait;

    protected $fillable = [
        "paysystem_id",
        "name",
    ];

    public function paysystem()
    {
        return $this->belongsTo(Paysystem::class);
    }
}