<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 22.08.18
 * Time: 15:44
 */

namespace Modules\Core\Model;

class ResidentialParent extends BaseModel
{
    protected $fillable = [
        "residential_id",
        "ip",
        "port",
    ];

    public function residential()
    {
        return $this->belongsTo(Residential::class, 'residential_id');
    }
}