<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 10.09.18
 * Time: 10:43
 */

namespace Modules\Core\Model;


class PriceType extends BaseModel
{
    protected $fillable = [
        "name",
    ];
}