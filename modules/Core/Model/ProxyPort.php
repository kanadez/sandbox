<?php

namespace Modules\Core\Model;

use Illuminate\Support\Facades\Auth;
use Modules\Core\Model\Network\Type as NetworkType;
use Modules\Core\Model\Ip\Type as IpType;
use Modules\Core\Model\Traffic\Volume as TrafficVolume;;

class ProxyPort extends BaseModel
{
		protected $table = 'proxy_ports';

		protected $casts = [
		    'created_at' => 'string',
		];

    protected $fillable = [
        "name",
        "user_id",
        "type",
        "ip_type",
        "time_ip_change",
        "pull_size",
        "trafic",
        "country_id",
        "city_id",
        "node_id",
        "http",
        "socks",
        "is_exported_by_api",
        "username",
        "password",
        "peer_rotate",
        "fingerprint",
        "asn",
        "uptime",
        "latency",
        "speed_download",
        "speed_upload",
        "allowed_ip",
        "max_connections",
        "bandwidth_in",
        "bandwidth_out",
        "time_limit",
    ];


    public static function boot() {
	    parent::boot();

	    static::creating(function($model){
	        foreach ($model->attributes as $key => $value) {
	        	if( $key != 'uptime' ) {
	        		$model->{$key} = empty($value) ? null : $value;
	        	}

	        }
	    });

	    static::updating(function($model){
	        foreach ($model->attributes as $key => $value) {
	            if( $key != 'uptime' ) {
	        		$model->{$key} = empty($value) ? null : $value;
	        	}
	        }
	    });

	}


	public function user()
	{
		return $this->belongsTo('Modules\Core\Model\User');
	}


	public function country()
	{
		return $this->belongsTo('Modules\Core\Model\CountryForProxyPort');
	}

	public function city()
	{
		return $this->belongsTo('Modules\Core\Model\CityForProxyPort');
	}

	public function node()
	{
		return $this->belongsTo('Modules\Core\Model\Node');
	}

	public function networkType() {
			return $this->belongsTo(NetworkType::class);
	}

	public function ipType() {
			return $this->belongsTo(IpType::class);
	}

	public function trafficVolume() {
			return $this->belongsTo(TrafficVolume::class);
	}

}
