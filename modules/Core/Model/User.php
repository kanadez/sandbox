<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 18.07.18
 * Time: 10:02
 */

namespace Modules\Core\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as BaseUser;
use Laratrust\Traits\LaratrustUserTrait;
use Modules\Core\Traits\UserRolesTrait;
use Illuminate\Support\Facades\Mail;
use Modules\Core\Mail\PasswordForgotten;
use Illuminate\Support\Facades\Log;

class User extends BaseUser
{
    use Notifiable, LaratrustUserTrait, UserRolesTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'dc_bandwidth_in',
        'dc_bandwidth_out',
        'dc_max_connections',
        'dc_user',
        'dc_pass',
        'rsd_bandwidth_in',
        'rsd_bandwidth_out',
        'rsd_max_connections',
        'rsd_user',
        'rsd_pass',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function ports()
    {
        return $this->belongsToMany(NodePort::class, "port_user")
            ->with(["node"]);
    }


    public function proxyPorts()
    {
        return $this->hasMany(ProxyPort::class);
    }

    public function dcPorts()
    {
        return $this->ports()
            ->whereHas('node', function ($query) {
                $query->where("dcable_type", Dc::class);
            });
    }

    public function rsdPorts()
    {
        return $this->ports()
            ->whereHas('node', function ($query) {
                $query->where("dcable_type", Residential::class);
            });
    }

    public function testPorts()
    {
        return $this->belongsToMany(NodePort::class, "port_user")
            ->with(["node"])
            ->whereHas('node', function ($query) {
                $query->where("is_test", true);
            });
    }

    public function orders() {
        return $this->hasMany(Order::class);
    }

    /**
    * Send the password reset notification.
    *
    * @param  string  $token
    * @return void
    */
    public function sendPasswordResetNotification($token)
    {
        $message = (new PasswordForgotten($this->email, $token))
            ->onQueue(config("mail.queue", "emails"));

        Mail::queue($message);
        Log::alert($token);
        return response()->json([
            "status" => "success",
        ]);
    }
}
