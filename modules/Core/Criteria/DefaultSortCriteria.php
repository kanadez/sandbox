<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 08.08.18
 * Time: 12:30
 */

namespace Modules\Core\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class DefaultSortCriteria implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->orderBy(
            "created_at","desc"
        )
        ->orderBy(
            "id","desc"
        );
    }

}