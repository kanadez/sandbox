<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 19.07.18
 * Time: 11:20
 */

namespace Modules\Core\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class FindRealtyCriteria implements CriteriaInterface
{
    
    private $adress_to_find;
    
    public function __construct($address) 
    {
        $this->adress_to_find = $address;
    }


    public function apply($model, RepositoryInterface $repository)
    {
        $parsed_address = $this->parseAddress();
        $sql = "";
        $counter = 1;
        
        foreach ($parsed_address as $adress_variant){
            $sql .= "desc_address like '$adress_variant%'" . (count($parsed_address) != $counter ? " or " : "");
            $counter++;
        }
        
        return $model
                ->whereRaw($sql)
                ->take(20);
    }
    
    private function parseAddress()
    {
        $route              = $this->adress_to_find["route"];
        $street_number      = $this->adress_to_find["street_number"] ?? "";
        $route_cleared      = $this->clearRoute($route);
        $route_exploded     = explode(" ", $route_cleared);
        $query_variants     = [];
        $route_exploding_buffer = "";
        
        for ($counter = 0; $counter < count($route_exploded); $counter++){
            $route_part                      = $route_exploded[$counter];
            $route_exploding_buffer          .= $route_part . " ";
            $route_exploding_buffer_trimmed  = trim($route_exploding_buffer);
            
            if (strlen($route_exploding_buffer_trimmed) > 0){
                $query = "$route_exploding_buffer_trimmed%$street_number";
                array_push($query_variants, $query);
            }
        }
        
        return array_reverse($query_variants);
    }
    
    private function clearRoute($route)
    {
        // возможно придется не удалять а заменять на авитовские обозначения
        $words_to_remove = [
            "улица",
            "проспект",
            "шоссе",
            "набережная реки",
            "набережная",
            "переулок",
            "проезд",
            "бульвар",
            "озеро",
            "река",
            "площадь",
        ];
        $cleared_route = $route;
        
        foreach ($words_to_remove as $word){
            $cleared_route = str_replace($word, "", $cleared_route);
        }
        
        return $cleared_route;
    }
}