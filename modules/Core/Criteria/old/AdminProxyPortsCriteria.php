<?php


namespace Modules\Core\Criteria;

use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class AdminProxyPortsCriteria implements CriteriaInterface
{

	/**
     * @var int
     */
    private $userId;

    /**
     * NodePortByNode constructor.
     * @param int $itemId
     */
    public function __construct(int $userId)
    {
        $this->userId = $userId;
    }

    
    public function apply($model, RepositoryInterface $repository)
    {
    	
        return $model->where('user_id', $this->userId);
    }
}