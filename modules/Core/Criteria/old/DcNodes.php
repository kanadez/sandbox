<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 27.08.18
 * Time: 10:43
 */

namespace Modules\Core\Criteria;

use Modules\Core\Model\Dc;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class DcNodes implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('dcable_type', Dc::class);
    }

}