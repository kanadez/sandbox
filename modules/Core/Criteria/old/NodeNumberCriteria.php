<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 02.10.18
 * Time: 10:23
 */

namespace Modules\Core\Criteria;


use Illuminate\Support\Facades\DB;
use Modules\Core\Model\Node;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class NodeNumberCriteria implements CriteriaInterface
{
    /**
     * @var Node
     */
    private $node;

    public function __construct(Node $node)
    {
        $this->node = $node;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        /** @var Node $model*/
        if( !is_null($this->node["slug"]) && $this->node["slug"] != '' ) {
            $result = $model
                ->select(DB::raw('MAX(number) as number'))
                ->where("slug", $this->node["slug"]);
        } else {
            $result = $model
                ->select(DB::raw('MAX(number) as number'))
                ->where("assignment_id", $this->node["assignment_id"])
                ->where("rotation_period_id", $this->node["rotation_period_id"])
                ->where("dcable_id", $this->node["dcable_id"])
                ->where("dcable_type", $this->node["dcable_type"]);
        }
        return $result;
    }

}