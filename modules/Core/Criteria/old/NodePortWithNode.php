<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 07.08.18
 * Time: 16:30
 */

namespace Modules\Core\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class NodePortWithNode implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->with([
            "node",
            "node.assignment",
            "node.rotationPeriod",
            "node.provider",
            "node.dcable",
//            "node.dc.country",
        ]);
    }
}