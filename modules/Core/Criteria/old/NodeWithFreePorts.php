<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 27.08.18
 * Time: 10:52
 */

namespace Modules\Core\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class NodeWithFreePorts implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->with(["ports" => function($query) {
            $query
                ->join('port_user', 'node_ports.id', '=', 'port_user.node_port_id', 'left outer')
                ->whereNull('port_user.node_port_id');
        }]);
    }
}