<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 08.08.18
 * Time: 11:19
 */

namespace Modules\Core\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class NodeWithAll implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->with([
            "assignment",
            "rotationPeriod",
            "provider",
            "dcable",
//            "dc.country",
            "ports",
        ]);
    }
}