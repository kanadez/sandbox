<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 26.07.18
 * Time: 10:23
 */

namespace Modules\Core\Providers;

use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;

class UtilsServiceProvider extends ServiceProvider
{
    public function register()
    {
        if($this->app->environment('production')) {
            URL::forceScheme('https');
        }
    }
}