<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 21.08.18
 * Time: 13:00
 */

namespace Modules\Core\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Contracts\RemoteManagerContract;
use Modules\Core\Services\RemoteManager;

class RemoteServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(RemoteManagerContract::class, function ($app) {
            return new RemoteManager($app);
        });
    }
}