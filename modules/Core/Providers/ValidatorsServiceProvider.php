<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 19.07.18
 * Time: 10:40
 */

namespace Modules\Core\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class ValidatorsServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Validator::extend(
            'recaptcha',
            'Modules\Core\Validators\ReCaptcha@validate'
        );
    }
}