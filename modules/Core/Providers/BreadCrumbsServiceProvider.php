<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 19.07.18
 * Time: 14:38
 */

namespace Modules\Core\Providers;

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Support\ServiceProvider;

class BreadCrumbsServiceProvider extends ServiceProvider
{
    public function register()
    {
        Breadcrumbs::for('admin.index', function ($trail) {
            $trail->push('Admin', route('admin.index'));
        });

        // User
        Breadcrumbs::for('admin.user.index', function ($trail) {
            $trail->parent('admin.index');
            $trail->push('User', route('admin.user.index'));
        });

        Breadcrumbs::for('admin.user.edit', function ($trail, $user) {
            $trail->parent('admin.user.index');
            $trail->push("Edit", route('admin.user.edit', $user));
        });

        Breadcrumbs::for('admin.user.create', function ($trail) {
            $trail->parent('admin.user.index');
            $trail->push("Create", route('admin.user.create'));
        });

        // Proxy request
        Breadcrumbs::for('admin.proxy-request.index', function ($trail) {
            $trail->parent('admin.index');
            $trail->push('Proxy request', route('admin.proxy-request.index'));
        });

        Breadcrumbs::for('admin.proxy-request.edit', function ($trail, $item) {
            $trail->parent('admin.proxy-request.index');
            $trail->push("Edit", route('admin.proxy-request.edit', $item));
        });

        Breadcrumbs::for('admin.proxy-request.create', function ($trail) {
            $trail->parent('admin.proxy-request.index');
            $trail->push("Create", route('admin.proxy-request.create'));
        });

        // Nodes
        Breadcrumbs::for('admin.node.index', function ($trail) {
            $trail->parent('admin.index');
            $trail->push('Node', route('admin.node.index'));
        });

        Breadcrumbs::for('admin.node.edit', function ($trail, $item) {
            $trail->parent('admin.node.index');
            $trail->push("Edit", route('admin.node.edit', $item));
        });

        Breadcrumbs::for('admin.node.create', function ($trail) {
            $trail->parent('admin.node.index');
            $trail->push("Create", route('admin.node.create'));
        });

        Breadcrumbs::for('admin.node.proxy', function ($trail, $item) {
            $trail->parent('admin.node.index');
            $trail->push('Proxies', route('admin.node.proxy', $item));
        });

        Breadcrumbs::for('admin.node.port', function ($trail, $item) {
            $trail->parent('admin.node.index');
            $trail->push('Ports', route('admin.node.port', [$item]));
        });

        Breadcrumbs::for('admin.node.port.create', function ($trail, $item) {
            $trail->parent('admin.node.port', $item);
            $trail->push('Create', route('admin.node.create'));
        });

        Breadcrumbs::for('admin.node.port.edit', function ($trail, $node, $item) {
            $trail->parent('admin.node.port', $node);
            $trail->push('Edit', route('admin.node.edit', $node, $item));
        });

        // Country
        Breadcrumbs::for('admin.country.index', function ($trail) {
            $trail->parent('admin.index');
            $trail->push('Country', route('admin.country.index'));
        });

        Breadcrumbs::for('admin.country.edit', function ($trail, $item) {
            $trail->parent('admin.country.index');
            $trail->push("Edit", route('admin.country.edit', $item));
        });

        Breadcrumbs::for('admin.country.create', function ($trail) {
            $trail->parent('admin.country.index');
            $trail->push("Create", route('admin.country.create'));
        });

        // Dc provider
        Breadcrumbs::for('admin.dc_provider.index', function ($trail) {
            $trail->parent('admin.index');
            $trail->push('Dc provider', route('admin.dc_provider.index'));
        });

        Breadcrumbs::for('admin.dc_provider.edit', function ($trail, $item) {
            $trail->parent('admin.dc_provider.index');
            $trail->push("Edit", route('admin.dc_provider.edit', $item));
        });

        Breadcrumbs::for('admin.dc_provider.create', function ($trail) {
            $trail->parent('admin.dc_provider.index');
            $trail->push("Create", route('admin.dc_provider.create'));
        });

        // Dc
        Breadcrumbs::for('admin.dc.index', function ($trail) {
            $trail->parent('admin.index');
            $trail->push('Dc', route('admin.dc.index'));
        });

        Breadcrumbs::for('admin.dc.edit', function ($trail, $item) {
            $trail->parent('admin.dc.index');
            $trail->push("Edit", route('admin.dc.edit', $item));
        });

        Breadcrumbs::for('admin.dc.create', function ($trail) {
            $trail->parent('admin.dc.index');
            $trail->push("Create", route('admin.dc.create'));
        });

        // Residential
        Breadcrumbs::for('admin.residential.index', function ($trail) {
            $trail->parent('admin.index');
            $trail->push('Residential', route('admin.residential.index'));
        });

        Breadcrumbs::for('admin.residential.edit', function ($trail, $item) {
            $trail->parent('admin.residential.index');
            $trail->push("Edit", route('admin.residential.edit', $item));
        });

        Breadcrumbs::for('admin.residential.create', function ($trail) {
            $trail->parent('admin.residential.index');
            $trail->push("Create", route('admin.residential.create'));
        });

        // Remote command
        Breadcrumbs::for('admin.remote_command.index', function ($trail) {
            $trail->parent('admin.index');
            $trail->push('Remote commands', route('admin.remote_command.index'));
        });

        Breadcrumbs::for('admin.remote_command.edit', function ($trail, $item) {
            $trail->parent('admin.remote_command.index');
            $trail->push("Edit", route('admin.remote_command.edit', $item));
        });

        Breadcrumbs::for('admin.remote_command.create', function ($trail) {
            $trail->parent('admin.remote_command.index');
            $trail->push("Create", route('admin.remote_command.create'));
        });

        // Currency
        Breadcrumbs::for('admin.currency.index', function ($trail) {
            $trail->parent('admin.index');
            $trail->push('Currency', route('admin.currency.index'));
        });

        Breadcrumbs::for('admin.currency.edit', function ($trail, $item) {
            $trail->parent('admin.currency.index');
            $trail->push("Edit", route('admin.currency.edit', $item));
        });

        Breadcrumbs::for('admin.currency.create', function ($trail) {
            $trail->parent('admin.currency.index');
            $trail->push("Create", route('admin.currency.create'));
        });

        // Product
        Breadcrumbs::for('admin.product.index', function ($trail) {
            $trail->parent('admin.index');
            $trail->push('Product', route('admin.product.index'));
        });

        Breadcrumbs::for('admin.product.edit', function ($trail, $item) {
            $trail->parent('admin.product.index');
            $trail->push("Edit", route('admin.product.edit', $item));
        });

        Breadcrumbs::for('admin.product.create', function ($trail) {
            $trail->parent('admin.product.index');
            $trail->push("Create", route('admin.product.create'));
        });

        // Order
        Breadcrumbs::for('admin.order.index', function ($trail) {
            $trail->parent('admin.index');
            $trail->push('Order', route('admin.order.index'));
        });

        Breadcrumbs::for('admin.order.edit', function ($trail, $item) {
            $trail->parent('admin.order.index');
            $trail->push("Edit", route('admin.order.edit', $item));
        });

        Breadcrumbs::for('admin.order.create', function ($trail) {
            $trail->parent('admin.order.index');
            $trail->push("Create", route('admin.order.create'));
        });

    }
}