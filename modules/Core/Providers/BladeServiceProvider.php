<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 20.07.18
 * Time: 11:35
 */

namespace Modules\Core\Providers;


use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class BladeServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Blade::directive('yes_or_no', function ($expression) {
            return "<?php echo is_null($expression) ? '' : ($expression ? 'Yes' : 'No'); ?>";
        });

        Blade::directive('na', function ($expression) {
            return "<?php echo is_null($expression) ? 'N/A' : $expression; ?>";
        });
        Blade::directive('status', function ($expression) {
            return <<<HTML
<?php
    switch($expression) {
        case \Modules\Core\Paysystems\Robokassa::CREATED_STATUS_ID:
        case \Modules\Core\Paysystems\Interkassa::CREATED_STATUS_ID:
            echo '<span class="badge badge-light">Created</span>';
            break;
        case \Modules\Core\Paysystems\Robokassa::PAID_STATUS_ID:
        case \Modules\Core\Paysystems\Interkassa::PAID_STATUS_ID:
            echo '<span class="badge badge-success">Paid</span>';
            break;            
        default:
            echo '<span class="badge badge-warning">Unknown</span>';
            break;
    }
?>
HTML;

        });
    }
}