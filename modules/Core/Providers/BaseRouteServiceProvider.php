<?php

namespace Modules\Core\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

abstract class BaseRouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace;
    /**
     * Module name
     *
     * @var string
     */
    protected $module;

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the module.
     *
     * @return void
     */
    public function map()
    {
        $this->mapWebRoutes();

        $this->mapApiRoutes();

        $this->mapAdminRoutes();

        $this->mapLkRoutes();

        $this->mapBroadcastRoutes();
    }

    /**
     * Define the "web" routes for the module.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'as' => 'web.',
            'namespace'  => $this->namespace . '\Web',
        ], function ($router) {
            require module_path($this->module, 'Routes/web.php');
        });
    }

    /**
     * Define the "api" routes for the module.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'as' => 'api.',
            'namespace'  => $this->namespace . '\Api',
            'prefix'     => 'api',
        ], function ($router) {
            require module_path($this->module, 'Routes/api.php');
        });
    }

    protected function mapAdminRoutes()
    {
        Route::group([
            'middleware' => ['web', 'admin'],
            'as' => 'admin.',
            'namespace'  => $this->namespace . '\Admin',
            'prefix'     => env('APP_ADMIN_PREFIX', 'admin'),
        ], function ($router) {
            require module_path($this->module, 'Routes/admin.php');
        });
    }

    protected function mapLkRoutes()
    {
        Route::group([
            'middleware' => ['web', 'lk'],
            'as' => 'lk.',
            'namespace'  => $this->namespace . '\Lk',
            'prefix'     => env('APP_LK_PREFIX', 'dashboard'),
        ], function ($router) {
            require module_path($this->module, 'Routes/lk.php');
        });
    }

    protected function mapBroadcastRoutes()
    {
        require module_path($this->module, 'Routes/channels.php');
    }
}
