<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 18.07.18
 * Time: 10:19
 */

namespace Modules\Core\Providers;

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Broadcast;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends BaseRouteServiceProvider
{
    protected $namespace = 'Modules\Core\Http\Controllers';
    /**
     * @var string
     */
    protected $module = 'core';

    protected function mapAdminRoutes()
    {
        Route::group([
            'namespace'  => $this->namespace.'\Admin',
            'middleware' => [ 'web' ],
            'prefix'     => env('APP_ADMIN_PREFIX', 'admin'),
            'as' => 'admin.',
        ], function (Router $router) {
            // Login routes
            $router->get('login', 'Auth\LoginController@showLoginForm')
                ->name('login');
            $router->post('login', 'Auth\LoginController@login');
            $router->get('logout', 'Auth\LoginController@logout')
                ->name('logout');

            $router->group([
                'middleware' => [ 'admin' ],
            ], function ($router) {
                require module_path($this->module, 'Routes/admin.php');
            });
        });
    }

    /**
     * @throws \Caffeinated\Modules\Exceptions\ModuleNotFoundException
     */
    protected function mapBroadcastRoutes()
    {
        Broadcast::routes();

        require module_path($this->module, 'Routes/channels.php');
    }
}