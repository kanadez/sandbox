<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 09.08.18
 * Time: 11:27
 */

namespace Modules\Core\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Model\Node;
use Modules\Core\Observers\NodeObserver;

class ObserversServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Node::observe(NodeObserver::class);
    }
}