<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 23.08.18
 * Time: 11:18
 */

namespace Modules\Core\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Modules\Core\Events\FingerPrintApplied;
use Modules\Core\Events\FingerPrintProposed;
use Modules\Core\Events\NodePortSaved;
use Modules\Core\Events\NodeSaved;
use Modules\Core\Events\UserPortAdded;
use Modules\Core\Events\UserPortRemoved;
use Modules\Core\Listeners\DelUserRemoteCommand;
use Modules\Core\Listeners\FpHandlerRemoteCommand;
use Modules\Core\Listeners\GenMiddleRemoteCommand;
use Modules\Core\Listeners\GenUserRemoteCommand;
use Modules\Core\Listeners\NetCheckRemoteCommand;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        UserPortAdded::class => [
            GenUserRemoteCommand::class,
        ],
        UserPortRemoved::class => [
            DelUserRemoteCommand::class,
        ],
        NodePortSaved::class => [
            GenMiddleRemoteCommand::class,
        ],
        FingerPrintProposed::class => [
            NetCheckRemoteCommand::class
        ],
        FingerPrintApplied::class => [
            FpHandlerRemoteCommand::class
        ],
    ];
    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
        //
    }
}