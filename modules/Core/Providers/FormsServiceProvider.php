<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 19.07.18
 * Time: 15:01
 */

namespace Modules\Core\Providers;

use Collective\Html\FormFacade;
use Illuminate\Support\ServiceProvider;

class FormsServiceProvider extends ServiceProvider
{
    public function boot()
    {
        FormFacade::component('admText', 'core::admin.forms.input.text', ['name', 'label_name', 'attributes' => [], 'value']);
        FormFacade::component('admTextSmall', 'core::admin.forms.input.text_small', ['name', 'label_name', 'attributes' => [], 'value']);
        FormFacade::component('admTextWithPostfix', 'core::admin.forms.input.text_with_postfix', ['name', 'label_name', 'postfix', 'attributes' => [], 'value']);
        FormFacade::component('admLink', 'core::admin.forms.input.link', ['name', 'label_name', 'attributes' => [], 'value']);
        FormFacade::component('admHidden', 'core::admin.forms.input.hidden', ['name', 'value', 'attributes' => []]);
        FormFacade::component('admTextarea', 'core::admin.forms.input.textarea', ['name', 'label_name', 'attributes' => [], 'value']);
        FormFacade::component('admHtmlArea', 'core::admin.forms.input.htmlarea', ['name', 'label_name', 'attributes' => [], 'value']);
        FormFacade::component('admBool', 'core::admin.forms.input.bool', ['name', 'label_name', 'value']);
        FormFacade::component('admNumber', 'core::admin.forms.input.number', ['name', 'label_name', 'attributes' => [], 'default', 'value']);
        FormFacade::component('admEmail', 'core::admin.forms.input.email', ['name', 'label_name', 'attributes' => [], 'value']);
        FormFacade::component('admPassword', 'core::admin.forms.input.password', ['name', 'label_name', 'attributes' => [], 'value']);
        FormFacade::component('admSubmit', 'core::admin.forms.input.submit', ['value', 'attributes' => []]);
        FormFacade::component('admCheckboxInput', 'core::admin.forms.input.multiple_checkbox', [
            'name',
            'label_name',
            'values' => [],
            'selected' => [],
            'attributes' => [],
            'name_field' => 'name',
            'id_filed' => 'id']
        );
        FormFacade::component('admSelect', 'core::admin.forms.input.select', ['name', 'label_name', 'values' => [], 'attributes' => [], 'default' => null]);

        FormFacade::component('admButtonSelect', 'core::admin.forms.input.button_select', ['name', 'label_name', 'values', 'attributes' => [], 'value']);
    }
}