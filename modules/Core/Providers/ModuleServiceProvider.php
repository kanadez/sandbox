<?php

namespace Modules\Core\Providers;

use Caffeinated\Modules\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Foundation\ComposerScripts;
use Illuminate\Support\Facades\URL;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @param Factory $factory
     * @return void
     */
    public function boot(Factory $factory)
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'core');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'core');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'core');
        $this->loadConfigsFrom(__DIR__.'/../config');

        $factory->load(__DIR__ . '/../Database/Factories');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(UtilsServiceProvider::class);
        $this->app->register(EventServiceProvider::class);
        $this->app->register(RemoteServiceProvider::class);
        $this->app->register(BladeServiceProvider::class);
        $this->app->register(FormsServiceProvider::class);
        $this->app->register(BreadCrumbsServiceProvider::class);
        $this->app->register(ValidatorsServiceProvider::class);
        $this->app->register(RouteServiceProvider::class);
        
        // Collections
        $this->app->register(Collections\TestQuestionsCollectionServiceProvider::class);
    }
}
