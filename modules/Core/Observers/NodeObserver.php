<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 09.08.18
 * Time: 11:24
 */

namespace Modules\Core\Observers;

use Modules\Core\Model\Node;

class NodeObserver
{
    public function creating(Node $node)
    {

    }

    public function updating(Node $node)
    {

    }
}