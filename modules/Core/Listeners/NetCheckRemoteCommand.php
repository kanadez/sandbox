<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 30.08.18
 * Time: 10:20
 */

namespace Modules\Core\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Core\Events\FingerPrintNotification;
use Modules\Core\Events\FingerPrintProposed;

class NetCheckRemoteCommand extends RemoteCommand implements ShouldQueue
{
    protected $name = "net_check";

    /**
     * @param string $command
     * @param FingerPrintProposed $event
     * @return string
     */
    protected function replaceVariables(string $command, $event)
    {
        $command = str_replace('%middle_port%', $event->nodePort["number"], $command);

        return $command;
    }

    protected function sendNotification($event, string $command, string $result = null)
    {
        event(tap(new FingerPrintNotification(),
                function (FingerPrintNotification $notification) use ($command, $result, $event) {
                    $notification->channel = 'admin_notifications.' . $event->admin["id"];
                    $notification->title = 'net_check.sh';
                    $notification->command = $command;
                    $notification->log = nl2br($result);
                })
        );
    }
}