<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 30.08.18
 * Time: 12:30
 */

namespace Modules\Core\Listeners;


use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Core\Events\FingerPrintApplied;
use Modules\Core\Events\FingerPrintAppliedNotification;

class FpHandlerRemoteCommand extends RemoteCommand implements ShouldQueue
{
    protected $name = "fp_handler";

    /**
     * @param string $command
     * @param FingerPrintApplied $event
     * @return mixed|string
     */
    protected function replaceVariables(string $command, $event)
    {
        $command = str_replace('%middle_port%', $event->nodePort["number"], $command);
        $command = str_replace('%fp%', $event->nodePort->fingerPrint["name"], $command);

        return $command;
    }

    protected function sendNotification($event, string $command, string $result = null)
    {
        event(tap(new FingerPrintAppliedNotification(),
                function (FingerPrintAppliedNotification $notification) use ($command, $result, $event) {
                    $notification->channel = 'admin_notifications.' . $event->admin["id"];
                    $notification->title = 'fp_handler.sh';
                    $notification->command = $command;
                    $notification->log = nl2br($result);
                })
        );
    }
}