<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 24.08.18
 * Time: 12:09
 *
 * *******************

Генерация мидл порта на ноде:

Команда идет без указания ключа

/opt/radio/bapi/middle_gen.sh %dc_type% %middle_port% %ip% [debug] [8.8.8.8]

<rs | dc> указание под что создавать миддл (Резидентные\ДЦ)
<middle port id> номер мидл порта (от 1 до 100)
<IP address> IP мидл порта для ДЦ или IP address:PORT в случае RS
[8.8.8.8] Опциональный ключ DNS - cтавить при указании в явном виде.

 *******************
 */

namespace Modules\Core\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Core\Events\CommandEvent;
use Modules\Core\Events\MiddleGenNotification;
use Modules\Core\Events\NodePortSaved;
use Modules\Core\Model\Dc;

class GenMiddleRemoteCommand extends RemoteCommand implements ShouldQueue
{
    protected $name = "middle_gen";

    /**
     * @param string $command
     * @param NodePortSaved $event
     * @return string
     */
    protected function replaceVariables(string $command, $event)
    {
        $command = str_replace('%dc_type%', $event->nodePort->node->dcable instanceof Dc ? 'dc' : 'rs', $command);
        $command = str_replace('%middle_port%', $event->nodePort["number"], $command);
        $command = str_replace('%ip%', $event->nodePort["provider_point"], $command);
        return $command;
    }

    protected function sendNotification($event, string $command, string $result = null)
    {
        event(tap(new MiddleGenNotification(),
                function (MiddleGenNotification $notification) use ($command, $result, $event) {
                    $notification->channel = 'admin_notifications.' . $event->admin["id"];
                    $notification->title = 'middle_gen.sh';
                    $notification->command = $command;
                    $notification->log = $result;
                })
        );
    }
}