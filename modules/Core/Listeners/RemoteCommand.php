<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 24.08.18
 * Time: 9:59
 */

namespace Modules\Core\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Core\Contracts\RemoteManagerContract;
use Modules\Core\Events\CommandEvent;
use Modules\Core\Repository\RemoteCommandsRepository;

abstract class RemoteCommand
{
    /**
     * @var string
     */
    protected $name;
    /**
     * @var \Modules\Core\Model\RemoteCommand
     */
    protected $command;
    /**
     * @var RemoteManagerContract
     */
    protected $manager;

    public function __construct(RemoteManagerContract $manager,
                                RemoteCommandsRepository $repository)
    {
        $this->manager = $manager;
        $this->command = $repository->findByField('name', $this->name)->first();
    }

    /**
     * @param string $command
     * @param $event
     * @return string
     */
    protected function replaceVariables(string $command, $event)
    {
        return $command;
    }

    /**
     * Handle the event.
     *
     * @param  CommandEvent  $event
     * @return void
     */
    public function handle(CommandEvent $event)
    {
        $commands = explode("\n", $this->replaceVariables($this->command["command"], $event));

        $this->manager->into($event->node)->run($commands, function($line) use ($event) {
            $event->log .= $line.PHP_EOL;
        });

        $this->sendNotification($event, implode("\n", $commands), $event->log);
    }

    /**
     * @param $event
     * @param string $command
     * @param string|null $result
     * @return mixed
     */
    abstract protected function sendNotification($event, string $command, string $result = null);
}