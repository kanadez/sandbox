<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 23.08.18
 * Time: 11:19
 */

namespace Modules\Core\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Core\Events\CommandEvent;
use Modules\Core\Events\UserGenNotification;
use Modules\Core\Events\UserPortAdded;
use Modules\Core\Model\Dc;

class GenUserRemoteCommand extends RemoteCommand implements ShouldQueue
{
    protected $name = 'gen_user';

    /**
     * @param string $command
     * @param UserPortAdded $event
     * @return mixed|string
     */
    protected function replaceVariables(string $command, $event)
    {
        $email = str_replace(['.', '@'], '_', $event->user["email"]);
        $pass = str_random(6);

        if($event->port->node["dcable_type"] == Dc::class) {
            $login_field = "dc_user";
            $pass_field = "dc_pass";
        } else {
            $login_field = "rsd_user";
            $pass_field = "rsd_pass";
        }
        $event->user->update([
            $login_field => $email,
            $pass_field => $pass,
        ]);

        $command = str_replace('%user_id%', $event->user["id"], $command);
        $command = str_replace('%user_email%', $email, $command);
        $command = str_replace('%user_pass%', $pass, $command);
        $command = str_replace('%user_bandlimin%', $event->user["dc_bandwidth_in"], $command);
        $command = str_replace('%user_bandlimout%', $event->user["dc_bandwidth_out"], $command);
        $command = str_replace('%user_max_conn%', $event->user["dc_max_connections"], $command);
        $command = str_replace('%middle_port%', $event->port["number"], $command);
        return $command;
    }

    /**
     * @param string $command
     * @param string $result
     * @param UserPortAdded $event
     */
    protected function sendNotification($event, string $command, string $result = null)
    {
        event(tap(new UserGenNotification,
            function (UserGenNotification $notification) use ($command, $result, $event) {
                $notification->channel = 'admin_notifications.' . $event->admin["id"];
                $notification->title = 'gen_user.sh';
                $notification->command = $command;
                $notification->log = $result;
            })
        );
    }
}