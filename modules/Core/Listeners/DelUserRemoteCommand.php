<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 27.08.18
 * Time: 18:01
 */

namespace Modules\Core\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Core\Events\CommandEvent;
use Modules\Core\Events\UserDelNotification;

class DelUserRemoteCommand extends RemoteCommand implements ShouldQueue
{
    protected $name = 'del_user';

    protected function replaceVariables(string $command, $event)
    {
        $email = str_replace(['.', '@'], '_', $event->user["email"]);
        $command = str_replace('%user_email%', $email, $command);
        return $command;
    }

    protected function sendNotification($event, string $command, string $result = null)
    {
        event(tap(new UserDelNotification(),
                function (UserDelNotification $notification) use ($command, $result, $event) {
                    $notification->channel = 'admin_notifications.' . $event->admin["id"];
                    $notification->title = 'del_user.sh';
                    $notification->command = $command;
                    $notification->log = $result;
                })
        );
    }
}