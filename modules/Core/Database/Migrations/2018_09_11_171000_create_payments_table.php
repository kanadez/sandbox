<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id');
            $table->unsignedInteger('paysystem_id');
            $table->unsignedInteger('paysystem_method_id');
            $table->unsignedInteger('paysystem_status_id');
            $table->string("link", 1024)->nullable();
            $table->string("comment")->nullable();
            $table->boolean("is_test")->default(0);
            
            $table->timestamps();
        });


        Schema::table('payments', function ($table) {
            $table->foreign('order_id')->references('id')->on('orders')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('paysystem_id')->references('id')->on('paysystems')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('paysystem_status_id')->references('id')->on('paysystem_statuses')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('paysystem_method_id')->references('id')->on('paysystem_methods')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
