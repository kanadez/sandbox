<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNodePortsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('node_ports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('number')->unsigned();
            $table->integer("node_id")->unsigned();
            $table->integer('finger_print_id')->unsigned();
            $table->string("provider_point")->nullable();
//            $table->integer("interval")->unsigned()->default(600);
            $table->integer("mtu")->unsigned()->default(1440);
            $table->string("ip_geo")->nullable();
            $table->string("link")->nullable();
            $table->string("os_fingerprint")->nullable();
            $table->text("comment")->nullable();
            $table->unique(['number', 'node_id']);
            $table->foreign('node_id')->references('id')->on('nodes')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('finger_print_id')->references('id')->on('finger_print_types')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('node_ports');
    }
}
