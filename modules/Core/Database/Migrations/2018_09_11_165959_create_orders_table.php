<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('number');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('currency_id')->unsigned()->nullable();
//            $table->integer('paysystem_id')->unsigned()->nullable();
//            $table->integer('paysystem_method_id')->unsigned()->nullable();
            $table->bigInteger("price");
            
            $table->timestamps();
        });

        Schema::table('orders', function($table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('currency_id')->references('id')->on('currencies')
                ->onUpdate('cascade')->onDelete('cascade');
//            $table->foreign('paysystem_id')->references('id')->on('paysystems')
//                ->onUpdate('cascade')->onDelete('cascade');
//            $table->foreign('paysystem_method_id')->references('id')->on('paysystem_methods')
//                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
