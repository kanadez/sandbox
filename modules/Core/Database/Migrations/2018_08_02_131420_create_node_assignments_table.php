<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Core\Database\Seeds\NodeAssignmentSeeder;

class CreateNodeAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('node_assignments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',5);
            $table->string("type", 5);
            $table->string('description')->nullable();
            $table->unique("name");
            $table->timestamps();
        });

//        // ToDO: info about seeding to console
//        Artisan::call('db:seed', [
//            '--class' => NodeAssignmentSeeder::class,
//            '--force' => "yes",
//        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('node_assignments');
    }
}
