<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nodes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('domain');
            $table->integer('assignment_id')->unsigned();
            $table->integer('rotation_period_id')->unsigned();
            $table->integer('dcable_id')->unsigned()->nullable();
            $table->string('dcable_type')->nullable();
            $table->integer('number')->unsigned();
            $table->integer('hypid')->unsigned();
            $table->boolean("is_test")->default(0);
            $table->unique(['domain', 'assignment_id', 'rotation_period_id', 'number']);
            $table->string("server");
            $table->ipAddress("ip")->nullable();
            $table->timestamps();
            
        });

       Schema::table('nodes', function($table) {
           $table->foreign('assignment_id')->references('id')->on('node_assignments')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('rotation_period_id')->references('id')->on('node_rotation_periods')
                ->onUpdate('cascade')->onDelete('cascade');
//            $table->foreign('dc_id')->references('id')->on('dcs')
//                ->onUpdate('cascade')->onDelete('cascade');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nodes');
    }
}
