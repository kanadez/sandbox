<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Core\Model\DefaultProxyPortSettings;

class CreateDefaultProxyPortsSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('default_proxy_port_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username')->nullable();
            $table->string('password')->nullable();
            $table->string('peer_rotate')->nullable();
            $table->string('fingerprint')->nullable();
            $table->string('asn')->nullable();
            $table->string('uptime')->nullable();
            $table->integer('latency')->nullable();
            $table->string('speed_download')->nullable();
            $table->string('speed_upload')->nullable();
            $table->integer('http')->nullable();
            $table->integer('socks')->nullable();
            $table->string('allowed_ip')->nullable();
            $table->integer('max_connections')->nullable();
            $table->string('bandwidth_in')->nullable();
            $table->string('bandwidth_out')->nullable();
            $table->string('time_limit')->nullable();
            $table->timestamps();
        });

        $defaultSettings = new DefaultProxyPortSettings;
        $defaultSettings->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
