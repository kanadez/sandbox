<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Core\Model\DefaultProxyPortSettings;

class PortsDefaultFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('default_proxy_port_settings');

        Schema::table('proxy_ports', function (Blueprint $table) {
            $table->string('username')->after('node_id');
            $table->string('password')->after('node_id');
            $table->string('peer_rotate')->default('10m')->after('node_id');
            $table->string('fingerprint')->nullable()->default('')->after('node_id');
            $table->string('asn')->nullable()->default('')->after('node_id');
            $table->integer('uptime')->nullable()->default(0)->after('node_id');
            $table->integer('latency')->default(1500)->after('node_id');
            $table->string('speed_download')->default('100KB')->after('node_id');
            $table->string('speed_upload')->default('100KB')->after('node_id');
            $table->string('allowed_ip')->nullable()->after('node_id');
            $table->integer('max_connections')->default(1000)->after('node_id');
            $table->string('bandwidth_in')->default('10MB')->after('node_id');
            $table->string('bandwidth_out')->default('10MB')->after('node_id');
            $table->string('time_limit')->default('0m')->after('node_id');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
