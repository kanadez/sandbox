<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResidentialParentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('residential_parents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('residential_id')->unsigned();
            $table->ipAddress('ip');
            $table->integer('port');
            $table->timestamps();
            
        });

        Schema::table('residential_parents', function($table) {
            $table->foreign('residential_id')->references('id')->on('residentials')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('residential_parents');
    }
}
