<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Core\Database\Seeds\PermissionsSeeder;
use Modules\Core\Database\Seeds\RolesSeeder;
use Modules\Core\Database\Seeds\UsersSeeder;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->integer('dc_bandwidth_in')->default(10);
            $table->integer('dc_bandwidth_out')->default(10);
            $table->integer('dc_max_connections')->default(500);
            $table->string('dc_user')->nullable();
            $table->string('dc_pass')->nullable();
            $table->integer('rsd_bandwidth_in')->default(2);
            $table->integer('rsd_bandwidth_out')->default(2);
            $table->integer('rsd_max_connections')->default(30);
            $table->string('rsd_user')->nullable();
            $table->string('rsd_pass')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });

        // ToDO: info about seeding to console

        Artisan::call('db:seed', [
            '--class' => UsersSeeder::class,
            '--force' => "yes",
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
