<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDcPoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dc_pools', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dc_id')->unsigned();
            $table->ipAddress("ip");
            $table->ipAddress("mask")->default('255.255.255.0');
            $table->ipAddress("gw")->nullable();
            $table->ipAddress("dns")->default('8.8.8.8');
            $table->timestamps();
        });

       Schema::table('dc_pools', function($table) {
           $table->foreign('dc_id')->references('id')->on('dcs')
                ->onUpdate('cascade')->onDelete('cascade');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dc_pools');
    }
}
