<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaysystemMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paysystem_methods', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('paysystem_id')->unsigned();
            $table->boolean("active")->default(1);
            $table->string("name");
            $table->foreign('paysystem_id')->references('id')->on('paysystems')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paysystem_methods');
    }
}
