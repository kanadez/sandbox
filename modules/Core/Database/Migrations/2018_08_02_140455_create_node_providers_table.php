<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Core\Database\Seeds\NodeProvidersSeeder;

class CreateNodeProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('node_providers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',5);
            $table->string('description')->nullable();
            $table->timestamps();
        });

//        // ToDO: info about seeding to console
//        Artisan::call('db:seed', [
//            '--class' => NodeProvidersSeeder::class,
//            '--force' => "yes",
//        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('node_providers');
    }
}
