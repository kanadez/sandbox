<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Core\Database\Seeds\PaysystemStatusesSeeder;

class CreatePaysystemStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paysystem_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('paysystem_id')->unsigned();
            $table->string("name");
            $table->foreign('paysystem_id')->references('id')->on('paysystems')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });

        // ToDO: info about seeding to console
        Artisan::call('db:seed', [
            '--class' => PaysystemStatusesSeeder::class,
            '--force' => "yes",
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paysystem_statuses');
    }
}
