<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Core\Model\DefaultProxyPortSettings;

class PortsFieldsChanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('proxy_ports', function (Blueprint $table) {
           $table->integer('http')->nullable()->after('node_id');
           $table->integer('socks')->nullable()->after('node_id');
        });

        Schema::table('default_proxy_port_settings', function (Blueprint $table) {
            $table->dropColumn('http');
            $table->dropColumn('socks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
