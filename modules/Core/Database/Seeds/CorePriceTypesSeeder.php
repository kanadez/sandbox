<?php

namespace Modules\Core\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Core\Repository\PriceTypesRepository;

class CorePriceTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param PriceTypesRepository $repository
     * @return void
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(PriceTypesRepository $repository)
    {
        $repository->create([
            "name" => "base",
        ]);
    }
}
