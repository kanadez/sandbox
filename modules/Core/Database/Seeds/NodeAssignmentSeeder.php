<?php

namespace Modules\Core\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Core\Repository\NodeAssignmentsRepository;

class NodeAssignmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(NodeAssignmentsRepository $repository)
    {
        $repository->create([
            "name" => "dcd",
            "type" => "dc",
            "description" => "data center dedicated",
        ]);

        $repository->create([
            "name" => "dcs",
            "type" => "dc",
            "description" => "data center shared",
        ]);

        $repository->create([
            "name" => "rsd",
            "type" => "rs",
            "description" => "residential dedicated",
        ]);

        $repository->create([
            "name" => "rss",
            "type" => "rs",
            "description" => "residential shared",
        ]);
    }
}
