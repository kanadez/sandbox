<?php

namespace Modules\Core\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Core\Repository\UsersRepository;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param UsersRepository $repository
     * @return void
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(UsersRepository $repository)
    {
        $pass = empty(env("APP_ADMIN_PASS")) ? str_random(12) : env("APP_ADMIN_PASS");
        $repository->create([
            'name' => "admin",
            'email' => env("APP_ADMIN_EMAIL", "admin@localhost"),
            'password' => bcrypt($pass),
        ]);

        $clientPass = empty(env("APP_CLIENT_PASS")) ? str_random(12) : env("APP_CLIENT_PASS");
        $repository->create([
            'name' => "client",
            'email' => env("APP_CLIENT_EMAIL", "client@localhost"),
            'password' => bcrypt($clientPass),
        ]);

        $this->command->info(sprintf("Admin password is: %s", $pass));
        $this->command->info(sprintf("Client password is: %s", $clientPass));
    }
}
