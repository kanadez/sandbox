<?php

namespace Modules\Core\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Core\Repository\PaysystemsRepository;

class PaysystemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param PaysystemsRepository $repository
     * @return void
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(PaysystemsRepository $repository)
    {
        $repository->create(["name" => "robokassa"]);
        $repository->create(["name" => "interkassa"]);
    }
}
