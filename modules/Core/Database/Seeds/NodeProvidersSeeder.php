<?php

namespace Modules\Core\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Core\Repository\NodeProvidersRepository;

class NodeProvidersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param NodeProvidersRepository $repository
     * @return void
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(NodeProvidersRepository $repository)
    {
        $repository->create([
            "name" => "dpt",
            "description" => "dataplanet",
        ]);
    }
}
