<?php

namespace Modules\Core\Database\Seeds;

use Illuminate\Database\Seeder;

class CoreDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $this->call(PermissionsSeeder::class);
//        $this->call(RolesSeeder::class);
//        $this->call(UsersSeeder::class);
        $this->call(DcsSeeder::class);
//        $this->call(NodeDomainsSeeder::class);
        $this->call(NodeAssignmentSeeder::class);
        $this->call(NodeRotationPeriodsSeeder::class);
        $this->call(NodeProvidersSeeder::class);
        $this->call(ResidentialsSeeder::class);
        $this->call(NodesSeeder::class);
        $this->call(NodePortsSeeder::class);
    }
}
