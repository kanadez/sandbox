<?php

namespace Modules\Core\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Core\Repository\MobileOperatorsRepository;

class MobileOperatorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param MobileOperatorsRepository $repository
     * @return void
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(MobileOperatorsRepository $repository)
    {
        $repository->create(["name" => "beeline"]);
        $repository->create(["name" => "megafon"]);
        $repository->create(["name" => "mts"]);
        $repository->create(["name" => "tele2"]);
    }
}