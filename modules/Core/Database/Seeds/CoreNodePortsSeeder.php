<?php

namespace Modules\Core\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Core\Repository\NodePortsRepository;

class CoreNodePortsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param NodePortsRepository $repository
     * @return void
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(NodePortsRepository $repository)
    {
        $repository->create([
            "number" => 1,
            "node_id" => 1,
            "finger_print_id" => 1,
            "provider_point" => "37.203.246.141",
            "mtu" => 1493,
        ]);
    }
}
