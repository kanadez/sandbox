<?php

namespace Modules\Core\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Core\Repository\CountriesRepository;

class CountriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param CountriesRepository $repository
     * @return void
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(CountriesRepository $repository)
    {
        $repository->create([
            "name" => "Russia",
        ]);
        $repository->create([
            "name" => "United States",
        ]);
    }
}
