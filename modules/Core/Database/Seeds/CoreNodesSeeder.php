<?php

namespace Modules\Core\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Core\Model\Dc;
use Modules\Core\Repository\NodeAssignmentsRepository;
use Modules\Core\Repository\NodeProvidersRepository;
use Modules\Core\Repository\NodeRotationPeriodsRepository;
use Modules\Core\Repository\NodesRepository;

class CoreNodesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(NodeAssignmentsRepository $nodeAssignmentsRepository,
                        NodeRotationPeriodsRepository $nodeRotationPeriodsRepository,
                        NodeProvidersRepository $nodeProvidersRepository,
                        NodesRepository $nodesRepository)
    {
        $assignment = $nodeAssignmentsRepository->firstOrCreate([
            "name" => "dcd",
        ]);
        $rotationPeriod = $nodeRotationPeriodsRepository->firstOrCreate([
            "name" => "stc",
        ]);


        $nodesRepository->create([
            "name" => "dcd-stc-dpl-1.astroproxy.com",
            "domain" => "astroproxy.com",
            "assignment_id" => $assignment["id"],
            "rotation_period_id" => $rotationPeriod["id"],
            "dcable_id" => 1,
            "dcable_type" => Dc::class,
            "number" => 1,
            "hypid" => 2,
            "server" => "XXX",
            "ip" => "37.203.246.141",
        ]);
    }
}
