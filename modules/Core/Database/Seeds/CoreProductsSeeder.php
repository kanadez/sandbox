<?php

namespace Modules\Core\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Core\Model\Price;
use Modules\Core\Model\Product;
use Modules\Core\Repository\ProductsRepository;

class CoreProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param ProductsRepository $productsRepository
     * @return void
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(ProductsRepository $productsRepository)
    {
        /** @var Product $product1 */
        $product1 = $productsRepository->create([
            "name" => "Residential Shared (Rotating Backconnect Proxies 5 min)",
            "description" => <<<HTML
<p class="rate-item__desc">Резидентные прокси (прокси-каналы) с автоматической сменой внешнего 
адреса в Европе или США. (Смена адреса раз в 5 минут). <br>
Внешние IP будут полностью белыми адресами обычных провайдеров Интернета самых обычных городов. <br><br>
Абсолютная анонимность и скрытие в толпе пользователей "кабельного" интернета.<br>
Скорость делится между пользователями порта</p>

HTML
        ]);

        $product1->price()->make([
            "price_type_id" => Product::PRICE_TYPE,
            "currency_id" => 1,
            "value" => 10,
        ])->save();


        $product2 = $productsRepository->create([
            "name" => "Residential Premium (Rotating Backconnect Proxies 5 min)",
            "description" => <<<HTML
<p class="rate-item__desc">Резидентный Backconnect - прокси порт в одни руки.<br><br>
Максимальная скорость только для индивидуального использования.<br><br>
Смена внешнего адреса происходит один раз в 5 минут.<br><br>
Абсолютная анонимность и скрытие в толпе пользователей "домашнего" интернета.</p>
HTML
        ]);

        $product2->price()->make([
            "price_type_id" => Product::PRICE_TYPE,
            "currency_id" => 1,
            "value" => 20,
        ])->save();

        $product3 = $productsRepository->create([
            "name" => "Datacenter (Статические прокси)",
            "description" => <<<HTML
<p class="rate-item__desc">Чистый прокси со статическим адресом датацентра, уникальная возможность подмены 
Passive OS fingerprint для соотвествия Вашему User-Agent.<br><br>
Все адреса проходят тщательную отбор на чистоту.<br><br>
Статические адреса идеально подходят для ежедневных задач, например, таких как фарминг аккаунтов.</p>
HTML
        ]);

        $product3->price()->make([
            "price_type_id" => Product::PRICE_TYPE,
            "currency_id" => 1,
            "value" => 10,
        ])->save();

        $product4 = $productsRepository->create([
            "name" => "Datacenter (Динамические прокси)",
            "description" => <<<HTML
<p class="rate-item__desc">Чистый прокси динамическими адресами датацентра, уникальная возможность подмены Passive OS 
fingerprint для соотвествия Вашему User-Agent.<br><br>
Все адреса проходят тщательную отбор на чистоту.<br><br>
Смена адреса из пула адресов происходит автоматически по заданному интервалу времени или вызову api.</p>

HTML
        ]);

        $product4->price()->make([
            "price_type_id" => Product::PRICE_TYPE,
            "currency_id" => 1,
            "value" => 100,
        ])->save();
    }
}
