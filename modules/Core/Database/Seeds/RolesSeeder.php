<?php

namespace Modules\Core\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Core\Repository\RolesRepository;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param RolesRepository $repository
     * @return void
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(RolesRepository $repository)
    {
        $repository->create([
            "name" => "admin",
        ]);

        $repository->create([
            "name" => "client",
        ]);
    }
}
