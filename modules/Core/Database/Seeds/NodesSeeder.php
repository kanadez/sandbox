<?php

namespace Modules\Core\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;
use Modules\Core\Model\Node;
use Modules\Core\Model\NodePort;
use Modules\Core\Model\Proxy;
use Modules\Core\Repository\DcsRepository;
use Modules\Core\Repository\NodeAssignmentsRepository;
use Modules\Core\Repository\NodeDomainsRepository;
use Modules\Core\Repository\NodeProvidersRepository;
use Modules\Core\Repository\NodeRotationPeriodsRepository;
use Modules\Core\Repository\ResidentialsRepository;

class NodesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param NodeAssignmentsRepository $nodeAssignmentsRepository
     * @param NodeRotationPeriodsRepository $nodeRotationPeriodsRepository
     * @param NodeProvidersRepository $nodeProvidersRepository
     * @param DcsRepository $dcsRepository
     * @param ResidentialsRepository $residentialsRepository
     * @return void
     */
    public function run(NodeAssignmentsRepository $nodeAssignmentsRepository,
                        NodeRotationPeriodsRepository $nodeRotationPeriodsRepository,
                        NodeProvidersRepository $nodeProvidersRepository,
                        DcsRepository $dcsRepository,
                        ResidentialsRepository $residentialsRepository)
    {
        $assignments = $nodeAssignmentsRepository->all();
        $rotationPeriods = $nodeRotationPeriodsRepository->all();
        $providers = $nodeProvidersRepository->all();
        $residentials = $residentialsRepository->all();
        $dcs = $dcsRepository->all();
        $dcables = $dcs->concat($residentials);
        $counters = [];
        factory(Node::class, 15)->make()->each(function (Node $node) use ($assignments, $rotationPeriods, $providers, $dcables, &$counters) {
            $assignment = $assignments->random();
            $rotationPeriod = $rotationPeriods->random();
            $provider = $providers->random();
            $dc = $dcables->random();
            $index = "${assignment["id"]}.${rotationPeriod["id"]}.${provider["id"]}";
            if (empty($counters[$index])) {
                $counters[$index] = 1;
            }
            $node->assignment()->associate($assignment);
            $node->rotationPeriod()->associate($rotationPeriod);
            $node->provider()->associate($provider);
            $node->dcable()->associate($dc);
            $node["number"] = $counters[$index]++;
            $node["name"] = sprintf("%s-%s-%s-%s.%s",
                $assignment["name"],
                $rotationPeriod["name"],
                $provider["name"],
                $node["number"],
                $node["domain"]);
            $node->save();
            $node->proxies()->saveMany(
                factory(Proxy::class, rand(2,5))->make()
            );
        });;
    }
}
