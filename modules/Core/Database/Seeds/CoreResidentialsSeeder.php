<?php

namespace Modules\Core\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Core\Repository\ResidentialsRepository;

class CoreResidentialsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(ResidentialsRepository $repository)
    {
        $repository->create([
            "name" => "test",
        ]);
    }
}
