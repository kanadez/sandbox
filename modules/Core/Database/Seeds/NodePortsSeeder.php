<?php

namespace Modules\Core\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Core\Contracts\NodePortsCollectionContract;
use Modules\Core\Model\Node;
use Modules\Core\Model\NodePort;
use Modules\Core\Repository\NodesRepository;

class NodePortsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param NodePortsCollectionContract $collection
     * @param NodesRepository $nodesRepository
     * @return void
     */
    public function run(NodePortsCollectionContract $collection,
                        NodesRepository $nodesRepository)
    {
        $nodesRepository->all()->each(function (Node $node) use ($collection) {
            $ports = factory(NodePort::class, rand(3,7))->make();
            $numbers = $collection->random($ports->count());
            $ports->each(function (NodePort $nodePort) use ($node, $numbers) {
                $nodePort["number"] = $numbers->pop();
                $nodePort->node()->associate($node);
                $nodePort->save();
            });
        });
    }
}
