<?php

namespace Modules\Core\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Core\Repository\PermissionsRepository;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param PermissionsRepository $repository
     * @return void
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(PermissionsRepository $repository)
    {
        $repository->create([
            "name" => "admin-access",
        ]);
    }
}
