<?php

namespace Modules\Core\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Core\Repository\DcProvidersRepository;

class DcProvidersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param DcProvidersRepository $repository
     * @return void
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(DcProvidersRepository $repository)
    {
        $repository->create([
            "name" => "ruvds",
        ]);
    }
}
