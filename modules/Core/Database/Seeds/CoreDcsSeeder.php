<?php

namespace Modules\Core\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Core\Repository\DcsRepository;

class CoreDcsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param DcsRepository $repository
     * @return void
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(DcsRepository $repository)
    {
        $repository->create([
            "name" => "dpl",
            "country_id" => 1,
        ]);
    }
}
