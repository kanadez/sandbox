<?php

namespace Modules\Core\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Core\Repository\RemoteCommandsRepository;

class RemoteCommandsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param RemoteCommandsRepository $repository
     * @return void
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(RemoteCommandsRepository $repository)
    {
        $repository->create([
            "name" => "gen_user",
            "command" => '/opt/radio/bapi/gen_user.sh --user_id %user_id% --user_name %user_email% ' .
                '--md5_userpass %user_pass% --user_bandlimin %user_bandlimin% --user_bandlimout %user_bandlimout% ' .
                '--user_max_conn %user_max_conn% --middle_port %middle_port%',
            "comment" => <<<TEXT
*******************
Запускается при привязки middle порта к пользователю
Генерация пользователя на ноде:

gen_user.sh --user_id <131> --user_name <user_mail_com> --md5_userpass <pass> --user_bandlimin <10240000> --user_bandlimout <10240000> --user_max_conn <1000> --middle_port "1"

ВНИМАНИЕ! Команда идет с указанием ключа (--xxx)

--user_id ID пользователя уникальный
--user_name имя пользователя: почта, вместо точек и собак - нижнее подчеркивание
--md5_userpass пароль пользователя (не MD5), просто набор символов
--user_bandlimin скорость IN
--user_bandlimout скорость OUT
--user_max_conn кол-во одновременных коннектов
--middle_port номер мидл порта

*******************
TEXT
        ]);

        $repository->create([
            "name" => "del_user",
            "command" => '/opt/radio/bapi/del_user.sh --user_name %user_email%',
            "comment" => <<<TEXT
*******************
Запускается при отвязывании пользователя от порта   .
Удаление пользователя на ноде:

ВНИМАНИЕ! Команда идет с указанием ключа (--xxx)

/opt/radio/bapi/del_user.sh

--user_name имя пользователя: почта, вместо точек и собак - нижнее подчеркивание
*******************
TEXT
        ]);

        $repository->create([
            "name" => "middle_gen",
            "command" => '/opt/radio/bapi/middle_gen.sh %dc_type% %middle_port% %ip%',
            "comment" => <<<TEXT
*******************
Запускается при добавлении мидл морта в свойствах ноды
Генерация мидл порта на ноде:

Команда идет без указания ключа

/opt/radio/bapi/middle_gen.sh <rs | dc> <middle port id> <IP address | IP address:PORT> [debug] [8.8.8.8]

<rs | dc> указание под что создавать миддл (Резидентные\ДЦ)
<middle port id> номер мидл порта (от 1 до 100)
<IP address> IP мидл порта для ДЦ или IP address:PORT в случае RS 
[8.8.8.8] Опциональный ключ DNS - cтавить при указании в явном виде.
*******************
TEXT
        ]);

        $repository->create([
            "name" => "cfgen",
            "command" => '/opt/radio/bapi/cfgen.sh --gen',
            "comment" => <<<TEXT
*******************
Запускается при сохранении атрибутов ноды/порта
Обновление информации ноды / синхронизация ноды с админкой:

ВНИМАНИЕ! Команда идет с указанием ключа (--xxx)
*******************
TEXT
        ]);

        $repository->create([
            "name" => "net_check",
            "command" => '/opt/radio/app/net_check.sh %middle_port%',
            "comment" => <<<TEXT
*******************
Запрос fingerprint

Команда проверки FP | МTU | Network Link

Команда идет без указания ключа.

/opt/radio/app/net_check.sh 1

где аргумент команды номер миддл порта (Number).

пример вывода команды:

Detected OS   = Mac OS X  [generic]
MTU           = 1500
Network link  = Ethernet or modem
*******************
TEXT
        ]);

        $repository->create([
            "name" => "fp_handler",
            "command" => '/opt/radio/bapi/fp_handler.sh --port %middle_port% %fp%',
            "comment" => <<<TEXT
*******************
Команда установки FP на порту.

ВНИМАНИЕ! Команда идет с указанием ключа (--xxx)

/opt/radio/bapi/fp_handler.sh --port 2001 ios

--port миддл порт (2000 + Number)

fp которым могут быть следующие перменные:

# { id: 'api', name: 'API' },
# { id: 'win7', name: 'Windows 7 [generic]' },
# { id: 'win8', name: 'Windows 8 [generic]' },
# { id: 'winxp', name: 'Windows XP' },
# { id: 'android', name: 'Linux 2.2.x-3.x [generic]' },
# { id: 'ios', name: 'iOS iPhone / MacOS X [generic] ' },
# { id: 'macos', name: 'MacOS X [generic]' },
# { id: 'ntgeneric', name: 'Windows 10 / NT [generic]' },
# { id: 'unknown', name: '???' },
# { id: 'ntfuzzy', name: 'Windows 10 / NT [fuzzy]' },
# { id: 'winfuzzy', name: 'Windows 7 or 8 [fuzzy]' },
# { id: 'iosfuzzy', name: 'MacOS X [fuzzy]' },
# { id: 'notset', name: 'Linux 3.11 and newer' },
*******************
TEXT
        ]);
    }
}
