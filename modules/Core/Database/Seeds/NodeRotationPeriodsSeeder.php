<?php

namespace Modules\Core\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Core\Repository\NodeRotationPeriodsRepository;

class NodeRotationPeriodsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param NodeRotationPeriodsRepository $repository
     * @return void
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(NodeRotationPeriodsRepository $repository)
    {
        $repository->create([
            "name" => "2m",
            "description" => "2 minutes",
        ]);
        $repository->create([
            "name" => "5m",
            "description" => "5 minutes",
        ]);
        $repository->create([
            "name" => "10m",
            "description" => "10 minutes",
        ]);
        $repository->create([
            "name" => "15m",
            "description" => "15 minutes",
        ]);
        $repository->create([
            "name" => "stc",
            "description" => "static",
        ]);
        $repository->create([
            "name" => "api",
            "description" => "api rotation",
        ]);
    }
}
