<?php

namespace Modules\Core\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Core\Model\Residential;
use Modules\Core\Model\ResidentialParent;

class ResidentialsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Residential::class, 15)->create()->each(function (Residential $residential) {
            $residential->parents()->saveMany(factory(ResidentialParent::class, rand(3, 5))->make());
        });
    }
}
