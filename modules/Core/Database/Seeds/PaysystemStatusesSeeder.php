<?php

namespace Modules\Core\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Core\Model\Paysystem;
use Modules\Core\Repository\PaysystemStatusesRepository;

class PaysystemStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param PaysystemStatusesRepository $repository
     * @return void
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(PaysystemStatusesRepository $repository)
    {
        // Created
        $repository->create([
            "paysystem_id" => Paysystem::ROBOKASSA_ID,
            "name" => "Created",
        ]);

        $repository->create([
            "paysystem_id" => Paysystem::INTERKASSA_ID,
            "name" => "Created",
        ]);

        // Paid
        $repository->create([
            "paysystem_id" => Paysystem::ROBOKASSA_ID,
            "name" => "Paid",
        ]);

        $repository->create([
            "paysystem_id" => Paysystem::INTERKASSA_ID,
            "name" => "Paid",
        ]);

        // Error
        $repository->create([
            "paysystem_id" => Paysystem::ROBOKASSA_ID,
            "name" => "Error",
        ]);

        $repository->create([
            "paysystem_id" => Paysystem::INTERKASSA_ID,
            "name" => "Error",
        ]);

        // Fail
        $repository->create([
            "paysystem_id" => Paysystem::ROBOKASSA_ID,
            "name" => "Fail",
        ]);

        $repository->create([
            "paysystem_id" => Paysystem::INTERKASSA_ID,
            "name" => "Fail",
        ]);
    }
}
