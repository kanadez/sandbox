<?php

namespace Modules\Core\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Core\Model\Dc;
use Modules\Core\Model\DcPool;
use Modules\Core\Repository\DcsRepository;

class DcsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Dc::class, 15)->create()->each(function (Dc $dc) {
            $dc->pools()->saveMany(factory(DcPool::class, rand(3, 5))->make());
        });
    }
}
