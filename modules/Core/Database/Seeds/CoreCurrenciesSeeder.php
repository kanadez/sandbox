<?php

namespace Modules\Core\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Core\Repository\CurrenciesRepository;

class CoreCurrenciesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param CurrenciesRepository $repository
     * @return void
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(CurrenciesRepository $repository)
    {
        $repository->create([
            "name" => "Dollar",
            "short_name" => "&#x24;",
            "multiplier" => 100,
            "rk_name" => "USD",
            "ik_name" => "USD",
        ]);

        $repository->create([
            "name" => "Ruble",
            "short_name" => "&#x20bd",
            "multiplier" => 100,
            "rk_name" => "RUB",
            "ik_name" => "RUB",
        ]);
    }
}
