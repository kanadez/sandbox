<?php

namespace Modules\Core\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Core\Repository\FingerPrintTypesRepository;

class FingerPrintTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(FingerPrintTypesRepository $repository)
    {
        $repository->create([
            "name" => "api",
            "description" => "API",
        ]);
        $repository->create([
            "name" => "win7",
            "description" => "Windows 7 [generic]",
        ]);
        $repository->create([
            "name" => "win8",
            "description" => "Windows 8 [generic]",
        ]);
        $repository->create([
            "name" => "winxp",
            "description" => "Windows XP",
        ]);
        $repository->create([
            "name" => "android",
            "description" => "Linux 2.2.x-3.x [generic]",
        ]);
        $repository->create([
            "name" => "ios",
            "description" => "iOS iPhone / MacOS X [generic]",
        ]);
        $repository->create([
            "name" => "macos",
            "description" => "MacOS X [generic]",
        ]);
        $repository->create([
            "name" => "ntgeneric",
            "description" => "Windows 10 / NT [generic]",
        ]);
        $repository->create([
            "name" => "unknown",
            "description" => "???",
        ]);
        $repository->create([
            "name" => "ntfuzzy",
            "description" => "Windows 10 / NT [fuzzy]",
        ]);
        $repository->create([
            "name" => "winfuzzy",
            "description" => "Windows 7 or 8 [fuzzy]",
        ]);
        $repository->create([
            "name" => "iosfuzzy",
            "description" => "MacOS X [fuzzy]",
        ]);
        $repository->create([
            "name" => "notset",
            "description" => "Linux 3.11 and newer",
        ]);
    }
}
