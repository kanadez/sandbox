<?php

namespace Modules\Core\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Core\Contracts\PaysystemContract;
use Modules\Core\Model\PaysystemMethod;
use Modules\Core\Repository\PaysystemMethodsRepository;

class CorePaysystemMethodsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param PaysystemMethodsRepository $repository
     * @return void
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(PaysystemMethodsRepository $repository)
    {
        $qiwi = $repository->create([
            "paysystem_id" => PaysystemContract::ROBOKASSA_ID,
            "name" => "QiwiWallet",
        ]);
        /** @var PaysystemMethod $qiwi */
        $qiwi
            ->addMedia(resource_path("img/QiwiWallet.svg"))
            ->preservingOriginal()
            ->toMediaCollection('paysystem_methods');

        $card = $repository->create([
            "paysystem_id" => PaysystemContract::ROBOKASSA_ID,
            "name" => "BankCard",
        ]);
        /** @var PaysystemMethod $webMoney */
        $card
            ->addMedia(resource_path("img/BankCard.svg"))
            ->preservingOriginal()
            ->toMediaCollection('paysystem_methods');

//        $wm = $repository->create([
//            "paysystem_id" => PaysystemContract::INTERKASSA_ID,
//            "name" => "webmoney_webmoney_merchant_wmz",
//        ]);

//        $test = $repository->create([
//            "paysystem_id" => PaysystemContract::INTERKASSA_ID,
//            "name" => "test_interkassa_test_xts",
//        ]);

//        $visa = $repository->create([
//            "paysystem_id" => PaysystemContract::INTERKASSA_ID,
//            "name" => "visa_rfibank_merchant_rub",
//        ]);
//
//        $visa
//            ->addMedia(resource_path("img/visa.png"))
//            ->preservingOriginal()
//            ->toMediaCollection('paysystem_methods');
//
//        $mastercard = $repository->create([
//            "paysystem_id" => PaysystemContract::INTERKASSA_ID,
//            "name" => "mastercard_cpaytrz_merchant_rub",
//        ]);
//
//        $mastercard
//            ->addMedia(resource_path("img/mastercard.png"))
//            ->preservingOriginal()
//            ->toMediaCollection('paysystem_methods');


        $qiwi = $repository->create([
            "paysystem_id" => PaysystemContract::INTERKASSA_ID,
            "name" => "qiwi_rfibank_merchant_rub",
        ]);

        $qiwi
            ->addMedia(resource_path("img/qiwi.png"))
            ->preservingOriginal()
            ->toMediaCollection('paysystem_methods');

        $bcoin = $repository->create([
            "paysystem_id" => PaysystemContract::INTERKASSA_ID,
            "name" => "bitcoin_cubits_merchant_rub",
        ]);

        $bcoin
            ->addMedia(resource_path("img/bitcoin.png"))
            ->preservingOriginal()
            ->toMediaCollection('paysystem_methods');

//        $repository->create([
//            "paysystem_id" => PaysystemContract::ROBOKASSA_ID,
//            "name" => "Bank",
//        ]);
//        $repository->create([
//            "paysystem_id" => PaysystemContract::ROBOKASSA_ID,
//            "name" => "Terminals",
//        ]);
//        $repository->create([
//            "paysystem_id" => PaysystemContract::ROBOKASSA_ID,
//            "name" => "Mobile",
//        ]);
//        $repository->create([
//            "paysystem_id" => PaysystemContract::ROBOKASSA_ID,
//            "name" => "Other",
//        ]);
    }
}
