<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 22.08.18
 * Time: 22:03
 */

use Faker\Generator as Faker;
use Modules\Core\Model\ResidentialParent;

$factory->define(ResidentialParent::class, function (Faker $faker) {
    return [
        'ip' => $faker->ipv4,
        'port' => $faker->numberBetween(1,65000),
    ];
});