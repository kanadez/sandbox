<?php

use Faker\Generator as Faker;
use Modules\Core\Model\Node;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Node::class, function (Faker $faker) {
    return [
        'ip' => $faker->ipv4,
        'server' => $faker->company,
        'domain' => $faker->domainName,
        'hypid' => $faker->numberBetween(1, 10),
    ];
});
