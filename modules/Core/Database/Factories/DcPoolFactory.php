<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 22.08.18
 * Time: 22:11
 */

use Faker\Generator as Faker;
use Modules\Core\Model\DcPool;

$factory->define(DcPool::class, function (Faker $faker) {
    return [
        'ip' => $faker->ipv4,
        'mask' => $faker->ipv4,
        'gw' => $faker->ipv4,
        'dns' => $faker->ipv4,
    ];
});