<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 09.08.18
 * Time: 10:35
 */

use Faker\Generator as Faker;
use Modules\Core\Model\NodeDomain;

$factory->define(NodeDomain::class, function (Faker $faker) {
    /** @var \Illuminate\Support\Collection $providers */
    return [
        'name' => $faker->domainName,
    ];
});