<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 02.08.18
 * Time: 17:00
 */

use Faker\Generator as Faker;
use Modules\Core\Model\Dc;
use Modules\Core\Repository\CountriesRepository;
use Modules\Core\Repository\DcProvidersRepository;

$factory->define(Dc::class, function (Faker $faker) {
    /** @var \Illuminate\Support\Collection $providers */
    $providers = app(DcProvidersRepository::class)->all();
    $countries = app(CountriesRepository::class)->all();

    return [
        'name' => $faker->word,
//        'provider_id' => $providers->random()["id"],
        'country_id' => $countries->random()["id"],
    ];
});