<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 07.08.18
 * Time: 15:17
 */

use Faker\Generator as Faker;
use Modules\Core\Model\NodePort;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(NodePort::class, function (Faker $faker) {
    return [
        'interval' => $faker->numberBetween(400, 800),
        'provider_point' => $faker->ipv4 . ":" . $faker->numberBetween(1, 65000),
        'mtu' => $faker->numberBetween(1350, 1500),
    ];
});

