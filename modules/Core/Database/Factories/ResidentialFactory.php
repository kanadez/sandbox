<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 22.08.18
 * Time: 21:59
 */

use Faker\Generator as Faker;
use Modules\Core\Model\Residential;

$factory->define(Residential::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
    ];
});