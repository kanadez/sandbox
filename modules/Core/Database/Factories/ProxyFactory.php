<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 30.07.18
 * Time: 15:18
 */

use Faker\Generator as Faker;
use Modules\Core\Model\Proxy;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Proxy::class, function (Faker $faker) {
    return [
        'port' => rand(1, 65000),
        'login' => $faker->userName,
        'pass' => $faker->password,
    ];
});