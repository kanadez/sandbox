<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 19.07.18
 * Time: 11:39
 */

namespace Modules\Core\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class UserValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => 'required|string|min:3',
            'email'  => 'required|string|email|max:255|unique:users,email',
            'password' => 'required|string|min:6|confirmed',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => 'required|string|min:3',
            'email'  => 'required|string|email|max:255|unique:users,email' ,
        ]
    ];
}