<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 19.07.18
 * Time: 10:34
 */

namespace Modules\Core\Validators;

use GuzzleHttp\Client;

/**
 * Google ReCaptcha validator
 * Class ReCaptcha
 * @package Modules\Core\Validators
 */
class ReCaptcha
{
    public function validate(
        $attribute,
        $value,
        $parameters,
        $validator
    ){
        return with(new Client(), function (Client $client) use ($value) {
            $response = $client->post(
                'https://www.google.com/recaptcha/api/siteverify', [
                    'form_params' => [
                        'secret' => env('GOOGLE_RECAPTCHA_SECRET'),
                        'response' => $value,
                    ]
                ]
            );

            $body = json_decode((string)$response->getBody());
            return $body->success;
        });
    }
}