<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 09.08.18
 * Time: 16:58
 */

namespace Modules\Core\Validators;

use Illuminate\Contracts\Validation\Factory;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class NodeValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [


        ],
        ValidatorInterface::RULE_UPDATE => [
            //'ip' => 'required|ipv4',
            //'server' => 'required|string|min:3',
            //'assignment_id' => 'required|integer',
            //'rotation_period_id' => 'required|integer',
            //'provider_id' => 'required|integer',
            //'dc_id' => 'required|integer',
            //'hypid' => 'required|integer',
            'slug' => 'required',

        ]
    ];

    public function __construct(Factory $validator)
    {
        parent::__construct($validator);
//        dd($this->data);

        $this->rules[ValidatorInterface::RULE_UPDATE]['number'] =
            'required|integer|unique:nodes,number,null,id' .
            //',assignment_id,' . $this->data["assignment_id"] .
            //',rotation_period_id,' . $this->data["rotation_period_id"] .
            //',provider_id,'.$this->data["provider_id"] .
            //',domain_id,'.$this->data["domain_id"];
    }
}