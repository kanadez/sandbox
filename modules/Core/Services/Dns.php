<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 09.08.18
 * Time: 13:58
 */

namespace Modules\Core\Services;

use Modules\Core\Contracts\DnsContract;

class Dns implements DnsContract
{
    /**
     * @param string $hostname
     * @return null|string
     */
    public function getIp(?string $hostname): ?string
    {
        $ip = gethostbyname($hostname);

        return $ip == $hostname ? null : $ip;
    }
}