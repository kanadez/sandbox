<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 30.07.18
 * Time: 12:28
 */

namespace Modules\Core\Services\Payments;

use Illuminate\Http\Request;
use Modules\Core\Contracts\InterkassaContract;
use Modules\Core\Contracts\PaymentServiceContract;
use Modules\Core\Model\Currency;
use Modules\Core\Model\Order;
use Modules\Core\Model\Payment;
use Modules\Core\Model\Paysystem;
use Modules\Core\Model\User;
use Modules\Core\Paysystems\Interkassa;
use Modules\Core\Repository\OrdersRepository;
use Modules\Core\Repository\PaymentsRepository;
use Modules\Core\Traits\Strings;

class InterkassaService implements PaymentServiceContract
{
    use Strings;
    /**
     * @var InterkassaContract
     */
    private $interkassa;
    /**
     * @var OrdersRepository
     */
    private $ordersRepository;
    /**
     * @var PaymentsRepository
     */
    private $paymentsRepository;

    /**
     * InterkassaService constructor.
     * @param InterkassaContract $interkassa
     * @param OrdersRepository $ordersRepository
     * @param PaymentsRepository $paymentsRepository
     */
    public function __construct(InterkassaContract $interkassa,
                                OrdersRepository $ordersRepository,
                                PaymentsRepository $paymentsRepository)
    {
        $this->interkassa = $interkassa;
        $this->ordersRepository = $ordersRepository;
        $this->paymentsRepository = $paymentsRepository;
    }

    /**
     * @param Order $order
     * @return mixed
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function process(Order $order)
    {
        $sum = $this->formatPrice($sum);

        $order = $this->ordersRepository->create([
            "user_id" => $user["id"],
            "price" => $sum,
        ]);

        $payment = $this->paymentsRepository->create([
            "order_id" => $order["id"],
            "paysystem_id" => Paysystem::INTERKASSA_ID,
            "paysystem_status_id" => Interkassa::CREATED_STATUS_ID,
            "description" => sprintf("Payment for order №%s", $order["number"]),
            "is_test" => $this->interkassa->isTest(),
        ]);

        return $this->interkassa->getPaymentLink($sum, $payment["id"], "Payment");
    }

    /**
     * @param Request $request
     * @return bool
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function successPayment(Request $request)
    {
        $invId = $request->input("ik_pm_no");
        $sum = $request->input("ik_am");
        $signature = $request->input("ik_sign");

        /** @var Payment $payment */
        $payment = $this->paymentsRepository->with(["order"])->find($invId);

        if ($this->interkassa->checkSignature($request->toArray())) {
            // Set payed status
            $this->paymentsRepository->update([
                "paysystem_status_id" => Interkassa::PAID_STATUS_ID,
            ],$payment["id"]);


            return true;
        }
        return false;
    }
}