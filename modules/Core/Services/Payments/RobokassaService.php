<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 26.07.18
 * Time: 11:05
 */

namespace Modules\Core\Services\Payments;

use Modules\Core\Contracts\PaymentServiceContract;
use Modules\Core\Contracts\RobokassaContract;
use Modules\Core\Model\Currency;
use Modules\Core\Model\Order;
use Modules\Core\Model\Payment;
use Modules\Core\Model\Paysystem;
use Modules\Core\Model\User;
use Modules\Core\Paysystems\Robokassa;
use Modules\Core\Repository\OrdersRepository;
use Modules\Core\Repository\PaymentsRepository;
use Modules\Core\Traits\Strings;

class RobokassaService implements PaymentServiceContract
{
    use Strings;
    /**
     * @var OrdersRepository
     */
    private $ordersRepository;
    /**
     * @var RobokassaContract
     */
    private $robokassa;
    /**
     * @var PaymentsRepository
     */
    private $paymentsRepository;

    /**
     * RobokassaService constructor.
     * @param RobokassaContract $robokassa
     * @param OrdersRepository $ordersRepository
     * @param PaymentsRepository $paymentsRepository
     */
    public function __construct(RobokassaContract $robokassa,
                                OrdersRepository $ordersRepository,
                                PaymentsRepository $paymentsRepository)
    {
        $this->robokassa = $robokassa;
        $this->ordersRepository = $ordersRepository;
        $this->paymentsRepository = $paymentsRepository;
    }

    /**
     * @param Order $order
     * @return string
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function process(Order $order)
    {

        $payment = $this->paymentsRepository->make([
            "order_id" => $order["id"],
            "paysystem_id" => Paysystem::ROBOKASSA_ID,
            "paysystem_status_id" => Robokassa::CREATED_STATUS_ID,
            "description" => sprintf("Payment for order №%s", $order["number"]),
            "is_test" => $this->robokassa->isTest(),
        ]);


        return $this->robokassa->getPaymentLink($order["price"], "USD", $payment["id"], $payment["description"]);
    }

    /**
     * @param string $invId
     * @param string $signature
     * @return bool
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function successPayment(string $invId, string $signature)
    {
        /** @var Payment $payment */
        $payment = $this->paymentsRepository->with(["order"])->find($invId);

        if ($this->robokassa->checkSignature($payment["id"], $payment["order"]["price"], $signature)) {
            // Set payed status
            $this->paymentsRepository->update([
                    "paysystem_status_id" => Robokassa::PAID_STATUS_ID,
                ],$payment["id"]);
            

            return true;
        }
        // Set error status
        $this->paymentsRepository->update([
            "paysystem_status_id" => Robokassa::ERROR_STATUS_ID,
        ],$payment["id"]);

        return false;
    }
}