<?php
/**
 * Created by PhpStorm.
 * User: a6y
 * Date: 21.08.18
 * Time: 12:57
 */

namespace Modules\Core\Services;

use Collective\Remote\RemoteManager as CollectiveRemoteManager;
use Modules\Core\Contracts\RemoteManagerContract;

class RemoteManager extends CollectiveRemoteManager implements RemoteManagerContract
{
    /**
     * @param \Modules\Core\Contracts\RemoteHostContract $model
     * @return array
     */
    protected function getConfig($model)
    {
        return $model->getConfig();
    }
}