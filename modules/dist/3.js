(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[3],{

/***/ "./modules/App/Resources/Assets/js/app/components/ListErrors.vue":
/*!***********************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/components/ListErrors.vue ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ListErrors_vue_vue_type_template_id_671e7648_v_show_errors___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ListErrors.vue?vue&type=template&id=671e7648&v-show=errors& */ "./modules/App/Resources/Assets/js/app/components/ListErrors.vue?vue&type=template&id=671e7648&v-show=errors&");
/* harmony import */ var _ListErrors_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ListErrors.vue?vue&type=script&lang=js& */ "./modules/App/Resources/Assets/js/app/components/ListErrors.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ListErrors_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ListErrors_vue_vue_type_template_id_671e7648_v_show_errors___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ListErrors_vue_vue_type_template_id_671e7648_v_show_errors___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "modules/App/Resources/Assets/js/app/components/ListErrors.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/components/ListErrors.vue?vue&type=script&lang=js&":
/*!************************************************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/components/ListErrors.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ListErrors_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./ListErrors.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/ListErrors.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ListErrors_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/components/ListErrors.vue?vue&type=template&id=671e7648&v-show=errors&":
/*!********************************************************************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/components/ListErrors.vue?vue&type=template&id=671e7648&v-show=errors& ***!
  \********************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ListErrors_vue_vue_type_template_id_671e7648_v_show_errors___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./ListErrors.vue?vue&type=template&id=671e7648&v-show=errors& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/ListErrors.vue?vue&type=template&id=671e7648&v-show=errors&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ListErrors_vue_vue_type_template_id_671e7648_v_show_errors___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ListErrors_vue_vue_type_template_id_671e7648_v_show_errors___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/views/ArticleEdit.vue":
/*!*******************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/views/ArticleEdit.vue ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ArticleEdit_vue_vue_type_template_id_0320e24e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ArticleEdit.vue?vue&type=template&id=0320e24e& */ "./modules/App/Resources/Assets/js/app/views/ArticleEdit.vue?vue&type=template&id=0320e24e&");
/* harmony import */ var _ArticleEdit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ArticleEdit.vue?vue&type=script&lang=js& */ "./modules/App/Resources/Assets/js/app/views/ArticleEdit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ArticleEdit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ArticleEdit_vue_vue_type_template_id_0320e24e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ArticleEdit_vue_vue_type_template_id_0320e24e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "modules/App/Resources/Assets/js/app/views/ArticleEdit.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/views/ArticleEdit.vue?vue&type=script&lang=js&":
/*!********************************************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/views/ArticleEdit.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleEdit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./ArticleEdit.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/views/ArticleEdit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleEdit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/views/ArticleEdit.vue?vue&type=template&id=0320e24e&":
/*!**************************************************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/views/ArticleEdit.vue?vue&type=template&id=0320e24e& ***!
  \**************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleEdit_vue_vue_type_template_id_0320e24e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./ArticleEdit.vue?vue&type=template&id=0320e24e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/views/ArticleEdit.vue?vue&type=template&id=0320e24e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleEdit_vue_vue_type_template_id_0320e24e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleEdit_vue_vue_type_template_id_0320e24e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/ListErrors.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./modules/App/Resources/Assets/js/app/components/ListErrors.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "RwvListErorrs",
  props: {
    errors: {
      type: Object,
      required: true
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/views/ArticleEdit.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./modules/App/Resources/Assets/js/app/views/ArticleEdit.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../store */ "./modules/App/Resources/Assets/js/app/store/index.js");
/* harmony import */ var _components_ListErrors__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/ListErrors */ "./modules/App/Resources/Assets/js/app/components/ListErrors.vue");
/* harmony import */ var _store_actions_type__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../store/actions.type */ "./modules/App/Resources/Assets/js/app/store/actions.type.js");


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  name: "RwvArticleEdit",
  components: {
    RwvListErrors: _components_ListErrors__WEBPACK_IMPORTED_MODULE_3__["default"]
  },
  props: {
    previousArticle: {
      type: Object,
      required: false
    }
  },
  beforeRouteUpdate: function beforeRouteUpdate(to, from, next) {
    return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return _store__WEBPACK_IMPORTED_MODULE_2__["default"].dispatch(_store_actions_type__WEBPACK_IMPORTED_MODULE_4__["ARTICLE_RESET_STATE"]);

            case 2:
              return _context.abrupt("return", next());

            case 3:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }))();
  },
  beforeRouteEnter: function beforeRouteEnter(to, from, next) {
    return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.next = 2;
              return _store__WEBPACK_IMPORTED_MODULE_2__["default"].dispatch(_store_actions_type__WEBPACK_IMPORTED_MODULE_4__["ARTICLE_RESET_STATE"]);

            case 2:
              if (!(to.params.slug !== undefined)) {
                _context2.next = 5;
                break;
              }

              _context2.next = 5;
              return _store__WEBPACK_IMPORTED_MODULE_2__["default"].dispatch(_store_actions_type__WEBPACK_IMPORTED_MODULE_4__["FETCH_ARTICLE"], to.params.slug, to.params.previousArticle);

            case 5:
              return _context2.abrupt("return", next());

            case 6:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }))();
  },
  beforeRouteLeave: function beforeRouteLeave(to, from, next) {
    return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              _context3.next = 2;
              return _store__WEBPACK_IMPORTED_MODULE_2__["default"].dispatch(_store_actions_type__WEBPACK_IMPORTED_MODULE_4__["ARTICLE_RESET_STATE"]);

            case 2:
              next();

            case 3:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3);
    }))();
  },
  data: function data() {
    return {
      tagInput: null,
      inProgress: false,
      errors: {}
    };
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapGetters"])(["article"])),
  methods: {
    onPublish: function onPublish(slug) {
      var _this = this;

      var action = slug ? _store_actions_type__WEBPACK_IMPORTED_MODULE_4__["ARTICLE_EDIT"] : _store_actions_type__WEBPACK_IMPORTED_MODULE_4__["ARTICLE_PUBLISH"];
      this.inProgress = true;
      this.$store.dispatch(action).then(function (_ref) {
        var data = _ref.data;
        _this.inProgress = false;

        _this.$router.push({
          name: "article",
          params: {
            slug: data.article.slug
          }
        });
      })["catch"](function (_ref2) {
        var response = _ref2.response;
        _this.inProgress = false;
        _this.errors = response.data.errors;
      });
    },
    removeTag: function removeTag(tag) {
      this.$store.dispatch(_store_actions_type__WEBPACK_IMPORTED_MODULE_4__["ARTICLE_EDIT_REMOVE_TAG"], tag);
    },
    addTag: function addTag(tag) {
      this.$store.dispatch(_store_actions_type__WEBPACK_IMPORTED_MODULE_4__["ARTICLE_EDIT_ADD_TAG"], tag);
      this.tagInput = null;
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/ListErrors.vue?vue&type=template&id=671e7648&v-show=errors&":
/*!**************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./modules/App/Resources/Assets/js/app/components/ListErrors.vue?vue&type=template&id=671e7648&v-show=errors& ***!
  \**************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "ul",
    { staticClass: "error-messages" },
    _vm._l(_vm.errors, function(value, key) {
      return _c(
        "li",
        { key: key },
        [
          _c("span", { domProps: { textContent: _vm._s(key) } }),
          _vm._v(" "),
          _vm._l(value, function(err) {
            return _c("span", {
              key: err,
              domProps: { textContent: _vm._s(err) }
            })
          })
        ],
        2
      )
    }),
    0
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/views/ArticleEdit.vue?vue&type=template&id=0320e24e&":
/*!********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./modules/App/Resources/Assets/js/app/views/ArticleEdit.vue?vue&type=template&id=0320e24e& ***!
  \********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "editor-page" }, [
    _c("div", { staticClass: "container page" }, [
      _c("div", { staticClass: "row" }, [
        _c(
          "div",
          { staticClass: "col-md-10 offset-md-1 col-xs-12" },
          [
            _c("RwvListErrors", { attrs: { errors: _vm.errors } }),
            _vm._v(" "),
            _c(
              "form",
              {
                on: {
                  submit: function($event) {
                    $event.preventDefault()
                    return _vm.onPublish(_vm.article.slug)
                  }
                }
              },
              [
                _c("fieldset", { attrs: { disabled: _vm.inProgress } }, [
                  _c("fieldset", { staticClass: "form-group" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.article.title,
                          expression: "article.title"
                        }
                      ],
                      staticClass: "form-control form-control-lg",
                      attrs: { type: "text", placeholder: "Article Title" },
                      domProps: { value: _vm.article.title },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.article, "title", $event.target.value)
                        }
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("fieldset", { staticClass: "form-group" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.article.description,
                          expression: "article.description"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: {
                        type: "text",
                        placeholder: "What's this article about?"
                      },
                      domProps: { value: _vm.article.description },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            _vm.article,
                            "description",
                            $event.target.value
                          )
                        }
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("fieldset", { staticClass: "form-group" }, [
                    _c("textarea", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.article.body,
                          expression: "article.body"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: {
                        rows: "8",
                        placeholder: "Write your article (in markdown)"
                      },
                      domProps: { value: _vm.article.body },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.article, "body", $event.target.value)
                        }
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("fieldset", { staticClass: "form-group" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.tagInput,
                          expression: "tagInput"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { type: "text", placeholder: "Enter tags" },
                      domProps: { value: _vm.tagInput },
                      on: {
                        keypress: function($event) {
                          if (
                            !$event.type.indexOf("key") &&
                            _vm._k(
                              $event.keyCode,
                              "enter",
                              13,
                              $event.key,
                              "Enter"
                            )
                          ) {
                            return null
                          }
                          $event.preventDefault()
                          return _vm.addTag(_vm.tagInput)
                        },
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.tagInput = $event.target.value
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "tag-list" },
                      _vm._l(_vm.article.tagList, function(tag, index) {
                        return _c(
                          "span",
                          {
                            key: tag + index,
                            staticClass: "tag-default tag-pill"
                          },
                          [
                            _c("i", {
                              staticClass: "ion-close-round",
                              on: {
                                click: function($event) {
                                  return _vm.removeTag(tag)
                                }
                              }
                            }),
                            _vm._v(
                              "\n                  " +
                                _vm._s(tag) +
                                "\n                "
                            )
                          ]
                        )
                      }),
                      0
                    )
                  ])
                ]),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    staticClass: "btn btn-lg pull-xs-right btn-primary",
                    attrs: { disabled: _vm.inProgress, type: "submit" }
                  },
                  [_vm._v("\n            Publish Article\n          ")]
                )
              ]
            )
          ],
          1
        )
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);