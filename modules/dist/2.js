(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[2],{

/***/ "./modules/App/Resources/Assets/js/app/components/Comment.vue":
/*!********************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/components/Comment.vue ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Comment_vue_vue_type_template_id_3ff2f63c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Comment.vue?vue&type=template&id=3ff2f63c& */ "./modules/App/Resources/Assets/js/app/components/Comment.vue?vue&type=template&id=3ff2f63c&");
/* harmony import */ var _Comment_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Comment.vue?vue&type=script&lang=js& */ "./modules/App/Resources/Assets/js/app/components/Comment.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Comment_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Comment_vue_vue_type_template_id_3ff2f63c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Comment_vue_vue_type_template_id_3ff2f63c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "modules/App/Resources/Assets/js/app/components/Comment.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/components/Comment.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/components/Comment.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Comment_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Comment.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/Comment.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Comment_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/components/Comment.vue?vue&type=template&id=3ff2f63c&":
/*!***************************************************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/components/Comment.vue?vue&type=template&id=3ff2f63c& ***!
  \***************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Comment_vue_vue_type_template_id_3ff2f63c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Comment.vue?vue&type=template&id=3ff2f63c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/Comment.vue?vue&type=template&id=3ff2f63c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Comment_vue_vue_type_template_id_3ff2f63c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Comment_vue_vue_type_template_id_3ff2f63c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/components/CommentEditor.vue":
/*!**************************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/components/CommentEditor.vue ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CommentEditor_vue_vue_type_template_id_29d82a49___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CommentEditor.vue?vue&type=template&id=29d82a49& */ "./modules/App/Resources/Assets/js/app/components/CommentEditor.vue?vue&type=template&id=29d82a49&");
/* harmony import */ var _CommentEditor_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CommentEditor.vue?vue&type=script&lang=js& */ "./modules/App/Resources/Assets/js/app/components/CommentEditor.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _CommentEditor_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CommentEditor_vue_vue_type_template_id_29d82a49___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CommentEditor_vue_vue_type_template_id_29d82a49___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "modules/App/Resources/Assets/js/app/components/CommentEditor.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/components/CommentEditor.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/components/CommentEditor.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CommentEditor_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./CommentEditor.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/CommentEditor.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CommentEditor_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/components/CommentEditor.vue?vue&type=template&id=29d82a49&":
/*!*********************************************************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/components/CommentEditor.vue?vue&type=template&id=29d82a49& ***!
  \*********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CommentEditor_vue_vue_type_template_id_29d82a49___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./CommentEditor.vue?vue&type=template&id=29d82a49& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/CommentEditor.vue?vue&type=template&id=29d82a49&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CommentEditor_vue_vue_type_template_id_29d82a49___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CommentEditor_vue_vue_type_template_id_29d82a49___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/components/ListErrors.vue":
/*!***********************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/components/ListErrors.vue ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ListErrors_vue_vue_type_template_id_671e7648_v_show_errors___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ListErrors.vue?vue&type=template&id=671e7648&v-show=errors& */ "./modules/App/Resources/Assets/js/app/components/ListErrors.vue?vue&type=template&id=671e7648&v-show=errors&");
/* harmony import */ var _ListErrors_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ListErrors.vue?vue&type=script&lang=js& */ "./modules/App/Resources/Assets/js/app/components/ListErrors.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ListErrors_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ListErrors_vue_vue_type_template_id_671e7648_v_show_errors___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ListErrors_vue_vue_type_template_id_671e7648_v_show_errors___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "modules/App/Resources/Assets/js/app/components/ListErrors.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/components/ListErrors.vue?vue&type=script&lang=js&":
/*!************************************************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/components/ListErrors.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ListErrors_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./ListErrors.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/ListErrors.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ListErrors_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/components/ListErrors.vue?vue&type=template&id=671e7648&v-show=errors&":
/*!********************************************************************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/components/ListErrors.vue?vue&type=template&id=671e7648&v-show=errors& ***!
  \********************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ListErrors_vue_vue_type_template_id_671e7648_v_show_errors___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./ListErrors.vue?vue&type=template&id=671e7648&v-show=errors& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/ListErrors.vue?vue&type=template&id=671e7648&v-show=errors&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ListErrors_vue_vue_type_template_id_671e7648_v_show_errors___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ListErrors_vue_vue_type_template_id_671e7648_v_show_errors___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/components/VTag.vue":
/*!*****************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/components/VTag.vue ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _VTag_vue_vue_type_template_id_e0750592___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./VTag.vue?vue&type=template&id=e0750592& */ "./modules/App/Resources/Assets/js/app/components/VTag.vue?vue&type=template&id=e0750592&");
/* harmony import */ var _VTag_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./VTag.vue?vue&type=script&lang=js& */ "./modules/App/Resources/Assets/js/app/components/VTag.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _VTag_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _VTag_vue_vue_type_template_id_e0750592___WEBPACK_IMPORTED_MODULE_0__["render"],
  _VTag_vue_vue_type_template_id_e0750592___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "modules/App/Resources/Assets/js/app/components/VTag.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/components/VTag.vue?vue&type=script&lang=js&":
/*!******************************************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/components/VTag.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_VTag_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./VTag.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/VTag.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_VTag_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/components/VTag.vue?vue&type=template&id=e0750592&":
/*!************************************************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/components/VTag.vue?vue&type=template&id=e0750592& ***!
  \************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_VTag_vue_vue_type_template_id_e0750592___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./VTag.vue?vue&type=template&id=e0750592& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/VTag.vue?vue&type=template&id=e0750592&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_VTag_vue_vue_type_template_id_e0750592___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_VTag_vue_vue_type_template_id_e0750592___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/views/Article.vue":
/*!***************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/views/Article.vue ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Article_vue_vue_type_template_id_343e15a2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Article.vue?vue&type=template&id=343e15a2& */ "./modules/App/Resources/Assets/js/app/views/Article.vue?vue&type=template&id=343e15a2&");
/* harmony import */ var _Article_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Article.vue?vue&type=script&lang=js& */ "./modules/App/Resources/Assets/js/app/views/Article.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Article_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Article_vue_vue_type_template_id_343e15a2___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Article_vue_vue_type_template_id_343e15a2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "modules/App/Resources/Assets/js/app/views/Article.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/views/Article.vue?vue&type=script&lang=js&":
/*!****************************************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/views/Article.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Article_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Article.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/views/Article.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Article_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./modules/App/Resources/Assets/js/app/views/Article.vue?vue&type=template&id=343e15a2&":
/*!**********************************************************************************************!*\
  !*** ./modules/App/Resources/Assets/js/app/views/Article.vue?vue&type=template&id=343e15a2& ***!
  \**********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Article_vue_vue_type_template_id_343e15a2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Article.vue?vue&type=template&id=343e15a2& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/views/Article.vue?vue&type=template&id=343e15a2&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Article_vue_vue_type_template_id_343e15a2___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Article_vue_vue_type_template_id_343e15a2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/Comment.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./modules/App/Resources/Assets/js/app/components/Comment.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var _store_actions_type__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../store/actions.type */ "./modules/App/Resources/Assets/js/app/store/actions.type.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "RwvComment",
  props: {
    slug: {
      type: String,
      required: true
    },
    comment: {
      type: Object,
      required: true
    }
  },
  computed: _objectSpread({
    isCurrentUser: function isCurrentUser() {
      if (this.currentUser.username && this.comment.author.username) {
        return this.comment.author.username === this.currentUser.username;
      }

      return false;
    }
  }, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])(["currentUser"])),
  methods: {
    destroy: function destroy(slug, commentId) {
      this.$store.dispatch(_store_actions_type__WEBPACK_IMPORTED_MODULE_1__["COMMENT_DESTROY"], {
        slug: slug,
        commentId: commentId
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/CommentEditor.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./modules/App/Resources/Assets/js/app/components/CommentEditor.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ListErrors_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ListErrors.vue */ "./modules/App/Resources/Assets/js/app/components/ListErrors.vue");
/* harmony import */ var _store_actions_type_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../store/actions.type.js */ "./modules/App/Resources/Assets/js/app/store/actions.type.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "RwvCommentEditor",
  components: {
    RwvListErrors: _ListErrors_vue__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  props: {
    slug: {
      type: String,
      required: true
    },
    content: {
      type: String,
      required: false
    },
    userImage: {
      type: String,
      required: false
    }
  },
  data: function data() {
    return {
      comment: this.content || null,
      errors: {}
    };
  },
  methods: {
    onSubmit: function onSubmit(slug, comment) {
      var _this = this;

      this.$store.dispatch(_store_actions_type_js__WEBPACK_IMPORTED_MODULE_1__["COMMENT_CREATE"], {
        slug: slug,
        comment: comment
      }).then(function () {
        _this.comment = null;
        _this.errors = {};
      })["catch"](function (_ref) {
        var response = _ref.response;
        _this.errors = response.data.errors;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/ListErrors.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./modules/App/Resources/Assets/js/app/components/ListErrors.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "RwvListErorrs",
  props: {
    errors: {
      type: Object,
      required: true
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/VTag.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./modules/App/Resources/Assets/js/app/components/VTag.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "RwvTag",
  props: {
    name: {
      type: String,
      required: true
    },
    className: {
      type: String,
      "default": "tag-pill tag-default"
    }
  },
  computed: {
    homeRoute: function homeRoute() {
      return {
        name: "home-tag",
        params: {
          tag: name
        }
      };
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/views/Article.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./modules/App/Resources/Assets/js/app/views/Article.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var marked__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! marked */ "./node_modules/marked/lib/marked.js");
/* harmony import */ var marked__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(marked__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../store */ "./modules/App/Resources/Assets/js/app/store/index.js");
/* harmony import */ var _components_ArticleMeta__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/ArticleMeta */ "./modules/App/Resources/Assets/js/app/components/ArticleMeta.vue");
/* harmony import */ var _components_Comment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/Comment */ "./modules/App/Resources/Assets/js/app/components/Comment.vue");
/* harmony import */ var _components_CommentEditor__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/CommentEditor */ "./modules/App/Resources/Assets/js/app/components/CommentEditor.vue");
/* harmony import */ var _components_VTag__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/VTag */ "./modules/App/Resources/Assets/js/app/components/VTag.vue");
/* harmony import */ var _store_actions_type__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../store/actions.type */ "./modules/App/Resources/Assets/js/app/store/actions.type.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//








/* harmony default export */ __webpack_exports__["default"] = ({
  name: "rwv-article",
  props: {
    slug: {
      type: String,
      required: true
    }
  },
  components: {
    RwvArticleMeta: _components_ArticleMeta__WEBPACK_IMPORTED_MODULE_3__["default"],
    RwvComment: _components_Comment__WEBPACK_IMPORTED_MODULE_4__["default"],
    RwvCommentEditor: _components_CommentEditor__WEBPACK_IMPORTED_MODULE_5__["default"],
    RwvTag: _components_VTag__WEBPACK_IMPORTED_MODULE_6__["default"]
  },
  beforeRouteEnter: function beforeRouteEnter(to, from, next) {
    Promise.all([_store__WEBPACK_IMPORTED_MODULE_2__["default"].dispatch(_store_actions_type__WEBPACK_IMPORTED_MODULE_7__["FETCH_ARTICLE"], to.params.slug), _store__WEBPACK_IMPORTED_MODULE_2__["default"].dispatch(_store_actions_type__WEBPACK_IMPORTED_MODULE_7__["FETCH_COMMENTS"], to.params.slug)]).then(function () {
      next();
    });
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])(["article", "currentUser", "comments", "isAuthenticated"])),
  methods: {
    parseMarkdown: function parseMarkdown(content) {
      return marked__WEBPACK_IMPORTED_MODULE_1___default()(content);
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/Comment.vue?vue&type=template&id=3ff2f63c&":
/*!*********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./modules/App/Resources/Assets/js/app/components/Comment.vue?vue&type=template&id=3ff2f63c& ***!
  \*********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "card" }, [
    _c("div", { staticClass: "card-block" }, [
      _c("p", { staticClass: "card-text" }, [_vm._v(_vm._s(_vm.comment.body))])
    ]),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "card-footer" },
      [
        _c("a", { staticClass: "comment-author", attrs: { href: "" } }, [
          _c("img", {
            staticClass: "comment-author-img",
            attrs: { src: _vm.comment.author.image }
          })
        ]),
        _vm._v(" "),
        _c(
          "router-link",
          {
            staticClass: "comment-author",
            attrs: {
              to: {
                name: "profile",
                params: { username: _vm.comment.author.username }
              }
            }
          },
          [_vm._v("\n      " + _vm._s(_vm.comment.author.username) + "\n    ")]
        ),
        _vm._v(" "),
        _c("span", { staticClass: "date-posted" }, [
          _vm._v(_vm._s(_vm._f("date")(_vm.comment.createdAt)))
        ]),
        _vm._v(" "),
        _vm.isCurrentUser
          ? _c("span", { staticClass: "mod-options" }, [
              _c("i", {
                staticClass: "ion-trash-a",
                on: {
                  click: function($event) {
                    return _vm.destroy(_vm.slug, _vm.comment.id)
                  }
                }
              })
            ])
          : _vm._e()
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/CommentEditor.vue?vue&type=template&id=29d82a49&":
/*!***************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./modules/App/Resources/Assets/js/app/components/CommentEditor.vue?vue&type=template&id=29d82a49& ***!
  \***************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("RwvListErrors", { attrs: { errors: _vm.errors } }),
      _vm._v(" "),
      _c(
        "form",
        {
          staticClass: "card comment-form",
          on: {
            submit: function($event) {
              $event.preventDefault()
              return _vm.onSubmit(_vm.slug, _vm.comment)
            }
          }
        },
        [
          _c("div", { staticClass: "card-block" }, [
            _c("textarea", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.comment,
                  expression: "comment"
                }
              ],
              staticClass: "form-control",
              attrs: { placeholder: "Write a comment...", rows: "3" },
              domProps: { value: _vm.comment },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.comment = $event.target.value
                }
              }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "card-footer" }, [
            _c("img", {
              staticClass: "comment-author-img",
              attrs: { src: _vm.userImage }
            }),
            _vm._v(" "),
            _c("button", { staticClass: "btn btn-sm btn-primary" }, [
              _vm._v("Post Comment")
            ])
          ])
        ]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/ListErrors.vue?vue&type=template&id=671e7648&v-show=errors&":
/*!**************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./modules/App/Resources/Assets/js/app/components/ListErrors.vue?vue&type=template&id=671e7648&v-show=errors& ***!
  \**************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "ul",
    { staticClass: "error-messages" },
    _vm._l(_vm.errors, function(value, key) {
      return _c(
        "li",
        { key: key },
        [
          _c("span", { domProps: { textContent: _vm._s(key) } }),
          _vm._v(" "),
          _vm._l(value, function(err) {
            return _c("span", {
              key: err,
              domProps: { textContent: _vm._s(err) }
            })
          })
        ],
        2
      )
    }),
    0
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/components/VTag.vue?vue&type=template&id=e0750592&":
/*!******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./modules/App/Resources/Assets/js/app/components/VTag.vue?vue&type=template&id=e0750592& ***!
  \******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("router-link", {
    class: _vm.className,
    attrs: { to: _vm.homeRoute },
    domProps: { textContent: _vm._s(_vm.name) }
  })
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./modules/App/Resources/Assets/js/app/views/Article.vue?vue&type=template&id=343e15a2&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./modules/App/Resources/Assets/js/app/views/Article.vue?vue&type=template&id=343e15a2& ***!
  \****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "article-page" }, [
    _c("div", { staticClass: "banner" }, [
      _c(
        "div",
        { staticClass: "container" },
        [
          _c("h1", [_vm._v(_vm._s(_vm.article.title))]),
          _vm._v(" "),
          _c("RwvArticleMeta", {
            attrs: { article: _vm.article, actions: true }
          })
        ],
        1
      )
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "container page" }, [
      _c("div", { staticClass: "row article-content" }, [
        _c("div", { staticClass: "col-xs-12" }, [
          _c("div", {
            domProps: { innerHTML: _vm._s(_vm.parseMarkdown(_vm.article.body)) }
          }),
          _vm._v(" "),
          _c(
            "ul",
            { staticClass: "tag-list" },
            _vm._l(_vm.article.tagList, function(tag, index) {
              return _c(
                "li",
                { key: tag + index },
                [
                  _c("RwvTag", {
                    attrs: {
                      name: tag,
                      className: "tag-default tag-pill tag-outline"
                    }
                  })
                ],
                1
              )
            }),
            0
          )
        ])
      ]),
      _vm._v(" "),
      _c("hr"),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "article-actions" },
        [
          _c("RwvArticleMeta", {
            attrs: { article: _vm.article, actions: true }
          })
        ],
        1
      ),
      _vm._v(" "),
      _c("div", { staticClass: "row" }, [
        _c(
          "div",
          { staticClass: "col-xs-12 col-md-8 offset-md-2" },
          [
            _vm.isAuthenticated
              ? _c("RwvCommentEditor", {
                  attrs: { slug: _vm.slug, userImage: _vm.currentUser.image }
                })
              : _c(
                  "p",
                  [
                    _c("router-link", { attrs: { to: { name: "login" } } }, [
                      _vm._v("Sign in")
                    ]),
                    _vm._v("\n          or\n          "),
                    _c("router-link", { attrs: { to: { name: "register" } } }, [
                      _vm._v("sign up")
                    ]),
                    _vm._v(
                      "\n          to add comments on this article.\n        "
                    )
                  ],
                  1
                ),
            _vm._v(" "),
            _vm._l(_vm.comments, function(comment, index) {
              return _c("RwvComment", {
                key: index,
                attrs: { slug: _vm.slug, comment: comment }
              })
            })
          ],
          2
        )
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);