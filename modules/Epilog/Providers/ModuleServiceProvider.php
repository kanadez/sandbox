<?php

namespace Modules\Epilog\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'epilog');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'epilog');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'epilog');
        $this->loadConfigsFrom(__DIR__.'/../config');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
