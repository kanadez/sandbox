<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Database\Migrations\Migration;
use Modules\Epilog\Database\Seeds\AdminsSeeder;
use Modules\Epilog\Database\Seeds\ClientsSeeder;

class AttachPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Artisan::call('db:seed', [
            '--class' => AdminsSeeder::class,
            '--force' => "yes",
        ]);

        Artisan::call('db:seed', [
            '--class' => ClientsSeeder::class,
            '--force' => "yes",
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
