<?php

namespace Modules\Epilog\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Core\Model\Role;
use Modules\Core\Model\User;
use Modules\Core\Repository\PermissionsRepository;
use Modules\Core\Repository\RolesRepository;
use Modules\Core\Repository\UsersRepository;

class AdminsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param PermissionsRepository $permissionsRepository
     * @param RolesRepository $rolesRepository
     * @param UsersRepository $usersRepository
     * @return void
     */
    public function run(PermissionsRepository $permissionsRepository,
                        RolesRepository $rolesRepository,
                        UsersRepository $usersRepository)
    {
        $permissions = $permissionsRepository->all(["id"]);

        /** @var Role $role */
        $role = $rolesRepository->findByField("name", "admin")->first();
        $role->permissions()->sync($permissions);

        /** @var User $admin */
        $admin = $usersRepository->findByField("name", "admin")->first();
        $admin->attachRole($role);
    }
}
