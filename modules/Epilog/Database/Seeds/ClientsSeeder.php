<?php

namespace Modules\Epilog\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Core\Model\Role;
use Modules\Core\Model\User;
use Modules\Core\Repository\PermissionsRepository;
use Modules\Core\Repository\RolesRepository;
use Modules\Core\Repository\UsersRepository;
use Modules\Lk\Criteria\LkPermissionsCriteria;

class ClientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param PermissionsRepository $permissionsRepository
     * @param RolesRepository $rolesRepository
     * @param UsersRepository $usersRepository
     * @return void
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function run(PermissionsRepository $permissionsRepository,
                        RolesRepository $rolesRepository,
                        UsersRepository $usersRepository)
    {
        /** @var Role $role */
        $role = $rolesRepository->findWhere(["name" => "client"])->first();
        $permissions = $permissionsRepository->resetCriteria()->pushCriteria(LkPermissionsCriteria::class)->all();
        $role->permissions()->sync($permissions->pluck("id")->toArray());

        /** @var User $client */
        $client = $usersRepository->findByField("name", "client")->first();
        $client->attachRole($role);
    }
}
